﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="Shift_IncMaster.aspx.cs" Inherits="Shift_IncMaster" Title="Shift Incentive Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>



<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel23" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">

                <h1 class="page-header">Shift Incentive Master</h1>
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Shift Incentive Master</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">
                                    <h5></h5>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                                    <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                                </asp:DropDownList>


                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                                    class="form-control select2" Style="width: 100%;"
                                                    OnSelectedIndexChanged="ddlEmployeeType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Shift</label>
                                                <asp:DropDownList runat="server" ID="ddlshift" AutoPostBack="true"
                                                    class="form-control select2" Style="width: 100%;" OnSelectedIndexChanged="ddlshift_SelectedIndexChanged">
                                                </asp:DropDownList>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Amount</label>
                                                <asp:TextBox runat="server" ID="txtAmount" class="form-control" Text="0"></asp:TextBox>

                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Alloted Date</label>
                                                <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success"
                                                    OnClick="btnSave_Click" />
                                                <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger"
                                                    OnClick="btnClear_Click" />
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                    <!-- table start -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                <HeaderTemplate>
                                                    <table id="example" class="display table table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th>Category</th>
                                                                <th>Employee Type</th>
                                                                <th>Shift</th>
                                                                <th>Amount</th>
                                                                <th>Alloted Date</th>
                                                                <th>Mode</th>
                                                            </tr>
                                                        </thead>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td><%# Eval("Category")%></td>
                                                        <td><%# Eval("EmployeeType")%></td>
                                                        <td><%# Eval("Shift")%></td>
                                                        <td><%# Eval("Amount")%></td>
                                                        <td><%# Eval("Alloted_Date_Str")%></td>
                                                        <td>
                                                            <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument='<%# Eval("Category")%>' CommandName='<%# Eval("EmployeeType")%>'
                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Incentive details?');">
                                                            </asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate></table></FooterTemplate>
                                            </asp:Repeater>
                                        </div>
                                    </div>
                                    <!-- table End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#example').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>
    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
</asp:Content>
