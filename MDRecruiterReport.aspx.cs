﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;

public partial class MDRecruiterReport : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL;

    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    String CurrentYear1;
    static int CurrentYear;

    string Division;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Salary Cover Summary Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

                Load_Location();
                Load_Recruitment();
                Load_AgentName();
                Load_ParentsName();
                //ddlunit.SelectedValue = SessionLcode;
            }
        }
    }

    public void Load_Location()
    {
        string SSQL = "";
        DataTable dtempty = new DataTable();
        ddlunit.DataSource = dtempty;
        ddlunit.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode from Location_Mst where CompCode='" + SessionCcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlunit.DataSource = dt;
        DataRow dr = dt.NewRow();
        dr["LCode"] = "-Select-";
        dr["LCode"] = "-Select-";
        dt.Rows.InsertAt(dr, 0);
        ddlunit.DataTextField = "LCode";
        ddlunit.DataValueField = "LCode";
        ddlunit.DataBind();
    }

    private void Load_Recruitment()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtRecruitmentName.Items.Clear();
        query = "Select ROName from MstRecruitOfficer";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtRecruitmentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ROName"] = "-Select-";
        dr["ROName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtRecruitmentName.DataTextField = "ROName";
        txtRecruitmentName.DataValueField = "ROName";
        txtRecruitmentName.DataBind();

    }

    private void Load_AgentName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtAgentName.Items.Clear();
        query = "Select AgentName from MstAgent";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["AgentName"] = "-Select-";
        dr["AgentName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtAgentName.DataTextField = "AgentName";
        txtAgentName.DataValueField = "AgentName";
        txtAgentName.DataBind();
    }

    private void Load_ParentsName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlParentsName.Items.Clear();
        query = "Select Distinct RefParentsName from Employee_Mst where RefParentsName!=''";
        if (ddlunit.SelectedItem.Text != "-Select-")
        {
            query = query + " And  LocCode='" + ddlunit.SelectedItem.Text + "'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlParentsName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["RefParentsName"] = "-Select-";
        dr["RefParentsName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlParentsName.DataTextField = "RefParentsName";
        ddlParentsName.DataValueField = "RefParentsName";
        ddlParentsName.DataBind();
    }

    protected void ddlunit_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_ParentsName();
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        string Unit = "";
        string ROName = "";
        string AgentName = "";
        string ParentsName = "";


        if (ddlunit.SelectedItem.Text != "-Select-")
        {
            Unit = ddlunit.SelectedItem.Text;
        }


        if (txtRecruitmentName.SelectedItem.Text != "-Select-")
        {
            ROName = txtRecruitmentName.SelectedItem.Text;
        }

        if (txtAgentName.SelectedItem.Text != "-Select-")
        {
            AgentName = txtAgentName.SelectedItem.Text;
        }


        if (ddlParentsName.SelectedItem.Text != "-Select-")
        {
            ParentsName = ddlParentsName.SelectedItem.Text;
        }


        ResponseHelper.Redirect("MDRecruitmentReportDisplay.aspx?Unit=" + Unit + "&ROName=" + ROName + "&AgentName=" + AgentName + "&ParentsName=" + ParentsName, "_blank", "");
    }
}
