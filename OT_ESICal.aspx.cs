﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

public partial class OT_ESICal : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDTable = new DataTable();
    string SessionUserType;
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionEpay;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    string basesalary = "0";
    string PerHr = "0";
    string OTWages = "0";
    string OneDay = "0";
    string Tot_OT_Hrs = "0";
    string OT_Amt = "0";
    string Wages = "";
    string MachineID = "";
    string Emp_WH_Day = "";
    string WH = "0";
    string WH_Amt = "0";
    string Name = "";
    Boolean ErrFlag = false;
    DataTable dtAmt = new DataTable();
    DataTable dt = new DataTable();
    string OTAmount = "0";
    string TotAmount = "0";
    string NetPay = "0";
    string TotDed = "0";
    // string oneday = "0";
    string WH_Amount = "0";

    string OTAdd = "0";
    string Round = "0";
    string BasicDA = "0";
    string OTDay = "0";
    string fourHrs = "0";
    string EightsHrs = "0";
    string TotalInc = "0";
    string FourAmt = "0";
    string EightAmt = "0";
    string ESI = "0";
    string ESI_Wages = "0";
    string Total = "0";
    string NetAmt = "0";
    string WHAmt = "0";
    string whDays = "";
    string WagesType;
    string EmpType = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Weekly OT Salary";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
           
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }


    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        SSQL = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
       // ddlEmployeeType_SelectedIndexChanged(sender, e);
        // Load_Data();
    }
    protected void ESI_Cal_Click(object sender, EventArgs e)
    {

        WagesType = ddlEmployeeType.SelectedItem.Text;
        string TableName = "";
        TableName = "Employee_Mst";
        Wages = ddlEmployeeType.SelectedItem.Text;
        if (ddlCategory.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Select Employee Category');", true);
            ErrFlag = true;
        }
        if (ddlEmployeeType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Select Employee Type');", true);
            ErrFlag = true;
        }
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
            ErrFlag = true;
        }

        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
            ErrFlag = true;
        }
        if (ErrFlag != true)
        {


            string query = "";
            query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff from ( ";
            query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,DATENAME(dd, Attn_Date) as Day_V, ";
            query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 THEN 'WH/X' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
            query = query + " else (CASE WHEN LD.Present = 1.0 THEN 'X' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
            query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
            query = query + " where LD.LocCode='" + SessionLcode + "' ";
            query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + txtFromDate.Text + "',103) ";
            query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + txtToDate.Text + "',103) ";

            if (WagesType != "-Select-")
            {
                query = query + " And EM.Wages='" + WagesType + "'";
                query = query + " And LD.Wages='" + WagesType + "'";
            }
            if (txtTokenNo.Text != "")
            {
                SSQL = SSQL + " and LD.ExistingCode='" + txtTokenNo.Text + "'";
            }

            query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";

            query = query + " order by ExistingCode Asc";
            dt = objdata.RptEmployeeMultipleDetails(query);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                MachineID = dt.Rows[i]["MachineID"].ToString();




                //SSQL = "";
                //SSQL = "   select A.MachineID,A.EmpName,sum(Tot_OTHrs)Tot_OTHrs,Sum(Amount)Amount,sum(Four_Hrs_Inc)Four_Hrs,sum(Four_Hrs_inc_Amount)Four_Hrs_inc_Amount,Sum(Eight_Hrs_Inc)Eight_Hrs," +
                //     "sum(Eight_Hrs_inc_Amount)Eight_Hrs_inc_Amount,Sum(WH)WH,Sum(WH_Amt)WHAmt from (" +
                //    "select MachineID, EmpName, OT_AttenDate, cast(Tot_OTHrs as int)Tot_OTHrs, cast(Amount as decimal(18, 2)) Amount ,Oneday_Wages,OT_Wages,0 Four_Hrs_Inc, 0 Eight_Hrs_Inc,0 Four_Hrs_inc_Amount, " +
                //     "0 Eight_Hrs_inc_Amount,0 WH,0 WH_Amt " +
                //     " from  [Raajco_Epay]..OT_Amt where CCode = 'Raajco' and LCode = 'UNIT I' and  convert(DateTime,OT_AttenDate,103) >= Convert(Datetime,'" + txtFromDate.Text + "',103) and " +
                //     "convert(DateTime, OT_AttenDate, 103) <= Convert(Datetime, '" + txtToDate.Text + "', 103) and Wages='" + ddlEmployeeType.SelectedItem.Text
                //     + "' and MachineID='" + MachineID + "'" +
                //     "Union All " +
                //    "select MachineID,EmpName,OT_AttenDate,0 Tot_OTHrs,  0.0 Amount,0 Oneday_Wages,0 OT_Wages,Four_Hrs," +
                //    "Eight_Hrs,Four_Hrs_inc_Amount,Eight_Hrs_inc_Amount,WH,WH_Amt " +
                //     " from  [Raajco_Epay]..OT_Inc_Amt where  CCode = 'Raajco' and LCode = 'UNIT I' and MachineID='" + MachineID + "'  and  convert(DateTime,OT_AttenDate,103) >= Convert(Datetime,'" + txtFromDate.Text + "',103) and convert(DateTime,OT_AttenDate, 103) <= Convert(Datetime,'" + txtToDate.Text + "' , 103)  )A Group by A.MachineID,A.EmpName";

                SSQL = "Select MachineID,EmpName,sum(CAST(isnull(OT_Hrs,'0') as decimal(18,2))) as Tot_OTHrs,Sum(CAST(isnull(Amount,'0') as decimal(18,2))) as Amount,Sum(CAST(isnull(Four_Hrs_Inc,'0') as decimal(18,2))) as Four_Hrs,";
                SSQL = SSQL + "Sum(CAST(isnull(Eight_Hrs_Inc,'0') as decimal(18,2))) as Eight_Hrs,Sum(CAST(isnull(Four_Hrs_Inc_Amount,'0') as decimal(18,2))) as Four_Hrs_inc_Amount,Sum(CAST(isnull(Eight_Hrs_Inc_Amount,'0') as decimal(18,2))) as Eight_Hrs_inc_Amount,";
                SSQL = SSQL + "Sum(CAST(isnull(Weekoff,'0') as decimal(18,2))) as WH,Sum(CAST(Isnull(WH_Amt,'0') as decimal(18,2))) as WHAmt from [" + SessionEpay + "]..OT_Hrs_Mst";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MachineID='" + MachineID + "' and ";
                SSQL = SSQL + " Convert(date,Attn_Date_Str,103)>=Convert(date,'" + txtFromDate.Text + "',103) and Convert(date,Attn_Date_Str,103)<=Convert(date,'" + txtToDate.Text + "',103)";
                SSQL = SSQL + " and Wages='" + WagesType + "'";
                SSQL = SSQL + " group by MachineID,EmpName order by MachineID asc";
                dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);



                if (dtAmt.Rows.Count != 0)
                {
                    for (int j = 0; j < dtAmt.Rows.Count; j++)    

                    {
                        if (dtAmt.Rows[0]["Tot_OTHrs"].ToString() != "")

                            OTAdd = dtAmt.Rows[j]["Amount"].ToString();
                        fourHrs = dtAmt.Rows[j]["Four_Hrs"].ToString();
                        EightsHrs = dtAmt.Rows[j]["Eight_Hrs"].ToString();
                        FourAmt = dtAmt.Rows[j]["Four_Hrs_inc_Amount"].ToString();
                        EightAmt = dtAmt.Rows[j]["Eight_Hrs_inc_Amount"].ToString();
                        WHAmt = dtAmt.Rows[j]["WHAmt"].ToString();

                        TotalInc = (Convert.ToDecimal(FourAmt) + Convert.ToDecimal(EightAmt) + Convert.ToDecimal(WHAmt)).ToString();



                        //OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();
                        whDays = dtAmt.Rows[j]["WH"].ToString();


                        if (whDays != "0")
                        {
                            WH_Amount = (Convert.ToDecimal(OneDay) * Convert.ToDecimal(whDays)).ToString();

                        }
                        else
                        {
                            WH_Amount = "0";
                        }

                        OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();


                        if (OTAmount != "0")
                        {
                            ESI_Wages = OTAmount;
                            ESI = (Convert.ToDecimal(OTAmount) * Convert.ToDecimal(0.75)).ToString();
                            ESI = (Convert.ToDecimal(ESI) / Convert.ToDecimal(100)).ToString();
                            ESI = (Math.Round(Convert.ToDecimal(ESI), 0, MidpointRounding.AwayFromZero)).ToString();
                            DataTable Dt_Chk = new DataTable();
                            SSQL = "";
                            SSQL = "select * from ["+SessionEpay+"]..OT_ESI_Amt where Months='" + ddlMonths.SelectedItem.Text + "' and MachineID='" + MachineID + "' and FromDate='" + txtFromDate.Text + "' and ToDate='" + txtToDate.Text + "'";
                            Dt_Chk = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (Dt_Chk.Rows.Count != 0)
                            {
                                SSQL = "";
                                SSQL = "Delete from ["+ SessionEpay + "]..OT_ESI_Amt where Months='" + ddlMonths.SelectedItem.Text + "' and MachineID='" + MachineID + "' and FromDate='" + txtFromDate.Text + "' and ToDate='" + txtToDate.Text + "'";
                                objdata.RptEmployeeMultipleDetails(SSQL);
                            }
                            SSQL = "";
                            SSQL = "insert into ["+ SessionEpay + "]..OT_ESI_Amt(Compcode,LocCode,Category,EmployeeType,MachineID,Months,FromDate,Todate,FinYear,ESI_Amount,ESI,CreatedDate) values( ";
                            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + ddlCategory.SelectedItem.Text + "','" + ddlEmployeeType.SelectedItem.Text + "','" + MachineID + "','" + ddlMonths.SelectedItem.Text + "','" + txtFromDate.Text + "',";
                            SSQL = SSQL + "'" + txtToDate.Text + "','" + ddlFinance.SelectedValue + "','" + OTAmount + "','" + ESI + "','" + DateTime.Now.ToString("dd/MM/yyyy") + "')";
                            objdata.RptEmployeeMultipleDetails(SSQL);


                        }
                    }

                }

            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('OT ESI Amount Calculated Succesfully...');", true);
            clear();
        }
    }
    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }


    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }

    private void clear()
    {
        ddlCategory.SelectedValue = "-Select-";
        ddlEmployeeType.SelectedItem.Text = "";
        ddlMonths.SelectedItem.Text = "-Select-";
        txtFromDate.Text = "";
        txtToDate.Text = "";
        ddlMachineID.ClearSelection();
        txtTokenNo.Text = "";
        txtName.Text = "";  
    }


    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMachineID.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select * from Employee_Mst where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and (isActive='Yes') and machineID='" + ddlMachineID.SelectedItem.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtName.Text = dt.Rows[0]["FirstName"].ToString();
                txtTokenNo.Text = dt.Rows[0]["ExistingCode"].ToString();
            }
            else
            {
                txtTokenNo.Text = "";
                txtName.Text = "";
            }
        }else
        {
            txtTokenNo.Text = "";
            txtName.Text = "";
        }
    }

    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpNo();
    }
    private void Load_EmpNo()
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "'";
        SSQL = SSQL + " and isActive='Yes' and Wages='" + ddlEmployeeType.SelectedItem.Text + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID.DataSource = dt;
        ddlMachineID.DataTextField = "MachineID";
        ddlMachineID.DataValueField = "MachineID";
        ddlMachineID.DataBind();
        ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }
}