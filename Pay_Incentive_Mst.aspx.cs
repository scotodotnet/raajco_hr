﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class Pay_Incentive_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SessionEpay = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Load_MonthIncentive();
            Load_CivilIncentive();
        }
        Load_Data_Incentive();
        Load_Attenbonus();
    }

    private void Load_Data_Incentive()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [Raajco_Epay]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
       
    }
    
   
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {

        string query;
        DataTable DT = new DataTable();


        query = "select * from [Raajco_Epay]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'  And MonthDays='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from [Raajco_Epay]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And MonthDays='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Incentive Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Incentive();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtDaysOfMonth.Text.Trim() == "") || (txtDaysOfMonth.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Day of Month');", true);
                ErrFlag = true;
            }
            else if ((txtMinDaysWorked.Text.Trim() == "") || (txtMinDaysWorked.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Minimum Days Worked');", true);
                ErrFlag = true;
            }
            else if ((txtIncent_Amount.Text.Trim() == "") || (txtIncent_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Incentive Amount');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select * from [Raajco_Epay]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MonthDays='" + txtDaysOfMonth.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [Raajco_Epay]..WorkerIncentive_mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  and MonthDays='" + txtDaysOfMonth.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }
                Query = "Insert Into [Raajco_Epay]..WorkerIncentive_mst (Ccode,Lcode,WorkerDays,WorkerAmt,MonthDays)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtMinDaysWorked.Text + "','" + txtIncent_Amount.Text + "','" + txtDaysOfMonth.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

              
                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Data_Incentive();
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
            }
        }
        catch (Exception Ex)
        { 
        
        }
    }
    protected void btnIncen_Full_Amt_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string MessFlag = "Insert";

            if ((txtIncent_Month_Full_Amount.Text.Trim() == "") || (txtIncent_Month_Full_Amount.Text.Trim() == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Hostel Amount....!');", true);
                ErrFlag = true;
            }
         
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                Query = "Select Amt from [Raajco_Epay]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [Raajco_Epay]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Amt='" + txtIncent_Month_Full_Amount.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MessFlag = "Update";
                }

                Query = "Insert Into [Raajco_Epay]..HostelIncentive (Ccode,Lcode,Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays)";
                Query = Query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + txtIncent_Month_Full_Amount.Text + "','" + WorkDays.Checked.ToString() + "','" + Hallowed.Checked.ToString() + "',";
                Query = Query + "'" + OTdays.Checked.ToString() + "','" + NFHdays.Checked.ToString() + "','" + NFHwork.Checked.ToString() + "','" + CalWorkdays.Checked.ToString() + "',";
                Query = Query + "'" + CalHallowed.Checked.ToString() + "','" + CalOTdays.Checked.ToString() + "','" + CalNFHdays.Checked.ToString() + "','" + CalNFHwork.Checked.ToString() + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (MessFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                txtMinDaysWorked.Text = "0";
                txtDaysOfMonth.Text = "0";
                txtIncent_Amount.Text = "0";
            }
        }
        catch (Exception Ex)
        {

        }
    }
    public void Load_MonthIncentive()
    {
        DataTable dt = new DataTable();
        Query = "Select Amt,WDays,HAllowed,OTDays,NFHDays,NFHWorkDays,CalWDays,CalHAllowed,CalOTDays,CalNFHDays,CalNFHWorkDays from [Raajco_Epay]..HostelIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtIncent_Month_Full_Amount.Text = dt.Rows[0]["Amt"].ToString();
            if (dt.Rows[0]["WDays"].ToString().ToUpper() == "true".ToUpper()) { WorkDays.Checked = true; } else { WorkDays.Checked = false; }
            if (dt.Rows[0]["HAllowed"].ToString().ToUpper() == "true".ToUpper()) { Hallowed.Checked = true; } else { Hallowed.Checked = false; }
            if (dt.Rows[0]["OTDays"].ToString().ToUpper() == "true".ToUpper()) { OTdays.Checked = true; } else { OTdays.Checked = false; }
            if (dt.Rows[0]["NFHDays"].ToString().ToUpper() == "true".ToUpper()) { NFHdays.Checked = true; } else { NFHdays.Checked = false; }
            if (dt.Rows[0]["NFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { NFHwork.Checked = true; } else { NFHwork.Checked = false; }

            if (dt.Rows[0]["CalWDays"].ToString().ToUpper() == "true".ToUpper()) { CalWorkdays.Checked = true; } else { CalWorkdays.Checked = false; }
            if (dt.Rows[0]["CalHAllowed"].ToString().ToUpper() == "true".ToUpper()) { CalHallowed.Checked = true; } else { CalHallowed.Checked = false; }
            if (dt.Rows[0]["CalOTDays"].ToString().ToUpper() == "true".ToUpper()) { CalOTdays.Checked = true; } else { CalOTdays.Checked = false; }
            if (dt.Rows[0]["CalNFHDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHdays.Checked = true; } else { CalNFHdays.Checked = false; }
            if (dt.Rows[0]["CalNFHWorkDays"].ToString().ToUpper() == "true".ToUpper()) { CalNFHwork.Checked = true; } else { CalNFHwork.Checked = false; }
        }
        else
        {
            txtIncent_Month_Full_Amount.Text = "0.00";
            WorkDays.Checked = false; Hallowed.Checked = false; OTdays.Checked = false; NFHdays.Checked = false;
            NFHwork.Checked = false; CalWorkdays.Checked = false; CalHallowed.Checked = false; CalOTdays.Checked = false;
            CalNFHdays.Checked = false; CalNFHwork.Checked = false;
        }

    }
    protected void btnCivil_Incent_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = true;
            DataTable dt = new DataTable();
            if (txtEligible_Days.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Eligible Days....! ');", true);
                ErrFlag = true;
            }
            if (txtCivil_Incen_Amt.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Incentive Amount....! ');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Query = "";
                Query = Query + "select * from [Raajco_Epay]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = Query + "delete from [Raajco_Epay]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    SaveFlag = false;
                }
                Query = "Insert into [Raajco_Epay]..CivilIncentive(Ccode,Lcode,Amt,ElbDays) values(";
                Query = Query + "'" + SessionCcode + "','" + SessionLcode + "','" + txtEligible_Days.Text + "','" + txtCivil_Incen_Amt.Text + "')";
                objdata.RptEmployeeMultipleDetails(Query);

                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....! ');", true);

                }
                else 
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully....! ');", true);
                }
                txtEligible_Days.Text = "0";
                txtCivil_Incen_Amt.Text = "0";
                Load_CivilIncentive();
            }
        }
        catch (Exception ex)
        {
        }
    }
    public void Load_CivilIncentive()
    {
        DataTable dt = new DataTable();
        Query = "";
        Query = Query + "select * from [Raajco_Epay]..CivilIncentive where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if (dt.Rows.Count > 0)
        {
            txtEligible_Days.Text = dt.Rows[0]["ElbDays"].ToString();
            txtCivil_Incen_Amt.Text = dt.Rows[0]["Amt"].ToString();
        }
    }

    protected void btnAttendanceBonus_Click(object sender, EventArgs e)
    {
        string SSQL = "";
        bool ErrFlag = false;
        if (ddlSkill.SelectedValue == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select the Skill Level');", true);
            return;
        }
        if (txtSkillRate.Text == "0" || txtSkillRate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Skill Rate');", true);
            return;
        }
        if (!ErrFlag)
        {
            if (btnAttendanceBonus.Text == "Update")
            {
                SSQL = "";
                SSQL = "Select * from [" + SessionEpay + "]..MstAtten_Bonus where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and SkillLevel='" + ddlSkill.SelectedValue + "' and SkillRate='" + txtSkillRate.Text + "'";
                DataTable dt = new DataTable();
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt.Rows.Count > 0)
                {
                    SSQL = "";
                    SSQL = "Delete from [" + SessionEpay + "]..MstAtten_Bonus where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    SSQL = SSQL + " and SkillLevel='" + ddlSkill.SelectedValue + "' and SkillRate='" + txtSkillRate.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            else if (btnAttendanceBonus.Text == "Save")
            {
                SSQL = "";
                SSQL = "Delete from [" + SessionEpay + "]..MstAtten_Bonus where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and SkillLevel='" + ddlSkill.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            SSQL = "";
            SSQL = "Insert into [" + SessionEpay + "]..MstAtten_Bonus (Ccode,Lcode,SkillLevel,SkillRate)";
            SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + ddlSkill.SelectedItem.Text + "','" + txtSkillRate.Text + "')";
            objdata.RptEmployeeMultipleDetails(SSQL);

            if (btnAttendanceBonus.Text == "Update")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Details Updated Successfully');", true);
            }
            if (btnAttendanceBonus.Text == "Save")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Details Saved Successfully');", true);
            }
            btnAttendanceBonusClr_Click(sender,e);
        }
    }

    protected void btnAttendanceBonusClr_Click(object sender, EventArgs e)
    {
        txtSkillRate.Text = "0";
        ddlSkill.ClearSelection();
        btnAttendanceBonus.Text = "Save";
        Load_Attenbonus();
    }

    private void Load_Attenbonus()
    {
        string SSQL = "";
        SSQL = "Select * from [" + SessionEpay + "]..MstAtten_Bonus where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
       
            Repeater2.DataSource = dt;
            Repeater2.DataBind();
       
    }

    protected void btnDeleteEnquiryAttenbonus_Grid_Command(object sender, CommandEventArgs e)
    {
        string SSQL = "";
        SSQL = "Delete from [" + SessionEpay + "]..MstAtten_Bonus where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and SkillLevel='" + e.CommandArgument + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
    }
}
