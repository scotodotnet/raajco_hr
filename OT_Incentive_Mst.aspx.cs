﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class OT_Incentive_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string basic = "0";
    string HRA = "0";
    string Conv = "0";
    string Spl = "0";
    string WH = "0";
    string Medical = "0";
    string Edu = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            
            LoadShift();
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadShift();
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
        


    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
      // LoadShift();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Errflag = "False";
       
        if (ddlCategory.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Category...');", true);
        }
        if (ddlshift.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Shift...');", true);
        }

        if ((txtOTHrs_Frm.Text == "0")|| (txtOTHrs_Frm.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter OT Hrs...');", true);
        }
        if ((txtOTHrs_To.Text == "0") || (txtOTHrs_To.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter OT Hrs...');", true);
        }
        if ((txtAmount.Text == "0") || (txtOTHrs_Frm.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter Amount...');", true);
        }

        //if ((ddlEmployeeType.SelectedItem.Text == "-Select-") || (ddlEmployeeType.SelectedItem.Text == ""))
        //{
        //    Errflag = "true";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
        //}

        if (Errflag != "true")
        {
            try
            {
                String MsgFlag = "Insert";
                DataTable dt = new DataTable();
                Query = "select * from [Raajco_Epay]..OT_Inc_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Shift='" + ddlshift.SelectedItem.Text + "' and OT_Hrs_From='" +txtOTHrs_Frm.Text+"' and OT_Hrs_To ='" + txtOTHrs_To.Text+"' ";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [Raajco_Epay]..OT_Inc_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Shift = '" + ddlshift.SelectedItem.Text + "'and OT_Hrs_From='" + txtOTHrs_Frm.Text + "' and OT_Hrs_To ='" + txtOTHrs_To.Text + "' ";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MsgFlag = "Update";
                }
               
                Query = "";
                Query = Query + "Insert into [Raajco_Epay]..OT_Inc_Mst(Ccode,Lcode,Category,EmployeeType,Shift,OT_Hrs_From,OT_Hrs_To,Amount";
                Query = Query + ")Values('" + SessionCcode + "','" + SessionLcode + "',";
                Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + ddlshift.SelectedItem.Text + "','" + txtOTHrs_Frm.Text + "','" + txtOTHrs_To.Text + "','" + txtAmount.Text + "')";
                
                objdata.RptEmployeeMultipleDetails(Query);
                Clear();
                if (MsgFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
            }
            catch (Exception Ex)
            {

            }
        }
    }
  
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {

        ddlCategory.SelectedValue = "0";
        LoadShift();
        txtAmount.Text = "0";
        txtOTHrs_Frm.Text = "0";
        loadEmp();
        
    }
    private void loadEmp()
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        //ddlEmployeeType_SelectedIndexChanged(sender, e);

    }
  
   


  private void LoadShift()
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select ShiftDesc from Shift_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlshift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftDesc"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlshift.DataTextField = "ShiftDesc";
        //  ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlshift.DataBind();
    }

    private void LoadData()
    {
        DataTable dt = new DataTable();

        Query = "";
        Query = "select * from [Raajco_Epay]..OT_Inc_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Shift='" + ddlshift.SelectedItem.Text + "' ";
        dt = objdata.RptEmployeeMultipleDetails(Query);
        if(dt.Rows.Count !=0)
        {
            txtOTHrs_Frm.Text = dt.Rows[0]["OT_Hrs"].ToString();
            txtAmount.Text = dt.Rows[0]["Amount"].ToString();
        }
    }

    protected void ddlshift_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData();
    }
}


