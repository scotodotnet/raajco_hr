﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class ManualAttendanceDayWise : System.Web.UI.Page
{


    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();


    DateTime date1 = new DateTime();
        DataTable mLocalDS_INTAB = new DataTable();
         DataTable mLocalDS_OUTTAB = new DataTable();
         string Date_Value_Str ;
         string Time_IN_Str = "";
         string Time_Out_Str = "";
         int time_Check_dbl=0;
         string Total_Time_get = "";
         string Status;
    string SSQL = "";

    DataSet ds = new DataSet();

    DataTable mEmployeeDS = new DataTable();
    string Division = "";
    DataTable AutoDataTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    TimeSpan ts4;
      public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }

   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Manual Attendance Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division = Request.QueryString["Division"].ToString();
            Status = Request.QueryString["Status"].ToString();
            DataCell.Columns.Add("SNo");
            DataCell.Columns.Add("Dept");
            DataCell.Columns.Add("Type");
            DataCell.Columns.Add("Shift");


            DataCell.Columns.Add("EmpCode");
            DataCell.Columns.Add("ExCode");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("TimeIN");
            DataCell.Columns.Add("TimeOUT");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Category");
            DataCell.Columns.Add("SubCategory");
            DataCell.Columns.Add("TotalMIN");
            DataCell.Columns.Add("GrandTOT");
            DataCell.Columns.Add("ShiftDate");
            DataCell.Columns.Add("CompanyName");
            DataCell.Columns.Add("LocationName");
            //if (SessionUserType == "1")
            //{
            //    GetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                NonAdminGetManualAttdDayWise_Change();
            }
            else
            {
                GetManualAttdDayWise_Change();
            }

           
        }
    }


    public void NonAdminGetManualAttdDayWise_Change()
    {

        DataTable mLocalDS = new DataTable();


        date1 = Convert.ToDateTime(Date);


        SSQL = "";

        SSQL = "Select Distinct MD.Machine_No from ManAttn_Details MD inner join ";
        SSQL = SSQL + "Employee_Mst EM on MD.Machine_No=EM.MachineID where EM.IsActive='Yes' And EM.Compcode='" + SessionCcode + "' ";
        SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "'  And MD.CompCode='" + SessionCcode + "' And ";
        SSQL = SSQL + " MD.LocCode='" + SessionLcode + "' and MD.AttnDateStr='" + Date + "' and EM.Eligible_PF ='1' ";
        if (Division != "-Select-")
        {
            SSQL = SSQL + " and EM.Division='" + Division + "' ";
        }


        mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mLocalDS.Rows.Count <= 0)
        {
        }
        int mStartINRow = 1;
        int mStartOUTRow = 1;

        string Time_IN_Check_Shift = "";
        string Time_Out_Check_Shift = "";
        string Machine_ID_Check = "";
        string Machine_ID_Encrypt = "";
        string Emp_ShiftType = "";
        DataTable Shift_DataSet = new DataTable();
        DataTable mLocalDS_out = new DataTable();
        DataTable Man_Attn_DS = new DataTable();


        for (int iTabRow = 0; iTabRow < mLocalDS.Rows.Count; iTabRow++)
        {
            Machine_ID_Check = mLocalDS.Rows[iTabRow]["Machine_No"].ToString();
            Machine_ID_Encrypt = Encryption(Machine_ID_Check);


            SSQL = "Select * from ManAttn_Details where Machine_No='" + Machine_ID_Check + "' And Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' and AttnDateStr='" + Date + "'";
            Man_Attn_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if (Man_Attn_DS.Rows.Count <= 0)
            {
            }
            else
            {
                Time_IN_Check_Shift = Man_Attn_DS.Rows[0]["LogTimeIn"].ToString();
                Time_Out_Check_Shift = Man_Attn_DS.Rows[0]["LogTimeOut"].ToString();

                if (Man_Attn_DS.Rows.Count != 0)
                {
                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    if (Machine_ID_Check == "787")
                    {
                        Machine_ID_Check = "787";
                    }
                    SSQL = "Select * from Company_Mst ";
                    dt = objdata.RptEmployeeMultipleDetails(SSQL);
                    string name = dt.Rows[0]["CompName"].ToString();

                    DataCell.Rows[iTabRow]["Shift"] = "Manual Attendance";
                    //string a = String.Format("{0:hh:mm tt}", Convert.ToDateTime(Man_Attn_DS.Rows[0]["LogTimeIn"]));
                    DataCell.Rows[iTabRow]["TimeIN"] = String.Format("{0:hh:mm tt}", Convert.ToDateTime(Man_Attn_DS.Rows[0]["LogTimeIn"]));
                    DataCell.Rows[iTabRow]["TimeOUT"] = String.Format("{0:hh:mm tt}", Convert.ToDateTime(Man_Attn_DS.Rows[0]["LogTimeOut"]));
                    DataCell.Rows[iTabRow]["MachineID"] = Machine_ID_Check;
                    DataCell.Rows[iTabRow][14] = date1.ToString("dd/MM/yyyy");
                    DataCell.Rows[iTabRow][15] = name.ToString();
                    DataCell.Rows[iTabRow][16] = SessionLcode.ToString();


                    Time_IN_Str = "";
                    Time_Out_Str = "";

                    DateTime Fromdayy = new DateTime();

                    Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
                    Date_Value_Str = Fromdayy.ToString("yyyy/MM/dd");


                    SSQL = "Select LogTimeIn from ManAttn_Details where Machine_No='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And AttnDateStr ='" + Date + "' ";
                    mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select LogTimeOut from ManAttn_Details where Machine_No='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And AttnDateStr ='" + Date + "' ";
                    mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);
                    String Emp_Total_Work_Time_1 = "00:00";

                    if (mLocalDS_INTAB.Rows.Count > 10)
                    {
                    }
                    else
                    {

                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }


                        for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_Out_Str = "";

                            }
                            else
                            {
                                Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }

                        //  'Emp_Total_Work_Time
                        if (Time_IN_Str == "" || Time_Out_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {

                            string[] Time_Minus_Value_Check;
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                            TimeSpan ts1;

                            ts1 = date4.Subtract(date3);
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {

                                date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                //ts4 = ts4.Add(ts1);
                                ts4 = ts1;
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt32(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }

                            else
                            {

                                //ts4 = ts4.Add(ts1);
                                ts4 = ts1;
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                }
                            }
                            DataCell.Rows[iTabRow]["TotalMIN"] = Emp_Total_Work_Time_1;
                            DataCell.Rows[iTabRow]["GrandTOT"] = Emp_Total_Work_Time_1;

                        }






                    }
                }
                else
                {
                }






            }


        }

        for (int iRow2 = 0; iRow2 < DataCell.Rows.Count; iRow2++)
        {
            string MID = DataCell.Rows[iRow2]["MachineID"].ToString();

            SSQL = "";
            SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
            SSQL = SSQL + " from Employee_Mst  Where MachineID='" + MID + "' And Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

            if (Division != "-Select-")
            {
                SSQL = SSQL + " and Division='" + Division + "' ";
            }

            mEmployeeDS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (mEmployeeDS.Rows.Count > 0)
            {
                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {

                    //ss = Decryption(MID.ToString());
                    //DataCell.Rows[iRow2][19] = ss.ToString();
                    //string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();


                    DataCell.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                    DataCell.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                    DataCell.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                    DataCell.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                    DataCell.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                    DataCell.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                    DataCell.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];






                }
            }
        }

        ds.Tables.Add(DataCell);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/Attendance.rpt"));
        
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;
    }


    public void GetManualAttdDayWise_Change()
    {
        string TableName = "";

        if (Status == "Pending")
        {
            TableName = "Employee_Mst_New_Emp";
        }

        else
        {
            TableName = "Employee_Mst";
        }

        DataTable mLocalDS=new DataTable();
       

        date1 = Convert.ToDateTime(Date);
      

        SSQL = "";

       SSQL="Select Distinct MD.Machine_No from ManAttn_Details MD inner join ";
       SSQL = SSQL + "" + TableName + " EM on MD.Machine_No=EM.MachineID where EM.IsActive='Yes' And EM.Compcode='" + SessionCcode + "' ";
       SSQL = SSQL + " And EM.LocCode='" + SessionLcode + "'  And MD.CompCode='" + SessionCcode + "' And ";
       SSQL = SSQL + " MD.LocCode='" + SessionLcode + "' and MD.AttnDateStr='" + Date + "' "; 
        if (Division != "-Select-")
        {
            SSQL = SSQL + " and EM.Division='" + Division + "' ";
        }


        mLocalDS =  objdata.RptEmployeeMultipleDetails(SSQL);

        if(mLocalDS .Rows.Count <=0)
        {
        }
       int mStartINRow=1;
        int mStartOUTRow=1;

       string Time_IN_Check_Shift="";
        string Time_Out_Check_Shift="";
       string Machine_ID_Check="";
        string Machine_ID_Encrypt="";
        string Emp_ShiftType="";
        DataTable Shift_DataSet=new DataTable();
        DataTable mLocalDS_out=new DataTable();
        DataTable Man_Attn_DS=new DataTable();

       
        for (int iTabRow=0;iTabRow < mLocalDS.Rows.Count ;iTabRow++)
        {
             Machine_ID_Check = mLocalDS.Rows[iTabRow]["Machine_No"].ToString();
             Machine_ID_Encrypt = Encryption(Machine_ID_Check);


              SSQL = "Select * from ManAttn_Details where Machine_No='" + Machine_ID_Check + "' And Compcode='" + SessionCcode  + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode  + "' and AttnDateStr='" + Date + "'";
            Man_Attn_DS = objdata.RptEmployeeMultipleDetails(SSQL);
            if(Man_Attn_DS .Rows.Count <= 0 )
            {
            }
            else
            {
                Time_IN_Check_Shift = Man_Attn_DS.Rows[0]["LogTimeIn"].ToString();
                Time_Out_Check_Shift = Man_Attn_DS.Rows[0]["LogTimeOut"].ToString();
            
          if(Man_Attn_DS .Rows.Count != 0)
          {
              DataCell.NewRow();
                DataCell.Rows.Add();
                if (Machine_ID_Check == "787")
                  {
                     Machine_ID_Check = "787";
                   }
                SSQL = "Select * from Company_Mst ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt.Rows[0]["CompName"].ToString();
              
              DataCell.Rows [iTabRow]["Shift"]= "Manual Attendance";
              //string a = String.Format("{0:hh:mm tt}", Convert.ToDateTime(Man_Attn_DS.Rows[0]["LogTimeIn"]));
                DataCell.Rows [iTabRow]["TimeIN"]=   String.Format("{0:hh:mm tt}", Convert.ToDateTime(Man_Attn_DS.Rows[0]["LogTimeIn"]));
                DataCell.Rows [iTabRow]["TimeOUT"]=String.Format("{0:hh:mm tt}", Convert.ToDateTime(Man_Attn_DS.Rows[0]["LogTimeOut"]));
                DataCell.Rows [iTabRow]["MachineID"]= Machine_ID_Check;
                DataCell.Rows[iTabRow][14] = date1.ToString("dd/MM/yyyy");
                DataCell.Rows[iTabRow][15] = name.ToString();
                DataCell.Rows[iTabRow][16] = SessionLcode.ToString();
                   

                      Time_IN_Str = "";
                      Time_Out_Str = "";    
         
               DateTime Fromdayy=new DateTime();
            
                 Fromdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
                    Date_Value_Str=Fromdayy.ToString("yyyy/MM/dd");

                 
                    SSQL = "Select LogTimeIn from ManAttn_Details where Machine_No='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode  + "' And LocCode='" + SessionLcode  + "'";
                    SSQL = SSQL + " And AttnDateStr ='" + Date  + "' ";
                    mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                    SSQL = "Select LogTimeOut from ManAttn_Details where Machine_No='" + Machine_ID_Check + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And AttnDateStr ='" + Date + "' ";
                    mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);
                    String Emp_Total_Work_Time_1 = "00:00";
                  
                  if(mLocalDS_INTAB.Rows.Count >10)
                  {
                  }
                  else
                  {
                      
                      if(mLocalDS_INTAB.Rows.Count <= 0)
                      {
                            Time_IN_Str = "";
                      }
                      else
                      {
                          Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                      }


                      for (int tout = 0; tout < mLocalDS_OUTTAB.Rows.Count; tout++)
                      {
                          if (mLocalDS_OUTTAB.Rows.Count <= 0)
                          {
                              Time_Out_Str = "";

                          }
                          else
                          {
                              Time_Out_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                          }

                      }
                     
                      //  'Emp_Total_Work_Time
                      if (Time_IN_Str == "" || Time_Out_Str == "")
                      {
                          time_Check_dbl = 0;
                      }
                      else
                      {
                          
                          string[] Time_Minus_Value_Check;
                          DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                          DateTime date4 = System.Convert.ToDateTime(Time_Out_Str);
                          TimeSpan ts1;
                          
                          ts1 = date4.Subtract(date3);
                          ts1 = date4.Subtract(date3);
                          Total_Time_get = ts1.Hours.ToString();

                          if (Left_Val(Total_Time_get, 1) == "-")
                          {
                             
                              date4 = System.Convert.ToDateTime(Time_Out_Str).AddDays(1);
                              ts1 = date4.Subtract(date3);
                              ts1 = date4.Subtract(date3);
                              //ts4 = ts4.Add(ts1);
                              ts4 = ts1;
                              Total_Time_get = ts1.Hours.ToString();
                              time_Check_dbl = Convert.ToInt32(Total_Time_get);
                              Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                              Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                              if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                              {
                                  Emp_Total_Work_Time_1 = "00:00";
                              }
                              if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                              {
                                  Emp_Total_Work_Time_1 = "00:00";
                              }
                          }

                          else
                          {
                             
                              //ts4 = ts4.Add(ts1);
                              ts4 = ts1;
                              Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                              Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                              if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                              {
                                  Emp_Total_Work_Time_1 = "00:00";
                              }
                              if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                              {
                                  Emp_Total_Work_Time_1 = "00:00";
                                  time_Check_dbl = Convert.ToInt16(Total_Time_get);
                              }
                          }
                          DataCell.Rows[iTabRow]["TotalMIN"] = Emp_Total_Work_Time_1;
                          DataCell.Rows[iTabRow]["GrandTOT"] = Emp_Total_Work_Time_1;

                      }
                           

                           
             
                           
                               
                        }
          }
              else
              {
              }
                          
                         
                          
                  

                      
          }

                    
        }
     
        for (int iRow2 = 0; iRow2 < DataCell.Rows.Count; iRow2++)
        {
            string MID = DataCell.Rows[iRow2]["MachineID"].ToString();

            SSQL = "";
            SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
            SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
            SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
            SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
            SSQL = SSQL + " from " + TableName + "  Where MachineID='" + MID + "' And Compcode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

            //if (Division != "-Select-")
            //{
            //    SSQL = SSQL + " and Division='" + Division + "' ";
            //}

            mEmployeeDS = objdata.RptEmployeeMultipleDetails(SSQL);

            if (mEmployeeDS.Rows.Count > 0)
            {
                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                {

                    //ss = Decryption(MID.ToString());
                    //DataCell.Rows[iRow2][19] = ss.ToString();
                    //string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();


                    DataCell.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                    DataCell.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];
                    DataCell.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                    DataCell.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                    DataCell.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                    DataCell.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                    DataCell.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                 




                   
                }
            }
        }

        ds.Tables.Add(DataCell);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/Attendance.rpt"));
        
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
        CrystalReportViewer1.ReportSource = report;
    }
    public static string Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public static string Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }

}
