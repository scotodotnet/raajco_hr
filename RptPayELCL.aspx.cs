﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.IO;

public partial class RptPayELCL : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SessionPayroll;
    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_WagesType();
            Load_Division();
            Load_Department();
            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from ["+ SessionRights + "]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        query = "Select *from MstEmployeeType";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            query = query + " where EmpCategory='1'";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            query = query + " where EmpCategory='2'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    private void Load_Division()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtDivision.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtDivision.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "-Select-";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtDivision.DataTextField = "Division";
        txtDivision.DataValueField = "Division";
        txtDivision.DataBind();
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtDivision.Items.Clear();
        query = "Select *from Department_Mst";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDept.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptCode"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDept.DataTextField = "DeptName";
        ddlDept.DataValueField = "DeptCode";
        ddlDept.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }

    protected void btnELView_Click(object sender, EventArgs e)
    {
        int YR = 0;


        try
        {
            bool ErrFlag = false;

            if (ddlcategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }
                    string EligibleWorkDays = txtEligbleDays.Text.ToString();
                    string Str_ChkBelow = "";
                    string Str_ChkAbove = "";
                    string bonus_Period_From = "";
                    string bonus_Period_To = "";
                    
                    if (ChkBelow.Checked == true)
                    {
                        Str_ChkBelow = "1";
                    }
                    else { Str_ChkBelow = "0"; }
                    if (ChkAbove.Checked == true)
                    {
                        Str_ChkAbove = "1";
                    }
                    else { Str_ChkAbove = "0"; }

                    string Str_ChkLeft = "";
                    if (ChkLeft.Checked == true)
                    {
                        Str_ChkLeft = "1";
                    }
                    else { Str_ChkLeft = "0"; }
                    //&CashBank=" + RdbCashBank.SelectedValue + "
                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    bonus_Period_From = "01-01-" + (Convert.ToDecimal(txtFinancial_Year.SelectedValue.ToString())).ToString();
                    bonus_Period_To = "31-12-" + txtFinancial_Year.SelectedValue.ToString();

                    if (rdbPayslipFormat.SelectedItem.Text == "BankList")
                    {
                        string query = "";
                        query = "SELECT	ED.EmpNo,ED.FirstName as EmpName,('&nbsp;'+ED.ExistingCode) as ExisistingCode,MD.DeptName as DepartmentNm,(ED.AccountNo) as AccountNo,convert(varchar,ED.DOJ,103) as DOJ,ED.BaseSalary as Base,";
                        query = query + "(AD.Worked_Days) as Total_Days,AD.ELDays,AD.CLDays,AD.ELRate,AD.Bonus_Amt as Round_Bonus,AD.Allowance1,AD.Allowance2,AD.Allowance3,";
                        query = query + "AD.Deduction1,AD.Deduction2,AD.Deduction3,AD.Deduction4,AD.Final_Bonus_Amt";
                        query = query + " FROM	Employee_Mst ED";
                        query = query + " inner join Department_Mst as MD on MD.DeptCode = ED.DeptCode";
                        query = query + " inner join [" + SessionPayroll + "]..ELCL_Details as AD on AD.EmpNo=ED.EmpNo";
                        query = query + " WHERE ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And (AD.Bonus_Year)='" + txtFinancial_Year.SelectedItem.Text + "' and ED.IsActive='Yes'";
                        query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                        query = query + " And ED.Wages='" + txtEmployeeType.SelectedItem.Text + "'";

                        if (RdbPFNonPF.SelectedItem.Value.ToString() != "0")
                        {
                            if (RdbPFNonPF.SelectedItem.Value.ToString() == "1")
                            {
                                //PF Employee
                                query = query + " and (ED.Eligible_PF='1')";
                            }
                            else
                            {
                                //Non PF Employee
                                query = query + " and (ED.Eligible_PF='2')";
                            }
                        }
                        
                        //Add DeptName Condition
                        if (ddlDept.SelectedItem.Text != "" && ddlDept.SelectedItem.Text != "-Select-")
                        {
                            query = query + " and ED.DeptName='" + ddlDept.SelectedItem.Text + "'";
                        }


                        if (RdbCashBank.SelectedValue.ToString() != "0")
                        {
                            if (RdbCashBank.SelectedValue.ToString() == "1")
                            {
                                //Cash Employee
                                query = query + " and (ED.Salary_Through='1')";
                            }
                            else
                            {
                                //Bank Employee
                                query = query + " and (ED.Salary_Through='2')";
                            }

                        }
                        query = query + " and AD.ELDays>'0'";



                        //query = query + " Group by ED.EmpNo,ED.FirstName,ED.ExistingCode,MD.DeptName,ED.Designation,ED.DOJ,ED.BaseSalary";
                        query = query + " Order by ED.ExistingCode Asc";

                        DataTable dt = new DataTable();
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count > 0)
                        {
                            DataTable dt_Cat = new DataTable();
                            string SSQL = "", CmpName = "", Cmpaddress = "";
                            SSQL = "";
                            SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                            dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt.Rows.Count > 0)
                            {
                                CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                                Cmpaddress = (dt_Cat.Rows[0]["Location"].ToString());
                            }

                            GvBank.DataSource = dt;
                            GvBank.DataBind();
                            foreach (GridViewRow row in GvBank.Rows)
                            {
                                for (int i = 0; i < row.Cells.Count; i++)
                                {
                                    if (row.RowType == DataControlRowType.DataRow)
                                    {
                                        row.Cells[i].Attributes.Add("style", "textmode");
                                    }
                                }
                            }

                            string attachment = "attachment;filename=ELCL_Bank.xls";
                            Response.ClearContent();
                            Response.AddHeader("content-disposition", attachment);
                            Response.ContentType = "application/ms-excel";
                            StringWriter stw = new StringWriter();
                            HtmlTextWriter htextw = new HtmlTextWriter(stw);
                            GvBank.RenderControl(htextw);
                            Response.Write("<table>");
                            Response.Write("<tr align='center'>");
                            Response.Write("<td colspan='5' style='font-weight:bold;'>");
                            Response.Write(CmpName);
                            Response.Write("</td>");
                            Response.Write("</tr>");
                            Response.Write("<tr align='center'>");
                            Response.Write("<td colspan='5' style='font-weight:bold;'>");
                            Response.Write(Cmpaddress);
                            Response.Write("</td>");
                            Response.Write("</tr>");
                            Response.Write("<tr>");
                            Response.Write("<td colspan='5' style='font-weight:bold;'>");
                            Response.Write(txtReportHead.Text + " - " + Convert.ToDateTime(DateTime.Now.ToString()).ToString("yyyy"));
                            Response.Write("</td>");
                            Response.Write("</tr></table>");
                            Response.Write(stw.ToString());
                            
                            int row_= dt.Rows.Count + 4;

                            Response.Write("<table border=1><tr style='font-weight:bold;'>");
                            Response.Write("<td align='center' colspan='4'>Total</td>");
                            Response.Write("<td style='width:50px;'>=sum(E5:E" + row_ + ")</td>");
                            Response.Write("</tr></table>");
                            Response.Flush();
                            Response.End();
                            Response.Clear();

                        }
                    }
                    if ((ChkBelow.Checked == true || ChkAbove.Checked == true) && rdbPayslipFormat.SelectedValue == "5")
                    {
                        if (txtEligbleDays.Text == "")
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Please Insert the Days of Work');", true);
                            txtEligbleDays.Focus();
                            ErrFlag = true;
                            return;
                        }
                    }
                    if(!ErrFlag)
                    {
                        //ResponseHelper.Redirect("ViewReport.aspx?ReportHead=" + txtReportHead.Text + "&Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + Convert.ToDateTime(DateTime.Now.ToString()).ToString("yyyy") + "&fromdate=" + bonus_Period_From + "&ToDate=" + bonus_Period_To + "&Salary=" + "" + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + "" + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type + "&PFTypePost=" + Str_PFType + "&Left_Emp=" + Str_ChkLeft + "&EligbleWDays=" + EligibleWorkDays + "&Leftdate=" + txtLeftDate.Text + "&Division=" + ddlDept.SelectedItem.Text.ToString() + "&Report_Type=ELCL_REPORT" + "&BelowCheck=" + Str_ChkBelow + "&AboveCheck=" + Str_ChkAbove, "_blank", "");
                        ResponseHelper.Redirect("ViewReport.aspx?ReportHead=" + txtReportHead.Text + "&Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + txtFinancial_Year.SelectedValue.ToString() + "&fromdate=" + bonus_Period_From + "&ToDate=" + bonus_Period_To + "&Salary=" + "" + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + "" + "&EmpTypeCd=" + txtEmployeeType.SelectedValue.ToString() + "&EmpType=" + txtEmployeeType.SelectedItem.Text.ToString() + "&PayslipType=" + Payslip_Format_Type + "&PFTypePost=" + Str_PFType + "&Left_Emp=" + Str_ChkLeft + "&EligbleWDays=" + EligibleWorkDays + "&Leftdate=" + txtLeftDate.Text + "&Division=" + ddlDept.SelectedItem.Text.ToString() + "&Report_Type=ELCL_REPORT" + "&BelowCheck=" + Str_ChkBelow + "&AboveCheck=" + Str_ChkAbove, "_blank", "");
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
}
