﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class VisitorCanteenToken : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    string MonthID;
    string Month;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Visitor Canteen";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
           // Fin_Year_Add();
        }

        Load_Data_NFH();
    }

   
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        bool ErrFlag = false;
      

        DataTable DT_NFH = new DataTable();

        if (txtCompanyName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Company Name...');", true);
        }
        if (txtName.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Visitor Name...');", true);
        }
        if (txtToMeet.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Contact Person Name...');", true);
        }
        if (txtTokenDate.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Date...');", true);
        }
        if (ddlVisitorType.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Visitor Type...');", true);
        }
        if (ddlFoodType.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Food Type...');", true);
        }
        //if (ServerYr > yr)
        //{
        //    ErrFlag = true;
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Please Select Year Properly.');", true);
        //}

        if (!ErrFlag)
        {
            if (btnSave.Text == "Save")
            {
                query = "Select * from [Raajco_Epay]..Mst_VisitorToken where Name='" + txtName.Text +"'and CompanyName='" +txtCompanyName.Text +"'and FoodType='" +ddlFoodType.SelectedItem.Text + "' and VisitorType='" + ddlVisitorType.SelectedItem.Text +"' and  TokenDate=convert(datetime,'" + txtTokenDate.Text + "',105)";
                DT_NFH = objdata.RptEmployeeMultipleDetails(query);
                if (DT_NFH.Rows.Count != 0)
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Date was Already Registered');", true);
                }
                else
                {
                    query = "Insert into [Raajco_Epay]..Mst_VisitorToken (Compcode,LocCode,VisitorType,Name,TokenDate,CompanyName,ToMeet,FoodType,CreatedDate) ";
                    query = query + "values ('" + SessionCcode + "','" + SessionLcode + "','" + ddlVisitorType.SelectedItem.Text + "','" + txtName.Text + "',convert(varchar(20),'" + txtTokenDate.Text + "'),";
                    query = query + "'" + txtCompanyName.Text + "','" + txtToMeet.Text + "','" + ddlFoodType.SelectedItem.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy")  + "')";
                    objdata.RptEmployeeMultipleDetails(query);

                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
                }
            }
            else if (btnSave.Text == "Update")
            {
                //query = "Update NFH_Mst set NFHDate=convert(datetime,'" + txtLeaveDate.Text + "',103), Months='" + Month + "',Year='" + yr + "',";
              //  query = query + "Description='" + txtDesc.Text + "',DateStr=convert(varchar(20),'" + txtLeaveDate.Text + "'),NFH_Type='" + ddlNFHType.SelectedItem.Text + "',";
              //  query = query + "Dbl_Wages_Statutory='" + Double_Wages_Statutory + "',Form25_NFH_Present='" + Form25_NFH_Present + "',Form25DisplayText='" + txtForm25Text.Text + "' where ID='" + txtID.Value + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }

        }
        Load_Data_NFH();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }

   
    private void Clear_All_Field()
    {
        ddlVisitorType.SelectedValue = "-Select-";
        txtCompanyName.Text = "";
        txtName.Text = "";
        txtToMeet.Text ="";
        txtTokenDate.Text ="";
        ddlFoodType.SelectedValue="-Select-";
        btnSave.Text = "Save";
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select * from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count != 0)
        //{
        //    query = "Delete from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        //    objdata.RptEmployeeMultipleDetails(query);

        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Holiday Details Deleted Successfully');", true);

        //}
        //else
        //{
        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Holiday Details Not Found');", true);
        //}
        //Load_Data_NFH();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        //string query = "";
        //DataTable DT = new DataTable();
        //query = "Select * from NFH_Mst where ID='" + e.CommandName.ToString() + "'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count != 0)
        //{
        //    txtID.Value = DT.Rows[0]["ID"].ToString();
        //    ddlYear.SelectedValue = DT.Rows[0]["Year"].ToString();
        //    txtLeaveDate.Text = DT.Rows[0]["DateStr"].ToString();
        //    txtDesc.Text = DT.Rows[0]["Description"].ToString();
        //    ddlNFHType.SelectedValue = DT.Rows[0]["NFH_Type"].ToString();

        //    if (DT.Rows[0]["Dbl_Wages_Statutory"].ToString() == "Yes")
        //    {
        //        chkDoubleWages.Checked = true;
        //    }
        //    else
        //    {
        //        chkDoubleWages.Checked = false;
        //    }
        //    if (DT.Rows[0]["Form25_NFH_Present"].ToString() == "Yes")
        //    {
        //        chkForm25.Checked = true;
        //    }
        //    else
        //    {
        //        chkForm25.Checked = false;
        //    }
        //    txtForm25Text.Text = DT.Rows[0]["Form25DisplayText"].ToString();

        //    if (ddlNFHType.SelectedItem.Text == "Double Wages Checklist")
        //    {
        //        chkDoubleWages.Enabled = true;
        //    }
        //    else
        //    {
        //        chkDoubleWages.Enabled = false;

        //    }

        //    btnSave.Text = "Update";
        //}
        //else
        //{
        //    ddlYear.SelectedValue = "-Select-";
        //    txtDesc.Text = "";
        //    ddlNFHType.SelectedValue = "-Select-";
        //    chkDoubleWages.Checked = false;
        //    chkDoubleWages.Enabled = false;
        //    chkForm25.Checked = false;
        //    txtForm25Text.Text = "";
        //    txtID.Value = "";
        //    btnSave.Text = "Save";
        //}
    }

    private void Load_Data_NFH()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from [Raajco_Epay]..Mst_VisitorToken";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
}
