﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
public partial class PayrollReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string SessionPayroll;
    string SessionUser;
    string EmpType;
    string Month;
    string FinYear;
    string ReportName;
    int YR;
    string SSQL = "";
    string[] Years;
    string Category;
    string YearCheck;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        SessionUser = Session["Usernmdisplay"].ToString();
        Load_DB();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            DropDown_Department();
            Load_MachineID();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }

    private void Load_MachineID()
    {
        SSQL = "";
        SSQL = "Select MachineID from Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlMachineID.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID.DataTextField = "MachineID";
        ddlMachineID.DataValueField = "MachineID";
        ddlMachineID.DataBind();
        ddlMachineID.Items.Insert(0, new ListItem("-Select-", "0"));
    }

    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Raajco_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    protected void ddlMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlFinYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    public void Load_TwoDates()
    {
        if (ddlMonth.SelectedValue != "-Select-" && ddlFinYear.SelectedValue != "-Select-" && ddlEmployeeType.SelectedValue != "")
        {
            if (ddlEmployeeType.SelectedValue != "2" && ddlEmployeeType.SelectedValue != "5" && ddlEmployeeType.SelectedValue != "10")
            {
                decimal Month_Total_days = 0;
                string Month_Last = "";
                string Year_Last = "0";
                string Temp_Years = "";
                string[] Years;
                string FromDate = "";
                string ToDate = "";

                Temp_Years = ddlFinYear.SelectedItem.Text;
                Years = Temp_Years.Split('-');
                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "March") || (ddlMonth.SelectedValue == "May") || (ddlMonth.SelectedValue == "July") || (ddlMonth.SelectedValue == "August") || (ddlMonth.SelectedValue == "October") || (ddlMonth.SelectedValue == "December"))
                {
                    Month_Total_days = 31;
                }
                else if ((ddlMonth.SelectedValue == "April") || (ddlMonth.SelectedValue == "June") || (ddlMonth.SelectedValue == "September") || (ddlMonth.SelectedValue == "November"))
                {
                    Month_Total_days = 30;
                }

                else if (ddlMonth.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(Years[0]) + 1);
                    if ((yrs % 4) == 0)
                    {
                        Month_Total_days = 29;
                    }
                    else
                    {
                        Month_Total_days = 28;
                    }
                }
                switch (ddlMonth.SelectedItem.Text)
                {
                    case "January": Month_Last = "01";
                        break;
                    case "February": Month_Last = "02";
                        break;
                    case "March": Month_Last = "03";
                        break;
                    case "April": Month_Last = "04";
                        break;
                    case "May": Month_Last = "05";
                        break;
                    case "June": Month_Last = "06";
                        break;
                    case "July": Month_Last = "07";
                        break;
                    case "August": Month_Last = "08";
                        break;
                    case "September": Month_Last = "09";
                        break;
                    case "October": Month_Last = "10";
                        break;
                    case "November": Month_Last = "11";
                        break;
                    case "December": Month_Last = "12";
                        break;
                    default:
                        break;
                }

                if ((ddlMonth.SelectedValue == "January") || (ddlMonth.SelectedValue == "February") || (ddlMonth.SelectedValue == "March"))
                {
                    Year_Last = Years[1];
                }
                else
                {
                    Year_Last = Years[0];
                }
                FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
                ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

                txtFrmdate.Text = FromDate.ToString();
                txtTodate.Text = ToDate.ToString();

            }
            else
            {
                txtTodate.Text = "";
                txtFrmdate.Text = "";
            }

        }
    }
    protected void ListRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        RptLabel.Text = ListRptName.SelectedItem.Text.Trim();
        //  Load_Cat();
        if(RptLabel.Text == "CONVEY ALLOWANCE")
        {
            ddlDepartment.Enabled = true;
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlMonth.Enabled = true;
            ddlFinYear.Enabled = true;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            ddlCategory.Enabled = true;
            ddlEmployeeType.Enabled = true;

        }else
        if (RptLabel.Text == "BANK PAYMENT")
        {
            ddlDepartment.Enabled = true;
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlMonth.Enabled = true;
            ddlFinYear.Enabled = true;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            ddlCategory.Enabled = true;
            ddlEmployeeType.Enabled = true;

        }else
        if (RptLabel.Text == "CASH PAYMENT")
        {
            ddlDepartment.Enabled = true;
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlMonth.Enabled = true;
            ddlFinYear.Enabled = true;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            ddlCategory.Enabled = true;
            ddlEmployeeType.Enabled = true;

        }
        else if (RptLabel.Text == "MAN DAYS LIST")
       {
            ddlDepartment.Enabled = true;
            btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlMonth.Enabled = false;
           ddlFinYear.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           ddlCategory.Enabled = true;
           ddlEmployeeType.Enabled = true;
       }
       else if (RptLabel.Text == "EMPLOYEE WORK LIST")
       {
            ddlDepartment.Enabled = true;
            btnReport.Enabled = true;
           btnExcel.Enabled = false;
           ddlMonth.Enabled = false;
           ddlFinYear.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           ddlCategory.Enabled = true;
           ddlEmployeeType.Enabled = true;
       }
       else if (RptLabel.Text == "FORM 6"|| RptLabel.Text == "FORM 6 ZERODAYS")
       {
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlMonth.Enabled = true;
           ddlFinYear.Enabled = true;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           ddlCategory.Enabled = true;
           ddlEmployeeType.Enabled = true;
            ddlDepartment.Enabled = true;
       }
        else if ( RptLabel.Text == "FORM 6-A")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlMonth.Enabled = true;
            ddlFinYear.Enabled = true;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            ddlCategory.Enabled = true;
            ddlEmployeeType.Enabled = true;
        }
        else if (RptLabel.Text == "FORM 5")
       {
           btnReport.Enabled = false;
           btnExcel.Enabled = true;
           ddlMonth.Enabled = false;
           ddlFinYear.Enabled = false;
           txtFrmdate.Enabled = true;
           txtTodate.Enabled = true;
           ddlCategory.Enabled = true;
           ddlEmployeeType.Enabled = true;
       }
      else
        if (RptLabel.Text == "FORM-12")
        {

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlMonth.Enabled = false;
            ddlDepartment.Enabled = true;
            ddlEmployeeType.Enabled = true;
            ddlCategory.Enabled = true;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
        }
        else
        if (RptLabel.Text == "FORM 15")
        {
            btnReport.Enabled = false;
            btnExcel.Enabled = true;
            ddlDepartment.Enabled = false;
            ddlEmployeeType.Enabled = true;
            ddlMonth.Enabled = false;
            ddlCategory.Enabled = true;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
        }
        else
        if (RptLabel.Text == "FORM-15 CL WAGES")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlDepartment.Enabled = false;
            ddlEmployeeType.Enabled = true;
            ddlMonth.Enabled = false;
            ddlCategory.Enabled = true;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
        }
        else
        if (RptLabel.Text == "FORM 2 PF")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlDepartment.Enabled = true;
            ddlMachineID.Enabled = true;
            ddlEmployeeType.Enabled = true;
            ddlFinYear.Enabled = false;
            ddlMonth.Enabled = false;
            ddlCategory.Enabled = true;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
        }
        else
        if (RptLabel.Text == "FORM 2 ESI")
        {
            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlDepartment.Enabled = true;
            ddlEmployeeType.Enabled = true;
            ddlMonth.Enabled = false;
            ddlCategory.Enabled = true;
            txtFrmdate.Enabled = false;
            txtTodate.Enabled = false;
            ddlFinYear.Enabled = false;
            ddlMachineID.Enabled = true;
        }
        else
        {
            if (ddlRptName.SelectedItem.Text == "ALL")
            {
                ddlDepartment.Enabled = true;
            }

            btnReport.Enabled = true;
            btnExcel.Enabled = false;
            ddlMonth.Enabled = true;
            ddlFinYear.Enabled = true;
            txtFrmdate.Enabled = true;
            txtTodate.Enabled = true;
            ddlCategory.Enabled = true;
            ddlEmployeeType.Enabled = true;
        }
    }
    
    protected void ddlRptName_SelectedIndexChanged(object sender, EventArgs e)
    {
        btnReport.Enabled = false;
        btnExcel.Enabled = false;
        if (SessionUserType != "1" && SessionUserType != "4")
        {
            ListRptName.Items.Clear();
            RptLabel.Text = "";
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from [Raajco_Rights]..Pay_Report_User_Rights where CompCode='" + SessionCcode + "'";
            query = query + " And LocCode='" + SessionLcode + "' And ReportType='" + ddlRptName.SelectedItem.Text.Trim() + "' And AddRights='1' And UserName='" + SessionUser + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            ListRptName.Items.Clear();
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                }
            }

        }
        else
        {
            ListRptName.Items.Clear();
            RptLabel.Text = "";
            string query = "";
            DataTable DT = new DataTable();
            query = "Select * from [Raajco_Rights]..PayReportType_Name ";
            query = query + " where ReportType='" + ddlRptName.SelectedItem.Text.Trim() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            ListRptName.Items.Clear();
            if (DT.Rows.Count != 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    ListRptName.Items.Add(DT.Rows[i]["ReportName"].ToString());
                }
            }
        }
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";


        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();


    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();

    }
    public void DropDown_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select *from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptCode"] = "0";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptCode";
        ddlDepartment.DataBind();


    }
    protected void btnExcel_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        if (RptLabel.Text == "FORM 6-A")
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            else if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Load_Month_Year();

                ReportName = "PF DEDUCTION";

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("RptEPF_Form_6-A.aspx?Deptname=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&fromdate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Months=" + Month + "&yr=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
            }
        }

        if (RptLabel.Text == "FORM 15")
        {
            if (ddlEmployeeType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else if (ddlFinYear.SelectedItem.Text == "- select -")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Year');", true);
            }
            else
            {
                //DateTime date1;
                //DateTime date2;
                //date1 = Convert.ToDateTime(txtFrmdate.Text);
                //date2 = Convert.ToDateTime(txtTodate.Text);
                //string monthname = date1.ToString("MMMM");
                //string Year = ddlYear.SelectedItem.Text;
                //string[] Fyear = Year.Split('-');
                //string First_Year = Fyear[0];
                //string Second_Year = Fyear[1];

                //string FromDate = "01/04/" + First_Year.ToString();
                //string Todate = "31/03/" + Second_Year.ToString();

                //DateTime FDate = Convert.ToDateTime(FromDate.ToString());
                //DateTime TDate = Convert.ToDateTime(Todate.ToString());


                //if (date1 >= FDate && date2 <= TDate)
                //{
                string TempWages = ddlEmployeeType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("Form15.aspx?Deptname=" + ddlDepartment.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages + "&Year=" + ddlFinYear.SelectedItem.Value, "_blank", "");

                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check Finacial Year');", true);
                //}

                //ResponseHelper.Redirect("Form15.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");
            }
        }
       if (RptLabel.Text == "BANK PAYMENT") //BANK PAYMENT
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            else if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                ReportName = "BANK PAYMENT";

                Load_Month_Year();

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("ReportDisplay.aspx?Deptname=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
            }
        }
         if (RptLabel.Text == "CASH PAYMENT") //CASH PAYMENT
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            else if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                ReportName = "CASH PAYMENT";

                Load_Month_Year();

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
            }
        }

        if (RptLabel.Text == "FORM-12")
        {
            if (ddlEmployeeType.SelectedItem.Text == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
            }
            else if (txtFrmdate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
            }
            else if (txtTodate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
            }
            else
            {
                string TempWages = ddlEmployeeType.SelectedItem.Text.Replace("&", "_");
                ResponseHelper.Redirect("Form12.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM 6")
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            //if (ddlDepartment.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department.');", true);
            //    ErrFlag = true;
            //}
            //if (ddlCategory.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
            //    ErrFlag = true;
            //}
            //if (ddlEmployeeType.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type.');", true);
            //    ErrFlag = true;
            //}
             if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Load_Month_Year();

                ReportName = "ESI DEDUCTION";

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("RptESI_Form_6.aspx?Report=" + ReportName + "&Wages=" + EmpType + "&fromdate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Months=" + Month + "&yr=" + YearCheck + "&Cate=" + Category + "&DeptName=" + ddlDepartment.SelectedItem.Text + "&RptYear=" + YR, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM 6 ZERODAYS")
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            //if (ddlDepartment.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Department.');", true);
            //    ErrFlag = true;
            //}
            //if (ddlCategory.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
            //    ErrFlag = true;
            //}
            //if (ddlEmployeeType.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type.');", true);
            //    ErrFlag = true;
            //}
            if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Load_Month_Year();

                ReportName = "ESI DEDUCTION";

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("RptESI_Form_6_ZeroDays.aspx?Report=" + ReportName + "&Wages=" + EmpType + "&fromdate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Months=" + Month + "&yr=" + YearCheck + "&Cate=" + Category + "&DeptName=" + ddlDepartment.SelectedItem.Text + "&RptYear=" + YR, "_blank", "");
            }
        }
        if (RptLabel.Text == "FORM 5")
        {
            //if (ddlMonth.SelectedValue == "0")
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
            //    ErrFlag = true;
            //}
            //else 
            if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                string[] FromDateStr = txtFrmdate.Text.Split('/');
                string[] ToDateStr = txtTodate.Text.Split('/');
                string FromYear = FromDateStr[2];
                string ToYear = ToDateStr[2];

                #region MonthName
                string From_Mont = "";
                switch (Convert.ToInt32(FromDateStr[1]))
                {
                    case 1:
                        From_Mont = "January";
                        break;

                    case 2:
                        From_Mont = "February";
                        break;
                    case 3:
                        From_Mont = "March";
                        break;
                    case 4:
                        From_Mont = "April";
                        break;
                    case 5:
                        From_Mont = "May";
                        break;
                    case 6:
                        From_Mont = "June";
                        break;
                    case 7:
                        From_Mont = "July";
                        break;
                    case 8:
                        From_Mont = "August";
                        break;
                    case 9:
                        From_Mont = "September";
                        break;
                    case 10:
                        From_Mont = "October";
                        break;
                    case 11:
                        From_Mont = "November";
                        break;
                    case 12:
                        From_Mont = "December";
                        break;
                    default:
                        break;
                }
                #endregion


                #region MonthName
                string TO_Mont = "";
                switch (Convert.ToInt32(ToDateStr[1]))
                {
                    case 1:
                        TO_Mont = "January";
                        break;

                    case 2:
                        TO_Mont = "February";
                        break;
                    case 3:
                        TO_Mont = "March";
                        break;
                    case 4:
                        TO_Mont = "April";
                        break;
                    case 5:
                        TO_Mont = "May";
                        break;
                    case 6:
                        TO_Mont = "June";
                        break;
                    case 7:
                        TO_Mont = "July";
                        break;
                    case 8:
                        TO_Mont = "August";
                        break;
                    case 9:
                        TO_Mont = "September";
                        break;
                    case 10:
                        TO_Mont = "October";
                        break;
                    case 11:
                        TO_Mont = "November";
                        break;
                    case 12:
                        TO_Mont = "December";
                        break;
                    default:
                        break;
                }
                #endregion
                Load_Month_Year();

                ReportName = "ESI DEDUCTION";

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                //ResponseHelper.Redirect("RptESI_Form_5.aspx?Report=" + ReportName + "&Wages=" + EmpType + "&fromdate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Months=" + Month + "&yr=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");

                ResponseHelper.Redirect("RptESI_Form_5.aspx?Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&yr=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR + "&FromMonth=" + From_Mont + "&ToMonth=" + TO_Mont + "&FromYear=" + FromYear + "&ToYear=" + ToYear, "_blank", "");
            }
        }

    }
    public void Load_Month_Year()
    {
        Month = ddlMonth.SelectedItem.Text;

        Years = ddlFinYear.SelectedValue.Split('-');
        YearCheck = Years[0].ToString();


        if (ddlMonth.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinYear.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonth.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinYear.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonth.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinYear.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinYear.SelectedValue);
        }
    }
    protected void btnReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;

        //if (ddlMonth.SelectedValue == "0")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
        //    ErrFlag = true;
        //}
        //else if (txtFrmdate.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
        //    ErrFlag = true;
        //}
        //else if (txtTodate.Text.Trim() == "")
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
        //    ErrFlag = true;
        //}

        if(RptLabel.Text== "CONVEY ALLOWANCE")
        {
            if (ddlMonth.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            else if (txtFrmdate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTodate.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                Load_Month_Year();

                ReportName = RptLabel.Text;

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
            }
        }

        if (RptLabel.Text == "FORM-12")
        {
            if (ddlEmployeeType.SelectedValue == "0")
            {
                EmpType = "";
            }
            else if (ddlEmployeeType.SelectedValue == "")
            {
                EmpType = "";
            }
            else
            {
                EmpType = ddlEmployeeType.SelectedItem.Text;
            }

            ResponseHelper.Redirect("Form12Check.aspx?Year=" + ddlDepartment.SelectedItem.Text.ToString().Split('-').First().ToString() + "&DeptName=" + ddlDepartment.SelectedItem.Text + "&CatName=" + ddlCategory.SelectedItem.Text + "&Wages=" + EmpType, "_blank", "");

        }
        if (!ErrFlag)
        {
            if (RptLabel.Text == "PF DEDUCTION")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    Load_Month_Year();

                    ReportName = "PF DEDUCTION";

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
                
            }


            if (RptLabel.Text == "PF DEDUCTION REELING")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    Load_Month_Year();

                    ReportName = "PF DEDUCTION REELING";

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&YearCode=" + ddlFinYear.SelectedItem.Text + "&RptYear=" + YR, "_blank", "");
                }
            }


            if (RptLabel.Text=="FORM 2 ESI")
            {
                ReportName = "FORM 2 ESI";

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&DeptName=" + ddlDepartment.SelectedItem.Text + "&CatName=" + Category + "&MachineID=" + ddlMachineID.SelectedValue, "_blank", "");

            }

            if (RptLabel.Text == "FORM 2 PF")
            {
                ReportName = "FORM 2 PF";

                if (ddlCategory.SelectedValue == "")
                {
                    Category = "";
                }
                else if (ddlCategory.SelectedValue == "0")
                {
                    Category = "";
                }
                else
                {
                    Category = ddlCategory.SelectedItem.Text;
                }


                if (ddlEmployeeType.SelectedValue == "0")
                {
                    EmpType = "";
                }
                else if (ddlEmployeeType.SelectedValue == "")
                {
                    EmpType = "";
                }
                else
                {
                    EmpType = ddlEmployeeType.SelectedItem.Text;
                }

                ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&DeptName=" + ddlDepartment.SelectedItem.Text + "&CatName=" + Category + "&MachineID=" + ddlMachineID.SelectedItem.Text, "_blank", "");

            }

            if (RptLabel.Text == "FORM-15 CL WAGES")
            {
                if (ddlEmployeeType.SelectedItem.Text == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter Wages Type');", true);
                }
                //else if (txtFrmdate.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter FromDate');", true);
                //}
                //else if (txtTodate.Text == "")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter ToDate');", true);
                //}
                else if (ddlFinYear.SelectedItem.Text == "- select -")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select Year');", true);
                }
                else
                {
                    //DateTime date1;
                    //DateTime date2;
                    //date1 = Convert.ToDateTime(txtFrmdate.Text);
                    //date2 = Convert.ToDateTime(txtTodate.Text);
                    //string monthname = date1.ToString("MMMM");
                    //string Year = ddlYear.SelectedItem.Text;
                    //string[] Fyear = Year.Split('-');
                    //string First_Year = Fyear[0];
                    //string Second_Year = Fyear[1];

                    //string FromDate = "01/04/" + First_Year.ToString();
                    //string Todate = "31/03/" + Second_Year.ToString();

                    //DateTime FDate = Convert.ToDateTime(FromDate.ToString());
                    //DateTime TDate = Convert.ToDateTime(Todate.ToString());


                    //if (date1 >= FDate && date2 <= TDate)
                    //{
                    string TempWages = ddlEmployeeType.SelectedItem.Text.Replace("&", "_");
                    ResponseHelper.Redirect("RptForm15_CLWages.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + TempWages + "&Year=" + ddlFinYear.SelectedItem.Value, "_blank", "");

                    //}
                    //else
                    //{
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Check Finacial Year');", true);
                    //}

                    //ResponseHelper.Redirect("Form15.aspx?FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&WagesType=" + ddlWagesType.SelectedItem.Text + "&Year=" + ddlYear.SelectedItem.Text, "_blank", "");
                }
            }
            else if (RptLabel.Text == "ESI DEDUCTION")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "ESI DEDUCTION";

                    Load_Month_Year();

                    //Years = ddlFinYear.SelectedValue.Split('-');
                    //YearCheck = Years[0].ToString();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
                
            }
            else if (RptLabel.Text == "LIC DEDUCTION")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "LIC DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
                
            }
            else if (RptLabel.Text == "HOUSE RENT DEDUCTION")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "HOUSE RENT DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "TEMPLE DEDUCTION")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "TEMPLE DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "STORES DEDUCTION")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "STORES DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "OFFICE ADVANCE")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "OFFICE ADVANCE";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "GENERAL ADVANCE")
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "GENERAL ADVANCE";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "MISC2 DEDUCTION") //Ded4
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "MISC2 DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "MISC4 DEDUCTION") // Ded5
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "MISC4 DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "HOUSE LOAN DEDUCTION") //House Loan
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "HOUSE LOAN DEDUCTION";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "COMMISSION LIST") //COmmission
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "COMMISSION LIST";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "OT AMOUNT") //OT Amt
            {
                ReportName = "OT AMOUNT";


                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }

                if (!ErrFlag)
                {
                    Load_Month_Year();
                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }

                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "-Select-")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }
                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&Year=" + YearCheck + " &Month=" + ddlMonth.SelectedItem.Text + "&FromDate=" + txtFrmdate.Text + "&RptYear="+ YR + "&Todate=" + txtTodate.Text + "&CatName=" + Category, "_blank", "");
                }
            }
           
            else if (RptLabel.Text == "OTHER PAYSLIP") //PAYSLIP
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "OTHER PAYSLIP";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "MAN DAYS LIST") //PAYSLIP
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "MAN DAYS LIST";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
            else if (RptLabel.Text == "EMPLOYEE WORK LIST") //PAYSLIP
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string[] FromDateStr = txtFrmdate.Text.Split('/');
                    string[] ToDateStr = txtTodate.Text.Split('/');
                    string FromYear = FromDateStr[2];
                    string ToYear = ToDateStr[2];

                    #region MonthName
                    string From_Mont = "";
                    switch (Convert.ToInt32(FromDateStr[1]))
                    {
                        case 1:
                            From_Mont = "January";
                            break;

                        case 2:
                            From_Mont = "February";
                            break;
                        case 3:
                            From_Mont = "March";
                            break;
                        case 4:
                            From_Mont = "April";
                            break;
                        case 5:
                            From_Mont = "May";
                            break;
                        case 6:
                            From_Mont = "June";
                            break;
                        case 7:
                            From_Mont = "July";
                            break;
                        case 8:
                            From_Mont = "August";
                            break;
                        case 9:
                            From_Mont = "September";
                            break;
                        case 10:
                            From_Mont = "October";
                            break;
                        case 11:
                            From_Mont = "November";
                            break;
                        case 12:
                            From_Mont = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion


                    #region MonthName
                    string TO_Mont = "";
                    switch (Convert.ToInt32(ToDateStr[1]))
                    {
                        case 1:
                            TO_Mont = "January";
                            break;

                        case 2:
                            TO_Mont = "February";
                            break;
                        case 3:
                            TO_Mont = "March";
                            break;
                        case 4:
                            TO_Mont = "April";
                            break;
                        case 5:
                            TO_Mont = "May";
                            break;
                        case 6:
                            TO_Mont = "June";
                            break;
                        case 7:
                            TO_Mont = "July";
                            break;
                        case 8:
                            TO_Mont = "August";
                            break;
                        case 9:
                            TO_Mont = "September";
                            break;
                        case 10:
                            TO_Mont = "October";
                            break;
                        case 11:
                            TO_Mont = "November";
                            break;
                        case 12:
                            TO_Mont = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion

                    //Get To Mon

                    ReportName = "EMPLOYEE WORK LIST";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR + "&FromMonth=" + From_Mont + "&ToMonth=" + TO_Mont + "&FromYear=" + FromYear + "&ToYear=" + ToYear, "_blank", "");
                }
            }
            else if (RptLabel.Text == "SALARY HISTORY") //PAYSLIP
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string[] FromDateStr = txtFrmdate.Text.Split('/');
                    string[] ToDateStr = txtTodate.Text.Split('/');
                    string FromYear = FromDateStr[2];
                    string ToYear = ToDateStr[2];

                    #region MonthName
                    string From_Mont = "";
                    switch (Convert.ToInt32(FromDateStr[1]))
                    {
                        case 1:
                            From_Mont = "January";
                            break;

                        case 2:
                            From_Mont = "February";
                            break;
                        case 3:
                            From_Mont = "March";
                            break;
                        case 4:
                            From_Mont = "April";
                            break;
                        case 5:
                            From_Mont = "May";
                            break;
                        case 6:
                            From_Mont = "June";
                            break;
                        case 7:
                            From_Mont = "July";
                            break;
                        case 8:
                            From_Mont = "August";
                            break;
                        case 9:
                            From_Mont = "September";
                            break;
                        case 10:
                            From_Mont = "October";
                            break;
                        case 11:
                            From_Mont = "November";
                            break;
                        case 12:
                            From_Mont = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion


                    #region MonthName
                    string TO_Mont = "";
                    switch (Convert.ToInt32(ToDateStr[1]))
                    {
                        case 1:
                            TO_Mont = "January";
                            break;

                        case 2:
                            TO_Mont = "February";
                            break;
                        case 3:
                            TO_Mont = "March";
                            break;
                        case 4:
                            TO_Mont = "April";
                            break;
                        case 5:
                            TO_Mont = "May";
                            break;
                        case 6:
                            TO_Mont = "June";
                            break;
                        case 7:
                            TO_Mont = "July";
                            break;
                        case 8:
                            TO_Mont = "August";
                            break;
                        case 9:
                            TO_Mont = "September";
                            break;
                        case 10:
                            TO_Mont = "October";
                            break;
                        case 11:
                            TO_Mont = "November";
                            break;
                        case 12:
                            TO_Mont = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion

                    //Get To Mon

                    ReportName = "SALARY HISTORY";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR + "&FromMonth=" + From_Mont + "&ToMonth=" + TO_Mont + "&FromYear=" + FromYear + "&ToYear=" + ToYear, "_blank", "");
                }
            }
            else if (RptLabel.Text == "WAGES ABSTRACT") //WAGES ABSTRACT
            {
                if (ddlMonth.SelectedValue == "0")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                    ErrFlag = true;
                }
                else if (txtFrmdate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the From Date Properly.');", true);
                    ErrFlag = true;
                }
                else if (txtTodate.Text.Trim() == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    ReportName = "WAGES ABSTRACT";

                    Load_Month_Year();

                    if (ddlCategory.SelectedValue == "")
                    {
                        Category = "";
                    }
                    else if (ddlCategory.SelectedValue == "0")
                    {
                        Category = "";
                    }
                    else
                    {
                        Category = ddlCategory.SelectedItem.Text;
                    }


                    if (ddlEmployeeType.SelectedValue == "0")
                    {
                        EmpType = "";
                    }
                    else if (ddlEmployeeType.SelectedValue == "")
                    {
                        EmpType = "";
                    }
                    else
                    {
                        EmpType = ddlEmployeeType.SelectedItem.Text;
                    }

                    ResponseHelper.Redirect("ReportDisplay.aspx?DeptName=" + ddlDepartment.SelectedItem.Text + "&Report=" + ReportName + "&Wages=" + EmpType + "&FromDate=" + txtFrmdate.Text + "&ToDate=" + txtTodate.Text + "&Month=" + Month + "&Year=" + YearCheck + "&CatName=" + Category + "&RptYear=" + YR, "_blank", "");
                }
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlDepartment.ClearSelection();
        ddlMachineID.ClearSelection();
        ddlCategory.ClearSelection();
        ddlEmployeeType.ClearSelection();
        ddlFinYear.ClearSelection();
        txtFrmdate.Text = "";
        txtTodate.Text = "";
        ddlMonth.ClearSelection();
    }
}
