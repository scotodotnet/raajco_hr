﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class BasicSalary_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionPayroll;

    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SSQL = "";
    string SpFinyear = "";
    string btnName = "Save";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Load_WagesType();
            Months_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
        Load_Data();
    }
     private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";

        if ((ddlcategory.SelectedItem.Text == "STAFF") || (ddlcategory.SelectedItem.Text == "LABOUR"))

        {
            //dr["EmpTypeCd"] = "1";
            //dr["EmpType"] = "ALL";
        }
        dtdsupp.Rows.InsertAt(dr, 0);

        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }
    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }
   

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Errflag = "False";
        btnName = "Save";

        if (ddlcategory.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Category...');", true);
        }
        if (txtEmployeeType.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Employee Type...');", true);
        }
        if ((ddlFinance.SelectedItem.Text == "0") || (ddlFinance.SelectedItem.Text == "-Select-"))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter Financial Year...');", true);
        }
        if ((txtPercentage.Text == "0") || (txtPercentage.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter Percentage Values...');", true);
        }

        //string FinYear = ddlFinance.SelectedItem.Text;
        //string[] FinyearSplit = FinYear.Split('-');
        // SpFinyear = FinyearSplit[0];
       

        if(Errflag == "False")
        {
            DataTable DT_Chk = new DataTable();
            SSQL = "";
            SSQL = "Select * from Mst_BasicSalary where Category='" + ddlcategory.SelectedItem.Text + "' and EmployeeType='" + txtEmployeeType.SelectedItem.Text + "' and FinYear ='" + ddlFinance.SelectedItem.Text + "' and Months ='" + ddlMonths.SelectedItem.Text + "'  ";
            DT_Chk = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Chk.Rows.Count != 0)
            {
                SSQL = "";
                SSQL = "Delete from Mst_BasicSalary where Category='" + ddlcategory.SelectedItem.Text + "' and EmployeeType='" + txtEmployeeType.SelectedItem.Text + "' and FinYear ='" + ddlFinance.SelectedItem.Text + "' and Months ='" + ddlMonths.SelectedItem.Text + "'  "; 
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
            SSQL = "";
            SSQL = "Insert into Mst_BasicSalary(Compcode,LocCode,Category,EmployeeType,FinYear,Months,Percentage,CreatedDate,Conform) values( ";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + ddlcategory.SelectedItem.Text + "','" + txtEmployeeType.SelectedItem.Text + "'," ;
            SSQL = SSQL + "'" + ddlFinance.SelectedItem.Text + "','" + ddlMonths.SelectedItem.Text + "','" + txtPercentage.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy") + "','Pending')";
            objdata.RptEmployeeMultipleDetails(SSQL);
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Basic Variable Pay inserted Succesfully...');", true);

            if(btnName =="Update")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Basic Variable Pay Updated Succesfully...');", true);
            }
            Load_Data();
            clear();
        }






     

    }
    private void clear()
    {
        ddlcategory.SelectedValue = "-Select-";
        Load_WagesType();
        Months_load();
        txtPercentage.Text = "";
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
    }
    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = " select * from Mst_BasicSalary";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string EmpType = "";
        string FinYear = "";
        string Months = "";

        {
            string[] D1 = e.CommandArgument.ToString().Split('|');
            EmpType = D1[0];
            FinYear = D1[1];
            Months = D1[3];
        }



        string query = "";
        DataTable DT = new DataTable();
        query = "select * from Mst_BasicSalary";
        query = query + " where EmployeeType='" + EmpType + "' and FinYear='" + FinYear + "' and Months='" + Months + "' and conform='Pending' ";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlcategory.SelectedItem.Text = DT.Rows[0]["Category"].ToString();
            txtEmployeeType.SelectedItem.Text= DT.Rows[0]["EmployeeType"].ToString();
            //Session["Cate"] = DT.Rows[0]["Category"].ToString();
            //Session["Emp"] = DT.Rows[0]["EmployeeType"].ToString();
            ddlFinance.SelectedItem.Text = DT.Rows[0]["FinYear"].ToString();
            ddlMonths.SelectedItem.Text = DT.Rows[0]["Months"].ToString();
            txtPercentage.Text = DT.Rows[0]["Percentage"].ToString();
            btnName = "Update";

        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Basic Variable Pay Confirmed so not able to change ...');", true);
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string EmpType = "";
        string FinYear = "";
        string Months = "";

        {
            string[] D1 = e.CommandArgument.ToString().Split('|');
            EmpType = D1[0];
            FinYear = D1[1];
            Months = D1[3];
        }
        string query = "";
        DataTable DT = new DataTable();
        query = "select * from Mst_BasicSalary";
        query = query + " where EmployeeType='" + EmpType + "' and FinYear='" + FinYear + "' and Months='" + Months + "' and conform='Pending' ";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if(DT.Rows.Count !=0)
        {
            query = "";
            query = "Delete from Mst_BasicSalary ";
            query = query + " where EmployeeType='" + EmpType + "' and FinYear='" + FinYear + "' and Months='" + Months + "' and conform='Pending' ";
            DT = objdata.RptEmployeeMultipleDetails(query);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Basic Variable Pay Confirmed so not able to Delete ...');", true);
        }
    }

    protected void btnConform_Click(object sender, EventArgs e)
    {
        DataTable DT_Chk = new DataTable();
        SSQL = "";
        SSQL = "Select * from Mst_BasicSalary where Category='" + ddlcategory.SelectedItem.Text + "' and EmployeeType='" + txtEmployeeType.SelectedItem.Text + "' and FinYear ='" + ddlFinance.SelectedItem.Text + "' and Months ='" + ddlMonths.SelectedItem.Text + "'  ";
        DT_Chk = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT_Chk.Rows.Count != 0)
        {
            SSQL = "";
            SSQL = "Update Mst_BasicSalary set conform='Confirm' where Category='" + ddlcategory.SelectedItem.Text + "' and EmployeeType='" + txtEmployeeType.SelectedItem.Text + "' and FinYear ='" + ddlFinance.SelectedItem.Text + "' and Months ='" + ddlMonths.SelectedItem.Text + "'  ";
            objdata.RptEmployeeMultipleDetails(SSQL);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Basic Variable Pay Confirm Succesfully...');", true);
            Load_Data();
            clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Save the Details First...');", true);
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
}
