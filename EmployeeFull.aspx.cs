﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;


public partial class EmployeeFull : System.Web.UI.Page
{

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string SSQL;
    //string mvarUserType = "IF User";
    string mvarUserType;
    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataTable ExDataTable = new DataTable();
    DataTable Familydt = new DataTable();

    string iEmpDet = "";
    //char[] delimiters = new char[] { "-",">" };

    string[] delimiters = { "-", ">" };

    string mUnitName = ""; string status = "";
    string strPhotoPath = "";
    FileStream fs;
    BinaryReader br;
    DataRow drow;
    string Empcode;
    DataSet ds = new DataSet();
    string Division;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Employee Full Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");
            }


            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();

            Empcode = Request.QueryString["Empcode"].ToString();
            Division = Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();


            GetEmpFullProfileTable();

            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/single_emp_profile1.rpt"));

            report.SetDataSource(ds.Tables[0]);
            report.Subreports[0].SetDataSource(Familydt);
            //report.Database.Tables[0].SetDataSource(Dx.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

            CrystalReportViewer1.ReportSource = report;


        }
    }
    public void GetEmpFullProfileTable()
    {

        AutoDataTable.Columns.Add("CompCode");
        AutoDataTable.Columns.Add("LocCode");
        AutoDataTable.Columns.Add("EmpPrefix");
        AutoDataTable.Columns.Add("EmpNo");
        AutoDataTable.Columns.Add("ExistingCode");
        AutoDataTable.Columns.Add("MachineID");
        AutoDataTable.Columns.Add("FirstName");
        AutoDataTable.Columns.Add("LastName");
        AutoDataTable.Columns.Add("MiddleInitial");
        AutoDataTable.Columns.Add("ShiftType");

        AutoDataTable.Columns.Add("Gender");
        AutoDataTable.Columns.Add("BirthDate");
        AutoDataTable.Columns.Add("Age");
        AutoDataTable.Columns.Add("DOJ");
        AutoDataTable.Columns.Add("MaritalStatus");
        AutoDataTable.Columns.Add("CatName");
        AutoDataTable.Columns.Add("SubCatName");
        AutoDataTable.Columns.Add("TypeName");
        AutoDataTable.Columns.Add("EmpStatus");
        AutoDataTable.Columns.Add("IsActive");

        AutoDataTable.Columns.Add("DeptName");
        AutoDataTable.Columns.Add("Designation");
        AutoDataTable.Columns.Add("PayPeriod_Desc");
        AutoDataTable.Columns.Add("BaseSalary");
        AutoDataTable.Columns.Add("PFNo");
        AutoDataTable.Columns.Add("Nominee");
        AutoDataTable.Columns.Add("ESINo");
        AutoDataTable.Columns.Add("StdWrkHrs");
        AutoDataTable.Columns.Add("OTEligible");
        AutoDataTable.Columns.Add("Nationality");

        AutoDataTable.Columns.Add("Qualification");
        AutoDataTable.Columns.Add("Certificate");
        AutoDataTable.Columns.Add("FamilyDetails");
        AutoDataTable.Columns.Add("Address1");
        AutoDataTable.Columns.Add("Address2");
        AutoDataTable.Columns.Add("RecuritmentThro");
        AutoDataTable.Columns.Add("IDMark1");
        AutoDataTable.Columns.Add("IDMark2");
        AutoDataTable.Columns.Add("BloodGroup");
        AutoDataTable.Columns.Add("Handicapped");
        AutoDataTable.Columns.Add("Height");
        AutoDataTable.Columns.Add("Weight");
        AutoDataTable.Columns.Add("BankName");
        AutoDataTable.Columns.Add("BranchCode");
        AutoDataTable.Columns.Add("AccountNo");

        AutoDataTable.Columns.Add("BusNo");
        AutoDataTable.Columns.Add("Refrence");
        AutoDataTable.Columns.Add("Permanent_Dist");
        AutoDataTable.Columns.Add("Present_Dist");
        AutoDataTable.Columns.Add("WeekOff");
        AutoDataTable.Columns.Add("parentsMobile");
        AutoDataTable.Columns.Add("EmployeeMobile");
        AutoDataTable.Columns.Add("Wages");
        AutoDataTable.Columns.Add("Adhar_No");

        //16/03/2020
        AutoDataTable.Columns.Add("Qualification1");
        AutoDataTable.Columns.Add("Qualification2");
        AutoDataTable.Columns.Add("Qualification3");
        AutoDataTable.Columns.Add("School/clg1");
        AutoDataTable.Columns.Add("School/clg2");
        AutoDataTable.Columns.Add("School/clg3");
        AutoDataTable.Columns.Add("YearofPassing1");
        AutoDataTable.Columns.Add("YearofPassing2");
        AutoDataTable.Columns.Add("YearofPassing3");
        AutoDataTable.Columns.Add("Marks1");
        AutoDataTable.Columns.Add("Marks2");
        AutoDataTable.Columns.Add("Marks3");
        AutoDataTable.Columns.Add("Course");

        AutoDataTable.Columns.Add("readTamil");
        AutoDataTable.Columns.Add("WriteTamil");
        AutoDataTable.Columns.Add("speakTamil");
        AutoDataTable.Columns.Add("readEng");
        AutoDataTable.Columns.Add("writeEng");
        AutoDataTable.Columns.Add("speakEng");
        AutoDataTable.Columns.Add("readOthers");
        AutoDataTable.Columns.Add("writeOthers");
        AutoDataTable.Columns.Add("speakOthers");

        AutoDataTable.Columns.Add("Company1");
        AutoDataTable.Columns.Add("Company2");
        //AutoDataTable.Columns.Add("Companyname2");
        AutoDataTable.Columns.Add("Doj1");
        AutoDataTable.Columns.Add("Doj2");
        //AutoDataTable.Columns.Add("Doj3");
        AutoDataTable.Columns.Add("Dol1");
        AutoDataTable.Columns.Add("Dol2");
        //AutoDataTable.Columns.Add("Dol3");
        AutoDataTable.Columns.Add("DrawnWages1");
        AutoDataTable.Columns.Add("DrawnWages2");
        //AutoDataTable.Columns.Add("DrawnWages3");
        AutoDataTable.Columns.Add("Reason1");
        AutoDataTable.Columns.Add("Reason2");
        //AutoDataTable.Columns.Add("Reason3");
        AutoDataTable.Columns.Add("Stay");
        AutoDataTable.Columns.Add("EyeSight");
        AutoDataTable.Columns.Add("extracurricular");
        AutoDataTable.Columns.Add("FamilyDt");
        AutoDataTable.Columns.Add("FN");
        AutoDataTable.Columns.Add("FR");
        AutoDataTable.Columns.Add("FDob");
        AutoDataTable.Columns.Add("Fage");
        AutoDataTable.Columns.Add("Profession");

        AutoDataTable.Columns.Add("img", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("Add_Proof", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_3", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_4", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_5", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_6", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_7", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_8", System.Type.GetType("System.Byte[]"));
        AutoDataTable.Columns.Add("ID_Proof_9", System.Type.GetType("System.Byte[]"));

        Familydt.Columns.Add("FN");
        Familydt.Columns.Add("FR");
        Familydt.Columns.Add("FDob");
        Familydt.Columns.Add("Fage");
        Familydt.Columns.Add("Profession");

        //AutoDataTable.Columns.Add("Height");
        //AutoDataTable.Columns.Add("Weight");
        //AutoDataTable.Columns.Add("Nationality");
        //AutoDataTable.Columns.Add("BloodGroup");    


        string s = Empcode;
        string[] delimiters = new string[] { "->" };
        string[] items = s.Split(delimiters, StringSplitOptions.None);
        string ss = items[0];
        iEmpDet = items[1];



        //string Emp_No = null;
        //string Add_Where_Cont = null;


        //if (iEmpDet[0] != "ALL" & iEmpDet[0] == "")
        //{
        //    Emp_No = iEmpDet;
        //    iEmpDet = Emp_No.Split(delimiters, StringSplitOptions.None);

        //    Add_Where_Cont = "";
        //    Add_Where_Cont += " Where CompCode='" + CompanyCode + "'";
        //    Add_Where_Cont += " and LocCode='" + LocationCode + "' and EmpNo='" + iEmpDet[2] + "'";
        //}
        //else
        //{
        //    Add_Where_Cont = "";
        //    Add_Where_Cont += " Where CompCode='" + CompanyCode + "'";
        //    Add_Where_Cont += " LocCode='" + LocationCode + "'";
        //}


        SSQL = "";
        SSQL = "select isnull(CompCode,'') as [CompCode]";
        SSQL += ",isnull(LocCode,'') as [LocCode]";
        SSQL += ",isnull(EmpPrefix,'') as [EmpPrefix]";
        SSQL += ",isnull(EmpNo,'') as [EmpNo]";
        SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
        SSQL += ",isnull(MachineID,'') as [MachineID]";
        SSQL += ",isnull(FirstName,'') as [FirstName]";
        SSQL += ",isnull(LastName,'') as [LastName]";
        SSQL += ",isnull(MiddleInitial,'') as [MiddleInitial]";
        SSQL += ",isnull(ShiftType,'') as [ShiftType]";
        SSQL += ",isnull(Gender,'') as [Gender]";
        SSQL += ",isnull(BirthDate,'') as [BirthDate]";
        SSQL += ",isnull(Age,'') as [Age]";
        SSQL += ",isnull(DOJ,'') as [DOJ]";
        SSQL += ",isnull(MaritalStatus,'') as [MaritalStatus]";
        SSQL += ",isnull(CatName,'') as [CatName]";
        SSQL += ",isnull(SubCatName,'') as [SubCatName]";
        SSQL += ",isnull(TypeName,'') as [TypeName]";
        SSQL += ",isnull(EmpStatus,'') as [EmpStatus]";
        SSQL += ",isnull(IsActive,'') as [IsActive]";
        SSQL += ",isnull(DeptName,'') as [DeptName]";
        SSQL += ",isnull(Designation,'') as [Designation]";
        SSQL += ",isnull(PayPeriod_Desc,'') as [PayPeriod_Desc]";
        SSQL += ",BaseSalary";
        SSQL += ",isnull(PFNo,'') as [PFNo]";
        SSQL += ",isnull(Nominee,'') as [Nominee]";
        SSQL += ",isnull(ESINo,'') as [ESINo]";
        SSQL += ",StdWrkHrs";
        SSQL += ",isnull(OTEligible,'') as [OTEligible]";
        SSQL += ",isnull(Nationality,'') as [Nationality]";
        SSQL += ",isnull(Qualification,'') as [Qualification]";
        SSQL += ",isnull(Certificate,'') as [Certificate]";
        SSQL += ",isnull(FamilyDetails,'') as [FamilyDetails]";
        SSQL += ",isnull(Address1,'') as [Address1]";
        SSQL += ",isnull(Address2,'') as [Address2]";
        SSQL += ",isnull(RecuritmentThro,'') as [RecuritmentThro]";
        SSQL += ",isnull(IDMark1,'') as [IDMark1]";
        SSQL += ",isnull(IDMark2,'') as [IDMark2]";
        SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";
        SSQL += ",isnull(Handicapped,'') as [Handicapped]";
        SSQL += ",isnull(Height,'') as [Height]";
        SSQL += ",isnull(Weight,'') as [Weight]";
        SSQL += ",isnull(BankName,'') as [BankName]";
        SSQL += ",isnull(BranchCode,'') as [BranchCode]";
        SSQL += ",isnull(AccountNo,'') as [AccountNo]";
        SSQL += ",isnull(BusNo,'') as [BusNo]";
        SSQL += ",isnull(Refrence,'') as [Refrence]";
        SSQL += ",isnull(Permanent_Dist,'') as [Permanent_Dist]";
        SSQL += ",isnull(Present_Dist,'') as [Present_Dist]";
        SSQL += ",isnull(WeekOff,'') as [WeekOff]";
        SSQL += ",isnull(parentsMobile,'') as [parentsMobile]";

        SSQL += ",isnull(EmployeeMobile,'') as [EmployeeMobile]";
        SSQL += ",isnull(Wages,'') as [Wages]";
        SSQL += ",isnull(Adhar_No,'') as [Adhar_No]";
        SSQL += ",isnull(Stay,'') as [Stay]";
        SSQL += ",isnull(EyeSight,'') as [EyeSight]";
        SSQL += ",isnull(extracurricular,'') as [extracurricular]";
        SSQL += ",isnull(Height,'') as [Height]";
        SSQL += ",isnull(Weight,'') as [Weight]";
        SSQL += ",isnull(Nationality,'') as [Nationality]";
        SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";
        if (status == "Approval")
        {
            SSQL = SSQL + " from Employee_Mst";
        }
        else
        {
            SSQL = SSQL + " from Employee_Mst_New_Emp";
        }

        //SSQL += " from employee_mst";
        SSQL += " WHERE  EmpNo = '" + iEmpDet + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

        if (Division != "-Select-")
        {
            SSQL += " and Division='" + Division + "'";
        }

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

        if (mDataSet.Rows.Count == 0)
        {
            SSQL = "";
            SSQL = "select isnull(CompCode,'') as [CompCode]";
            SSQL += ",isnull(LocCode,'') as [LocCode]";
            SSQL += ",isnull(EmpPrefix,'') as [EmpPrefix]";
            SSQL += ",isnull(EmpNo,'') as [EmpNo]";
            SSQL += ",isnull(ExistingCode,'') as [ExistingCode]";
            SSQL += ",isnull(MachineID,'') as [MachineID]";
            SSQL += ",isnull(FirstName,'') as [FirstName]";
            SSQL += ",isnull(LastName,'') as [LastName]";
            SSQL += ",isnull(MiddleInitial,'') as [MiddleInitial]";
            SSQL += ",isnull(ShiftType,'') as [ShiftType]";
            SSQL += ",isnull(Gender,'') as [Gender]";
            SSQL += ",isnull(BirthDate,'') as [BirthDate]";
            SSQL += ",isnull(Age,'') as [Age]";
            SSQL += ",isnull(DOJ,'') as [DOJ]";
            SSQL += ",isnull(MaritalStatus,'') as [MaritalStatus]";
            SSQL += ",isnull(CatName,'') as [CatName]";
            SSQL += ",isnull(SubCatName,'') as [SubCatName]";
            SSQL += ",isnull(TypeName,'') as [TypeName]";
            SSQL += ",isnull(EmpStatus,'') as [EmpStatus]";
            SSQL += ",isnull(IsActive,'') as [IsActive]";
            SSQL += ",isnull(DeptName,'') as [DeptName]";
            SSQL += ",isnull(Designation,'') as [Designation]";
            SSQL += ",isnull(PayPeriod_Desc,'') as [PayPeriod_Desc]";
            SSQL += ",BaseSalary";
            SSQL += ",isnull(PFNo,'') as [PFNo]";
            SSQL += ",isnull(Nominee,'') as [Nominee]";
            SSQL += ",isnull(ESINo,'') as [ESINo]";
            SSQL += ",StdWrkHrs";
            SSQL += ",isnull(OTEligible,'') as [OTEligible]";
            SSQL += ",isnull(Nationality,'') as [Nationality]";
            SSQL += ",isnull(Qualification,'') as [Qualification]";
            SSQL += ",isnull(Certificate,'') as [Certificate]";
            SSQL += ",isnull(FamilyDetails,'') as [FamilyDetails]";
            SSQL += ",isnull(Address1,'') as [Address1]";
            SSQL += ",isnull(Address2,'') as [Address2]";
            SSQL += ",isnull(RecuritmentThro,'') as [RecuritmentThro]";
            SSQL += ",isnull(IDMark1,'') as [IDMark1]";
            SSQL += ",isnull(IDMark2,'') as [IDMark2]";
            SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";
            SSQL += ",isnull(Handicapped,'') as [Handicapped]";
            SSQL += ",isnull(Height,'') as [Height]";
            SSQL += ",isnull(Weight,'') as [Weight]";
            SSQL += ",isnull(BankName,'') as [BankName]";
            SSQL += ",isnull(BranchCode,'') as [BranchCode]";
            SSQL += ",isnull(AccountNo,'') as [AccountNo]";
            SSQL += ",isnull(BusNo,'') as [BusNo]";
            SSQL += ",isnull(Refrence,'') as [Refrence]";
            SSQL += ",isnull(Permanent_Dist,'') as [Permanent_Dist]";
            SSQL += ",isnull(Present_Dist,'') as [Present_Dist]";
            SSQL += ",isnull(WeekOff,'') as [WeekOff]";
            SSQL += ",isnull(parentsMobile,'') as [parentsMobile]";

            SSQL += ",isnull(EmployeeMobile,'') as [EmployeeMobile]";
            SSQL += ",isnull(Wages,'') as [Wages]";
            SSQL += ",isnull(Adhar_No,'') as [Adhar_No]";
            SSQL += ",isnull(stay,'') as [Stay]";
            SSQL += ",isnull(EyeSight,'') as [EyeSight]";
            SSQL += ",isnull(extracurricular,'') as [extracurricular]";
            SSQL += ",isnull(Height,'') as [Height]";
            SSQL += ",isnull(Weight,'') as [Weight]";
            SSQL += ",isnull(Nationality,'') as [Nationality]";
            SSQL += ",isnull(BloodGroup,'') as [BloodGroup]";

            SSQL += " from Employee_Mst_New_Emp";
            SSQL += " WHERE  EmpNo = '" + iEmpDet + "'and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' ";

            if (Division != "-Select-")
            {
                SSQL += " and Division='" + Division + "'";
            }

            mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        if (mDataSet.Rows.Count > 0)
        {

            string mUnitName = "";
            string strPhotoPath = "";

            if (SessionLcode == "UNIT I")
            {
                mUnitName = "Unit1-Photos";
            }
            else if (SessionLcode == "UNIT II")
            {
                mUnitName = "Unit2-Photos";
            }
            else
            {
                mUnitName = "mUnitName";
            }

            for (int iRow = 0; iRow < mDataSet.Rows.Count; iRow++)
            {



                DataTable DT_Photo = new DataTable();
                string SS = "Select * from Photo_Path_Det";
                DT_Photo = objdata.RptEmployeeMultipleDetails(SS);

                string PhotoDet = "";
                string UNIT_Folder = "";
                if (DT_Photo.Rows.Count != 0)
                {
                    PhotoDet = DT_Photo.Rows[0]["Photo_Path"].ToString();
                }
                //if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = "Emp_Scan/UNIT_I/"; }
                //if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = "Emp_Scan/UNIT_II/"; }
                //if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = "Emp_Scan/UNIT_III/"; }
                //if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = "//SABA-PC/To Share/Narmatha/Emp_Scan/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                if (SessionLcode.ToUpper() == "UNIT II") { UNIT_Folder = PhotoDet + "/UNIT_II/"; }
                if (SessionLcode.ToUpper() == "UNIT III") { UNIT_Folder = PhotoDet + "/UNIT_III/"; }
                if (SessionLcode.ToUpper() == "UNIT IV") { UNIT_Folder = PhotoDet + "/UNIT_IV/"; } //UNIT_Folder = "Emp_Scan/UNIT_IV/"; }



                byte[] imageByteData_Photo = new byte[0];
                byte[] imageByteData_ID_1 = new byte[0];
                byte[] imageByteData_ID_2 = new byte[0];
                byte[] imageByteData_ID_3 = new byte[0];
                byte[] imageByteData_ID_4 = new byte[0];
                byte[] imageByteData_ID_5 = new byte[0];
                byte[] imageByteData_ID_6 = new byte[0];
                byte[] imageByteData_ID_7 = new byte[0];
                byte[] imageByteData_ID_8 = new byte[0];
                byte[] imageByteData_ID_9 = new byte[0];

                string mid = mDataSet.Rows[iRow]["MachineID"].ToString();
                // string path = "D:/Photo/" + mid + ".jpg";

                //Employee_Phto

                //string path_1 = UNIT_Folder + "/Photos/" + mid + ".jpg";

                if (SessionLcode.ToUpper() == "UNIT I") { UNIT_Folder = PhotoDet + "/UNIT_I/"; }
                string path_1 = PhotoDet + "\\" + mid + ".jpg";
                byte[] imageBytes = new byte[0];

                string rootPath = "";
                path_1 = Path.GetFullPath(Path.Combine(rootPath, path_1));
                string Photo_Path = "";
                Photo_Path = path_1;
                if (!System.IO.File.Exists(Photo_Path))
                {
                    //imageByteData_Photo = System.IO.File.ReadAllBytes(path);
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_Photo = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 1
                //path_1 = "~/" + UNIT_Folder + "/ID_Proof/A_Copy/A_" + mid + ".jpg";
                path_1 = UNIT_Folder + "ID_Proof/A_Copy/A_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                //Photo_Path = HttpContext.Current.Request.MapPath(path_1);
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_1 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 2
                //path_1 = "~/" + UNIT_Folder + "/ID_Proof/P_Copy/P_" + mid + ".jpg";

                path_1 = UNIT_Folder + "/ID_Proof/P_Copy/P_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_2 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 3
                path_1 = UNIT_Folder + "/ID_Proof/V_Copy/V_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_3 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 4
                path_1 = UNIT_Folder + "/ID_Proof/R_Copy/R_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_4 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 5
                path_1 = UNIT_Folder + "/ID_Proof/DL_Copy/DL_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_5 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 6
                path_1 = UNIT_Folder + "/ID_Proof/SC_Copy/SC_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_6 = System.IO.File.ReadAllBytes(Photo_Path);


                //ID Proof 7
                path_1 = UNIT_Folder + "/ID_Proof/B_PB_Copy/B_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_7 = System.IO.File.ReadAllBytes(Photo_Path);


                //ID Proof 8
                path_1 = UNIT_Folder + "/ID_Proof/pass_Copy/Pass_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_8 = System.IO.File.ReadAllBytes(Photo_Path);

                //ID Proof 9
                path_1 = UNIT_Folder + "/ID_Proof/Others_Copy/O_" + mid + ".jpg";
                Photo_Path = "";
                Photo_Path = path_1;
                Photo_Path = Path.GetFullPath(Path.Combine(rootPath, path_1));
                if (!System.IO.File.Exists(Photo_Path))
                {
                    Photo_Path = HttpContext.Current.Request.MapPath("~/assets/images/No_Image.jpg");
                }
                imageByteData_ID_9 = System.IO.File.ReadAllBytes(Photo_Path);


                string datee = mDataSet.Rows[iRow]["DOJ"].ToString();
                string[] date1 = datee.Split(' ');
                string familyDetails = "";

                //Family Details by Selva
                SSQL = "";
                SSQL = "Select DepandantName as FN,RelationShip as FR, DOB as FDob,age as Fage, Profession  from Mst_Family where MachineID='" + mid + "'";
                SSQL = SSQL + " and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
                DataTable dt_Family = new DataTable();
                dt_Family = objdata.RptEmployeeMultipleDetails(SSQL);
                Familydt = dt_Family.Copy();
                //if (dt_Family.Rows.Count > 0)
                //{
                //    if (dt_Family.Rows.Count > 1)
                //    {
                //        for (int ij = 0; ij < dt_Family.Rows.Count; ij++)
                //        {
                //            familyDetails = familyDetails + " | " + dt_Family.Rows[ij]["details"].ToString();
                //        }
                //    }
                //    else
                //    {
                //        familyDetails = dt_Family.Rows[0]["details"].ToString();
                //    }
                //}
                //familyDetails = "<p>" + familyDetails + "</p>";

                string birthh = mDataSet.Rows[iRow]["BirthDate"].ToString();
                string[] birthdatee = birthh.Split(' ');
                SSQL = "Select * from Company_Mst";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();



                AutoDataTable.Rows[iRow]["CompCode"] = dt.Rows[0]["CompName"]; ;
                AutoDataTable.Rows[iRow]["LocCode"] = SessionLcode.ToString();
                AutoDataTable.Rows[iRow]["EmpPrefix"] = mDataSet.Rows[iRow]["EmpPrefix"].ToString();
                AutoDataTable.Rows[iRow]["EmpNo"] = mDataSet.Rows[iRow]["EmpNo"];
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"];
                AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"];
                AutoDataTable.Rows[iRow]["ExistingCode"] = mDataSet.Rows[iRow]["ExistingCode"];
                AutoDataTable.Rows[iRow]["MachineID"] = mDataSet.Rows[iRow]["MachineID"];
                AutoDataTable.Rows[iRow]["FirstName"] = mDataSet.Rows[iRow]["FirstName"];
                AutoDataTable.Rows[iRow]["LastName"] = mDataSet.Rows[iRow]["LastName"];
                AutoDataTable.Rows[iRow]["MiddleInitial"] = mDataSet.Rows[iRow]["MiddleInitial"];
                AutoDataTable.Rows[iRow]["ShiftType"] = mDataSet.Rows[iRow]["ShiftType"];
                AutoDataTable.Rows[iRow]["FamilyDt"] = familyDetails;


                AutoDataTable.Rows[iRow]["Gender"] = mDataSet.Rows[iRow]["Gender"].ToString();
                AutoDataTable.Rows[iRow]["BirthDate"] = birthdatee[0];
                AutoDataTable.Rows[iRow]["Age"] = mDataSet.Rows[iRow]["Age"];
                AutoDataTable.Rows[iRow]["DOJ"] = date1[0];
                AutoDataTable.Rows[iRow]["MaritalStatus"] = mDataSet.Rows[iRow]["MaritalStatus"];
                AutoDataTable.Rows[iRow]["CatName"] = mDataSet.Rows[iRow]["CatName"];
                AutoDataTable.Rows[iRow]["SubCatName"] = mDataSet.Rows[iRow]["SubCatName"];
                AutoDataTable.Rows[iRow]["TypeName"] = mDataSet.Rows[iRow]["TypeName"];
                AutoDataTable.Rows[iRow]["EmpStatus"] = mDataSet.Rows[iRow]["EmpStatus"];
                AutoDataTable.Rows[iRow]["IsActive"] = mDataSet.Rows[iRow]["IsActive"];

                AutoDataTable.Rows[iRow]["Height"] = mDataSet.Rows[iRow]["Height"];
                AutoDataTable.Rows[iRow]["Weight"] = mDataSet.Rows[iRow]["Weight"];
                AutoDataTable.Rows[iRow]["Nationality"] = mDataSet.Rows[iRow]["Nationality"];
                AutoDataTable.Rows[iRow]["BloodGroup"] = mDataSet.Rows[iRow]["BloodGroup"];

                string Dept = "";
                Dept = mDataSet.Rows[iRow]["DeptName"].ToString();
                if (Dept == "-Select-")
                {
                    Dept = "";
                }

                //  AutoDataTable.Rows[iRow]["DeptName"] = mDataSet.Rows[iRow]["DeptName"].ToString();
                AutoDataTable.Rows[iRow]["DeptName"] = Dept;
                string Desig = "";
                Desig = mDataSet.Rows[iRow]["Designation"].ToString();
                if (Desig == "-Select-")
                {
                    Desig = "";
                }
                AutoDataTable.Rows[iRow]["Designation"] = Desig;
                //  AutoDataTable.Rows[iRow]["Designation"] = mDataSet.Rows[iRow]["Designation"];

                AutoDataTable.Rows[iRow]["PayPeriod_Desc"] = mDataSet.Rows[iRow]["PayPeriod_Desc"];
                AutoDataTable.Rows[iRow]["BaseSalary"] = mDataSet.Rows[iRow]["BaseSalary"];
                AutoDataTable.Rows[iRow]["PFNo"] = mDataSet.Rows[iRow]["PFNo"];
                AutoDataTable.Rows[iRow]["Nominee"] = mDataSet.Rows[iRow]["Nominee"];
                AutoDataTable.Rows[iRow]["ESINo"] = mDataSet.Rows[iRow]["ESINo"];
                AutoDataTable.Rows[iRow]["StdWrkHrs"] = mDataSet.Rows[iRow]["StdWrkHrs"];
                AutoDataTable.Rows[iRow]["OTEligible"] = mDataSet.Rows[iRow]["OTEligible"];
                AutoDataTable.Rows[iRow]["Nationality"] = mDataSet.Rows[iRow]["Nationality"];

                AutoDataTable.Rows[iRow]["Qualification"] = mDataSet.Rows[iRow]["Qualification"].ToString();
                AutoDataTable.Rows[iRow]["Certificate"] = mDataSet.Rows[iRow]["Certificate"];
                AutoDataTable.Rows[iRow]["FamilyDetails"] = mDataSet.Rows[iRow]["FamilyDetails"];
                AutoDataTable.Rows[iRow]["Address1"] = mDataSet.Rows[iRow]["Address1"];
                AutoDataTable.Rows[iRow]["Address2"] = mDataSet.Rows[iRow]["Address2"];
                AutoDataTable.Rows[iRow]["RecuritmentThro"] = mDataSet.Rows[iRow]["RecuritmentThro"];
                AutoDataTable.Rows[iRow]["IDMark1"] = mDataSet.Rows[iRow]["IDMark1"];
                AutoDataTable.Rows[iRow]["IDMark2"] = mDataSet.Rows[iRow]["IDMark2"];
                AutoDataTable.Rows[iRow]["BloodGroup"] = mDataSet.Rows[iRow]["BloodGroup"];
                AutoDataTable.Rows[iRow]["Handicapped"] = mDataSet.Rows[iRow]["Handicapped"];

                AutoDataTable.Rows[iRow]["Height"] = mDataSet.Rows[iRow]["Height"].ToString();
                AutoDataTable.Rows[iRow]["Weight"] = mDataSet.Rows[iRow]["Weight"];
                AutoDataTable.Rows[iRow]["BankName"] = mDataSet.Rows[iRow]["BankName"];
                AutoDataTable.Rows[iRow]["BranchCode"] = mDataSet.Rows[iRow]["BranchCode"];
                AutoDataTable.Rows[iRow]["AccountNo"] = mDataSet.Rows[iRow]["AccountNo"];



                string busno = "";
                busno = mDataSet.Rows[iRow]["BusNo"].ToString();
                if (busno == "-Select-")
                {
                    busno = "";
                }
                AutoDataTable.Rows[iRow]["BusNo"] = busno;
                // AutoDataTable.Rows[iRow]["BusNo"] = mDataSet.Rows[iRow]["BusNo"].ToString();

                AutoDataTable.Rows[iRow]["Refrence"] = mDataSet.Rows[iRow]["Refrence"];
                AutoDataTable.Rows[iRow]["Permanent_Dist"] = mDataSet.Rows[iRow]["Permanent_Dist"];
                AutoDataTable.Rows[iRow]["Present_Dist"] = mDataSet.Rows[iRow]["Present_Dist"];
                AutoDataTable.Rows[iRow]["WeekOff"] = mDataSet.Rows[iRow]["WeekOff"];

                AutoDataTable.Rows[iRow]["parentsMobile"] = mDataSet.Rows[iRow]["parentsMobile"].ToString();
                AutoDataTable.Rows[iRow]["EmployeeMobile"] = mDataSet.Rows[iRow]["EmployeeMobile"];
                AutoDataTable.Rows[iRow]["Wages"] = mDataSet.Rows[iRow]["Wages"];
                AutoDataTable.Rows[iRow]["Adhar_No"] = mDataSet.Rows[iRow]["Adhar_No"];
                AutoDataTable.Rows[iRow]["Stay"] = mDataSet.Rows[iRow]["stay"];
                AutoDataTable.Rows[iRow]["EyeSight"] = mDataSet.Rows[iRow]["EyeSight"];
                AutoDataTable.Rows[iRow]["extracurricular"] = mDataSet.Rows[iRow]["extracurricular"];

                //byte[] imgbyte = new byte[fs.Length + 1];
                //imgbyte = br.ReadBytes(Convert.ToInt32((fs.Length)));

                AutoDataTable.Rows[iRow]["img"] = imageByteData_Photo;
                AutoDataTable.Rows[iRow]["Add_Proof"] = imageByteData_ID_1;
                AutoDataTable.Rows[iRow]["ID_Proof"] = imageByteData_ID_2;
                AutoDataTable.Rows[iRow]["ID_Proof_3"] = imageByteData_ID_3;
                AutoDataTable.Rows[iRow]["ID_Proof_4"] = imageByteData_ID_4;
                AutoDataTable.Rows[iRow]["ID_Proof_5"] = imageByteData_ID_5;
                AutoDataTable.Rows[iRow]["ID_Proof_6"] = imageByteData_ID_6;

                AutoDataTable.Rows[iRow]["ID_Proof_7"] = imageByteData_ID_7;
                AutoDataTable.Rows[iRow]["ID_Proof_8"] = imageByteData_ID_8;
                AutoDataTable.Rows[iRow]["ID_Proof_9"] = imageByteData_ID_9;




                //add Extra Employee Details For Raajco(16/03/2020)

                DataTable DT_EDU = new DataTable();
                string below = "";
                string Qualification = "";
                SSQL = "";
                SSQL = "Select * from Mst_EmpEducation";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[iRow]["MachineID"] + "'order by Qualification Asc";
                DT_EDU = objdata.RptEmployeeMultipleDetails(SSQL);


                if (DT_EDU.Rows.Count != 0)
                {
                    AutoDataTable.Rows[iRow]["Qualification1"] = DT_EDU.Rows[0]["Qualification"];
                    Qualification = DT_EDU.Rows[0]["Qualification"].ToString();
                    if (Qualification == "Below SSLC")
                    {
                        AutoDataTable.Rows[iRow]["Qualification1"] = DT_EDU.Rows[0]["Standard1"];
                    }

                    AutoDataTable.Rows[iRow]["School/clg1"] = DT_EDU.Rows[0]["School_clg"];
                    AutoDataTable.Rows[iRow]["YearofPassing1"] = DT_EDU.Rows[0]["Yearofpassing"];
                    AutoDataTable.Rows[iRow]["Marks1"] = DT_EDU.Rows[0]["Marks"];
                    if (DT_EDU.Rows.Count > 1)
                    {
                        AutoDataTable.Rows[iRow]["Qualification2"] = DT_EDU.Rows[1]["Qualification"]; ;
                        AutoDataTable.Rows[iRow]["School/clg2"] = DT_EDU.Rows[1]["School_clg"];
                        AutoDataTable.Rows[iRow]["YearofPassing2"] = DT_EDU.Rows[1]["Yearofpassing"];
                        AutoDataTable.Rows[iRow]["Marks2"] = DT_EDU.Rows[1]["Marks"];
                    }
                    if (DT_EDU.Rows.Count > 2)
                    {
                        AutoDataTable.Rows[iRow]["Qualification3"] = DT_EDU.Rows[2]["Qualification"]; ; ;
                        AutoDataTable.Rows[iRow]["School/clg3"] = DT_EDU.Rows[2]["School_clg"];
                        AutoDataTable.Rows[iRow]["Marks3"] = DT_EDU.Rows[2]["Marks"];
                        AutoDataTable.Rows[iRow]["YearofPassing3"] = DT_EDU.Rows[2]["Yearofpassing"];
                    }

                    // AutoDataTable.Rows[iRow]["Course"] = DT_EDU.Rows[2]["Course"];
                }


                DataTable DT_EXP = new DataTable();
                SSQL = "";
                SSQL = "Select * from Mst_Experience";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And MachineID='" + mDataSet.Rows[iRow]["MachineID"] + "'Order by Dol Asc";
                DT_EXP = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT_EXP.Rows.Count != 0)
                {
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //DataSet Dx = new DataSet();
                    //ExDataTable.Columns.Add("Companyname");
                    //ExDataTable.Columns.Add("Doj");
                    //ExDataTable.Columns.Add("Dol");
                    //ExDataTable.Columns.Add("DrawnWages");
                    //ExDataTable.Columns.Add("Reason");
                    //ExDataTable.NewRow();
                    //ExDataTable.Rows.Add();

                    //ExDataTable.Rows[i]["Companyname"] = DT_EXP.Rows[0]["Companyname"];

                    //ExDataTable.Rows[i]["Doj"] = DT_EXP.Rows[0]["Doj"];

                    //ExDataTable.Rows[i]["Dol"] = DT_EXP.Rows[0]["Dol"];

                    //ExDataTable.Rows[i]["DrawnWages"] = DT_EXP.Rows[0]["DrawnWages"];

                    //ExDataTable.Rows[i]["Reason"] = DT_EXP.Rows[0]["Reason"];


                    //Dx.Tables.Add(ExDataTable);

                    //// DataSet ds = new DataSet();
                    ////  DataTable t1 = ds.Tables.Add("t1");  // Create
                    //DataTable t1Again = ds.Tables["t1"];
                    ////ds.Tables["t1Again",""].Add(ExDataTable);
                    //    ds.Tables.Add(t1Again);

                    ////ReportDocument report = new ReportDocument();
                    ////report.Load(Server.MapPath("crystal/single_emp_profile1.rpt"));
                    ////report.Database.Tables[0].SetDataSource(Dx.Tables[0]);


                    string company1 = "";
                    string company2 = "";
                    //string company3 = "";
                    string Doj1 = "";
                    string Doj2 = "";
                    //string Doj3 = "";
                    string Dol1 = "";
                    string Dol2 = "";
                    //string Dol3 = "";
                    string DrawnWages1 = "";
                    string DrawnWages2 = "";
                    string reason1 = "";
                    string reason2 = "";

                    for (int i = 0; i < DT_EXP.Rows.Count; i++)
                    {
                        company1 = DT_EXP.Rows[0]["Companyname"].ToString();
                        Doj1 = DT_EXP.Rows[0]["Doj"].ToString();
                        Dol1 = DT_EXP.Rows[0]["Dol"].ToString();
                        DrawnWages1 = DT_EXP.Rows[0]["DrawnWages"].ToString();
                        reason1 = DT_EXP.Rows[0]["reason"].ToString();

                        AutoDataTable.Rows[iRow]["company1"] = company1;

                        AutoDataTable.Rows[iRow]["Doj1"] = Doj1;

                        AutoDataTable.Rows[iRow]["Dol1"] = Dol1;

                        AutoDataTable.Rows[iRow]["DrawnWages1"] = DrawnWages1;

                        AutoDataTable.Rows[iRow]["Reason1"] = reason1;

                        if (i == 1)
                        {
                            company2 = DT_EXP.Rows[1]["Companyname"].ToString();
                            Doj2 = DT_EXP.Rows[1]["Doj"].ToString();
                            Dol2 = DT_EXP.Rows[1]["Dol"].ToString();
                            DrawnWages2 = DT_EXP.Rows[1]["DrawnWages"].ToString();
                            reason2 = DT_EXP.Rows[1]["Reason"].ToString();
                            AutoDataTable.Rows[iRow]["company2"] = company2;
                            AutoDataTable.Rows[iRow]["Doj2"] = Doj2;
                            AutoDataTable.Rows[iRow]["Dol2"] = Dol2;
                            AutoDataTable.Rows[iRow]["DrawnWages2"] = DrawnWages2;
                            AutoDataTable.Rows[iRow]["Reason2"] = reason2;
                        }

                    }
                }
                DataTable DT_Lang = new DataTable();
                SSQL = "Select LanguageKnown from Mst_Language where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'and MachineID='" + mDataSet.Rows[iRow]["MachineID"] + "'";
                DT_Lang = objdata.RptEmployeeMultipleDetails(SSQL);


                if (DT_Lang.Rows.Count != 0)
                {

                    string language = "";
                    string readTamil = "";
                    string readOthers = "";
                    string readEng = "";
                    string WriteTamil = "";
                    string writeOthers = "";
                    string writeEng = "";
                    string speakTamil = "";
                    string speakOthers = "";
                    string speakEng = "";
                    for (int i = 0; i < DT_Lang.Rows.Count; i++)
                    {
                        language = DT_Lang.Rows[i]["LanguageKnown"].ToString();


                        DataTable DT_LangCheck = new DataTable();

                        SSQL = "Select *from Mst_Language where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'and MachineID='" + mDataSet.Rows[iRow]["MachineID"] + "'and LanguageKnown ='" + language + "'";
                        DT_LangCheck = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DT_LangCheck.Rows.Count != 0)
                        {
                            if (language == "Tamil")
                            {
                                readTamil = DT_LangCheck.Rows[0]["Read1"].ToString();
                                if (readTamil != "")
                                {
                                    readTamil = "YES";

                                }
                                WriteTamil = DT_LangCheck.Rows[0]["Write"].ToString();
                                if (WriteTamil != "")
                                {
                                    WriteTamil = "YES";

                                }
                            }
                            speakTamil = DT_LangCheck.Rows[0]["Speak"].ToString();
                            {
                                if (speakTamil != "")
                                {
                                    speakTamil = "YES";

                                }
                            }
                            if (language == "English")
                            {
                                readEng = DT_LangCheck.Rows[0]["Read1"].ToString();
                                if (readEng != "")
                                {
                                    readEng = "YES";

                                }
                                writeEng = DT_LangCheck.Rows[0]["Write"].ToString();
                                if (writeEng != "")
                                {
                                    writeEng = "YES";

                                }
                                speakEng = DT_LangCheck.Rows[0]["Speak"].ToString();
                                if (speakEng != "")
                                {
                                    speakEng = "YES";

                                }
                            }
                            if (language == "Others")
                            {
                                readOthers = DT_LangCheck.Rows[0]["Read1"].ToString();
                                if (readOthers != "")
                                {
                                    readOthers = "YES";  
                                }
                                writeOthers = DT_LangCheck.Rows[0]["Write"].ToString();
                                if (writeOthers != "")
                                {
                                    writeOthers = "YES";  
                                }
                                speakOthers = DT_LangCheck.Rows[0]["Speak"].ToString();
                                if (speakOthers != "")
                                {
                                    speakOthers = "YES";    
                                }     
                            }
                        }

                        AutoDataTable.Rows[iRow]["readTamil"] = readTamil;
                        AutoDataTable.Rows[iRow]["WriteTamil"] = WriteTamil;
                        AutoDataTable.Rows[iRow]["speakTamil"] = speakTamil;
                        AutoDataTable.Rows[iRow]["readEng"] = readEng;
                        AutoDataTable.Rows[iRow]["writeEng"] = writeEng;
                        AutoDataTable.Rows[iRow]["speakEng"] = speakEng;
                        AutoDataTable.Rows[iRow]["readOthers"] = readOthers;
                        AutoDataTable.Rows[iRow]["writeOthers"] = writeOthers;
                        AutoDataTable.Rows[iRow]["speakOthers"] = speakOthers;
                    }             
                }   
            }     
        }
    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
