﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;

public partial class RptPayPFESI : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;

    ReportDocument report = new ReportDocument();

    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();
    int Fin_Year_DB = 0;
    string rbt = "";
    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Months_load();
            Load_Division();
            Load_WagesType();
        }
    }

    private void Months_load()
    {

        //Financial Year Add
        int currentYear = Utility.GetFinancialYear;
        txtFinancial_Year.Items.Add("-Select-");
        txtFrom_Month.Items.Add("-Select-");
        txtPFMonth.Items.Add("-Select-");
        for (int i = 0; i <= 11; i++)
        {
            txtFinancial_Year.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
        txtFinancial_Year.SelectedIndex = 1;
        //Single Year Add
        txtYear.Items.Add("-Select-");
        currentYear = Utility.GetCurrentYearOnly;
        for (int i = 0; i < 10; i++)
        {
            txtYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
            //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
            currentYear = currentYear - 1;
        }
        txtYear.SelectedIndex = 1;


        //Month Add
        System.Globalization.DateTimeFormatInfo mfi = new
        System.Globalization.DateTimeFormatInfo();
        string strMonthName = "";
        for (int i = 3; i <= 11; i++)
        {
            //strMonthName = mfi.GetAbbreviatedMonthName(i + 1).ToString();
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
            txtPFMonth.Items.Add(strMonthName);
        }
        for (int i = 0; i <= 2; i++)
        {
            strMonthName = mfi.GetMonthName(i + 1).ToString();
            txtFrom_Month.Items.Add(strMonthName);
            txtPFMonth.Items.Add(strMonthName);
        }

    }

    private void Load_Division()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtDivision.Items.Clear();
        txtDivision_PF.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtDivision.DataSource = dtdsupp;
        txtDivision_PF.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "-Select-";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);


       
        txtDivision.DataTextField = "Division";
        txtDivision.DataValueField = "Division";
        txtDivision.DataBind();

        txtDivision_PF.DataTextField = "Division";
        txtDivision_PF.DataValueField = "Division";
        txtDivision_PF.DataBind();
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        query = "Select *from [Raajco_Epay]..MstEmployeeType";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            query = query + " where EmpCategory='1'";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            query = query + " where EmpCategory='2'";
        }
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpTypeCd"] = "0";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
    }

    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
    }

    protected void btnexport_Click(object sender, EventArgs e)
    {
        //All Header Details Get
        string CmpName = "";
        string Cmpaddress = "";
        string From_Month_DB = "";
        string Department_ID = "";
        string NetPay_Count = "";
        bool ErrFlag = false;
        string query = "";
        DataTable NetPay_DT = new DataTable();

        if (txtFrom_Month.SelectedValue == "" || txtFrom_Month.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month Properly');", true);
            ErrFlag = true;
        }

        int YR = 0;
        if (!ErrFlag)
        {
            if (txtFrom_Month.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else if (txtFrom_Month.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else if (txtFrom_Month.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(txtFinancial_Year.SelectedValue);
            }

            if (txtFrom_Month.SelectedValue != "0") { From_Month_DB = txtFrom_Month.SelectedItem.Text.ToString(); }

            
            string[] Fin_Year_Split = txtFinancial_Year.SelectedItem.Text.Split('-');
            Fin_Year_DB = Convert.ToInt32(Fin_Year_Split[0].ToString());
            rbt = rbsalary.SelectedItem.Text;
            //     getPFDetails();
            if (txtFrom_Month.SelectedValue != "0") { From_Month_DB = txtFrom_Month.SelectedItem.Text.ToString(); }
            {
                ResponseHelper.Redirect("RptPF_Esi_Online.aspx?month=" + txtFrom_Month.SelectedValue + "&year=" + Fin_Year_DB + "&rbt=" + rbt, "_blank", "");
            }

        }
    }


    private void getPFDetails()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Emp.Code");
        DataCell.Columns.Add("UAN");
        DataCell.Columns.Add("Member Name");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("GrossWages");
        DataCell.Columns.Add("EPF Wages");
        DataCell.Columns.Add("EPS Wages");
        DataCell.Columns.Add("EDLI Wages");
        DataCell.Columns.Add("EPF Contribution remitted");
        DataCell.Columns.Add("EPS Contribution remitted");
        DataCell.Columns.Add("EPF and EPS Diff remitted");
        DataCell.Columns.Add("NCP Days");
        DataCell.Columns.Add("Refund of Advances");


        SSQL = "";
        SSQL = "Select Em.ExistingCode,em.UAN,em.FirstName,em.DOJ,sd.PfSalary,sd.Emp_PF,sd.EmployeerPFone,LOPDays from";
        SSQL = SSQL + " Employee_Mst em inner join[Raajco_Epay]..SalaryDetails sd on em.MachineID = sd.MachineNo where Month = '" + txtFrom_Month.SelectedItem.Text + "' and FinancialYear='" + Fin_Year_DB + "' and em.Eligible_PF = '1'";
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                string Epf = "0";
                string Eps = "0";
                string diff = "0";
                Epf = AutoDTable.Rows[i]["Emp_PF"].ToString();
                Eps = AutoDTable.Rows[i]["EmployeerPFone"].ToString();

                diff = (Convert.ToDecimal(Epf) - Convert.ToDecimal(Eps)).ToString();

                DataCell.NewRow();
                DataCell.Rows.Add();
                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Emp.Code"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["UAN"] = AutoDTable.Rows[i]["UAN"].ToString();
                DataCell.Rows[i]["Member Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["DOJ"] = AutoDTable.Rows[i]["DOJ"].ToString();
                DataCell.Rows[i]["GrossWages"] = AutoDTable.Rows[i]["PfSalary"].ToString();
                DataCell.Rows[i]["EPF Wages"] = AutoDTable.Rows[i]["PfSalary"].ToString();
                DataCell.Rows[i]["EPS Wages"] = AutoDTable.Rows[i]["PfSalary"].ToString();
                DataCell.Rows[i]["EDLI Wages"] = AutoDTable.Rows[i]["PfSalary"].ToString();

                DataCell.Rows[i]["EPF Contribution remitted"] = AutoDTable.Rows[i]["Emp_PF"].ToString();
                DataCell.Rows[i]["EPS Contribution remitted"] = AutoDTable.Rows[i]["EmployeerPFone"].ToString();
                DataCell.Rows[i]["EPF and EPS Diff remitted"] = diff;
                DataCell.Rows[i]["NCP Days"] = AutoDTable.Rows[i]["LOPDays"].ToString();
                DataCell.Rows[i]["Refund of Advances"] = "0";

                sno += 1;
            }
        }
        grid.DataSource = DataCell;
        grid.DataBind();
        string attachment = "attachment;filename=PF_ESI Online.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan=" + 12 + ">");

        Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
        Response.Write("<tr Font-Bold='true' align='Center'>");
        Response.Write("<td colspan='12'>");
        //Response.Write("<a style=\"font-weight:bold\">DAILY ATTENDANCE -" + ShiftType1 +"-" + Session["Lcode"].ToString() + "-" + Date + "</a>");
        Response.Write("<a style=\"font-weight:bold\">PF Online  Statement(ECR)- '" + txtFrom_Month.SelectedItem.Text + "-"  +Fin_Year_DB +"</a>");

        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();

    }


    protected void btnPFForm3A_Click(object sender, EventArgs e)
    {

      

    }

    protected void btnPFAddLeft_Click(object sender, EventArgs e)
    {
       
       
    }

    protected void btnESIAddLeft_Click(object sender, EventArgs e)
    {
        
    }

    protected void btnEmpAddLeft_Click(object sender, EventArgs e)
    {
       
    }
   }
