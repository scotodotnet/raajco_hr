﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="LeaveApprove.aspx.cs" Inherits="LeaveApprove" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">



<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual</a></li>
				<li class="active">Leave Approval</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Leave Approval </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Leave Approval</h4>
                        </div>
                        <div class="panel-body">
                       
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>EmpNo</th>
                                                <th>ExistingCode</th>
                                                <th>EmpName</th>
                                                <th>FromDate</th>
                                                <th>ToDate</th>
                                                <th>Total Days</th>
                                                <th>Leave Type</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                    <td><%# Eval("EmpNo")%></td>
                                    <td><%# Eval("ExistingCode")%></td>
                                    <td><%# Eval("EmpName")%></td>
                                    <td><%# Eval("FromDate")%></td>
                                    <td><%# Eval("ToDate")%></td>
                                    <td><%# Eval("TotalDays")%></td>
                                    <td><%# Eval("LeaveType")%></td>
                                    <td>
                                     <asp:LinkButton ID="btnApprvEnquiry_Grid" class="btn btn-success btn-sm fa fa-check"  runat="server" 
                                           Text="" OnCommand="GridApproveEnquiryClick" CommandArgument='<%#Eval("FromDate")+","+ Eval("ToDate")%>' CommandName='<%# Eval("EmpNo")%>'
                                           CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Approve this Leave details?');">
                                     </asp:LinkButton>
                                     <asp:LinkButton ID="btnCancelEnquiry_Grid" class="btn btn-danger btn-sm fa fa-ban"  runat="server" 
                                        Text="" OnCommand="GridCancelEnquiryClick" CommandArgument='<%#Eval("FromDate")+","+ Eval("ToDate")%>' CommandName='<%# Eval("EmpNo")%>' 
                                       CausesValidation="true" OnClientClick="return confirm('Are you sure you want to Cancel this Leave details?');">
                                     </asp:LinkButton>
                                    
                                    </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>




</asp:Content>

