﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class MismatchReport : System.Web.UI.Page
{

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    BALDataAccess objdata = new BALDataAccess();

    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;
    DateTime date1;
    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    DataTable mLocalDS = new DataTable();
    DataTable mLocalDT = new DataTable();
    DataTable dtIPaddress = new DataTable();
    DataTable AutoDTable = new DataTable();
    DataTable Logindays = new DataTable();
    
    DataTable dt = new DataTable();
    string Time_Out_Str = "";
    string SSQL = "";
    int itab = 0;
    string SessionUserType;
    string Division = ""; string status = "";

    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    public string Right_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(Value.Length - Length, Length);

        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-MisMatch Report";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            //SessionUserType = Session["UserType"].ToString();

            DataSet ds = new DataSet();

            DataColumn auto = new DataColumn();
            auto.AutoIncrement = true;
            auto.AutoIncrementSeed = 1;



          



             AutoDTable.Columns.Add("CompanyName");
            AutoDTable.Columns.Add("LocationName");
             AutoDTable.Columns.Add("ShiftDate");
            AutoDTable.Columns.Add("SNo");
            AutoDTable.Columns.Add("Dept");
            AutoDTable.Columns.Add("Type");
            AutoDTable.Columns.Add("Shift");
               AutoDTable.Columns.Add("Category");
             AutoDTable.Columns.Add("SubCategory");
            AutoDTable.Columns.Add("EmpCode");
            AutoDTable.Columns.Add("ExCode");
            AutoDTable.Columns.Add("Name");
            AutoDTable.Columns.Add("TimeIN");
            AutoDTable.Columns.Add("TimeOUT");
            AutoDTable.Columns.Add("MachineID");
           AutoDTable.Columns.Add("PrepBy");
             AutoDTable.Columns.Add("PrepDate");
            AutoDTable.Columns.Add("TotalMIN");
            AutoDTable.Columns.Add("GrandTOT");
           
           
          
            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
             Division = Request.QueryString["Division"].ToString();
             status = Request.QueryString["status"].ToString();

            GetShiftMismacth();

            ds.Tables.Add(AutoDTable);
            ReportDocument report = new ReportDocument();
            //report.Load(Server.MapPath("crystal/Attendance.rpt"));
            report.Load(Server.MapPath("crystal/Attendance_MisMatch.rpt"));

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
    }

    public void GetShiftMismacth()
    {
        SSQL = "select LD.MachineID,LD.ExistingCode,LD.DeptName,LD.Shift,isnull(LD.FirstName,'') as FirstName,LD.TimeIN,LD.TimeOUT,";
        SSQL = SSQL + "LD.Total_Hrs1 as Total_Hrs,LD.TypeName from LogTime_Days LD";
        SSQL = SSQL + " inner join ";
        if (status == "Approval")
        {
            SSQL = SSQL + "Employee_Mst";
        }
        else
        {
            SSQL = SSQL + "Employee_Mst_New_Emp";
        }
        SSQL = SSQL + " EM on EM.MachineID = LD.MachineID";
        SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
        SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";

        if (Date != "")
        {
            SSQL = SSQL + " And CONVERT(DATETIME,LD.Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
        }
        if (Division != "-Select-")
        {
            SSQL = SSQL + " and LD.Division='" + Division + "' ";

        }
        SSQL = SSQL + " And LD.Shift='No Shift' And LD.TimeIN <> ''";
        SSQL = SSQL + " And (LD.TypeName='GENERAL Mismatch' or LD.TypeName='Mismatch' or LD.TypeName='Improper')";

        Logindays = objdata.RptEmployeeMultipleDetails(SSQL);

         SSQL = "Select * from Company_Mst";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);


                DataTable Emp_dt = new DataTable();
                string category;
                string subCat;
                if (Logindays.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < Logindays.Rows.Count; i++)
            {

                SSQL = "Select * from ";
                if (status == "Approval")
                {
                    SSQL = SSQL + "Employee_Mst";
                }
                else
                {
                    SSQL = SSQL + "Employee_Mst_New_Emp";
                }
                SSQL = SSQL + " where MachineID='" + Logindays.Rows[i]["MachineID"].ToString() + "' ";
                Emp_dt = objdata.RptEmployeeMultipleDetails(SSQL);

                category = "";
                subCat = "";
                if (Emp_dt.Rows.Count != 0)
                {
                    category = Emp_dt.Rows[0]["CatName"].ToString();
                    subCat = Emp_dt.Rows[0]["SubCatName"].ToString();
                }

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();




                AutoDTable.Rows[i]["CompanyName"] = dt.Rows[0]["CompName"].ToString();
                AutoDTable.Rows[i]["LocationName"] = SessionLcode;
                AutoDTable.Rows[i]["ShiftDate"] = Date;
                AutoDTable.Rows[i]["SNo"] = sno;

                AutoDTable.Rows[i]["Dept"] = Logindays.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[i]["Type"] = Logindays.Rows[i]["TypeName"].ToString();
                AutoDTable.Rows[i]["Shift"] = Logindays.Rows[i]["Shift"].ToString();
                AutoDTable.Rows[i]["Category"] = category.ToString();
                AutoDTable.Rows[i]["SubCategory"] = subCat.ToString();
                AutoDTable.Rows[i]["EmpCode"] = Logindays.Rows[i]["MachineID"].ToString();


                AutoDTable.Rows[i]["ExCode"] = Logindays.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[i]["Name"] = Logindays.Rows[i]["FirstName"].ToString();
                AutoDTable.Rows[i]["TimeIN"] = Logindays.Rows[i]["TimeIN"].ToString();
                AutoDTable.Rows[i]["TimeOUT"] = Logindays.Rows[i]["TimeOUT"].ToString();
                AutoDTable.Rows[i]["MachineID"] = Logindays.Rows[i]["MachineID"].ToString();
                AutoDTable.Rows[i]["PrepBy"] = "User";
                AutoDTable.Rows[i]["PrepDate"] = Date;
                AutoDTable.Rows[i]["TotalMIN"] = "";
                AutoDTable.Rows[i]["GrandTOT"] = Logindays.Rows[i]["Total_Hrs"].ToString();


                sno = sno + 1;
            }
        }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
