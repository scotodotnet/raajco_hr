﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="ELCL_Process.aspx.cs" Inherits="ELCL_Process" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
 <!-- begin #content -->
		<div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">EL/CL</a></li>
            <li class="active">EL&CL Process</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">EL&CL Process</h1>
        <!-- end page-header -->

     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                  <ContentTemplate>
        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">EL&CL Process</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <asp:DropDownList runat="server" ID="ddlCategory" autofocus="true" class="form-control select2" AutoPostBack="true"
                                            Style="width: 100%;" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="1">Staff</asp:ListItem>
                                            <asp:ListItem Value="2">Labour</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Employee Type</label>
                                        <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" Style="width: 100%;" TabIndex="1">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>EL CL Year</label>
                                        <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" Style="width: 100%;" TabIndex="2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                            </div>
                            <!-- end row -->
                            <!-- begin row -->
                            <div class="row">
                                <div class="col-md-4"></div>
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnELCLDown" Text="Download"
                                            class="btn btn-success" OnClick="btnELCLDown_Click" TabIndex="3" />
                                        <asp:Button runat="server" Visible="false" ID="btnCancel" Text="Cancel"
                                            class="btn btn-danger" OnClick="btnCancel_Click" />
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <div class="col-md-4"></div>
                            </div>
                            <fieldset><h4>EL & CL Wages Calculation</h4></fieldset>
                            <div class="row">

                            </div>
                            <!-- begin row -->
                            <div class="row">
                                  <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								<br />
								 <asp:FileUpload ID="FileUpload" CssClass="btn btn-default fileinput-button" runat="server" TabIndex="4" />
								</div>
								</div>
								 <!-- end col-4 -->
                                 <!-- begin col-4 -->
                              <div class="col-md-2">
                                  <div class="form-group">
                                      <br />
                                       <asp:Button runat="server" ID="btnUpload" Text="Upload"
                                            class="btn btn-success" OnClick="btnUpload_Click" TabIndex="5" />
                                      </div>
                              </div>
                                </div>

                            <div id="Download_loader" style="display: none" />
                        </div>
                        <!-- end row -->
                                <div class="form-group">
                          <div class="row">
                              <asp:GridView ID="gvEmp" runat="server" AutoGenerateColumns="false" >
                                                                    <Columns>
                                                                         <asp:TemplateField>
                                                                            <HeaderTemplate>S.no</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Sno" runat="server" Text='<%# Container.DataItemIndex+1 %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                       <%-- <asp:TemplateField>
                                                                            <HeaderTemplate>DeptName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="DeptName" runat="server" Text='<%# Eval("DepartmentNm") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <%--<asp:TemplateField>
                                                                            <HeaderTemplate>MachineID</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="MachineID" runat="server" Text='<%# Eval("MachineNo") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EmpNo</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblEmpNo" runat="server" Text='<%# Eval("MachineID") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                      <%--  <asp:TemplateField>
                                                                            <HeaderTemplate>ExistingCode</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="ExistingCode" runat="server" Text='<%# Eval("ExisistingCode") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>FirstName</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="FirstName" runat="server" Text='<%# Eval("FirstName") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Total Working Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="Days" runat="server" Text='<%# Eval("WorkedDays") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>EL Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="CL" runat="server" Text='<%# Eval("ELDays") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>CL Days</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblHomeTown" runat="server" Text='<%# Eval("CLDays") %>' ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                          <asp:TemplateField>
                                                                            <HeaderTemplate>OneDay Rate</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAdvance" runat="server" Text='<%# Eval("OT_Salary") %>' ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 1</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers1" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 2</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDedOthers2" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        
                                                                        
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Allowance 3</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblAdvance1" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 1</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblAdvance2" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 2</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDed3" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 3</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblDed4" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 4</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDed5" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <%--<asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 6</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDed6" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        
                                                                       <%-- <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 7</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                            <asp:Label ID="lblDed7" runat="server" Text="0.00" ></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <HeaderTemplate>Deduction 8</HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDed8" runat="server" Text="0.00" ></asp:Label>
                                                                             </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                        
                                                                   
                                                                        
                                                                        
                                                                      
                                                                    </Columns>
                                                                </asp:GridView>
                          </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end panel -->
        </div>
          </ContentTemplate>
                  <Triggers>
                     <asp:PostBackTrigger ControlID="btnELCLDown" />
                     <asp:PostBackTrigger ControlID="btnUpload" />
                  </Triggers>
                </asp:UpdatePanel>
        <!-- end col-12 -->
    </div>
            <!-- end row -->
    

    </div>
    

</asp:Content>
