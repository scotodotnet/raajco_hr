﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;

using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptESI_Form_5 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month = "";
    string str_yr = "";
    string Wages = "";
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string FinYearVal = "";
    string Month;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate = "";
    string ToDate = "";
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;

    string PFTypeGet;


    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    string SSQL = "";
    string RptYear = "";
    string Category = "";
    string FromMonth = "";
    string ToMonth = "";
    string FromYear = "";
    string ToYear = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();


        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionAdmin = Session["Isadmin"].ToString();
        //string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        //str_cate = Request.QueryString["Cate"].ToString();
        FinYearVal = Request.QueryString["yr"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();

        //str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        //Report_Types = Request.QueryString["ReportFormat"].ToString();
        //AgentName = Request.QueryString["AgentName"].ToString();

        RptYear = Request.QueryString["RptYear"].ToString();
        Category = Request.QueryString["CatName"].ToString();

        FromMonth = Request.QueryString["FromMonth"].ToString();
        ToMonth = Request.QueryString["ToMonth"].ToString();
        FromYear = Request.QueryString["FromYear"].ToString();
        ToYear = Request.QueryString["ToYear"].ToString();

        //salaryType = Request.QueryString["Salary"].ToString();
        //EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        EmployeeType = Request.QueryString["Wages"].ToString();
        Wages = EmployeeType;

        Load_Report();
    }

    private void Load_Report()
    {
        DateTime From, To;

        From = Convert.ToDateTime(fromdate.ToString());
        To = Convert.ToDateTime(ToDate.ToString());

        DataTable dt1 = new DataTable();

        SSQL = "";
        SSQL = "Select EmpDet.ESINo,SalDet.ExisistingCode as ExistingCode,EmpDet.FirstName,'DINDIGUL' as Dispensary ,SUM(ISNULL(SalDet.WorkedDays,'0.00')) as WorkedDays";
        SSQL = SSQL + ",'YES' as WagesCeiling, SUM(ISNULL(SalDet.GrossEarnings,'0')) as GrossEarnings,SUM(NULLIF(SalDet.ESI,'0')) as EmpContribution";
        SSQL = SSQL + ",CAST(SUM(NULLIF(CAST(NULLIF(SalDet.GrossEarnings,'0') as decimal(18,2)),'0'))/SUM(NULLIF(CAST(NULLIF(SalDet.WorkedDays,'0')as decimal(18,2)),'0'))as decimal(18,2)) as dailywages";
        SSQL = SSQL + " from Employee_Mst EmpDet inner join [" + SessionPayroll + "]..SalaryDetails SalDet on SalDet.EmpNo=EmpDet.EmpNo where ";
        SSQL = SSQL + " SalDet.FromDate>=Convert(datetime,'" + From.ToString("dd/MM/yyyy") + "',103) and SalDet.ToDate<=Convert(datetime,'" + To.ToString("dd/MM/yyyy") + "',103)";
        if (EmployeeType != "-Select-")
        {
            SSQL = SSQL + " and SalDet.Wages='" + EmployeeType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }
        SSQL = SSQL + " and SalDet.ESIEligible='1' and SalDet.WorkedDays > 0 group by SalDet.ExisistingCode,EmpDet.ESINo,EmpDet.FirstName order by SalDet.ExisistingCode";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count > 0)
        {


            string attachment = "attachment;filename=ESI_Form5.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);

            Response.Write("<table>");
            if (EmployeeType != "CONTRACTCLEANING I" || EmployeeType != "CONTRACTCLEANING II")
            {
                Response.Write("<tr>");
                Response.Write("<td align='Left' colspan='4'>");
                Response.Write("IN QUADRUPLICATE EMPLOYEES STATE INSURANCE CORPORATION");
                Response.Write("</td>");
                Response.Write("<td  align='Center' colspan='3'>");
                Response.Write("<b>FORM - 5</b>");
                Response.Write("</td>");
                Response.Write("<td align='Right' colspan='4'>");
                Response.Write("EMPLOYER'S CODE NO. 57-29669-11");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='4' style='font-weight: bold;' align='Left'>");
                Response.Write("CATEGORY : " + Wages.ToString().ToUpper() + "");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='4' align='Left'>");
                Response.Write("PERIOD : " + FromMonth + " " + FromYear + " TO " + ToMonth + " " + ToYear + "");
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            Response.Write("<td colspan='11'>");
            Response.Write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td>");
            Response.Write("SL.No");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("INSURANCE NUMBER");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("CODE NO");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("NAME OF THE INSURED PERSON");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("NO OF DAYS FOR WHICH WAGES PAID");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("TOTAL AMOUNT OF WAGES PAID");
            Response.Write("</td>");
            Response.Write("<td aligng='center'>");
            Response.Write("EMPLOYEE'S CONTRIBUTION");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("DAILY WAGES");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("WHETHER STILL CONTINUES WORKING AND DRAWING DISPENSARY WAGES WITHIN THE INSURABLE WAGES CEILING");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("DISPENSARY");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("REMARKS");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='11'>");
            Response.Write("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("<table>");

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                Response.Write("<tr style='font-weight: bold;' align='center'>");
                Response.Write("<td>" + Convert.ToInt32(i + 1).ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["ESINo"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["ExistingCode"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["FirstName"].ToString() + "</td>");

                Response.Write("<td align='right'>" + dt1.Rows[i]["WorkedDays"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["GrossEarnings"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["EmpContribution"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["dailywages"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["WagesCeiling"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["Dispensary"].ToString() + "</td>");

                Response.Write("</tr>");
            }

            Response.Write("<tr>");
            Response.Write("<td colspan='5'>");
            Response.Write("</td>");
            Response.Write("<td colspan='2'>");
            Response.Write("-------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='4'>");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("TOTAL");
            Response.Write("</td>");

            if (dt1.Rows.Count > 0)
            {

                Response.Write("<td style='font-weight:bold;'>=Sum(F7:F" + Convert.ToDecimal(dt1.Rows.Count + 7) + ")");
                Response.Write("<td style='font-weight:bold;'>=Sum(G7:G" + Convert.ToDecimal(dt1.Rows.Count + 7) + ")");

            }

            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='5'>");
            Response.Write("</td>");
            Response.Write("<td colspan='2'>");
            Response.Write("-------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            Response.End();
            Response.Clear();
        }
    }
}