﻿<%@ Page Language="C#"MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="OT_Incentive_Mst1.aspx.cs" Inherits="OT_Incentive_Mst1" Title=" OT Incentive master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel23" runat="server">
   <ContentTemplate>
     <!-- begin #content -->
<div id="content" class="content">
    
	<h1 class="page-header">OT Incentive Master</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">OT Incentive Master</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <h5></h5>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                           <%--  <div class="col-md-3">
								<div class="form-group">
								    <label>Shift</label>
								    <asp:DropDownList runat="server" ID="ddlshift" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" OnSelectedIndexChanged="ddlshift_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>--%>

                              <div class="col-md-2">
								<div class="form-group">
								    <label>Start IN</label>
								 	  <asp:TextBox runat="server" ID="txt_Start_IN" class="form-control" Text=""></asp:TextBox>
                                       
							 	  
								</div>
                            </div>
                      

                
                           <%--  <div class="col-md-2">
								<div class="form-group">
								  <label>OT Hours From</label>
								  <asp:TextBox runat="server" ID="txtOTHrs_Frm" class="form-control" Text="0"></asp:TextBox>
								  <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>--%>
							<%--	</div>
                            </div>--%>
                         <%--   <div class="col-md-2">
								<div class="form-group">
								  <label> OT Hours To</label>
								  <asp:TextBox runat="server" ID="txtOTHrs_To" class="form-control" Text="0"></asp:TextBox>
								  <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>--%>
							<%--	</div>
                            </div>--%>


                          <div class="col-md-2">
								<div class="form-group">
								    <label>End IN</label>
								   <asp:TextBox runat="server" ID="txtEnd_In" class="form-control" Text=""></asp:TextBox>
                                      
							 	    
								</div>
                            </div>

                              </div>

                                            <div class="row">
                          <div class="col-md-3">
								<div class="form-group">
								    <label>End IN Days</label>
								    <asp:DropDownList runat="server" ID="ddlDays"
                                        class="form-control select2" style="width:100%;">
                                           <asp:ListItem Text="-Select-" Value="-Select-"></asp:ListItem>
								       <asp:ListItem Text="0" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>


							 	    </asp:DropDownList>
								</div>
                            </div>


                             <div class="col-md-3">
								<div class="form-group">
								    <label>OT hrs</label>
								    <asp:DropDownList runat="server" ID="ddlOTHrs" 
                                        class="form-control select2" style="width:100%;" OnSelectedIndexChanged="ddlshift_SelectedIndexChanged">
                                          <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="4-7" Value="4-7"></asp:ListItem>
                                        <asp:ListItem Text="7:30 and Above" Value="7:30 and Above"></asp:ListItem>


							 	    </asp:DropDownList>
								</div>
                            </div>



                            <div class="col-md-2">
								<div class="form-group">
								  <label>Amount</label>
								  <asp:TextBox runat="server" ID="txtAmount" class="form-control" Text="0"></asp:TextBox>
								  <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>--%>
								</div>
                            </div>

                                <div class="col-md-2">
								<div class="form-group">
								  <label>WH Amount</label>
								  <asp:TextBox runat="server" ID="TxtWH_Amt" class="form-control" Text="0"></asp:TextBox>
								 
								</div>
                            </div>

                            </div>





                            		  <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                           
                        </div>

                        <div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                 <th>S.No</th>
                                                <th>Emp Type</th>
                                                <th>Start IN</th>
                                                <th>End IN</th>
                                                <th>OT hrs</th>
                                                <th>Amount</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                          <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("EmpType")%></td>
                                        <td><%# Eval("Start_IN")%></td>
                                        <td><%# Eval("End_IN")%></td>
                                        <td><%# Eval("OT_Hrs")%></td>
                                        <td><%# Eval("Amount")%></td>
                                        
                                        <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                    Text="" OnCommand="GridEditEnquiryClick" CommandArgument='<%#Eval("EmployeeType") + "|" +Eval("Start_IN")+"|" +"|"+Eval("OT_Hrs") +"|"%>'
                                           CausesValidation="true"  OnClientClick="return confirm('Are you sure you want to Edit this details? ');">
                                                          </asp:LinkButton>



                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%#Eval("EmployeeType") + "|" +Eval("Start_IN")+"|" +"|"+Eval("OT_Hrs") +"|"%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this  details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

