﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="BankAccNoUpdate_Approve.aspx.cs" Inherits="BankAccNoUpdate_Approve" Title="HR Module :: Bank Account Number Approve" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css"/>
 <script>
     $(document).ready(function() {
     $('#example').dataTable();
     $('.datepicker').datepicker({
         format: "dd/mm/yyyy",
         autoclose: true
     });
     });
 </script>

<style>
.well1{padding:15px;background:#eeeeee;box-shadow:none;-webkit-box-shadow:none}   
.LabelColor{color:#116dca} 
.BorderStyle{border:1px solid #5f656b;color: #020202;font-weight: bold;}
.select2 {border:1px solid #5f656b;color: #020202;font-weight: bold;}
</style>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
                $('#example').dataTable();
            }
        });
    };
</script>
<script type="text/javascript">
    function showimagepreview(input) {
        if (input.files && input.files[0]) {
            var filerdr = new FileReader();
            filerdr.onload = function(e) {
                $('#img1').attr('src', e.target.result);
            }
            filerdr.readAsDataURL(input.files[0]);
        }
    }
     </script>

 <script type="text/javascript">
     function showimagepreview2(input) {
         if (input.files && input.files[0]) {
             var filerdr = new FileReader();
             filerdr.onload = function(e) {
                 $('#img2').attr('src', e.target.result);
             }
             filerdr.readAsDataURL(input.files[0]);
         }
     }
     </script>
<script type="text/javascript">

    function loadm(url) {
        var img = new Image();
        var bcgDiv = document.getElementById("divBackground");
        var imgDiv = document.getElementById("divImage");
        var imgFull = document.getElementById("imgFull");
        var imgLoader = document.getElementById("imgLoader");
        imgLoader.style.display = "block";
        img.onload = function() {
            imgFull.src = img.src;
            imgFull.style.display = "block";
            imgLoader.style.display = "none";
        };
        img.src = url;
        var width = document.body.clientWidth;
        if (document.body.clientHeight > document.body.scrollHeight) {
            bcgDiv.style.height = document.body.clientHeight + "px";
        }
        else {
            bcgDiv.style.height = document.body.scrollHeight + "px";
        }
        imgDiv.style.left = (width - 650) / 2 + "px";
        imgDiv.style.top = "100px";
        bcgDiv.style.width = "100%";

        bcgDiv.style.display = "block";
        imgDiv.style.display = "block";
        return false;
    }
    function HideDiv() {
        var bcgDiv = document.getElementById("divBackground");
        var imgDiv = document.getElementById("divImage");
        var imgFull = document.getElementById("imgFull");
        if (bcgDiv != null) {
            bcgDiv.style.display = "none";
            imgDiv.style.display = "none";
            imgFull.style.display = "none";
        }
    }
  
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Employee Profile</a></li>
				<li class="active">Bank Account No. Approve</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Bank Account No. Approve</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Bank Account No. Approve</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        
                        
                        <!-- begin row -->
                          <div class="row">
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Wages Type</label>
								 <asp:Label runat="server" ID="ddlWages" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Token No</label>
								<asp:Label runat="server" ID="txtTokenNo" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                           <label>Machine ID</label>
                           <asp:Label runat="server" ID="txtMachineID" class="form-control" BackColor="#F3F3F3"></asp:Label>
                            </div>
                           </div>
                           <!-- end col-4 -->
                         </div>
                        <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Department</label>
								<asp:Label runat="server" ID="txtDeptName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 --> 
                          <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Designation</label>
								<asp:Label runat="server" ID="txtDesignation" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 --> 
                         </div>
                        <!-- end row -->
                         
                         <div class="row">
                          <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="LabelColor">Bank Name</label>
                                                    <asp:DropDownList ID="ddlBankName" runat="server" class="form-control BorderStyle select2" style="width:100%;" 
                                                        AutoPostBack="true" onselectedindexchanged="ddlBankName_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                     <asp:RequiredFieldValidator ControlToValidate="ddlBankName" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                     </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="LabelColor">Branch</label>
                                                    <asp:DropDownList ID="txtBranch" runat="server" 
                                                        class="form-control BorderStyle select2" style="width:100%;" 
                                                        onselectedindexchanged="txtBranch_SelectedIndexChanged" AutoPostBack="true">
                                                    </asp:DropDownList>
                                                     <asp:RequiredFieldValidator ControlToValidate="txtBranch" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                     </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="LabelColor">IFSC Code</label>
                                                    <asp:TextBox ID="txtIFSC" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                    
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                            <!-- begin col-3 -->
                                            
                                            <!-- end col-3 -->
                                             <!-- begin col-3 -->
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="LabelColor">Account Number</label><span class="mandatory">*</span>
                                                   <asp:TextBox ID="txtAccNo" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ControlToValidate="txtAccNo" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                     </asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                            <!-- end col-3 -->
                                        </div>
                                        <div class="row"></div>
                                        <!-- begin row -->  
                        <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Approve" class="btn btn-success"  
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row --> 
                                        
                         <div class="row">
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>Token ID</th>
                                                        <th>Name</th>
                                                        <th>DeptName</th>
                                                        <th>Designation</th>
                                                        <th>Bank</th>
                                                        <th>Passbook</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("ExistingCode")%></td>
                                                <td><%# Eval("FirstName")%></td>
                                                <td><%# Eval("DeptName")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("BankName")%></td>
                                                <td>
                                                    <asp:ImageButton ID="Image1" ImageUrl='<%# Eval("imgurl") %>' runat="server" Height="50px"
                                                        Width="50px" Style="cursor: pointer" OnClientClick="Javascript:return loadm(this.src);"/>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("MachineID")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                        </div>
                      <div id="divBackground" class="modal">
                                    </div>
                                    <div id="divImage">
                                    <table style="height: 100%; width: 100%">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <img id="imgLoader" alt="" src="images/loader.gif" />
                                                <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="bottom">
                                                <input id="btnClose" type="button" value="close" onclick="HideDiv()" />
                                            </td>
                                        </tr>
                                    </table>
                                    </div>
                       
                        
                        </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

