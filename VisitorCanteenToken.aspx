﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="VisitorCanteenToken.aspx.cs" Inherits="VisitorCanteenToken" Title="" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js" type="text"></script>
<script src="assets/js/master_list_jquery-ui.min.js" type="text"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Manual Entry</a></li>
				<li class="active">Canteen Token</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Canteen Token </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Canteen Token</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Visitor Type</label>
								  <asp:DropDownList runat="server" ID="ddlVisitorType" class="form-control select2" style="width:100%;">
                                      <asp:ListItem value="-Select-">-Select-</asp:ListItem>
                                       <asp:ListItem value="Guest">Guest</asp:ListItem>
                                      <asp:ListItem value="Driver">Driver</asp:ListItem>
								  </asp:DropDownList>
								
								</div>
                               </div>
                              <!-- end col-4 -->
                               <div class="col-md-4">
								<div class="form-group">
								  <label>Name</label>
								  <asp:TextBox runat="server" ID="txtName" class="form-control"></asp:TextBox>
								</div>
                               </div>
                               <!-- begin col-4 -->
                                <div class="col-md-4">
								<div class="form-group">
								  <label>Company Name</label>
								  <asp:TextBox runat="server" ID="txtCompanyName" class="form-control"></asp:TextBox>
								</div>
                               </div>
                               <div class="col-md-3">
								<div class="form-group">
								  <label>To Meet</label>
								  <asp:TextBox runat="server" ID="txtToMeet" class="form-control"></asp:TextBox>

								</div>
                               </div>

                              <div class="col-md-3">
								<div class="form-group">
								  <label>Token Date</label>
								  <asp:TextBox runat="server" ID="txtTokenDate" class="form-control datepicker"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtTokenDate" Display="Dynamic"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                             
                              <!-- end col-4 -->
                              
                            
                           
                          <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Food Type</label>
								  <asp:DropDownList runat="server" ID="ddlFoodType" class="form-control select2" 
                                        style="width:100%;">
								  <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								  <asp:ListItem Value="Breakfast">Breakfast</asp:ListItem>
                                  <asp:ListItem Value="Lunch">Lunch</asp:ListItem>
                                  <asp:ListItem Value="Dinner">Dinner</asp:ListItem>
								  </asp:DropDownList>
								 
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                      </div>
                       
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="ValidateDept_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr> <th>S.No</th>
                                                <th>Visitor Type</th>
                                                <th>Name</th>
                                                <th>Company Name</th>
                                                <th>To Meet</th>
                                                <th>Food Type</th>
                                                <th>Date</th>
                                               <%-- <th>Mode</th>--%>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("VisitorType")%></td>
                                        <td><%# Eval("Name")%></td>
                                        <td><%# Eval("CompanyName")%></td>
                                        <td><%# Eval("ToMeet")%></td>
                                        <td><%# Eval("FoodType")%></td>
                                        <td><%# Eval("TokenDate")%></td>
                                        <%--<td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("ID")%>'></asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("ID")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this NFH details?');">
                                                    </asp:LinkButton>
                                                </td>--%>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

