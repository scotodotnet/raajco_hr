﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="CLCalcMst.aspx.cs" Inherits="CLCalcMst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
 <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">CL Days</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">ELCL Days </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">ELCL Days</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                           <div class="col-md-4">
                           <div class="form-group">
                           <label>Token No</label>
                           <asp:DropDownList runat="server" ID="txtExistingCode" class="form-control select2" 
                                   style="width: 100%;" AutoPostBack="true" 
                                   onselectedindexchanged="txtExistingCode_SelectedIndexChanged">
                           </asp:DropDownList>
                           <asp:HiddenField ID="txtMachineID" runat="server" />
                           <asp:RequiredFieldValidator ControlToValidate="txtExistingCode" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                           </asp:RequiredFieldValidator>
                            </div>
                           </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Employee Name</label>
								<asp:Label runat="server" ID="txtEmpName" class="form-control" BackColor="#F3F3F3"></asp:Label>
								
								</div>
                             </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                            <div class="col-md-4">
							   <div class="form-group">
								<label>Department</label>
								<asp:Label runat="server" ID="txtDepartment" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                             </div>
                           <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Year</label>
								  <asp:DropDownList runat="server" ID="ddlYear" class="form-control select2" style="width:100%;">
								  </asp:DropDownList>
								  <asp:HiddenField ID="txtID" runat="server" />
								  <asp:RequiredFieldValidator ControlToValidate="ddlYear" Display="Dynamic" InitialValue="-Select-" ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>From Date</label>
								  <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>To Date</label>
								  <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->

                              <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>EL Days</label>
								  <asp:TextBox runat="server" ID="txtELDays" class="form-control"></asp:TextBox>
								  
								</div>
                               </div>
                              <!-- end col-4 -->

                              <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>CL Days</label>
								  <asp:TextBox runat="server" ID="txtCLdays" class="form-control"></asp:TextBox>
								  
								</div>
                               </div>
                              <!-- end col-4 -->

                              
                            
                              </div>
                        <!-- end row -->
                         
                       
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="ValidateDept_Field" onclick="btnSave_Click"/>
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                         <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>Emp Code</th>
                                                <th>Emp Name</th>
                                                <th>Form Date</th>
                                                <th>To Date</th>
                                                <th>EL Days</th>
                                                <th>EL Balance</th>
                                                <th>CL Days</th>
                                                <th>CL Balance</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("EmpNo")%></td>
                                        <td><%# Eval("EmpName")%></td>
                                        <td><%# Eval("FromDate")%></td>
                                        <td><%# Eval("ToDate")%></td>
                                        <td><%# Eval("ELAddDays")%></td>
                                        <td><%# Eval("ELMinDays")%></td>
                                        <td><%# Eval("CLAddDays")%></td>
                                        <td><%# Eval("CLMinDays")%></td>
                                        <%--<td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("ID")%>'></asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("ID")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this NFH details?');">
                                                    </asp:LinkButton>
                                                </td>--%>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

