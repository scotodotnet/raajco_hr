﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class EmployeeDeActive : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["UserId"] == null)
        //{
        //    Response.Redirect("Default.aspx");
        //    Response.Write("Your session expired");
        //}
        SessionCcode = "Raajco";//Session["Ccode"].ToString();
        SessionLcode = "UNIT I";//Session["Lcode"].ToString();
        Load_EmpDeActive();
        //SessionLcode = "UNIT II";
        //Load_EmpDeActive();
        //SessionLcode = "UNIT III";
        //Load_EmpDeActive();
        //SessionLcode = "UNIT IV";
        //Load_EmpDeActive();
        lblmsg.Text = "Employee De-Activate Completed Successfully";
    }

    public void Load_EmpDeActive()
    {
        string query = "";
        DataTable dt = new DataTable();
        DataTable DT_Log = new DataTable();
        DataTable DT_Leave = new DataTable();
        DataTable DT_Emp = new DataTable();

        query = "Select MD.EmpType,MD.Days,convert(varchar,GETDATE(),103) as CurrDate from MstDeActiveDays MD";
        query = query + " where MD.Ccode='" + SessionCcode + "' And MD.Lcode='" + SessionLcode + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                int days = 30;
                if (dt.Rows[i]["Days"].ToString() != "")
                {
                    days = Convert.ToInt32(dt.Rows[i]["Days"]);
                }
                DateTime Date1 = Convert.ToDateTime(dt.Rows[i]["CurrDate"].ToString()).AddDays(-days);
                DateTime Date2 = Convert.ToDateTime(dt.Rows[i]["CurrDate"].ToString());

                //check Present count in LogTime Days
                query = "Select ED.EmpNo,count(LD.Present) as Present from Employee_Mst ED ";
                query = query + "inner join LogTime_Days LD on ED.EmpNo=LD.MachineID";
                query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ED.IsActive='Yes'";
                query = query + " And ED.Wages='" + dt.Rows[i]["EmpType"].ToString() + "'";
                query = query + " And LD.CompCode='" + SessionCcode + "' And LD.LocCode='" + SessionLcode + "'";
                query = query + " And CONVERT(DATETIME,LD.Attn_Date_Str, 103)>=CONVERT(DATETIME,'" + Date1.ToString("dd/M/yyyy") + "',103) ";
                query = query + " And CONVERT(DATETIME,LD.Attn_Date_Str, 103)<=CONVERT(DATETIME,'" + Date2.ToString("dd/MM/yyyy") + "',103)";
                query = query + " Group by ED.EmpNo";
                query = query + " having SUM(LD.Present)='0.0'";
                DT_Log = objdata.RptEmployeeMultipleDetails(query);
                if (DT_Log.Rows.Count != 0)
                {
                    for (int j = 0; j < DT_Log.Rows.Count; j++)
                    {
                        //check Leave in Leave Resister
                        query = "Select * from Leave_History where CompCode='" + SessionCcode + "'";
                        query = query + " And LocCode='" + SessionLcode + "' And EmpNo='" + DT_Log.Rows[j]["EmpNo"].ToString() + "'";
                        query = query + " And CONVERT(DATETIME,FromDate, 103)>=CONVERT(DATETIME,'" + Date1.ToString("dd/M/yyyy") + "',103)";
                        query = query + " And CONVERT(DATETIME,FromDate, 103)<=CONVERT(DATETIME,'" + Date2.ToString("dd/M/yyyy") + "',103)";
                        DT_Leave = objdata.RptEmployeeMultipleDetails(query);

                        if (DT_Leave.Rows.Count == 0)
                        {
                            //check Leave in Employee Mst
                            query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "'";
                            query = query + " And LocCode='" + SessionLcode + "' And EmpNo='" + DT_Log.Rows[j]["EmpNo"].ToString() + "'";
                            query = query + " And CONVERT(DATETIME,LeaveFrom, 103)>=CONVERT(DATETIME,'" + Date1.ToString("dd/M/yyyy") + "',103)";
                            query = query + " And CONVERT(DATETIME,LeaveTo, 103)<=CONVERT(DATETIME,'" + Date2.ToString("dd/M/yyyy") + "',103)";
                            DT_Emp = objdata.RptEmployeeMultipleDetails(query);

                            if (DT_Emp.Rows.Count == 0)
                            {

                                if (Convert.ToDecimal(DT_Log.Rows[j]["Present"].ToString()) >= Convert.ToDecimal(days.ToString()))
                                {

                                    query = "Update Employee_Mst set IsActive='No',DOR='" + dt.Rows[i]["CurrDate"].ToString() + "'";
                                    query = query + " where CompCode='" + SessionCcode + "'";
                                    query = query + " And LocCode='" + SessionLcode + "' And EmpNo='" + DT_Log.Rows[j]["EmpNo"].ToString() + "'";
                                    objdata.RptEmployeeMultipleDetails(query);
                                }
                                //query = "Update Employee_Mst set IsActive='Yes',DOR=''";
                                //query = query + " where CompCode='" + SessionCcode + "'";
                                //query = query + " And LocCode='" + SessionLcode + "' And EmpNo='" + DT_Log.Rows[j]["EmpNo"].ToString() + "'";
                                //objdata.RptEmployeeMultipleDetails(query);
                            }
                        }
                    }
                }
            }
        }
    }
}
