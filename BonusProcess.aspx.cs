﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using Payroll.Configuration;
using Payroll.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

public partial class BonusProcess : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    static string CmpName;
    static string Cmpaddress;
    string SessionUserType;
    string SessionEpay;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string Query = "";


    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            ddlCategory_SelectedIndexChanged1(sender, e);
            ddlcategory_Rpt_SelectedIndexChanged(sender, e);
            int currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                txtBonusYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));

                currentYear = currentYear - 1;
            }
            Load_Division_Name();
        }
    }
    protected void ddlCategory_SelectedIndexChanged1(object sender, EventArgs e)
    {
        DataTable dtemp = new DataTable();
        Query = "Select 0 EmpTypeCd,'-Select-' EmpType union ";
        Query = Query + "Select EmpTypeCd,EmpType from mstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        if (SessionAdmin == "2")
        {
            Query = Query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }
        dtemp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtemp;
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
    }

    protected void ddlcategory_Rpt_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtemp = new DataTable();
        Query = "Select 0 EmpTypeCd,'-Select-' EmpType union ";
        Query = Query + "Select EmpTypeCd,EmpType from mstEmployeeType where EmpCategory='" + ddlcategory_Rpt.SelectedValue + "'";
        if (SessionAdmin == "2")
        {
            Query = Query + " And EmpType <> 'SUB-STAFF' And EmpType <> 'CIVIL' And EmpType <> 'OTHERS'";
        }
        dtemp = objdata.RptEmployeeMultipleDetails(Query);
        ddlRptEmpType.DataSource = dtemp;
        ddlRptEmpType.DataTextField = "EmpType";
        ddlRptEmpType.DataValueField = "EmpTypeCd";
        ddlRptEmpType.DataBind();
    }
    private void Load_Division_Name()
    {

        DataTable dt = new DataTable();

        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDivision.Items.Clear();
        query = "Select *from Division_Master where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDivision.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Division"] = "0";
        dr["Division"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDivision.DataTextField = "Division";
        ddlDivision.DataValueField = "Division";
        ddlDivision.DataBind();


        //string query = "";
        //DataTable DIV_DT = new DataTable();
        //query = "Select '0' as Row_Num,'-Select-' as Division union ";
        //query = query + "Select '1' as Row_Num,Division from [eAlert]..Division_Master";
        //query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        //query = query + " Order by Row_Num,Division Asc";
        //DIV_DT = objdata.RptEmployeeMultipleDetails(query);
        //ddlDivision.DataSource = DIV_DT;
        //ddlDivision.DataTextField = "Division";
        //ddlDivision.DataValueField = "Division";
        //ddlDivision.DataBind();
    }
    protected void btnBonusProcess_Click(object sender, EventArgs e)
    {
        try
        {
            lblBonus_Error.Visible = false;
            bool ErrFlag = false;
            bool SaveFlag = false;
            string bonus_Period_From = "";
            string bonus_Period_To = "";

            string Gross_Sal_Add = "0";
            string Percent_Gross_Sal = "0";
            string Percent_Bonus_Amount = "0";
            string Round_Bonus_Amt = "0";
            string Round_Near_10_Bonus = "0";
            string Get_Bonus_Percent = "0";
            string BasicSalary = "0.00";
            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category.');", true);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type Properly.');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Days Properly", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            //Bonus Process Start
            if (!ErrFlag)
            {
                DataTable dt = new DataTable();
                bonus_Period_From = "01-10-" + (Convert.ToDecimal(ddlFinance.SelectedValue.ToString()) - Convert.ToDecimal(1)).ToString();
                bonus_Period_To = "01-09-" + ddlFinance.SelectedValue.ToString();
                DateTime Bonus_From_Date = DateTime.ParseExact(bonus_Period_From, "dd-MM-yyyy", null);
                DateTime Bonus_To_Date = DateTime.ParseExact(bonus_Period_To, "dd-MM-yyyy", null);

                string query2 = "";
                //Get Bonus Process Employee Details
                if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && ddlFinance.SelectedValue.ToString() == "2015")
                {
                    query2 = "Select ED.EmpNo,ED.MachineID as BiometricID,ED.ExistingCode as ExisistingCode,ED.FirstName as EmpName,";
                    query2 = query2 + " ED.FamilyDetails as  FatherName,ED.DeptName as DepartmentNm,ED.DeptCode as Department,";
                    query2 = query2 + " ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining,";
                    //Experiance get
                    query2 = query2 + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, Convert(Datetime,ED.DOR,103))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, Convert(Datetime,ED.DOR,103)) % 12))";
                    query2 = query2 + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                    query2 = query2 + " ) as Experiance,";

                    //query2 = query2 + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";


                    query2 = query2 + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,SD.WDays,SD.NFh,SD.OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                    query2 = query2 + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                    query2 = query2 + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,ED.BaseSalary as Base,";

                    query2 = query2 + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                    query2 = query2 + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                    query2 = query2 + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                    query2 = query2 + " from Employee_Mst ED";
                    query2 = query2 + " inner join [" + SessionEpay + "]..SalaryDetails_Bonus SD on ED.ExistingCode=SD.ExisistingCode And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                    query2 = query2 + " inner join MstEmployeeType MT on MT.EmpType=ED.Wages";
                    query2 = query2 + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And MT.EmpTypeCd='" + ddlEmployeeType.SelectedValue + "'";
                    query2 = query2 + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                    query2 = query2 + " And SD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And SD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";

                    query2 = query2 + " group by ED.EmpNo,ED.MachineID,ED.ExistingCode,ED.FirstName,ED.FamilyDetails,";
                    query2 = query2 + " ED.DeptName,ED.DeptCode,ED.Designation,ED.DOJ,ED.IsActive,ED.BaseSalary,ED.DOR";
                    query2 = query2 + " Order by ED.ExistingCode Asc";
                }
                else
                {
                    query2 = "Select ED.EmpNo,ED.MachineID as  BiometricID,ED.ExistingCode ExisistingCode,ED.FirstName as EmpName,ED.FamilyDetails as  FatherName,ED.DeptName as DepartmentNm,ED.DeptCode as Department,";
                    query2 = query2 + " ED.Designation,convert(varchar,ED.DOJ,105) as Dateofjoining,";

                    //Experiance get
                    query2 = query2 + " (case when ED.IsActive = 'No' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, Convert(Datetime,ED.DOR,103))/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, Convert(Datetime,ED.DOR,103)) % 12))";
                    query2 = query2 + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2019-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2019-09-30 00:00:00.000') % 12)) end";
                    query2 = query2 + " ) as Experiance,";

                    //query2 = query2 + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";
                    if (ddlEmployeeType.SelectedValue == "1" || ddlEmployeeType.SelectedValue == "6")
                    {
                        query2 = query2 + " Sum(AD.Days) as W_Days,SUM(cast(AD.WH_Work_Days as decimal(18,2))) as OT_Days,SUM(AD.NFh) as NFH_Days,";
                    }
                    else
                    {
                        query2 = query2 + " Sum(AD.Days) as W_Days,SUM(AD.ThreeSided) as OT_Days,SUM(AD.NFh) as NFH_Days,";
                    }

                    query2 = query2 + " SUM(";
                    query2 = query2 + " cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + ";

                    query2 = query2 + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                    query2 = query2 + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                    query2 = query2 + " cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances2";
                    query2 = query2 + " ) as Gross_Sal, sum(cast(SD.BasicAndDANew as decimal(18,2))) as BasicDA,ED.IsActive as ActivateMode,ED.BaseSalary as Base";
                    query2 = query2 + " from Employee_Mst ED";
                    query2 = query2 + " inner join [" + SessionEpay + "]..SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                    query2 = query2 + " inner join [" + SessionEpay + "]..AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT And SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And SD.FromDate=AD.FromDate";
                    query2 = query2 + " inner join MstEmployeeType MT on MT.EmpType=ED.Wages";
                    query2 = query2 + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And MT.EmpTypeCd='" + ddlEmployeeType.SelectedValue + "'";
                    query2 = query2 + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                    query2 = query2 + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                    query2 = query2 + " And SD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And SD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";
                    query2 = query2 + " And AD.FromDate >=convert(datetime,'" + Bonus_From_Date + "', 105) And AD.FromDate <=convert(datetime,'" + Bonus_To_Date + "', 105)";

                    query2 = query2 + " group by ED.EmpNo,ED.MachineID,ED.ExistingCode,ED.FirstName,ED.FamilyDetails,";
                    query2 = query2 + " ED.DeptName,ED.DeptCode,ED.Designation,ED.DOJ,ED.IsActive,ED.BaseSalary,ED.DOR";
                    query2 = query2 + " Order by ED.ExistingCode Asc";
                }
                dt = objdata.RptEmployeeMultipleDetails(query2);

                if (dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DataTable Bonus_Fixed_DT = new DataTable();
                        DataTable Last_Year_Bonus_DT = new DataTable();
                        DataTable Insert_DT = new DataTable();
                        string BasicDA = dt.Rows[i]["Base"].ToString();
                        BasicSalary = dt.Rows[i]["BasicDA"].ToString();
                        string Hostel_Stage = "0.0";
                        BasicSalary = (Math.Round(Convert.ToDecimal(BasicSalary), 2, MidpointRounding.ToEven)).ToString();

                        //STAFF And Regular Bonus Process Start
                        if (ddlEmployeeType.SelectedValue.ToString() == "1" || ddlEmployeeType.SelectedValue.ToString() == "2" || ddlEmployeeType.SelectedValue.ToString() == "3" || ddlEmployeeType.SelectedValue.ToString() == "6" || ddlEmployeeType.SelectedValue.ToString() == "7" || ddlEmployeeType.SelectedValue.ToString() == "4")
                        {

                            string[] Experiance_Check = dt.Rows[i]["Experiance"].ToString().Split('*');
                            if (Experiance_Check.Length != 1)
                            {
                                dt.Rows[i]["Experiance"] = "0.2";
                            }

                            bool exgratia_check = false;
                            string Eight_Percent_Bonus_Val = "0.00";
                            string Eight_Percent_Bonus_Val_1 = "0.00";
                            string signlist_bonus_Amt = "0.00";
                            string signlist_exgratia_Amt = "0.00";
                            string Min_Amount_Check = "0.00";
                            if (dt.Rows[i]["ExisistingCode"].ToString() == "SS4104")
                            {
                                Min_Amount_Check = "0.00";
                            }

                            // query2 = "Select * from ["+ SessionEpay + "]..MstBonusMaster where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmployeeType='" + ddlEmployeeType.SelectedItem.Text + "'";
                            string SSQL = "";
                            // string Bonus_Per = "0.00";
                            SSQL = "Select * from [" + SessionEpay + "]..MstBasicDet where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                            SSQL = SSQL + " and Category='" + ddlCategory.SelectedItem.Value + "' and EmployeeType='" + ddlEmployeeType.SelectedItem.Value + "'";
                            DataTable dt_Basic = new DataTable();
                            dt_Basic = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt_Basic.Rows.Count > 0)
                            {
                                Get_Bonus_Percent = dt_Basic.Rows[0]["BonusPer"].ToString();
                            }
                            else
                            {
                                Get_Bonus_Percent = "0.00";
                            }
                            //Bonus_Fixed_DT = objdata.RptEmployeeMultipleDetails(query2);
                            //if (Bonus_Fixed_DT.Rows.Count != 0)
                            //{
                            string LY_Bonus_Year_Get = (Convert.ToDecimal(ddlFinance.SelectedValue) - Convert.ToDecimal(1)).ToString();
                            string LY_GS = "0";
                            string LY_Percent_GS = "0";
                            string LY_Bonus_Percent = "0";
                            string LY_Final_Bonus = "0";
                            string LY_Total_Days = "0";
                            //Min_Amount_Check = Bonus_Fixed_DT.Rows[0]["Min_Amount"].ToString();

                            //if (ddlFinance.SelectedValue.ToString() == "2015")
                            //{
                            //    query2 = "Select * from ["+ SessionEpay + "]..Bonus_Details_Last_Year where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"].ToString() + "'";
                            //}
                            //else
                            //{
                            //    query2 = "Select Gross_Sal as Gross_Amount,Percent_Gross_Sal as Percent_Gross_Amount,";
                            //    query2 = query2 + " Bonus_Percent as Bonus_Perncet,Final_Bonus_Amt as Final_Bonus_Amt,";
                            //    query2 = query2 + " (Worked_Days + OT_Days + NFH_Days + isnull(Voucher_Days,0)) as Total_Days";
                            //    query2 = query2 + " from ["+ SessionEpay + "]..Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"].ToString() + "'";
                            //    query2 = query2 + " And Bonus_Year='" + LY_Bonus_Year_Get + "'";
                            //    //query2 = query2 + " And Bonus_Year='2015'";
                            //}
                            //Last_Year_Bonus_DT = objdata.RptEmployeeMultipleDetails(query2);
                            //if (Last_Year_Bonus_DT.Rows.Count != 0)
                            //{
                            //    LY_GS = Last_Year_Bonus_DT.Rows[0]["Gross_Amount"].ToString();
                            //    LY_Percent_GS = Last_Year_Bonus_DT.Rows[0]["Percent_Gross_Amount"].ToString();
                            //    LY_Bonus_Percent = Last_Year_Bonus_DT.Rows[0]["Bonus_Perncet"].ToString();
                            //    LY_Final_Bonus = Last_Year_Bonus_DT.Rows[0]["Final_Bonus_Amt"].ToString();
                            //    LY_Total_Days = Last_Year_Bonus_DT.Rows[0]["Total_Days"].ToString();
                            //}
                            //else
                            //{
                            //    LY_GS = "0.00";
                            //    LY_Percent_GS = "0.00";
                            //    LY_Bonus_Percent = "0.00";
                            //    LY_Final_Bonus = "0.00";
                            //    LY_Total_Days = "0.00";
                            //}

                            //Get Voucher Amt
                            string Voucher_Amt = "0.00";
                            string Voucher_Days = "0.00";
                            //DataTable Vo_DT = new DataTable();
                            //query2 = "Select * from ["+ SessionEpay + "]..SalaryDetails_Voucher where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"] + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                            //Vo_DT = objdata.RptEmployeeMultipleDetails(query2);
                            //if (Vo_DT.Rows.Count != 0)
                            //{
                            //    Voucher_Amt = Vo_DT.Rows[0]["Voucher_Amt"].ToString();
                            //    Voucher_Days = Vo_DT.Rows[0]["Total_Days"].ToString();
                            //}
                            //else
                            //{
                            //    Voucher_Amt = "0.00";
                            //    Voucher_Days = "0.00";
                            //}


                            //Gross Salary Percent Calculate
                            //Percent_Gross_Sal = (Convert.ToDecimal(dt.Rows[i]["Gross_Sal"].ToString()) + Convert.ToDecimal(Voucher_Amt)).ToString();
                            Percent_Gross_Sal = (Convert.ToDecimal(dt.Rows[i]["BasicDA"].ToString()) + Convert.ToDecimal(Voucher_Amt)).ToString();
                            // Percent_Gross_Sal = ((Convert.ToDecimal(Percent_Gross_Sal) * Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["Gross_Sal_Percent"].ToString())) / 100).ToString();
                            Percent_Gross_Sal = (Math.Round(Convert.ToDecimal(Percent_Gross_Sal), 2, MidpointRounding.ToEven)).ToString();

                            //Bonus Percent Calculate;
                            string Total_work_Days = "0";
                            Total_work_Days = (Convert.ToDecimal(dt.Rows[i]["W_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["NFH_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["OT_Days"].ToString())).ToString();
                            //Eight_Percent_Bonus_Val_1 = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                            //string[] exp_Spilit = dt.Rows[i]["Experiance"].ToString().Split('.');
                            //if (Convert.ToDecimal(exp_Spilit[0].ToString()) != 0)
                            //{
                            //    if (Convert.ToDecimal(dt.Rows[i]["Experiance"].ToString()) >= Convert.ToDecimal(1))
                            //    {
                            //        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveOneYear"].ToString();
                            //        exgratia_check = true;
                            //    }
                            //    else
                            //    {
                            //        if (Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString()) <= Convert.ToDecimal(Total_work_Days))
                            //        {
                            //            Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveDaysPercent"].ToString();
                            //            exgratia_check = true;
                            //        }
                            //        else
                            //        {
                            //            Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                            //        }
                            //    }
                            //}
                            //else
                            //{
                            //    if (Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString()) <= Convert.ToDecimal(Total_work_Days))
                            //    {
                            //        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveDaysPercent"].ToString();
                            //        exgratia_check = true;
                            //    }
                            //    else
                            //    {
                            //        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                            //    }
                            //}



                            Percent_Bonus_Amount = ((Convert.ToDecimal(Percent_Gross_Sal) * Convert.ToDecimal(Get_Bonus_Percent)) / 100).ToString();
                            Percent_Bonus_Amount = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 2, MidpointRounding.ToEven)).ToString();

                            //Final Bonus Amount
                            string[] Round_Bonus_Amt_Splt = Percent_Bonus_Amount.Split('.');
                            //Round_Bonus_Amt = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 0, MidpointRounding.AwayFromZero)).ToString();
                            Round_Bonus_Amt = Round_Bonus_Amt_Splt[0].ToString();
                            if (Convert.ToDecimal(Round_Bonus_Amt) < 0)
                            {
                                Round_Bonus_Amt = "0.00";
                            }
                            double d1 = Convert.ToDouble(Round_Bonus_Amt.ToString());
                            int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                            Round_Near_10_Bonus = rounded1.ToString();


                            if (Convert.ToDecimal(Min_Amount_Check) >= Convert.ToDecimal(Round_Near_10_Bonus))
                            {
                                Round_Near_10_Bonus = Min_Amount_Check;
                                signlist_bonus_Amt = Round_Near_10_Bonus;
                                signlist_exgratia_Amt = "0.00";
                            }
                            else
                            {
                                //Round_Near_10_Bonus = Round_Near_10_Bonus;
                                if (exgratia_check == true)
                                {
                                    Eight_Percent_Bonus_Val = ((Convert.ToDecimal(Percent_Gross_Sal) * Convert.ToDecimal(Eight_Percent_Bonus_Val_1)) / 100).ToString();
                                    Eight_Percent_Bonus_Val = (Math.Round(Convert.ToDecimal(Eight_Percent_Bonus_Val), 0, MidpointRounding.AwayFromZero)).ToString();
                                    signlist_bonus_Amt = Eight_Percent_Bonus_Val;
                                    signlist_exgratia_Amt = (Convert.ToDecimal(Round_Near_10_Bonus) - Convert.ToDecimal(signlist_bonus_Amt)).ToString();
                                }
                                else
                                {
                                    signlist_bonus_Amt = Round_Near_10_Bonus;
                                    signlist_exgratia_Amt = "0.00";
                                }
                            }

                            //Insert Bonus Table Start
                            query2 = "Select * from [" + SessionEpay + "]..Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                            Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                            if (Insert_DT.Rows.Count != 0)
                            {
                                query2 = "Delete from [" + SessionEpay + "]..Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                            }
                            Hostel_Stage = "0.0";
                            query2 = "Insert Into [" + SessionEpay + "]..Bonus_Details(Ccode,Lcode,EmpNo,MachineID,ExisistingCode,EmpName,FatherName,Department,";
                            query2 = query2 + " DepartmentName,Designation,DOJ,Experiance,Worked_Days,NFH_Days,OT_Days,Gross_Sal,Gross_Sal_Fixed_Per,Percent_Gross_Sal,";
                            query2 = query2 + " Bonus_Percent,Bonus_Amt,Round_Bonus,Final_Bonus_Amt,LY_Gross_Sal,LY_Percent_Gross_Sal,LY_Bonus_Percent,LY_Final_Bonus_Amt,";
                            query2 = query2 + " LY_Total_Days,Bonus_Year,ActivateMode,BaseSalary,Employee_Type,signlist_bonus_Amt,signlist_exgratia_Amt,Hostel_Stage,Voucher_Days,Voucher_Amt,BasicDA) Values('" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["EmpNo"] + "',";
                            query2 = query2 + " '" + dt.Rows[i]["BiometricID"] + "','" + dt.Rows[i]["ExisistingCode"] + "','" + dt.Rows[i]["EmpName"] + "','" + dt.Rows[i]["FatherName"] + "',";
                            query2 = query2 + " '" + dt.Rows[i]["Department"] + "','" + dt.Rows[i]["DepartmentNm"] + "','" + dt.Rows[i]["Designation"] + "','" + dt.Rows[i]["Dateofjoining"] + "','" + dt.Rows[i]["Experiance"] + "',";
                            query2 = query2 + " '" + dt.Rows[i]["W_Days"] + "','" + dt.Rows[i]["NFH_Days"] + "','" + dt.Rows[i]["OT_Days"] + "','" + dt.Rows[i]["Gross_Sal"] + "','0.00',";
                            query2 = query2 + " '" + Percent_Gross_Sal + "','" + Get_Bonus_Percent + "','" + Percent_Bonus_Amount + "','" + Round_Bonus_Amt + "','" + Round_Near_10_Bonus + "',";
                            query2 = query2 + " '" + LY_GS + "','" + LY_Percent_GS + "','" + LY_Bonus_Percent + "','" + LY_Final_Bonus + "','" + LY_Total_Days + "',";
                            query2 = query2 + " '" + ddlFinance.SelectedValue.ToString() + "','" + dt.Rows[i]["ActivateMode"] + "','" + dt.Rows[i]["Base"] + "',";
                            query2 = query2 + " '" + ddlEmployeeType.SelectedValue + "','" + signlist_bonus_Amt + "','" + signlist_exgratia_Amt + "','" + Hostel_Stage + "','" + Voucher_Days + "','" + Voucher_Amt + "','" + BasicDA + "')";
                            Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                            //Insert Bonus Table End
                        }

                        //}
                        //STAFF And Regular Bonus Process End


                        else if (ddlEmployeeType.SelectedValue.ToString() == "12")  // Hostel Bonus Process Start
                        {
                            string signlist_bonus_Amt = "0.00";
                            string signlist_exgratia_Amt = "0.00";
                            Hostel_Stage = "0.0";
                            string Min_Amount_Check = "0.00";
                            query2 = "Select * from [" + SessionEpay + "]..MstBonusMaster_Hostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                            Bonus_Fixed_DT = objdata.RptEmployeeMultipleDetails(query2);
                            if (Bonus_Fixed_DT.Rows.Count != 0)
                            {
                                Min_Amount_Check = Bonus_Fixed_DT.Rows[0]["Min_Amount"].ToString();

                                string LY_GS = "0";
                                string LY_Percent_GS = "0";
                                string LY_Bonus_Percent = "0";
                                string LY_Final_Bonus = "0";
                                string LY_Total_Days = "0";

                                string Total_work_Days = "0";
                                Total_work_Days = (Convert.ToDecimal(dt.Rows[i]["W_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["NFH_Days"].ToString()) + Convert.ToDecimal(dt.Rows[i]["OT_Days"].ToString())).ToString();

                                //Get Bonus Value;
                                query2 = "Select * from [" + SessionEpay + "]..MstBasicDetHostel where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TotalAmt='" + BasicSalary + "'";
                                DataTable Query_DT = new DataTable();
                                Query_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Query_DT.Rows.Count != 0)
                                {
                                    Hostel_Stage = Query_DT.Rows[0]["Stage"].ToString();
                                    if (Query_DT.Rows[0]["Basic_Year"].ToString() == "0")
                                    {
                                        if (Convert.ToDecimal(Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString()) <= Convert.ToDecimal(Total_work_Days))
                                        {
                                            Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveDaysPercent"].ToString();
                                        }
                                        else
                                        {
                                            Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowDaysPercent"].ToString();
                                        }
                                        //Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["BelowOneYear"].ToString();
                                    }
                                    else if (Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) >= Convert.ToDecimal(1) && Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) < Convert.ToDecimal(2))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveOneBelowTwo"].ToString();
                                    }
                                    else if (Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) >= Convert.ToDecimal(2) && Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) < Convert.ToDecimal(3))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveTwoBelowThree"].ToString();
                                    }
                                    else if (Convert.ToDecimal(Query_DT.Rows[0]["Basic_Year"].ToString()) >= Convert.ToDecimal(3))
                                    {
                                        Get_Bonus_Percent = Bonus_Fixed_DT.Rows[0]["AboveThree"].ToString();
                                    }
                                }
                                else
                                {
                                    Get_Bonus_Percent = "0";
                                }


                                //Get Voucher Amt
                                string Voucher_Amt = "0.00";
                                string Voucher_Days = "0.00";
                                DataTable Vo_DT = new DataTable();
                                query2 = "Select * from [" + SessionEpay + "]..SalaryDetails_Voucher where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And ExisistingCode='" + dt.Rows[i]["ExisistingCode"] + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Vo_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Vo_DT.Rows.Count != 0)
                                {
                                    Voucher_Amt = Vo_DT.Rows[0]["Voucher_Amt"].ToString();
                                    Voucher_Days = Vo_DT.Rows[0]["Total_Days"].ToString();
                                }
                                else
                                {
                                    Voucher_Amt = "0.00";
                                    Voucher_Days = "0.00";
                                }
                                Percent_Bonus_Amount = (Convert.ToDecimal(Total_work_Days) + Convert.ToDecimal(Voucher_Days)).ToString();
                                Percent_Bonus_Amount = (Convert.ToDecimal(Percent_Bonus_Amount) * Convert.ToDecimal(Get_Bonus_Percent)).ToString();
                                Percent_Bonus_Amount = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 2, MidpointRounding.ToEven)).ToString();

                                //Final Bonus Amount
                                string[] Round_Bonus_Amt_Splt = Percent_Bonus_Amount.Split('.');
                                //Round_Bonus_Amt = (Math.Round(Convert.ToDecimal(Percent_Bonus_Amount), 0, MidpointRounding.AwayFromZero)).ToString();
                                Round_Bonus_Amt = Round_Bonus_Amt_Splt[0].ToString();

                                if (Convert.ToDecimal(Round_Bonus_Amt) < 0)
                                {
                                    Round_Bonus_Amt = "0.00";
                                }
                                double d1 = Convert.ToDouble(Round_Bonus_Amt.ToString());
                                int rounded1 = ((int)((d1 + 5) / 10)) * 10;  // Output is 20
                                Round_Near_10_Bonus = rounded1.ToString();

                                if (Convert.ToDecimal(Min_Amount_Check) >= Convert.ToDecimal(Round_Near_10_Bonus))
                                {
                                    Round_Near_10_Bonus = Min_Amount_Check;
                                }
                                else
                                {
                                    Round_Near_10_Bonus = Round_Near_10_Bonus;
                                }

                                signlist_bonus_Amt = "0.00";
                                signlist_exgratia_Amt = Round_Near_10_Bonus;

                                //Insert Bonus Table Start
                                query2 = "Select * from [" + SessionEpay + "]..Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                if (Insert_DT.Rows.Count != 0)
                                {
                                    query2 = "Delete from [" + SessionEpay + "]..Bonus_Details where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And EmpNo='" + dt.Rows[i]["EmpNo"].ToString() + "' And Bonus_Year='" + ddlFinance.SelectedValue.ToString() + "'";
                                    Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                }
                                query2 = "Insert Into [" + SessionEpay + "]..Bonus_Details(Ccode,Lcode,EmpNo,MachineID,ExisistingCode,EmpName,FatherName,Department,";
                                query2 = query2 + " DepartmentName,Designation,DOJ,Experiance,Worked_Days,NFH_Days,OT_Days,Gross_Sal,Gross_Sal_Fixed_Per,Percent_Gross_Sal,";
                                query2 = query2 + " Bonus_Percent,Bonus_Amt,Round_Bonus,Final_Bonus_Amt,LY_Gross_Sal,LY_Percent_Gross_Sal,LY_Bonus_Percent,LY_Final_Bonus_Amt,";
                                query2 = query2 + " LY_Total_Days,Bonus_Year,ActivateMode,BaseSalary,Employee_Type,signlist_bonus_Amt,signlist_exgratia_Amt,Hostel_Stage,Voucher_Days,Voucher_Amt) Values('" + SessionCcode + "','" + SessionLcode + "','" + dt.Rows[i]["EmpNo"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["BiometricID"] + "','" + dt.Rows[i]["ExisistingCode"] + "','" + dt.Rows[i]["EmpName"] + "','" + dt.Rows[i]["FatherName"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["Department"] + "','" + dt.Rows[i]["DepartmentNm"] + "','" + dt.Rows[i]["Designation"] + "','" + dt.Rows[i]["Dateofjoining"] + "','" + dt.Rows[i]["Experiance"] + "',";
                                query2 = query2 + " '" + dt.Rows[i]["W_Days"] + "','" + dt.Rows[i]["NFH_Days"] + "','" + dt.Rows[i]["OT_Days"] + "','" + dt.Rows[i]["Gross_Sal"] + "','0.00',";
                                query2 = query2 + " '0.00','" + Get_Bonus_Percent + "','" + Percent_Bonus_Amount + "','" + Round_Bonus_Amt + "','" + Round_Near_10_Bonus + "',";
                                query2 = query2 + " '" + LY_GS + "','" + LY_Percent_GS + "','" + LY_Bonus_Percent + "','" + LY_Final_Bonus + "','" + LY_Total_Days + "',";
                                query2 = query2 + " '" + ddlFinance.SelectedValue.ToString() + "','" + dt.Rows[i]["ActivateMode"] + "','" + dt.Rows[i]["Base"] + "',";
                                query2 = query2 + " '" + ddlEmployeeType.SelectedValue + "','" + signlist_bonus_Amt + "','" + signlist_exgratia_Amt + "','" + Hostel_Stage + "','" + Voucher_Days + "','" + Voucher_Amt + "')";
                                Insert_DT = objdata.RptEmployeeMultipleDetails(query2);
                                //Insert Bonus Table End

                            }

                        }  // Hostel Bonus Process End


                    }
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Finished Success Fully');", true);
                }
            }
            //Bonus Process End

        }

        catch (Exception ex)
        {
            //lblBonus_Error.Visible = true;
            lblBonus_Error.Text = ex.Message;
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Bonus Process ERROR...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnBonusView_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            int YR = 0;
            string Stafflabour = "";

            if (ddlcategory_Rpt.SelectedValue == "0" || ddlcategory_Rpt.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((ddlRptEmpType.SelectedValue == "") || (ddlRptEmpType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    if (ddlcategory_Rpt.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory_Rpt.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }


                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string SalaryThrough = RdbCashBank.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = RdbLeftType.SelectedValue.ToString();
                    string bonus_Period_From = "";
                    string bonus_Period_To = "";
                    string Report_Type_Call = "";

                    if (rdbPayslipFormat.SelectedValue == "1")
                    {
                        Report_Type_Call = "Bonus_Individual";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "2")
                    {
                        //Report_Type_Call = "Bonus_Recociliation";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "3")
                    {
                        Report_Type_Call = "Bonus_Checklist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "4")
                    {
                        Report_Type_Call = "Bonus_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "5")
                    {
                        Report_Type_Call = "Bonus_Blank_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "6")
                    {
                        Report_Type_Call = "Bonus_Cover";
                    }

                    bonus_Period_From = "01-10-" + (Convert.ToDecimal(txtBonusYear.SelectedValue.ToString()) - Convert.ToDecimal(1)).ToString();
                    bonus_Period_To = "01-09-" + txtBonusYear.SelectedValue.ToString();
                    if (rdbPayslipFormat.SelectedValue == "4")
                    {
                        ResponseHelper.Redirect("ViewReport.aspx?Cate=" + Stafflabour + "&Depat=" + "" + "&Months=" + "" + "&yr=" + txtBonusYear.SelectedValue + "&fromdate=" + bonus_Period_From + "&ToDate=" + bonus_Period_To + "&Salary=" + "" + "&CashBank=" + RdbCashBank.SelectedValue + "&ESICode=" + "" + "&EmpTypeCd=" + ddlRptEmpType.SelectedValue.ToString() + "&EmpType=" + ddlRptEmpType.SelectedItem.Text.ToString() + "&PayslipType=" + rdbPayslipFormat.SelectedValue.ToString() + "&PFTypePost=" + RdbPFNonPF.SelectedValue.ToString() + "&Left_Emp=" + RdbLeftType.SelectedValue.ToString() + "&Leftdate=" + "" + "&Division=" + ddlDivision.Text.ToString() + "&Report_Type=" + Report_Type_Call, "_blank", "");
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnBonusExcelView_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            int YR = Convert.ToInt32(txtBonusYear.SelectedValue);
            string Stafflabour = "";

            if (ddlcategory_Rpt.SelectedValue == "0" || ddlcategory_Rpt.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((ddlRptEmpType.SelectedValue == "") || (ddlRptEmpType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }


            if (!ErrFlag)
            {
                //if (ddldept.SelectedValue == "0")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Department Properly');", true);
                //    ErrFlag = true;
                //}
                string query = "";
                DataTable dt = new DataTable();
                query = " Select Cname,Location,Address1,Address2,Location,Pincode from [Raajco_Epay]..AdminRights ";
                query = query + " where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                if (!ErrFlag)
                {
                    if (ddlcategory_Rpt.SelectedValue == "1")
                    {
                        Stafflabour = "S";
                    }
                    else if (ddlcategory_Rpt.SelectedValue == "2")
                    {
                        Stafflabour = "L";
                    }


                    string Payslip_Format_Type = rdbPayslipFormat.SelectedValue.ToString();
                    string SalaryThrough = RdbCashBank.SelectedValue.ToString();
                    string Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    string Str_ChkLeft = RdbLeftType.SelectedValue.ToString();
                    string bonus_Period_From = "";
                    string bonus_Period_To = "";
                    string Report_Type_Call = "";

                    if (rdbPayslipFormat.SelectedValue == "1")
                    {
                        Report_Type_Call = "Bonus_Individual";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "2")
                    {
                        Report_Type_Call = "Bonus_Recociliation";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "3")
                    {
                        Report_Type_Call = "Bonus_Checklist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "4")
                    {
                        //Report_Type_Call = "Bonus_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "5")
                    {
                        //Report_Type_Call = "Bonus_Blank_Signlist";
                    }
                    else if (rdbPayslipFormat.SelectedValue == "6")
                    {
                        //Report_Type_Call = "Bonus_Cover";
                    }

                    bonus_Period_From = "01-10-" + (Convert.ToDecimal(txtBonusYear.SelectedValue.ToString()) - Convert.ToDecimal(1)).ToString();
                    bonus_Period_To = "01-09-" + txtBonusYear.SelectedValue.ToString();

                    if (Report_Type_Call == "Bonus_Recociliation") //Bonus_Recociliation
                    {
                        //query = "Select ExistingCode as Token_No,FirstName as Name,Dateofjoining as DOJ,";
                        //query = query + " sum(case when Months = 'Oct' then WDays else 0 end) as O_Day,sum(case when Months = 'Oct' then WithInc_Amt else 0 end) as O_Earned,";
                        //query = query + " sum(case when Months = 'Nov' then WDays else 0 end) as N_Day,sum(case when Months = 'Nov' then WithInc_Amt else 0 end) as N_Earned,";
                        //query = query + " sum(case when Months = 'Dec' then WDays else 0 end) as D_Day,sum(case when Months = 'Dec' then WithInc_Amt else 0 end) as D_Earned,";
                        //query = query + " sum(case when Months = 'Jan' then WDays else 0 end) as J_Day,sum(case when Months = 'Jan' then WithInc_Amt else 0 end) as J_Earned,";
                        //query = query + " sum(case when Months = 'Feb' then WDays else 0 end) as F_Day,sum(case when Months = 'Feb' then WithInc_Amt else 0 end) as F_Earned,";
                        //query = query + " sum(case when Months = 'Mar' then WDays else 0 end) as Ma_Day,sum(case when Months = 'Mar' then WithInc_Amt else 0 end) as Ma_Earned,";
                        //query = query + " sum(case when Months = 'Apr' then WDays else 0 end) as A_Day,sum(case when Months = 'Apr' then WithInc_Amt else 0 end) as A_Earned,";
                        //query = query + " sum(case when Months = 'May' then WDays else 0 end) as M_Day,sum(case when Months = 'May' then WithInc_Amt else 0 end) as M_Earned,";
                        //query = query + " sum(case when Months = 'Jun' then WDays else 0 end) as Jun_Day,sum(case when Months = 'Jun' then WithInc_Amt else 0 end) as Jun_Earned,";
                        //query = query + " sum(case when Months = 'Jul' then WDays else 0 end) as Jul_Day,sum(case when Months = 'Jul' then WithInc_Amt else 0 end) as Jul_Earned,";
                        //query = query + " sum(case when Months = 'Aug' then WDays else 0 end) as Au_Day,sum(case when Months = 'Aug' then WithInc_Amt else 0 end) as Au_Earned,";
                        //query = query + " sum(case when Months = 'Sep' then WDays else 0 end) as S_Day,sum(case when Months = 'Sep' then WithInc_Amt else 0 end) as S_Earned,";

                        //query = query + " sum(WDays) as T_Days,sum(WithInc_Amt) as T_Earned,";

                        //query = query + " SUM(Incentive_Amt) as Incentive_Amt,(SUM(WithInc_Amt) - SUM(Incentive_Amt)) as Bonus_Earned from (";
                        //if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && txtBonusYear.SelectedValue.ToString() == "2015")
                        //{
                        //    query = query + " Select ED.ExistingCode,ED.FirstName,convert(varchar,ED.DOJ,105) as Dateofjoining,sum(SD.WDays + SD.OTDays + SD.NFH) as WDays,";
                        //    query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";

                        //    query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                        //    query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                        //    query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                        //    query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1) as WithInc_Amt,";
                        //    query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";
                        //    query = query + " from Employee_Mst ED inner join ["+ SessionEpay + "]..SalaryDetails_Bonus SD on ED.ExistingCode COLLATE DATABASE_DEFAULT =SD.ExisistingCode COLLATE DATABASE_DEFAULT And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                        //    query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType";
                        //    query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And  ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue.ToString() + "'";
                        //    query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                        //    query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        //}
                        //else
                        //{
                        //    query = query + " Select ED.ExistingCode,ED.FirstName,convert(varchar,ED.DOJ,105) as Dateofjoining,";
                        //    if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                        //    {
                        //        query = query + " sum(AD.Days + cast(AD.WH_Work_Days as decimal(18,2)) + AD.NFh) as WDays,";
                        //    }
                        //    else
                        //    {
                        //        query = query + " sum(AD.Days + AD.ThreeSided + AD.NFh) as WDays,";
                        //    }
                        //    query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";

                        //    query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                        //    query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                        //    query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                        //    query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1 + SD.allowances2) as WithInc_Amt,";
                        //    query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";
                        //    query = query + " from Employee_Mst ED inner join[Raajco_Epay]..SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                        //    query = query + " inner join["+ SessionEpay + "]..AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode COLLATE DATABASE_DEFAULT = ED.LocCode COLLATE DATABASE_DEFAULT and SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And AD.FromDate=SD.FromDate";
                        //    query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType ";
                        //    query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue + "'";
                        //    query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                        //    query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        //    query = query + " And AD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And AD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        //}

                        query = "Select ExistingCode as Token_No,FirstName as Name,Dateofjoining as DOJ,";
                        query = query + " sum(case when Months = 'Oct' then WorkedDays else 0 end) as O_Day,sum(case when Months = 'Oct' then BasicandDANew else 0 end) as O_BasicDA,sum(case when Months = 'Oct' then GrossEarnings else 0 end) as O_Gross,";
                        query = query + " sum(case when Months = 'Nov' then WorkedDays else 0 end) as N_Day,sum(case when Months = 'Nov' then BasicandDANew else 0 end) as N_BasicDA,sum(case when Months = 'Nov' then GrossEarnings else 0 end) as N_Gross,";
                        query = query + " sum(case when Months = 'Dec' then WorkedDays else 0 end) as D_Day,sum(case when Months = 'Dec' then BasicandDANew else 0 end) as D_BasicDA,sum(case when Months = 'Dec' then GrossEarnings else 0 end) as D_Gross,";
                        query = query + " sum(case when Months = 'Jan' then WorkedDays else 0 end) as J_Day,sum(case when Months = 'Jan' then BasicandDANew else 0 end) as J_BasicDA,sum(case when Months = 'Jan' then GrossEarnings else 0 end) as J_Gross,";
                        query = query + " sum(case when Months = 'Feb' then WorkedDays else 0 end) as F_Day,sum(case when Months = 'Feb' then BasicandDANew else 0 end) as F_BasicDA,sum(case when Months = 'Feb' then GrossEarnings else 0 end) as F_Gross,";
                        query = query + " sum(case when Months = 'Mar' then WorkedDays else 0 end) as Ma_Day,sum(case when Months = 'Mar' then BasicandDANew else 0 end) as Ma_BasicDA,sum(case when Months = 'Mar' then GrossEarnings else 0 end) as Ma_Gross,";
                        query = query + " sum(case when Months = 'Apr' then WorkedDays else 0 end) as A_Day,sum(case when Months = 'Apr' then BasicandDANew else 0 end) as A_BasicDA,sum(case when Months = 'Apr' then GrossEarnings else 0 end) as A_Gross,";
                        query = query + " sum(case when Months = 'May' then WorkedDays else 0 end) as M_Day,sum(case when Months = 'May' then BasicandDANew else 0 end) as M_BasicDA,sum(case when Months = 'May' then GrossEarnings else 0 end) as M_Gross,";
                        query = query + " sum(case when Months = 'Jun' then WorkedDays else 0 end) as Jun_Day,sum(case when Months = 'Jun' then BasicandDANew else 0 end) as Jun_BasicDA,sum(case when Months = 'Jun' then GrossEarnings else 0 end) as Jun_Gross,";
                        query = query + " sum(case when Months = 'Jul' then WorkedDays else 0 end) as Jul_Day,sum(case when Months = 'Jul' then BasicandDANew else 0 end) as Jul_BasicDA,sum(case when Months = 'Jul' then GrossEarnings else 0 end) as Jul_Gross,";
                        query = query + " sum(case when Months = 'Aug' then WorkedDays else 0 end) as Au_Day,sum(case when Months = 'Aug' then BasicandDANew else 0 end) as Au_BasicDA,sum(case when Months = 'Aug' then GrossEarnings else 0 end) as Au_Gross,";
                        query = query + " sum(case when Months = 'Sep' then WorkedDays else 0 end) as S_Day,sum(case when Months = 'Sep' then BasicandDANew else 0 end) as S_BasicDA,sum(case when Months = 'Sep' then GrossEarnings else 0 end) as S_Gross,";

                        query = query + " sum(WorkedDays) as T_Days,sum(BasicandDANew) as T_BasicDA,SUM(GrossEarnings) as T_Gross";

                        query = query + " from (";
                        //if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && txtBonusYear.SelectedValue.ToString() == "2015")
                        //{
                        //    query = query + " Select ED.ExistingCode,ED.FirstName,convert(varchar,ED.DOJ,105) as Dateofjoining,sum(SD.WDays + SD.OTDays + SD.NFH) as WDays,";
                        //    query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2)) + cast(SD.BasicHRA as decimal(18,2)) + cast(SD.ConvAllow as decimal(18,2)) + cast(SD.EduAllow as decimal(18,2)) +";

                        //    query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                        //    query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                        //    query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                        //    query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1) as WithInc_Amt,";
                        //    query = query + " SUM(SD.allowances1) as Incentive_Amt,left(DATENAME(MM, SD.FromDate),3) as Months";
                        //    query = query + " from Employee_Mst ED inner join [" + SessionEpay + "]..SalaryDetails_Bonus SD on ED.ExistingCode COLLATE DATABASE_DEFAULT =SD.ExisistingCode COLLATE DATABASE_DEFAULT And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                        //    query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType";
                        //    query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And  ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue.ToString() + "'";
                        //    query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                        //    query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        //}
                        //else
                        {
                            query = query + " Select ED.ExistingCode,ED.FirstName,convert(varchar,ED.DOJ,105) as Dateofjoining,";
                            //if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                            //{
                            query = query + " sum(AD.Days + cast(AD.WH_Work_Days as decimal(18,2)) + AD.NFh) as WorkedDays,";
                            //}
                            //else
                            //{
                            //    query = query + " sum(AD.Days + AD.ThreeSided + AD.NFh) as WDays,";
                            //}
                            query = query + " SUM(cast(SD.BasicAndDANew as decimal(18,2))) as BasicandDANew,";
                            query = query + " SUM(cast(SD.GrossEarnings as decimal(18,2))) as GrossEarnings,";

                            //query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) + cast(SD.Basic_Uniform as decimal(18,2)) + ";
                            //query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) + cast(SD.Basic_Communication as decimal(18,2)) + ";
                            //query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) + cast(SD.Basic_Incentives as decimal(18,2)) + ";

                            //query = query + " cast(SD.MediAllow as decimal(18,2)) + cast(SD.BasicRAI as decimal(18,2)) + cast(SD.WashingAllow as decimal(18,2)) + SD.ThreesidedAmt + SD.DayIncentive + SD.allowances1 + SD.allowances2) as WithInc_Amt,";
                            query = query + " left(DATENAME(MM, SD.FromDate),3) as Months";
                            query = query + " from Employee_Mst ED inner join[" + SessionEpay + "]..SalaryDetails SD on ED.EmpNo=SD.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                            query = query + " inner join[" + SessionEpay + "]..AttenanceDetails AD on ED.EmpNo=AD.EmpNo And AD.Lcode COLLATE DATABASE_DEFAULT = ED.LocCode COLLATE DATABASE_DEFAULT and SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And AD.FromDate=SD.FromDate";
                            query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType ";
                            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                            query = query + " And AD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And AD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }



                        //Activate Employee Check
                        if (RdbLeftType.SelectedValue != "1")
                        {
                            if (RdbLeftType.SelectedValue == "2")
                            {
                                query = query + " and ED.IsActive='Yes'";
                            }
                            if (RdbLeftType.SelectedValue == "3")
                            {
                                query = query + " and ED.ISActive='No'";
                            }
                        }

                        //PF Check
                        if (RdbPFNonPF.SelectedValue.ToString() != "0")
                        {
                            if (RdbPFNonPF.SelectedValue.ToString() == "1")
                            {
                                //PF Employee
                                query = query + " and (ED.Eligible_PF='1')";
                            }
                            else
                            {
                                //Non PF Employee
                                query = query + " and (ED.Eligible_PF='2')";
                            }
                        }

                        //Salary Through Bank Or Cash Check
                        if (RdbCashBank.SelectedValue.ToString() != "0")
                        {
                            if (RdbCashBank.SelectedValue.ToString() == "1")
                            {
                                //Cash Employee
                                query = query + " and (ED.Salary_Through='1')";
                            }
                            else
                            {
                                //Bank Employee
                                query = query + " and (ED.Salary_Through='2')";
                            }
                        }
                        //Add Division Condition
                        if (ddlDivision.Text.ToString() != "" && ddlDivision.Text.ToString() != "-Select-")
                        {
                            query = query + " and ED.Division='" + ddlDivision.Text.ToString() + "'";
                        }


                        query = query + " group by ED.ExistingCode,ED.FirstName,ED.DOJ,DATENAME(MM, SD.FromDate)";
                        query = query + " ) as P";
                        query = query + " Group by ExistingCode,FirstName,Dateofjoining";
                        query = query + " Order by ExistingCode Asc";
                        DataTable dt_1 = new DataTable();
                        dt_1 = objdata.RptEmployeeMultipleDetails(query);
                        GridExcelView.DataSource = dt_1;
                        GridExcelView.DataBind();

                        string attachment = "attachment;filename=BONUS CHECK LIST.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GridExcelView.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='" + dt_1.Columns.Count + "' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                        //Response.Write(CmpName + " - " + SessionLcode);
                        Response.Write("RAAJCO SPINNING MILLS" + " - " + SessionLcode);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='" + dt_1.Columns.Count + "' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");

                        //if (txtBonusYear.SelectedValue.ToString() == "2015")
                        //{
                        //    if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                        //    {
                        //        Response.Write("STAFF RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        //    }
                        //    else if (ddlRptEmpType.SelectedValue == "3")
                        //    {
                        //        Response.Write("WORKERS RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        //    }
                        //    else if (ddlRptEmpType.SelectedValue == "4")
                        //    {
                        //        Response.Write("WORKERS CASUAL / RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        //    }
                        //    else
                        //    {
                        //        Response.Write("STAFF RECOCILIATION DETAILS - FROM : 2014 TO : 2015");
                        //    }
                        //}
                        //else
                        //{
                        //    if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                        //    {
                        //        Response.Write("STAFF RECOCILIATION DETAILS - FROM : " + (YR - 1) + " TO : " + (YR));
                        //    }
                        //    else if (ddlRptEmpType.SelectedValue == "3")
                        //    {
                        //        Response.Write("WORKERS RECOCILIATION DETAILS - FROM : " + (YR - 1) + " TO : " + (YR));
                        //    }
                        //    else if (ddlRptEmpType.SelectedValue == "4")
                        //    {
                        //        Response.Write("WORKERS CASUAL / RECOCILIATION DETAILS - FROM : " + (YR - 1) + " TO : " + (YR));
                        //    }
                        //    else
                        //    {
                        //        Response.Write("STAFF RECOCILIATION DETAILS - FROM : " + (YR - 1) + " TO : " + (YR));
                        //    }
                        //}
                        Response.Write("BONUS CHECK LIST - FROM : " + (YR - 1) + " TO : " + (YR));
                        //Response.Write("Recociliation Report");


                        Response.Write("</td>");
                        Response.Write("</tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        //Heading Fixed
                        Response.Write("<table border='1'>");
                        Response.Write("<tr>");
                        Response.Write("<td></td><td></td><td></td>");
                        Response.Write("<td colspan='3' align='Center'>Oct " + (YR - 1) + "</td><td colspan='3' align='Center'>Nov " + (YR - 1) + "</td><td colspan='3' align='Center'>Dec " + (YR - 1) + "</td>");
                        Response.Write("<td colspan='3' align='Center'>Jan " + YR + "</td><td colspan='3' align='Center'>Feb " + YR + "</td><td colspan='3' align='Center'>Mar " + YR + "</td>");
                        Response.Write("<td colspan='3' align='Center'>Apr " + YR + "</td><td colspan='3' align='Center'>May " + YR + "</td><td colspan='3' align='Center'>Jun " + YR + "</td>");
                        Response.Write("<td colspan='3' align='Center'>Jul " + YR + "</td><td colspan='3' align='Center'>Aug " + YR + "</td><td colspan='3' align='Center'>Sep " + YR + "</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");


                        //Grid Write Excel
                        Response.Write(stw.ToString());

                        //Total Add
                        string Total_Row_Count = (Convert.ToDecimal(5) + Convert.ToDecimal(GridExcelView.Rows.Count)).ToString();
                        Response.Write("<table border='1'>");
                        Response.Write("<tr style='font-weight:bold;'>");
                        Response.Write("<td colspan='3' align='right'>TOTAL</td>");
                        Response.Write("<td>=sum(D6:D" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(E6:E" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(F6:F" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(G6:G" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(H6:H" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(I6:I" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(J6:J" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(K6:K" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(L6:L" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(M6:M" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(N6:N" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(O6:O" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(P6:P" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Q6:Q" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(R6:R" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(S6:S" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(T6:T" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(U6:U" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(V6:V" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(W6:W" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(X6:X" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Y6:Y" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Z6:Z" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AA6:AA" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AB6:AB" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AC6:AC" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AD6:AD" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AE6:AE" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AF6:AF" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AG6:AG" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AH6:AH" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AI6:AI" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AJ6:AJ" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AK6:AK" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AL6:AL" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AM6:AM" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AN6:AN" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AO6:AO" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(AP6:AP" + Total_Row_Count.ToString() + ")</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");

                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);


                    }

                    if (Report_Type_Call == "Bonus_Checklist") //Bonus_Checklist
                    {
                        //if (ddlRptEmpType.SelectedValue == "4")
                        //{

                        //    query = "Select ED.ExistingCode as Token_No,ED.FirstName as Name,convert(varchar,ED.DOJ,105) as DOJ,MD.DeptNAme as Dept_Name,ED.Designation,";
                        //    //Experiance Get
                        //    query = query + " (case when ED.IsActive = 'N0' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, ED.DOR)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH,  ED.DOJ, ED.DOR) % 12)) ";
                        //    query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end ";
                        //    query = query + " ) as [Year & Month],";

                        //    //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";

                        //    query = query + " BD.Worked_Days,BD.NFH_Days as NFH,BD.OT_Days,isnull(BD.Voucher_Days,0) as Voucher_Days,(BD.Worked_Days+BD.NFH_Days+BD.OT_Days+isnull(BD.Voucher_Days,0)) as [Total Days],";
                        //    query = query + " ED.BaseSalary as [Perday Sal],BD.Hostel_Stage as [Stage], BD.Bonus_Percent as [Amount / Day],BD.Bonus_Amt as [Bonus Amt],";
                        //    query = query + " BD.Final_Bonus_Amt as [Near By Rs.10]";
                        //    query = query + " from Employee_Mst  ED inner join [Raajco_Epay]..Bonus_Details BD on ED.EmpNo=BD.EmpNo And ED.LocCode COLLATE DATABASE_DEFAULT=BD.Lcode COLLATE DATABASE_DEFAULT";
                        //    query = query + "  inner join  Department_Mst MD on MD.DeptCode=ED.DeptCode ";
                        //    query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType";
                        //    query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue + "'";
                        //    query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + txtBonusYear.SelectedValue + "'";

                        //    //Activate Employee Check
                        //    if (RdbLeftType.SelectedValue != "1")
                        //    {
                        //        if (RdbLeftType.SelectedValue == "2")
                        //        {
                        //            query = query + " and ED.IsActive='Yes'";
                        //        }
                        //        if (RdbLeftType.SelectedValue == "3")
                        //        {
                        //            query = query + " and ED.IsActive='No'";
                        //        }
                        //    }

                        //    //PF Check
                        //    if (RdbPFNonPF.SelectedValue.ToString() != "0")
                        //    {
                        //        if (RdbPFNonPF.SelectedValue.ToString() == "1")
                        //        {
                        //            //PF Employee
                        //            query = query + " and (ED.Eligible_PF='1')";
                        //        }
                        //        else
                        //        {
                        //            //Non PF Employee
                        //            query = query + " and (ED.Eligible_PF='2')";
                        //        }
                        //    }

                        //    //Salary Through Bank Or Cash Check
                        //    if (RdbCashBank.SelectedValue.ToString() != "0")
                        //    {
                        //        if (RdbCashBank.SelectedValue.ToString() == "1")
                        //        {
                        //            //Cash Employee
                        //            query = query + " and (ED.Salary_Through='1')";
                        //        }
                        //        else
                        //        {
                        //            //Bank Employee
                        //            query = query + " and (ED.Salary_Through='2')";
                        //        }
                        //    }
                        //    //Add Division Condition
                        //    if (ddlDivision.Text.ToString() != "" && ddlDivision.Text.ToString() != "-Select-")
                        //    {
                        //        query = query + " and ED.Division='" + ddlDivision.Text.ToString() + "'";
                        //    }

                        //    query = query + " Order by ED.ExistingCode Asc";
                        //}
                        //else
                        //{
                        //query = "Select ED.ExistingCode as Token_No,ED.FirstName as Name,MD.DeptNAme as Dept_Name,ED.Designation,convert(varchar,ED.DOJ,105) as DOJ,";
                        //query = query + " SUM(BD.Worked_Days + BD.OT_Days + BD.NFH_Days + isnull(BD.Voucher_Days,0)) as Total_Days,BD.Gross_Sal as [Gross Salary],";
                        //query = query + " isnull(BD.Voucher_Amt,0) as Voucher_Amt,(BD.Gross_Sal+isnull(BD.Voucher_Amt,0)) as [Total Gross Salary],BD.Percent_Gross_Sal as [70% Gross Sal],BD.Bonus_Percent as [Bonus %],BD.Bonus_Amt,";
                        //query = query + " BD.Final_Bonus_Amt as [Near By Rs.10],LY_Final_Bonus_Amt as [LY. Bonus Amt],LY_Gross_Sal as [LY. Earning],LY_Bonus_Percent as [LY. %],LY_Total_Days as [LY. Days],";
                        //query = query + " (BD.Final_Bonus_Amt - LY_Final_Bonus_Amt) as [CY - LY]";
                        //query = query + " from Employee_Mst ED inner join [Raajco_Epay]..Bonus_Details BD on BD.EmpNo=ED.EmpNo And BD.Lcode COLLATE DATABASE_DEFAULT=ED.LocCode COLLATE DATABASE_DEFAULT";
                        //query = query + " inner join  Department_Mst MD on MD.DeptCode=ED.DeptCode  ";
                        //query = query + " inner join MstEmployeeType ET on ED.Wages=ET.EmpType ";
                        //query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue + "'";
                        //query = query + " And BD.Ccode='" + SessionCcode + "' And BD.Lcode='" + SessionLcode + "' And BD.Bonus_Year='" + txtBonusYear.SelectedValue + "'";


                        //query = "";
                        //query = "Select ROW_NUMBER() OVER (PARTITION BY SD.ExisintingCode ORDER BY EM.MachineID Asc) as [S.NO], EM.MachineID,BD.Exisitingcode,EM.FirstName as EmpName";

                        //Activate Employee Check
                        //if (RdbLeftType.SelectedValue != "1")
                        //{
                        //    if (RdbLeftType.SelectedValue == "2")
                        //    {
                        //        query = query + " and ED.IsActive='Yes'";
                        //    }
                        //    if (RdbLeftType.SelectedValue == "3")
                        //    {
                        //        query = query + " and ED.IsActive='No'";
                        //    }
                        //}

                        ////PF Check
                        //if (RdbPFNonPF.SelectedValue.ToString() != "0")
                        //{
                        //    if (RdbPFNonPF.SelectedValue.ToString() == "1")
                        //    {
                        //        //PF Employee
                        //        query = query + " and (ED.Eligible_PF='1')";
                        //    }
                        //    else
                        //    {
                        //        //Non PF Employee
                        //        query = query + " and (ED.Eligible_PF='2')";
                        //    }
                        //}

                        ////Salary Through Bank Or Cash Check
                        //if (RdbCashBank.SelectedValue.ToString() != "0")
                        //{
                        //    if (RdbCashBank.SelectedValue.ToString() == "1")
                        //    {
                        //        //Cash Employee
                        //        query = query + " and (ED.Salary_Through='1')";
                        //    }
                        //    else
                        //    {
                        //        //Bank Employee
                        //        query = query + " and (ED.Salary_Through='2')";
                        //    }
                        //}
                        ////Add Division Condition
                        //if (ddlDivision.Text.ToString() != "" && ddlDivision.Text.ToString() != "-Select-")
                        //{
                        //    query = query + " and ED.Division='" + ddlDivision.Text.ToString() + "'";
                        //}

                        //query = query + " group by ED.ExistingCode,ED.FirstName,MD.DeptNAme,ED.Designation,ED.DOJ,BD.Gross_Sal,BD.Voucher_Amt,BD.Percent_Gross_Sal,BD.Bonus_Percent,BD.Bonus_Amt,";
                        //query = query + " BD.Round_Bonus,BD.Final_Bonus_Amt,LY_Final_Bonus_Amt,LY_Gross_Sal,LY_Bonus_Percent,LY_Total_Days";
                        //query = query + " Order by ED.ExistingCode Asc";
                        // }

                        DataTable dt_1 = new DataTable();
                        // dt_1 = objdata.RptEmployeeMultipleDetails(query);
                        GridExcelView.DataSource = dt_1;
                        GridExcelView.DataBind();

                        string attachment = "attachment;filename=Bonus_Checklist.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GridExcelView.RenderControl(htextw);

                        if (ddlRptEmpType.SelectedValue == "4")
                        {
                            Response.Write("<table>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='16' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                            Response.Write(CmpName + " - " + SessionLcode);
                            Response.Write("</td>");
                            Response.Write("</tr>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='16' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");
                        }
                        else
                        {
                            Response.Write("<table>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='18' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                            Response.Write(CmpName + " - " + SessionLcode);
                            Response.Write("</td>");
                            Response.Write("</tr>");
                            Response.Write("<tr align='Center'>");
                            Response.Write("<td colspan='18' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");
                        }

                        if (txtBonusYear.SelectedValue.ToString() == "2015")
                        {
                            if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                            {
                                Response.Write("STAFF BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                            else if (ddlRptEmpType.SelectedValue == "3")
                            {
                                Response.Write("WORKERS BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                            else if (ddlRptEmpType.SelectedValue == "4")
                            {
                                Response.Write("WORKERS CASUAL / BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                            else
                            {
                                Response.Write("BONUS CUMULATIVE - FOR THE PERIOD OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                        }
                        else
                        {
                            if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                            {
                                Response.Write("STAFF BONUS CUMULATIVE - FOR THE PERIOD OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                            else if (ddlRptEmpType.SelectedValue == "3")
                            {
                                Response.Write("WORKERS BONUS CUMULATIVE - FOR THE PERIOD OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                            else if (ddlRptEmpType.SelectedValue == "4")
                            {
                                Response.Write("WORKERS CASUAL / BONUS CUMULATIVE - FOR THE PERIOD OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                            else
                            {
                                Response.Write("BONUS CUMULATIVE - FOR THE PERIOD OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                        }
                        Response.Write("</td>");
                        Response.Write("</tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        Response.Write(stw.ToString());


                        //Total Add
                        string Total_Row_Count = (Convert.ToDecimal(4) + Convert.ToDecimal(GridExcelView.Rows.Count)).ToString();
                        if (ddlRptEmpType.SelectedValue == "4")
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr>");
                            Response.Write("<td colspan='5' align='right'>TOTAL</td>");
                            Response.Write("<td>=sum(F5:F" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(G5:G" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(H5:H" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(I5:I" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(J5:J" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(K5:K" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(L5:L" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td></td>");
                            Response.Write("<td>=sum(N5:N" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(O5:O" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(P5:P" + Total_Row_Count.ToString() + ")</td>");
                        }
                        else
                        {
                            Response.Write("<table border='1'>");
                            Response.Write("<tr>");
                            Response.Write("<td colspan='5' align='right'>TOTAL</td>");
                            Response.Write("<td>=sum(F5:F" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(G5:G" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(H5:H" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(I5:I" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(J5:J" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(K5:K" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(L5:L" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(M5:M" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(N5:N" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(O5:O" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(P5:P" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(Q5:Q" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("<td>=sum(R5:R" + Total_Row_Count.ToString() + ")</td>");
                            Response.Write("</tr>");
                            Response.Write("</table>");
                        }
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                    }

                    if (Report_Type_Call == "Bonus_Individual") //Bonus_Individual
                    {

                        //Get Report_Date
                        string Basic_Report_Date = "";
                        string Basic_Report_Type = "";
                        query = "Select * from [" + SessionEpay + "]..MstBasic_Report_Date where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        DataTable DT_RPT = new DataTable();
                        string Query_Date_From_Check = "";
                        DT_RPT = objdata.RptEmployeeMultipleDetails(query);
                        if (DT_RPT.Rows.Count != 0)
                        {
                            Basic_Report_Date = DT_RPT.Rows[0]["Report_Date"].ToString();
                        }
                        else
                        {
                            Basic_Report_Date = "";
                        }
                        if (Basic_Report_Date != "")
                        {
                            DateTime Query_Date_Check_From = Convert.ToDateTime(bonus_Period_From);
                            DateTime Query_Date_Check_To = Convert.ToDateTime(bonus_Period_To);
                            DateTime DB_Basic_Report_Date = Convert.ToDateTime(Basic_Report_Date);
                            if (Query_Date_Check_From <= DB_Basic_Report_Date && Query_Date_Check_To >= DB_Basic_Report_Date)
                            {
                                Basic_Report_Type = "Mixed";
                            }
                            else if (Query_Date_Check_From >= DB_Basic_Report_Date)
                            {
                                Basic_Report_Type = "New";
                            }
                            else
                            {
                                Basic_Report_Type = "OLD";
                            }
                        }
                        else
                        {
                            Basic_Report_Type = "Mixed";
                        }







                        if ((SessionLcode == "UNIT I" || SessionLcode == "UNIT IV") && txtBonusYear.SelectedValue.ToString() == "2015")
                        {


                            query = "Select ED.ExistingCode,ED.FirstName ,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptNAme as DepartmentNm,ED.Designation,";

                            //Experiance Get
                            query = query + " (case when ED.IsActive  = 'N0' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, ED.DOR)/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, ED.DOR) % 12))";
                            query = query + " else (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                            query = query + " ) as Experiance,";

                            //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";

                            query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,SD.WDays,SD.NFh,SD.OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                            query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                            query = query + " cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,ED.BaseSalary,";

                            query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                            query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                            query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                            query = query + " from Employee_Mst ED inner join [Raajco_Epay].. SalaryDetails_Bonus SD on ED.ExistingCode COLLATE DATABASE_DEFAULT =SD.ExisistingCode COLLATE DATABASE_DEFAULT And";
                            query = query + " ED.LocCode COLLATE DATABASE_DEFAULT=SD.Lcode COLLATE DATABASE_DEFAULT ";
                            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode inner join MstEmployeeType ET on ED.Wages=ET.EmpType";
                            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue.ToString() + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                            //query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }
                        else
                        {
                            query = "Select ED.ExistingCode,ED.FirstName,convert(varchar,ED.DOJ,105) as Dateofjoining,MD.DeptNAme as DepartmentNm,ED.Designation,";

                            //Experiance Get
                            query = query + " (case when ED.IsActive = 'N' then (convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, ED.DOR)/12) + '.'+ convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, ED.DOR) % 12))";
                            query = query + "   else(convert(varchar(3),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, ED.DOJ, '2016-09-30 00:00:00.000') % 12)) end";
                            query = query + " ) as Experiance,";

                            //query = query + " (convert(varchar(3),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000')/12) + '.' + convert(varchar(2),DATEDIFF(MONTH, OP.Dateofjoining, '2016-09-30 00:00:00.000') % 12)) AS Experiance,";
                            if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                            {
                                query = query + " (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,cast(AD.WH_Work_Days as decimal(18,2)) as OTDays,cast(SD.BasicAndDANew as decimal(18,2)) as BasicAndDANew,";
                            }
                            else
                            {
                                query = query + "  (left(DATENAME(MM, SD.FromDate),3) + '-' + right(Year(SD.FromDate),2)) as Months,AD.Days as WDays,AD.NFh,AD.ThreeSided as OTDays,cast(SD.BasicAndDANew as decimal(18,2))as BasicAndDANew,";
                            }
                            query = query + " cast(SD.BasicHRA as decimal(18,2)) as BasicHRA,cast(SD.ConvAllow as decimal(18,2)) as ConvAllow,cast(SD.EduAllow as decimal(18,2)) as EduAllow,cast(SD.MediAllow as decimal(18,2)) as MediAllow,";
                            query = query + "cast(SD.BasicRAI as decimal(18,2)) as BasicRAI,cast(SD.WashingAllow as decimal(18,2)) as WashingAllow,SD.ThreesidedAmt,SD.DayIncentive,SD.allowances1,SD.allowances2,ED.BaseSalary,ED.Alllowance2 as OthersSal,";

                            query = query + " cast(SD.Basic_Spl_Allowance as decimal(18,2)) as Basic_Spl_Allowance,cast(SD.Basic_Uniform as decimal(18,2)) as Basic_Uniform,";
                            query = query + " cast(SD.Basic_Vehicle_Maintenance as decimal(18,2)) as Basic_Vehicle_Maintenance,cast(SD.Basic_Communication as decimal(18,2)) as Basic_Communication,";
                            query = query + " cast(SD.Basic_Journ_Perid_Paper as decimal(18,2)) as Basic_Journ_Perid_Paper,cast(SD.Basic_Incentives as decimal(18,2)) as Basic_Incentives";

                            query = query + " from Employee_Mst ED inner join[" + SessionEpay + "].. SalaryDetails SD on ED.EmpNo=SD.EmpNo And ED.LocCode COLLATE DATABASE_DEFAULT=SD.Lcode COLLATE DATABASE_DEFAULT";
                            query = query + " inner join [Raajco_Epay].. AttenanceDetails AD on AD.EmpNo=ED.EmpNo And SD.Lcode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT And SD.FromDate=AD.FromDate And ED.LocCode COLLATE DATABASE_DEFAULT=AD.Lcode COLLATE DATABASE_DEFAULT ";

                            query = query + " inner join Department_Mst MD on MD.DeptCode=ED.DeptCode inner join MstEmployeeType ET on ED.Wages=ET.EmpType";
                            query = query + " where ED.CompCode='" + SessionCcode + "' And ED.LocCode='" + SessionLcode + "' And ET.EmpTypeCd='" + ddlRptEmpType.SelectedValue + "'";
                            query = query + " And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "'";
                            query = query + " And AD.Ccode='" + SessionCcode + "' And AD.Lcode='" + SessionLcode + "'";
                            query = query + " And SD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And SD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                            query = query + " And AD.FromDate >=convert(datetime,'" + bonus_Period_From + "', 105) And AD.FromDate <=convert(datetime,'" + bonus_Period_To + "', 105)";
                        }

                        //Activate Employee Check
                        if (RdbLeftType.SelectedValue != "1")
                        {
                            if (RdbLeftType.SelectedValue == "2")
                            {
                                query = query + " and ED.IsActive='Yes'";
                            }
                            if (RdbLeftType.SelectedValue == "3")
                            {
                                query = query + " and ED.IsActive='No'";
                            }
                        }

                        //PF Check
                        if (RdbPFNonPF.SelectedValue.ToString() != "0")
                        {
                            if (RdbPFNonPF.SelectedValue.ToString() == "1")
                            {
                                //PF Employee
                                query = query + " and (ED.Eligible_PF='1')";
                            }
                            else
                            {
                                //Non PF Employee
                                query = query + " and (ED.Eligible_PF='2')";
                            }
                        }

                        //Salary Through Bank Or Cash Check
                        if (RdbCashBank.SelectedValue.ToString() != "0")
                        {
                            if (RdbCashBank.SelectedValue.ToString() == "1")
                            {
                                //Cash Employee
                                query = query + " and (ED.Salary_Through='1')";
                            }
                            else
                            {
                                //Bank Employee
                                query = query + " and (ED.Salary_Through='2')";
                            }
                        }

                        //Add Division Condition
                        if (ddlDivision.Text.ToString() != "" && ddlDivision.Text.ToString() != "-Select-")
                        {
                            query = query + " and ED.Division='" + ddlDivision.Text.ToString() + "'";
                        }

                        query = query + " Order by ED.ExistingCode,SD.Fromdate Asc";
                        DataTable dt_1 = new DataTable();
                        dt_1 = objdata.RptEmployeeMultipleDetails(query);
                        GridExcelView.DataSource = dt_1;
                        GridExcelView.DataBind();


                        string attachment = "attachment;filename=Bonus_Individual.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";
                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GridExcelView.RenderControl(htextw);

                        Response.Write("<table>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='18' style='font-size:12.0pt;font-weight:bold;text-decoration:underline;''>");
                        Response.Write(CmpName + " - " + SessionLcode);
                        Response.Write("</td>");
                        Response.Write("</tr>");
                        Response.Write("<tr align='Center'>");
                        Response.Write("<td colspan='18' style='font-size:10.0pt;font-weight:bold;text-decoration:underline;''>");

                        if (txtBonusYear.SelectedValue.ToString() == "2015")
                        {
                            if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                            {
                                Response.Write("STAFF BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                            else if (ddlRptEmpType.SelectedValue == "3")
                            {
                                Response.Write("WORKERS BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                            else if (ddlRptEmpType.SelectedValue == "4")
                            {
                                Response.Write("WORKERS CASUAL / BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                            else
                            {
                                Response.Write("BONUS PERIOD FROM OCTOBER 2014 TO SEPTEMBER 2015");
                            }
                        }
                        else
                        {
                            if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                            {
                                Response.Write("STAFF BONUS PERIOD FROM OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                            else if (ddlRptEmpType.SelectedValue == "3")
                            {
                                Response.Write("WORKERS BONUS PERIOD FROM OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                            else if (ddlRptEmpType.SelectedValue == "4")
                            {
                                Response.Write("WORKERS CASUAL / BONUS PERIOD FROM OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                            else
                            {
                                Response.Write("BONUS PERIOD FROM OCTOBER " + (YR - 1) + " TO SEPTEMBER " + YR);
                            }
                        }

                        Response.Write("</td>");
                        Response.Write("</tr><tr><td></td></tr>");
                        Response.Write("</table>");

                        string OLD_Token_No_Check = "";
                        string Current_Token_No_Check = "";
                        string First_Row_Count = "6";
                        string Last_Row_Count = "0";
                        int SI_Count = 0;
                        for (int k = 0; k < dt_1.Rows.Count; k++)
                        {
                            Current_Token_No_Check = dt_1.Rows[k]["ExistingCode"].ToString();
                            if (k == 0)
                            {
                                OLD_Token_No_Check = dt_1.Rows[k]["ExistingCode"].ToString();
                                Response.Write("<table border='1' style='font-weight:bold;'>");
                                Response.Write("<tr>");
                                Response.Write("<td colspan='4'>Name : " + dt_1.Rows[k]["FirstName"] + "</td>");
                                Response.Write("<td colspan='2'>Token No : " + dt_1.Rows[k]["ExistingCode"] + "</td>");
                                Response.Write("<td colspan='3'>Dept. Name : " + dt_1.Rows[k]["DepartmentNm"] + "</td>");
                                Response.Write("<td colspan='3'>Designation : " + dt_1.Rows[k]["Designation"] + "</td>");
                                Response.Write("<td colspan='2'>DOJ : " + dt_1.Rows[k]["Dateofjoining"] + "</td>");
                                Response.Write("<td colspan='2'>Cur.Salary : " + dt_1.Rows[k]["BaseSalary"] + " + " + dt_1.Rows[k]["OthersSal"] + "</td>");
                                Response.Write("<td>Service : " + dt_1.Rows[k]["Experiance"] + "</td>");
                                Response.Write("<td>" + (k + 1) + "</td>");
                                Response.Write("</tr>");
                                Response.Write("</table>");

                                Response.Write("<table border='1' style='font-weight:bold;'>");
                                Response.Write("<tr>");
                                Response.Write("<td>Month</td><td>Days</td>");
                                Response.Write("<td>NFH</td><td>OT Days</td>");
                                Response.Write("<td>Total Days</td><td>Basic Amount</td>");
                                if (Basic_Report_Type == "Mixed")
                                {
                                    if (ddlRptEmpType.SelectedValue == "7")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Conv.Allow/Vh.Main</td>");
                                        Response.Write("<td>Edu.Allow/Commu.</td><td>Medi.Allow/JP.P</td>");
                                        Response.Write("<td>Washing Allow</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Conv.Allow</td>");
                                        Response.Write("<td>Edu.Allow</td><td>Medi.Allow</td>");
                                        Response.Write("<td>Washing Allow</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "3")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Con.Allow/Uniform</td>");
                                        Response.Write("<td>Medi.Allow</td>");
                                        Response.Write("<td>Washing Allow</td><td>RAI/Incentive</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "4")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Con.Allow/Uniform</td>");
                                        Response.Write("<td>Medi.Allow</td>");
                                        Response.Write("<td>Washing Allow</td><td>Incentive</td>");
                                    }
                                    else
                                    {
                                        Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                        Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                        Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                    }
                                }
                                else if (Basic_Report_Type == "New")
                                {
                                    if (ddlRptEmpType.SelectedValue == "7")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Vh.Main</td>");
                                        Response.Write("<td>Communication</td><td>J.P.Paper</td>");
                                        Response.Write("<td>Washing Allow</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Conv.Allow</td>");
                                        Response.Write("<td>Edu.Allow</td><td>Medi.Allow</td>");
                                        Response.Write("<td>Washing Allow</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "3")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Uniform</td>");
                                        Response.Write("<td>Medi.Allow</td>");
                                        Response.Write("<td>Washing Allow</td><td>Incentive</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "4")
                                    {
                                        Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Uniform</td>");
                                        Response.Write("<td>Medi.Allow</td>");
                                        Response.Write("<td>Washing Allow</td><td>Incentive</td>");
                                    }
                                    else
                                    {
                                        Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                        Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                        Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                    }
                                }
                                else
                                {
                                    Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                    Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                    Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                }


                                Response.Write("<td>OT Amount</td><td>Attn. Inc</td>");
                                Response.Write("<td>Earned Amount</td><td>SPG. Inc</td>");
                                Response.Write("<td>Others3</td>");
                                Response.Write("<td>Total Amount</td>");
                                Response.Write("</tr>");
                                Response.Write("</table>");
                                Response.Write("</tr>");

                                Response.Write("<table border='1'>");
                                Response.Write("<tr>");
                                Response.Write("<td>" + dt_1.Rows[k]["Months"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["WDays"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["NFh"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["OTDays"] + "</td>");
                                string Total_Days_Cal = (Convert.ToDecimal(dt_1.Rows[k]["WDays"]) + Convert.ToDecimal(dt_1.Rows[k]["NFh"]) + Convert.ToDecimal(dt_1.Rows[k]["OTDays"])).ToString();
                                Response.Write("<td>" + Total_Days_Cal + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["BasicAndDANew"] + "</td>");
                                if (Basic_Report_Type == "Mixed")
                                {
                                    if (ddlRptEmpType.SelectedValue == "7")
                                    {

                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])) + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"])) + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"])) + "</td>");
                                        //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "3")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])) + "</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "4")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Incentives"] + "</td>");
                                    }
                                    else
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                }
                                else if (Basic_Report_Type == "New")
                                {
                                    if (ddlRptEmpType.SelectedValue == "7")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])) + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"])) + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"])) + "</td>");
                                        //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "3")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])) + "</td>");
                                    }
                                    else if (ddlRptEmpType.SelectedValue == "4")
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["Basic_Incentives"] + "</td>");
                                    }
                                    else
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                }
                                else
                                {
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                }
                                Response.Write("<td>" + dt_1.Rows[k]["ThreesidedAmt"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["DayIncentive"] + "</td>");

                                string Earned_Amt_Cal = "";
                                Earned_Amt_Cal = (Convert.ToDecimal(dt_1.Rows[k]["BasicAndDANew"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicHRA"]) + Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"])).ToString();
                                Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"])).ToString();
                                Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["WashingAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["ThreesidedAmt"]) + Convert.ToDecimal(dt_1.Rows[k]["DayIncentive"])).ToString();

                                Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Spl_Allowance"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])).ToString();
                                Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])).ToString();

                                Response.Write("<td>" + Earned_Amt_Cal + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["allowances1"] + "</td>");
                                Response.Write("<td>" + dt_1.Rows[k]["allowances2"] + "</td>");

                                string Total_Amt_Cal = "";
                                Total_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances1"])).ToString();
                                Total_Amt_Cal = (Convert.ToDecimal(Total_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances2"])).ToString();
                                Response.Write("<td>" + Total_Amt_Cal + "</td>");

                                Response.Write("</tr>");
                                Response.Write("</table>");
                                SI_Count = SI_Count + 1;
                            }

                            else
                            {
                                if (OLD_Token_No_Check.ToUpper().ToString() == Current_Token_No_Check.ToUpper().ToString())
                                {
                                    Response.Write("<table border='1'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>" + dt_1.Rows[k]["Months"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WDays"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["NFh"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["OTDays"] + "</td>");
                                    string Total_Days_Cal = (Convert.ToDecimal(dt_1.Rows[k]["WDays"]) + Convert.ToDecimal(dt_1.Rows[k]["NFh"]) + Convert.ToDecimal(dt_1.Rows[k]["OTDays"])).ToString();
                                    Response.Write("<td>" + Total_Days_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicAndDANew"] + "</td>");
                                    if (Basic_Report_Type == "Mixed")
                                    {
                                        if (ddlRptEmpType.SelectedValue == "7")
                                        {

                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"])) + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "3")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])) + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "4")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Incentives"] + "</td>");
                                        }
                                        else
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                    }
                                    else if (Basic_Report_Type == "New")
                                    {
                                        if (ddlRptEmpType.SelectedValue == "7")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"])) + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "3")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])) + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "4")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Incentives"] + "</td>");
                                        }
                                        else
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                    }
                                    else
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }

                                    Response.Write("<td>" + dt_1.Rows[k]["ThreesidedAmt"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["DayIncentive"] + "</td>");

                                    string Earned_Amt_Cal = "";
                                    Earned_Amt_Cal = (Convert.ToDecimal(dt_1.Rows[k]["BasicAndDANew"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicHRA"]) + Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["WashingAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["ThreesidedAmt"]) + Convert.ToDecimal(dt_1.Rows[k]["DayIncentive"])).ToString();

                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Spl_Allowance"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])).ToString();

                                    Response.Write("<td>" + Earned_Amt_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["allowances1"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["allowances2"] + "</td>");

                                    string Total_Amt_Cal = "";
                                    Total_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances1"])).ToString();
                                    Total_Amt_Cal = (Convert.ToDecimal(Total_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances2"])).ToString();
                                    Response.Write("<td>" + Total_Amt_Cal + "</td>");

                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                    SI_Count = SI_Count + 1;
                                }
                                else
                                {
                                    OLD_Token_No_Check = dt_1.Rows[k]["ExistingCode"].ToString();
                                    //Total Add
                                    Last_Row_Count = (Convert.ToDecimal(SI_Count) + Convert.ToDecimal(First_Row_Count)).ToString();
                                    Last_Row_Count = (Convert.ToDecimal(Last_Row_Count) - Convert.ToDecimal(1)).ToString();

                                    Response.Write("<table border='1' style='font-weight:bold;'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>Total</td>");
                                    Response.Write("<td>=sum(B" + First_Row_Count.ToString() + ":B" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(C" + First_Row_Count.ToString() + ":C" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(D" + First_Row_Count.ToString() + ":D" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(E" + First_Row_Count.ToString() + ":E" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(F" + First_Row_Count.ToString() + ":F" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(G" + First_Row_Count.ToString() + ":G" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(H" + First_Row_Count.ToString() + ":H" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(I" + First_Row_Count.ToString() + ":I" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(J" + First_Row_Count.ToString() + ":J" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(K" + First_Row_Count.ToString() + ":K" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(L" + First_Row_Count.ToString() + ":L" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(M" + First_Row_Count.ToString() + ":M" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(N" + First_Row_Count.ToString() + ":N" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(O" + First_Row_Count.ToString() + ":O" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(P" + First_Row_Count.ToString() + ":P" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(Q" + First_Row_Count.ToString() + ":Q" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("<td>=sum(R" + First_Row_Count.ToString() + ":R" + Last_Row_Count.ToString() + ")</td>");
                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                    First_Row_Count = (Convert.ToDecimal(Last_Row_Count) + Convert.ToDecimal(5)).ToString();
                                    SI_Count = 1;
                                    //Total Add End

                                    Response.Write("<table><tr><td></tr></table>");

                                    Response.Write("<table border='1' style='font-weight:bold;'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td colspan='4'>Name : " + dt_1.Rows[k]["FirstName"] + "</td>");
                                    Response.Write("<td colspan='2'>Token No : " + dt_1.Rows[k]["ExistingCode"] + "</td>");
                                    Response.Write("<td colspan='3'>Dept. Name : " + dt_1.Rows[k]["DepartmentNm"] + "</td>");
                                    Response.Write("<td colspan='3'>Designation : " + dt_1.Rows[k]["Designation"] + "</td>");
                                    Response.Write("<td colspan='2'>DOJ : " + dt_1.Rows[k]["Dateofjoining"] + "</td>");
                                    Response.Write("<td colspan='2'>Cur.Salary : " + dt_1.Rows[k]["BaseSalary"] + " + " + dt_1.Rows[k]["OthersSal"] + "</td>");
                                    Response.Write("<td>Service : " + dt_1.Rows[k]["Experiance"] + "</td>");
                                    Response.Write("<td>" + (k + 1) + "</td>");
                                    Response.Write("</tr>");
                                    Response.Write("</table>");

                                    Response.Write("<table border='1' style='font-weight:bold;'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>Month</td><td>Days</td>");
                                    Response.Write("<td>NFH</td><td>OT Days</td>");
                                    Response.Write("<td>Total Days</td><td>Basic Amount</td>");

                                    if (Basic_Report_Type == "Mixed")
                                    {
                                        if (ddlRptEmpType.SelectedValue == "7")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Conv.Allow/Vh.Main</td>");
                                            Response.Write("<td>Edu.Allow/Commu.</td><td>Medi.Allow/JP.P</td>");
                                            Response.Write("<td>Washing Allow</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Conv.Allow</td>");
                                            Response.Write("<td>Edu.Allow</td><td>Medi.Allow</td>");
                                            Response.Write("<td>Washing Allow</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "3")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Con.Allow/Uniform</td>");
                                            Response.Write("<td>Medi.Allow</td>");
                                            Response.Write("<td>Washing Allow</td><td>RAI/Incentive</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "4")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Con.Allow/Uniform</td>");
                                            Response.Write("<td>Medi.Allow</td>");
                                            Response.Write("<td>Washing Allow</td><td>Incentive</td>");
                                        }
                                        else
                                        {
                                            Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                            Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                            Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                        }
                                    }
                                    else if (Basic_Report_Type == "New")
                                    {
                                        if (ddlRptEmpType.SelectedValue == "7")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Vh.Main</td>");
                                            Response.Write("<td>Communication</td><td>J.P.Paper</td>");
                                            Response.Write("<td>Washing Allow</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Conv.Allow</td>");
                                            Response.Write("<td>Edu.Allow</td><td>Medi.Allow</td>");
                                            Response.Write("<td>Washing Allow</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "3")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Uniform</td>");
                                            Response.Write("<td>Medi.Allow</td>");
                                            Response.Write("<td>Washing Allow</td><td>Incentive</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "4")
                                        {
                                            Response.Write("<td>Spl.Allow</td><td>HRA</td><td>Uniform</td>");
                                            Response.Write("<td>Medi.Allow</td>");
                                            Response.Write("<td>Washing Allow</td><td>Incentive</td>");
                                        }
                                        else
                                        {
                                            Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                            Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                            Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                        }
                                    }
                                    else
                                    {
                                        Response.Write("<td>HRA</td><td>Conv. Allow</td>");
                                        Response.Write("<td>Edu. Allow</td><td>Medi. Allow</td>");
                                        Response.Write("<td>RAI</td><td>Washing Allow</td>");
                                    }



                                    Response.Write("<td>OT Amount</td><td>Attn. Inc</td>");
                                    Response.Write("<td>Earned Amount</td><td>SPG. Inc</td><td>Others3</td>");
                                    Response.Write("<td>Total Amount</td>");
                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                    Response.Write("</tr>");

                                    Response.Write("<table border='1'>");
                                    Response.Write("<tr>");
                                    Response.Write("<td>" + dt_1.Rows[k]["Months"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["WDays"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["NFh"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["OTDays"] + "</td>");
                                    string Total_Days_Cal = (Convert.ToDecimal(dt_1.Rows[k]["WDays"]) + Convert.ToDecimal(dt_1.Rows[k]["NFh"]) + Convert.ToDecimal(dt_1.Rows[k]["OTDays"])).ToString();
                                    Response.Write("<td>" + Total_Days_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["BasicAndDANew"] + "</td>");
                                    if (Basic_Report_Type == "Mixed")
                                    {
                                        if (ddlRptEmpType.SelectedValue == "7")
                                        {

                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"])) + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "3")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])) + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "4")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Incentives"] + "</td>");
                                        }
                                        else
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                    }
                                    else if (Basic_Report_Type == "New")
                                    {
                                        if (ddlRptEmpType.SelectedValue == "7")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"])) + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"])) + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "1" || ddlRptEmpType.SelectedValue == "6")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            //Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "3")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])) + "</td>");
                                        }
                                        else if (ddlRptEmpType.SelectedValue == "4")
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Spl_Allowance"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + (Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"])) + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["Basic_Incentives"] + "</td>");
                                        }
                                        else
                                        {
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                            Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                        }
                                    }
                                    else
                                    {
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicHRA"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["ConvAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["EduAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["MediAllow"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["BasicRAI"] + "</td>");
                                        Response.Write("<td>" + dt_1.Rows[k]["WashingAllow"] + "</td>");
                                    }
                                    Response.Write("<td>" + dt_1.Rows[k]["ThreesidedAmt"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["DayIncentive"] + "</td>");

                                    string Earned_Amt_Cal = "";
                                    Earned_Amt_Cal = (Convert.ToDecimal(dt_1.Rows[k]["BasicAndDANew"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicHRA"]) + Convert.ToDecimal(dt_1.Rows[k]["ConvAllow"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["EduAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["MediAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["BasicRAI"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["WashingAllow"]) + Convert.ToDecimal(dt_1.Rows[k]["ThreesidedAmt"]) + Convert.ToDecimal(dt_1.Rows[k]["DayIncentive"])).ToString();

                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Spl_Allowance"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Uniform"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Vehicle_Maintenance"])).ToString();
                                    Earned_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Communication"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Journ_Perid_Paper"]) + Convert.ToDecimal(dt_1.Rows[k]["Basic_Incentives"])).ToString();

                                    Response.Write("<td>" + Earned_Amt_Cal + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["allowances1"] + "</td>");
                                    Response.Write("<td>" + dt_1.Rows[k]["allowances2"] + "</td>");

                                    string Total_Amt_Cal = "";
                                    Total_Amt_Cal = (Convert.ToDecimal(Earned_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances1"])).ToString();
                                    Total_Amt_Cal = (Convert.ToDecimal(Total_Amt_Cal) + Convert.ToDecimal(dt_1.Rows[k]["allowances2"])).ToString();
                                    Response.Write("<td>" + Total_Amt_Cal + "</td>");

                                    Response.Write("</tr>");
                                    Response.Write("</table>");
                                }
                            }
                        }
                        //Total Add
                        Last_Row_Count = (Convert.ToDecimal(SI_Count) + Convert.ToDecimal(First_Row_Count)).ToString();
                        Last_Row_Count = (Convert.ToDecimal(Last_Row_Count) - Convert.ToDecimal(1)).ToString();

                        Response.Write("<table border='1' style='font-weight:bold;'>");
                        Response.Write("<tr>");
                        Response.Write("<td>Total</td>");
                        Response.Write("<td>=sum(B" + First_Row_Count.ToString() + ":B" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(C" + First_Row_Count.ToString() + ":C" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(D" + First_Row_Count.ToString() + ":D" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(E" + First_Row_Count.ToString() + ":E" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(F" + First_Row_Count.ToString() + ":F" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(G" + First_Row_Count.ToString() + ":G" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(H" + First_Row_Count.ToString() + ":H" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(I" + First_Row_Count.ToString() + ":I" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(J" + First_Row_Count.ToString() + ":J" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(K" + First_Row_Count.ToString() + ":K" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(L" + First_Row_Count.ToString() + ":L" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(M" + First_Row_Count.ToString() + ":M" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(N" + First_Row_Count.ToString() + ":N" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(O" + First_Row_Count.ToString() + ":O" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(P" + First_Row_Count.ToString() + ":P" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(Q" + First_Row_Count.ToString() + ":Q" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("<td>=sum(R" + First_Row_Count.ToString() + ":R" + Last_Row_Count.ToString() + ")</td>");
                        Response.Write("</tr>");
                        Response.Write("</table>");
                        //Response.Write(stw.ToString());
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);

                    }

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
           server control at run time. */
    }
}
