﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Shift_IncMaster : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string basic = "0";
    string HRA = "0";
    string Conv = "0";
    string Spl = "0";
    string WH = "0";
    string Medical = "0";
    string Edu = "0";
    string SessionEpay = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {

            LoadShift();

        }
        Load_Data();
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadShift();
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);



    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // LoadShift();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Errflag = "False";

        if (ddlCategory.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Category...');", true);
        }
        if (ddlshift.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Shift...');", true);
        }




        //if ((ddlEmployeeType.SelectedItem.Text == "-Select-") || (ddlEmployeeType.SelectedItem.Text == ""))
        //{
        //    Errflag = "true";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
        //}

        if (Errflag != "true")
        {
            try
            {
                String MsgFlag = "Insert";
                DataTable dt = new DataTable();
                Query = "select * from [" + SessionEpay + "]..Shift_Inc_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Shift='" + ddlshift.SelectedItem.Text + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [" + SessionEpay + "]..Shift_Inc_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Shift = '" + ddlshift.SelectedItem.Text + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MsgFlag = "Update";
                }

                Query = "";
                Query = Query + "Insert into [" + SessionEpay + "]..Shift_Inc_Mst(Ccode,Lcode,Category,EmployeeType,Shift,Amount";
                Query = Query + "Alloted_Date,Alloted_Date_Str)Values('" + SessionCcode + "','" + SessionLcode + "',";
                Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + ddlshift.SelectedItem.Text + "','" + txtAmount.Text + "',Convert(date,'" + txtDate.Text + "'.103),'" + txtDate.Text + "')";

                objdata.RptEmployeeMultipleDetails(Query);
                Clear();
                Load_Data();
                if (MsgFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
            }
            catch (Exception Ex)
            {

            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {

        ddlCategory.SelectedValue = "0";
        LoadShift();
        txtAmount.Text = "0";
        txtDate.Text = "";
        loadEmp();

    }
    private void loadEmp()
    {
        DataTable dtdsupp1 = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp1 = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp1;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        //ddlEmployeeType_SelectedIndexChanged(sender, e);

    }




    private void LoadShift()
    {
        DataTable dtdsupp2 = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select ShiftDesc from Shift_Mst";
        dtdsupp2 = objdata.RptEmployeeMultipleDetails(Query);
        ddlshift.DataSource = dtdsupp2;
        DataRow dr = dtdsupp2.NewRow();
        dr["ShiftDesc"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        dtdsupp2.Rows.InsertAt(dr, 0);
        ddlshift.DataTextField = "ShiftDesc";
        //  ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlshift.DataBind();
    }

    private void LoadData()
    {
        DataTable dt1 = new DataTable();

        Query = "";
        Query = "select * from [" + SessionEpay + "]..Shift_Inc_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Shift='" + ddlshift.SelectedItem.Text + "' ";
        dt1 = objdata.RptEmployeeMultipleDetails(Query);
        if (dt1.Rows.Count != 0)
        {

            txtAmount.Text = dt1.Rows[0]["Amount"].ToString();
        }
    }

    protected void ddlshift_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData();
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string Catname = "";
        string Emptype = "";
        string SSQL = "";
        SSQL = "Select EmpTypeCd,EmpType from MstEmployeeType where  EmpType='" + e.CommandName.ToString() + "'";
        DataTable dt_ = new DataTable();
        dt_ = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_.Rows.Count > 0)
        {
            Emptype = dt_.Rows[0]["EmpTypeCd"].ToString();
        }

        SSQL = "Delete from [" + SessionEpay + "]..Shift_Inc_Mst where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + "  and EmployeeType='" + Emptype + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }

    private void Load_Data()
    {
        string SSQL = "";
        SSQL = "Select SI.Alloted_Date_Str, EMT.EmpType as EmployeeType,CASE when SI.Category='1' then 'STAFF' else 'LABOUR' end as Category,SI.Shift,SI.Amount from MstEmployeeType EMT inner join [" + SessionEpay + "]..Shift_Inc_Mst SI";
        SSQL = SSQL + " on SI.Employeetype=EMT.EmptypeCd";
        SSQL = SSQL + " where SI.Ccode='" + SessionCcode + "' and SI.Lcode='" + SessionLcode + "'";
        Repeater1.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataBind();
    }
}





