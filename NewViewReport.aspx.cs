﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class NewViewReport : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month;
    string str_yr;
    string HeadYear;
    string str_cate;
    string str_dept;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; 
    string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate;
    string ToDate;
    string salaryType;
    string Emp_ESI_Code;

    string EmployeeType; 
    string EmployeeTypeCd;
    string PayslipType;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    string query1 = "";
    string Basic_Report_To_Date = "";
    string Payslip_Folder = "";
    string type = "";
    string SessionPayroll = "";
    string AttMonths = "";
    string Finance = "";
    string Wages = "";
    string Tdate = "";
    string Str_PFType = "";
    string RptType = "";
    string FinYear = "";
    DataTable AutoDataTable = new DataTable();
    DataTable DT_Basic = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        str_cate = Request.QueryString["Cate"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();
        // str_dept = Request.QueryString["Depat"].ToString();
        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        // HeadYear = Request.QueryString["Headyr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        AttMonths = Request.QueryString["AttMonths"].ToString();
        EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        Finance = Request.QueryString["Finance"].ToString();
        Wages = Request.QueryString["Wages"].ToString();
        Payslip_Folder = "Payslip";
        Str_PFType = Request.QueryString["PF_NonPF"].ToString();
        RptType = Request.QueryString["Report_Type"].ToString();
        salaryType = Request.QueryString["Left_Emp"].ToString();

        if (RptType == "PercentageReport")
        {
            FinYear = Request.QueryString["FinYear"].ToString();
        }
        GetDetails();
        if (RptType != "PercentageReport" && RptType != "PayslipCashCover" && RptType != "PayslipSample" && RptType != "AttendanceBonusSignList")
        {
            if (AutoDataTable.Rows.Count > 0)
            {
                ds.Tables.Add(AutoDataTable);
                ReportDocument report = new ReportDocument();

                if (Str_PFType != "2")
                {
                    if ((Wages == "STAFF") || (Wages == "SUB STAFF"))
                    {

                        report.Load(Server.MapPath(Payslip_Folder + "/Staff_Payslip.rpt"));
                    }
                    if ((Wages == "SECURITY"))
                    {
                        report.Load(Server.MapPath(Payslip_Folder + "/Security_Payslip.rpt"));
                    }
                    else if ((Wages != "STAFF") && (Wages != "SUB STAFF") && (Wages != "SECURITY"))
                    {
                        report.Load(Server.MapPath(Payslip_Folder + "/Payslip_VDA.rpt"));
                        //report.Load(Server.MapPath(Payslip_Folder + "/Security_Payslip.rpt"));
                    }
                }

                if (Str_PFType == "2")
                {
                    {
                        if ((Wages == "STAFF") || (Wages == "SUB STAFF"))
                        {
                            report.Load(Server.MapPath(Payslip_Folder + "/Staff_Payslip.rpt"));
                        }
                        if ((Wages == "SECURITY"))
                        {
                            report.Load(Server.MapPath(Payslip_Folder + "/Security_Payslip.rpt"));
                        }
                        else if ((Wages != "STAFF") && (Wages != "SUB STAFF") && (Wages != "SECURITY"))
                        {
                            report.Load(Server.MapPath(Payslip_Folder + "/Payslip_VDA.rpt"));
                        }
                    }
                }

                int YR = Convert.ToInt32(str_yr);
                if (str_month == "March")
                {
                    YR = YR + 1;
                }
                else if (str_month == "February")
                {
                    YR = YR + 1;
                }
                else if (str_month == "January")
                {
                    YR = YR + 1;
                }
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.DataDefinition.FormulaFields["Wages"].Text = "'" + Wages + "'";
                report.DataDefinition.FormulaFields["Months"].Text = "'" + AttMonths + "'";
                report.DataDefinition.FormulaFields["Year"].Text = "'" + YR + "'";
                report.DataDefinition.FormulaFields["Tdate"].Text = "'" + Tdate + "'";
                // report.DataDefinition.FormulaFields["Tdate"].Text = "'" + Tdate + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
            }
        }
        if (RptType == "PercentageReport")
        {
            if (DT_Basic.Rows.Count > 0)
            {
                ds.Tables.Add(DT_Basic);
                ReportDocument report = new ReportDocument();

                report.Load(Server.MapPath(Payslip_Folder + "/BasicSalaryPercentage.rpt"));
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.DataDefinition.FormulaFields["Wages"].Text = "'" + Wages + "'";
                report.DataDefinition.FormulaFields["Months"].Text = "'" + AttMonths + "'";
                report.DataDefinition.FormulaFields["Year"].Text = "'" + HeadYear + "'";
                ////  report.DataDefinition.FormulaFields["Tdate"].Text = "'" + Tdate + "'";
                // report.DataDefinition.FormulaFields["Tdate"].Text = "'" + Tdate + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
            }
        }
    }
    public void GetDetails()

    {
        if (RptType == "AttendanceBonusSignList")
        {
            string SSQL = "";
            SSQL = "";
            SSQL = "Select ROW_NUMBER() OVER (ORDER BY EM.MachineID Asc) AS [S.No],EM.ExistingCode as ExCode,EM.FirstName as EmpName,EM.DeptName,EM.WeekOff,Convert(varchar,DOJ,105) as DOJ,ABD.Days as EmpDays,ABD.NH,ABD.WH,ABD.LOP,ABD.TotalDays";
            SSQL = SSQL + ",ABD.TotalAbsent,ABD.Eligible_Days as [EligiDays],ABD.AbsentDays as [AbsentDays],ABD.Attn_Bonus_Skill as [AttnBonusSkill]";
            SSQL = SSQL + ",ABD.Rate,ABD.PaidAmt as [PaidAmt],ABD.ExpectWorkDays,ABD.AmtProBase as [Amount Pro. Rate Base]";
            SSQL = SSQL + " from Employee_Mst EM inner join [" + SessionPayroll + "]..Atten_Bonus_Details ABD on ABD.MachineID =EM.MachineID  and ABD.Ccode COLLATE DATABASE_DEFAULT=EM.CompCode COLLATE DATABASE_DEFAULT and ABD.Lcode COLLATE DATABASE_DEFAULT=EM.LocCode COLLATE DATABASE_DEFAULT";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(datetime,ABD.FromDate,103)>=Convert(datetime,'" + fromdate + "',103) and Convert(datetime,ABD.ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";
            SSQL = SSQL + " and ABD.Month='" + str_month + "' and ABD.FinYear='" + Finance + "' and ABD.Year='" + str_yr + "'";
            if (str_cate != "-Select-")
            {
                SSQL = SSQL + " and ABD.Cate='" + str_cate + "'";
            }
            if (Wages != "-Select-")
            {
                SSQL = SSQL + " and ABD.EmpType='" + Wages + "'";
            }
            SSQL = SSQL + " order by CAST(EM.ExistingCode as int)  asc";

            DataTable dt_cashCover = new DataTable();
            dt_cashCover = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_cashCover.Rows.Count > 0)
            {
                string CmpName = "";
                string Location = "";
                SSQL = "";
                SSQL = "select Cname,Lcode,Location from [" + SessionPayroll + "]..AdminRights where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DataTable dt_Cmp = new DataTable();
                dt_Cmp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_Cmp.Rows.Count > 0)
                {
                    CmpName = dt_Cmp.Rows[0]["Cname"].ToString();
                    Location = dt_Cmp.Rows[0]["Location"].ToString();
                }

                ds.Tables.Add(dt_cashCover);
                ReportDocument report = new ReportDocument();

                report.Load(Server.MapPath(Payslip_Folder + "/AttenBonusSignList.rpt"));
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.DataDefinition.FormulaFields["Months"].Text = "'" + str_month + "'";
                report.DataDefinition.FormulaFields["Year"].Text = "'" + HeadYear + "'";
                report.DataDefinition.FormulaFields["FromDate"].Text = "'" + fromdate + "'";
                report.DataDefinition.FormulaFields["Tdate"].Text = "'" + ToDate + "'";
                report.DataDefinition.FormulaFields["Company"].Text = "'" + CmpName + "'";
                report.DataDefinition.FormulaFields["Location"].Text = "'" + Location + "'";
                report.DataDefinition.FormulaFields["Lcode"].Text = "'" + SessionLcode + "'";
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
            }
        }
        else if (RptType == "PayslipSample")
        {
            AutoDataTable.Columns.Add("Excode");
            AutoDataTable.Columns.Add("Name");
            AutoDataTable.Columns.Add("FName");
            AutoDataTable.Columns.Add("Epfno");
            AutoDataTable.Columns.Add("ESIno");
            AutoDataTable.Columns.Add("BAccno");
            AutoDataTable.Columns.Add("Dept");
            AutoDataTable.Columns.Add("Desig");
            AutoDataTable.Columns.Add("Wages");
            AutoDataTable.Columns.Add("Dob");
            AutoDataTable.Columns.Add("Doj");
            AutoDataTable.Columns.Add("Grade");
            AutoDataTable.Columns.Add("MonthDays");
            AutoDataTable.Columns.Add("WorkerDays");
            AutoDataTable.Columns.Add("NFH");
            AutoDataTable.Columns.Add("WeekOff");
            AutoDataTable.Columns.Add("Leave");
            AutoDataTable.Columns.Add("EarningsHead");
            AutoDataTable.Columns.Add("Actuals");
            AutoDataTable.Columns.Add("Earnings");
            AutoDataTable.Columns.Add("DeductionHead");
            AutoDataTable.Columns.Add("Deducted");
            AutoDataTable.Columns.Add("ActTotal");
            AutoDataTable.Columns.Add("EarnTotal");
            AutoDataTable.Columns.Add("DeductTotal");
            AutoDataTable.Columns.Add("NetPay");
            AutoDataTable.Columns.Add("RoundOff");

            DataTable dt_Head = new DataTable();
            dt_Head.Columns.Add("Head");
            dt_Head.Columns.Add("SQlHeadFix");
            dt_Head.Columns.Add("SQlHeadEarn");
            dt_Head.Rows.Add();
            dt_Head.Rows[dt_Head.Rows.Count - 1]["Head"] = "BASIC + DA / VDA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadFix"] = "FBasicDA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadEarn"] = "EBasicDA";
            dt_Head.Rows.Add();
            dt_Head.Rows[dt_Head.Rows.Count - 1]["Head"] = "HRA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadFix"] = "FHRA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadEarn"] = "EHRA";
            dt_Head.Rows.Add();
            dt_Head.Rows[dt_Head.Rows.Count - 1]["Head"] = "Washing Allowance";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadFix"] = "FWA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadEarn"] = "EWA";
            dt_Head.Rows.Add();
            dt_Head.Rows[dt_Head.Rows.Count - 1]["Head"] = "CEA / LTA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadFix"] = "FCEA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadEarn"] = "ECEA";
            dt_Head.Rows.Add();
            dt_Head.Rows[dt_Head.Rows.Count - 1]["Head"] = "Medical Allowance";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadFix"] = "FMA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadEarn"] = "EMA";
            dt_Head.Rows.Add();
            dt_Head.Rows[dt_Head.Rows.Count - 1]["Head"] = "Conveyance Allowance";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadFix"] = "FConveyA";
            dt_Head.Rows[dt_Head.Rows.Count - 1]["SQlHeadEarn"] = "EConveyA";

            DataTable dt_Deduct = new DataTable();
            dt_Deduct.Columns.Add("Head");
            dt_Deduct.Columns.Add("SQlHeadDeduct");
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "PF";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "PF";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "ESI";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "ESI";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "Canteen";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "Cantn";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "Mess";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "Mess";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "Rent";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "Rent";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "EB";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "EB";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "WO";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "WO";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "LIC";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "LIC";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "IT";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "IT";
            dt_Deduct.Rows.Add();
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["Head"] = "Other";
            dt_Deduct.Rows[dt_Deduct.Rows.Count - 1]["SQlHeadDeduct"] = "Other";

            string SSQL = "";
            SSQL = "Select SD.ExisistingCode as Excode, EM.FirstName as Name,EM.Lastname as FName,EM.PFNo as Epfno,EM.ESINo as ESIno ";
            SSQL = SSQL + ",EM.AccountNo as BAccno,SD.DeptName as Dept,SD.Designation as Desig,SD.Wages as Wages,Convert(varchar,EM.BirthDate,105) as DOB";
            SSQL = SSQL + ",Convert(varchar,SD.DOJ,105) as Doj,EM.Grade as Grade,SD.CatName";
            SSQL = SSQL + ",(sum(isnull(CAST(Atten.Days as decimal(18,2)),'0'))+sum(isnull(CAST(Atten.CompDays as decimal(18,2)),'0'))+sum(isnull(CAST(Atten.ELDAys as decimal(18,2)),'0'))+sum(isnull(CAST(Atten.CLDays as decimal(18,2)),'0'))) as WorkerDays, SUM(isnull(CAST(SD.Netpay as decimal(18,2)),'0')) as NetPaid,sum(isnull(CAST(Atten.TotalDays as decimal(18,2)),'0')) as MonthDays";
            SSQL = SSQL + ",SUM(isnull(CAST(SD.NFh as decimal(18,2)),'0')) as NFH,sum(isnull(CAST(SD.Weekoff as decimal(18,2)),'0')) as WeekOff,SUM(isnull(CAST(SD.LOPDays as decimal(18,2)),'0')) as Leave,SUM(isnull(CAST(SD.BasicAndDANew as decimal(18,2)),'0')) as EBasicDA";
            SSQL = SSQL + ",sum(isnull(CAST(SD.BasicHRA as decimal(18,2)),'0')) as EHRA,sum(isnull(CAST(SD.WashingAllow as decimal(18,2)),'0')) as EWA,SUM(isnull(CAST(SD.MediAllow as decimal(18,2)),'0')) as EMA";
            SSQL = SSQL + ",sum(isnull(CAST(SD.ConvAllow as decimal(18,2)),'0')) as EConveyA,SUM(isnull(CAST(SD.EduAllow as decimal(18,2)),'0')) as ECEA,SUM(isnull(SD.PfSalary,'0')) as PF";
            SSQL = SSQL + ",sum(isnull(SD.ESI,'0')) as ESI,SUM(isnull(CAST(SD.Messdeduction as decimal(18,2)),'0')) as Mess,sum(isnull(CAST(SD.ProfessionalTax as decimal(18,2)),'0')) as ProfTax";
            SSQL = SSQL + ",SUM(isnull(CAST(SD.deduction1 as decimal(18,2)),'0')) as Stiching,SUM(isnull(CAST(SD.FixBasicAndDA as decimal(18,2)),'0')) as FBasicDA,sum(isnull(CAST(SD.FixBasicHRA as decimal(18,2)),'0')) as FHRA";
            SSQL = SSQL + ",Sum(isnull(CAST(SD.FixConvAllow as decimal(18,2)),'0')) as FConveyA,sum(isnull(CAST(SD.FixEduAllow as decimal(18,2)),'0')) as FCEA,SUM(isnull(CAST(SD.FixMediAllow as decimal(18,2)),'0')) as FMA";
            SSQL = SSQL + ",SUM(isnull(CAST(SD.FixWashingAllow as decimal(18,2)),'0')) as FWA,SUM(isnull(CAST(SD.FixedLTA as decimal(18,2)),'0')) as FixedLTA";

            SSQL = SSQL + " from Employee_Mst EM inner join [" + SessionPayroll + "]..SalaryDetails SD on EM.MachineID=SD.MachineNO";
            SSQL = SSQL + " inner join [" + SessionPayroll + "]..AttenanceDetails Atten on EM.EmpNo=Atten.EmpNo";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + " and SD.CCode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Atten.CCode='" + SessionCcode + "' and Atten.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and SD.Month='" + str_month + "' and Convert(datetime,SD.FromDate,103)>=Convert(datetime,'" + fromdate + "',103)";
            SSQL = SSQL + " and Convert(datetime,SD.ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";
            SSQL = SSQL + " and SD.FinancialYear='" + str_yr + "' and EM.CatName='" + EmployeeTypeCd + "' and SD.workedDays>0";
            SSQL = SSQL + " and Atten.Months='" + str_month + "' and Convert(datetime,Atten.FromDate,103)>=Convert(datetime,'" + fromdate + "',103)";
            SSQL = SSQL + " and Convert(datetime,Atten.ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";
            SSQL = SSQL + " and Atten.FinancialYear='" + str_yr + "'";
            if ((Str_PFType == "1"))
            {
                SSQL = SSQL + " and SD.PFEligible != '1' and SD.ESIEligible='1' ";
            }

            if ((Str_PFType == "2"))
            {
                SSQL = SSQL + " and SD.PFEligible='2' and SD.ESIEligible ='1' ";
            }
            if (Str_PFType == "3")
            {
                query = query + " and SD.PFEligible='2' and SD.ESIEligible='2' ";
            }
            if (Wages != "")
            {
                SSQL = SSQL + " and SD.Wages='" + Wages + "'";
            }
            SSQL = SSQL + " group by SD.ExisistingCode,EM.FirstName,EM.Lastname,EM.PFNo,EM.ESINo,EM.AccountNo,SD.DeptName,SD.Designation";
            SSQL = SSQL + ",SD.Wages,EM.BirthDate,SD.DOJ,EM.Grade,SD.CatName";
            SSQL = SSQL + " order by SD.ExisistingCode asc";
            DataTable dt_cashCover = new DataTable();
            dt_cashCover = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_cashCover.Rows.Count > 0)
            {
                //DataRow dr;
                //for (int i = 0; i < dt_cashCover.Rows.Count; i++)
                //{


                //    for (int j = 0; j < dt_Head.Rows.Count; j++)
                //    {

                //        dr = AutoDataTable.NewRow();
                //        dr["Excode"] = dt_cashCover.Rows[i]["Excode"].ToString();
                //        dr["Name"] = dt_cashCover.Rows[i]["Name"].ToString();
                //        dr["FName"] = dt_cashCover.Rows[i]["FName"].ToString();
                //        dr["Epfno"] = dt_cashCover.Rows[i]["Epfno"].ToString();
                //        dr["ESIno"] = dt_cashCover.Rows[i]["ESIno"].ToString();
                //        dr["BAccno"] = dt_cashCover.Rows[i]["BAccno"].ToString();
                //        dr["Dept"] = dt_cashCover.Rows[i]["Dept"].ToString();
                //        dr["Desig"] = dt_cashCover.Rows[i]["Desig"].ToString();
                //        dr["Wages"] = dt_cashCover.Rows[i]["Wages"].ToString();
                //        dr["Dob"] = dt_cashCover.Rows[i]["Dob"].ToString();
                //        dr["Doj"] = dt_cashCover.Rows[i]["Doj"].ToString();
                //        dr["Grade"] = dt_cashCover.Rows[i]["Grade"].ToString();
                //        dr["MonthDays"] = dt_cashCover.Rows[i]["MonthDays"].ToString();
                //        dr["WorkerDays"] = dt_cashCover.Rows[i]["WorkerDays"].ToString();
                //        dr["NFH"] = dt_cashCover.Rows[i]["NFH"].ToString();
                //        dr["WeekOff"] = dt_cashCover.Rows[i]["WeekOff"].ToString();
                //        dr["Leave"] = dt_cashCover.Rows[i]["Leave"].ToString();
                //        dr["ActTotal"] = dt_cashCover.Rows[i]["ActTotal"].ToString();
                //        dr["EarnTotal"] = dt_cashCover.Rows[i]["EarnTotal"].ToString();
                //        dr["DeductTotal"] = dt_cashCover.Rows[i]["DeductTotal"].ToString();
                //        dr["NetPay"] = dt_cashCover.Rows[i]["NetPay"].ToString();
                //        dr["RoundOff"] = dt_cashCover.Rows[i]["RoundOff"].ToString();
                //    }
                //}

                ds.Tables.Add(dt_cashCover);
                ReportDocument report = new ReportDocument();
                string Cname = "";
                string Add = "";
                string regno = "";
                SSQL = "";
                SSQL = "Select CompName,(Add1+''+isnull(Add2,'')+'-'+Pincode) as Address,RegNo from Company_Mst where CompCode='" + SessionCcode + "'";
                DataTable dt_comp = new DataTable();
                dt_comp = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dt_comp.Rows.Count > 0)
                {
                    Cname = dt_comp.Rows[0]["CompName"].ToString();
                    Add = dt_comp.Rows[0]["Address"].ToString();
                    regno = dt_comp.Rows[0]["RegNo"].ToString();
                }

                int YR = Convert.ToInt32(str_yr);
                if (str_month == "March")
                {
                    YR = YR + 1;
                }
                else if (str_month == "February")
                {
                    YR = YR + 1;
                }
                else if (str_month == "January")
                {
                    YR = YR + 1;
                }



                report.Load(Server.MapPath(Payslip_Folder + "/SamplePayslip.rpt"));
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                report.DataDefinition.FormulaFields["Company"].Text = "'" + Cname + "'";
                report.DataDefinition.FormulaFields["Address"].Text = "'" + Add + "'";
                report.DataDefinition.FormulaFields["RegNo"].Text = "'" + regno + "'";
                report.DataDefinition.FormulaFields["Month"].Text = "'" + str_month + "'";
                report.DataDefinition.FormulaFields["Year"].Text = "'" + YR + "'";

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
            }
        }
        else if (RptType == "PayslipCashCover")
        {
            string SSQL = "";
            SSQL = "Select EM.ExistingCode as TokenNO, EM.FirstName as Name, sum(isnull(SD.WorkedDays,'0')) as Days, SUM(isnull(SD.Netpay,'0')) as NetAmt";
            SSQL = SSQL + " from Employee_Mst EM inner join [" + SessionPayroll + "]..SalaryDetails SD on EM.MachineID=SD.MachineNO";
            SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' ";
            SSQL = SSQL + " and SD.CCode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and SD.Month='" + str_month + "' and Convert(datetime,SD.FromDate,103)>=Convert(datetime,'" + fromdate + "',103)";
            SSQL = SSQL + " and Convert(datetime,SD.ToDate,103)<=Convert(datetime,'" + ToDate + "',103)";
            SSQL = SSQL + " and SD.FinancialYear='" + str_yr + "' and EM.CatName='" + EmployeeTypeCd + "'";
            if ((Str_PFType == "1"))
            {
                SSQL = SSQL + " and SD.ProvidentFund != '0' and ";
            }

            if ((Str_PFType == "2"))
            {
                SSQL = SSQL + " and SD.ProvidentFund='0' and ";
            }
            if (Wages != "")
            {
                SSQL = SSQL + " and EM.Wages='" + Wages + "'";
            }
            SSQL = SSQL + " group by EM.ExistingCode,EM.FirstName";
            SSQL = SSQL + " order by CAST(EM.ExistingCode as int) asc";
            DataTable dt_cashCover = new DataTable();
            dt_cashCover = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt_cashCover.Rows.Count > 0)
            {
                ds.Tables.Add(dt_cashCover);
                ReportDocument report = new ReportDocument();

                report.Load(Server.MapPath(Payslip_Folder + "/CaseCoverRpt.rpt"));
                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
            }
        }
        else if (RptType == "PercentageReport")
        {
            string SSQL = " ";
            //string[] FinYrsplit =
            SSQL = "select Em.ExistingCode,Em.FirstName,Em.DeptName,Em.Designation,Em.BaseSalary as BaseSalary,BS.Percentage,Sd.PercentageAmount as GrossEarnings" +
                   " from Employee_Mst Em inner join [" + SessionPayroll + "]..SalaryDetails Sd on Em.MachineID=Sd.MachineNo" +
                                 " inner join Mst_BasicSalary BS on Em.Wages=Bs.EmployeeType" +
                                 " Where Em.CatName='" + str_cate + "' and Em.Wages='" + Wages + "' and Sd.Month='" + str_month + "' and Sd.FinancialYear='" + str_yr + "' and " +
                                 "BS.Category = '" + str_cate + "' and Bs.EmployeeType = '" + Wages + "' and BS.Months = '" + str_month + "' and Bs.FinYear = '" + FinYear + "'" +
                                 "order by Em.ExistingCode";

            DT_Basic = objdata.RptEmployeeMultipleDetails(SSQL);
        }

        else if (RptType == "Payslip_Zero_Days")
        {
            string SSQL = "";
            //string[] FinYrsplit =
            SSQL = "select Em.ExistingCode,Em.FirstName,Em.DeptName,Em.Designation,Em.BaseSalary as BaseSalary,BS.Percentage,Sd.PercentageAmount as GrossEarnings" +
                   " from Employee_Mst Em inner join [" + SessionPayroll + "]..SalaryDetails Sd on Em.MachineID=Sd.MachineNo" +
                                 " inner join Mst_BasicSalary BS on Em.Wages=Bs.EmployeeType" +
                                 " Where Em.CatName='" + str_cate + "' and Em.Wages='" + Wages + "' and Sd.Month='" + str_month + "' and Sd.FinancialYear='" + str_yr + "' and " +
                                 "BS.Category = '" + str_cate + "' and Bs.EmployeeType = '" + Wages + "' and BS.Months = '" + str_month + "' and Bs.FinYear = '" + FinYear + "' and SD.WorkedDays=0 " +
                                 "order by Em.ExistingCode";
            DT_Basic = objdata.RptEmployeeMultipleDetails(SSQL);
        }
        else
        {

            query = "Select SalDet.ExisistingCode as EmpNo,EmpDet.FirstName,convert(varchar, SalDet.DOJ, 105) as DOJ,SalDet.DeptName,";
            query = query + " SalDet.Designation,SalDet.WorkedDays,SalDet.Weekoff,SalDet.LOPDays,SalDet.NFh,SalDet.Basic_SM,";
            query = query + " SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew, SalDet.BasicHRA as BasicHRA,SalDet.LTA as LTA,";
            query = query + " SalDet.EduAllow,SalDet.WashingAllow as WashingAllow,SalDet.Others as OthersAll,SalDet.MediAllow,SalDet.OverTime,";
            query = query + " SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,((SalDet.VPF)+ (SalDet.ProvidentFund))as ProvidentFund,SalDet.ESI,";
            query = query + " (SalDet.Deduction3) as IT,(SalDet.Deduction4) as Mess,(SalDet.Deduction5) as Rent,SalDet.TotalFixed as VDAAmt,";
            query = query + " SalDet.Advance,SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF,";
            query = query + " SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays,SalDet.TotalWorkingdays,";
            query = query + " SalDet.FixBasicAndDA,SalDet.RoundOffNetPay,SalDet.FixBasicHRA,SalDet.FixedLTA,SalDet.FixConvAllow,SalDet.FixEduAllow,";
            query = query + " SalDet.FixMediAllow,SalDet.FixBasic,SalDet.FixWashingAllow,SalDet.FixedOthers,SalDet.DAAmt as FixedVDA,";
            query = query + " CONVERT(date,SalDet.SalaryDate,105) as Tdate,SalDet.TotalDays, (SalDet.T_Shirt) as others,  ";
            query = query + " (AttnDet.Days+AttnDet.CompDays)[Days],";

            //14042021
            //query = query + " AttnDet.Days, ";

            query = query + " (AttnDet.ELDays + AttnDet.CLDays) as PLDays,";
            query = query + " ( AttnDet.Days+AttnDet.ELDays + AttnDet.CLDays+AttnDet.TotalWHdays+AttnDet.NFh+LOPDays+AttnDet.CompDays) as TotDays,";
            query = query + " SalDet.CatName [CatName]";
            query = query + " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo";
            query = query + " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode";
            query = query + " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo";
            query = query + " where SalDet.Month='" + str_month + "' AND SalDet.FinancialYear='" + str_yr + "' and SalDet.CatName='" + EmployeeTypeCd + "' and ";
            query = query + " AttnDet.Months='" + AttMonths + "' AND AttnDet.FinancialYear='" + Finance + "' and ";
            query = query + " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And ";
            query = query + " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and SalDet.Wages='" + Wages + "'";
            if (salaryType == "1")
            {
                query = query + " and EmpDet.IsActive='Yes'";
            }
            else if (salaryType == "2")
            {
                query = query + " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + fromdate + "', 105) and EmpDet.IsActive='No') and ";
            }

            if ((Str_PFType == "1"))
            {
                query = query + " and SalDet.PFEligible = '1' and SalDet.ESIEligible ='1' ";
            }

            if ((Str_PFType == "2"))
            {
                query = query + " and SalDet.PFEligible='2' and SalDet.ESIEligible ='1'  ";
            }
            if (Str_PFType == "3")
            {
                query = query + " and SalDet.PFEligible='2' and SalDet.ESIEligible='2'  ";
            }

            query = query + " and (convert(datetime,SalDet.FromDate,105) >= convert(datetime,'" + fromdate + "', 105)   and ";
            query = query + " (convert(datetime, SalDet.ToDate, 105) <= convert(datetime, '" + ToDate + "', 105)))";
            query = query + " Group by EmpDet.EmpNo,EmpDet.FirstName,SalDet.DeptName,SalDet.Designation,SalDet.Basic_SM,";
            query = query + " SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,";
            query = query + " SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime,SalDet.VDA,SalDet.ConvAllow,";
            query = query + " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3),(SalDet.Deduction4),";
            query = query + " (SalDet.Deduction5) ,SalDet.Advance,SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,";
            query = query + " SalDet.LIC,SalDet.EB,SalDet.VPF, SalDet.WorkedDays,SalDet.Weekoff,SalDet.Basic_SM,";
            query = query + " SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo ,SalDet.LOPDays,SalDet.NFh,SalDet.TotalDeductions,SalDet.NetPay,";
            query = query + " SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays,SalDet.DOJ,SalDet.TotalWorkingdays,SalDet.FixBasicAndDA,";
            query = query + " SalDet.RoundOffNetPay,SalDet.FixBasicHRA,SalDet.FixConvAllow,SalDet.FixEduAllow,";
            query = query + " SalDet.FixMediAllow,SalDet.FixBasic,SalDet.FixWashingAllow,CONVERT(date,SalDet.SalaryDate,105),";
            query = query + " SalDet.TotalDays,Days,ELDays,CLDays,AttnDet.NFh,AttnDet.CompDays,ExisistingCode,BasedOn,";
            query = query + " BasedOn,SalDet.LTA,SalDet.Others,SalDet.FixedOthers,SalDet.FixedLTA,SalDet.TotalFixed,SalDet.DAAmt,SalDet.CatName ";
            query = query + "Order by cast(SalDet.ExisistingCode as int) Asc";

            AutoDataTable = objdata.RptEmployeeMultipleDetails(query);

            if (AutoDataTable.Rows.Count != 0)
            {
                Tdate = AutoDataTable.Rows[0]["TDate"].ToString();
            }
            else
                Tdate = "";

        }
    }
}


