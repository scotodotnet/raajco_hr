﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Text;
using System.Security.Cryptography;
using System.IO;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;

public partial class Weekly_OT_Cal : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDTable = new DataTable();
    string SessionUserType;

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    string basesalary = "0";
    string PerHr = "0";
    string OTWages = "0";
    string OneDay = "0";
    string Tot_OT_Hrs = "0";
    string OT_Amt = "0";
    string Wages = "";
    string MachineID = "";
    string Emp_WH_Day = "";
    string WH = "0";
    string WH_Amt = "0";
    string Name = "";
    Boolean ErrFlag = false;



    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Weekly OT Salary";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_WagesType();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }

    private void Load_WagesType()
    {
        //string query = "";
        //DataTable dtdsupp = new DataTable();
        //ddlEmployeeType.Items.Clear();
        //query = "Select * from MstEmployeeType";
        //dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        //ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpTypeCd"] = "0";
        //dr["EmpType"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        //ddlEmployeeType.DataTextField = "EmpType";
        //ddlEmployeeType.DataValueField = "EmpTypeCd";
        //ddlEmployeeType.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        SSQL = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
        // Load_Data();
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {

        ////Load_Data();
        //if(ddlEmployeeType.SelectedItem.Text=="Security")
        //{
        //    SSQL = "";
        //}
        Load_EmpNo();
    }

    private void Load_EmpNo()
    {
        SSQL = "";
        SSQL = "Select * from Employee_mst where CompCode='" + SessionCcode + "' and LocCOde='" + SessionLcode + "'";
        SSQL = SSQL + " and isActive='Yes' and Wages='" + ddlEmployeeType.SelectedItem.Text + "'";
        DataTable dt = new DataTable();
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlMachineID.DataSource = dt;
        ddlMachineID.DataTextField = "MachineID";
        ddlMachineID.DataValueField = "MachineID";
        ddlMachineID.DataBind();
        ddlMachineID.Items.Insert(0, new ListItem("-Select-", "-Select-", true));

    }

    protected void btnOTSalary_Click(object sender, EventArgs e)
    {
        Wages = ddlEmployeeType.SelectedItem.Text;
        if (ddlCategory.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Select Employee Category');", true);
            ErrFlag = true;
        }
        if (ddlEmployeeType.SelectedItem.Text == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Select Employee Type');", true);
            ErrFlag = true;
        }
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
            ErrFlag = true;
        }

        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
            ErrFlag = true;
        }
        if (ErrFlag != true)
        {

            SSQL = "";
            SSQL = "select LD.MachineID,LD.ExistingCode,isnull(EM.BaseSalary,'0') as BaseSalary,isnull(OTEligible,'') as OTEligible,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,isnull(LD.NFH_Present_Count,'0') as NFH_Present_Count,";
            SSQL = SSQL + "LD.Attn_Date_Str, LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,Total_Hrs as TOT,Present_Absent as Status,LD.Wh_Present_Count,LD.Wages,EM.CatName from LogTime_Days LD";
            SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
            SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy") + "',103)";
            SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(txtToDate.Text).ToString("dd/MM/yyyy") + "',103)";
            // SSQL = SSQL + " and EM.MachineID='1501560'";
            //SSQL = SSQL + " and EM.Wages='" + ddlEmployeeType.SelectedItem.Text + "' And  LD.Shift !='No Shift' And LD.TimeIN!='' And EM.OTEligible='Yes' order by LD.Attn_Date_Str  ";
            if (txtTokenNo.Text != "")
            {
                SSQL = SSQL + " and LD.ExistingCode='" + txtTokenNo.Text + "'";
            }
            SSQL = SSQL + " and EM.Wages='" + ddlEmployeeType.SelectedItem.Text + "' And LD.TimeIN!='' And EM.OTEligible='Yes' order by LD.Attn_Date_Str  ";

            AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

            if (AutoDTable.Rows.Count != 0)
            {
                for (int i = 0; AutoDTable.Rows.Count > i; i++)
                {
                    int sno = 0;

                    string MachineID = AutoDTable.Rows[i]["MachineID"].ToString();
                    string AttnDate = AutoDTable.Rows[i]["Attn_Date_Str"].ToString();
                    string shift = AutoDTable.Rows[i]["Shift"].ToString();

                    if(MachineID== "1200121" && AttnDate=="04/11/2020")
                    {
                        string stop = "";
                    }

                    DataTable Dt_OT = new DataTable();
                    string Eligible;
                    string TOT_hrs = "";
                    int OT = 0;
                    string Tot1 = "";
                    string Tot2 = "";
                    string FourHrs_Inc = "";
                    string EightHrsOInc = "";
                    string Dept = "";
                    string NFH = "";
                    string TimeIN = "";
                    string TimeOUT = "";
                    DateTime DateIN = new DateTime();
                    DateTime DateOUT = new DateTime();
                    string chkOT = "";
                    string Emp_categ = "";
                    string Emp_Type = "";
                    string Inc_Amt = "0";
                    string FixWHAmt = "0";
                    string fourhrsamount = "0";
                    string Eighthrsamount = "0";
                    string Four_Inc = "0";
                    string Eight_Inc = "0";
                    string FourAmt = "0";
                    string EightAmt = "0";


                    Name = AutoDTable.Rows[i]["FirstName"].ToString();
                    Dept = AutoDTable.Rows[i]["DeptName"].ToString();
                    NFH = AutoDTable.Rows[i]["NFH_Present_Count"].ToString();
                    basesalary = AutoDTable.Rows[i]["BaseSalary"].ToString();
                    Eligible = AutoDTable.Rows[i]["OTEligible"].ToString();
                    TimeIN = AutoDTable.Rows[i]["TimeIN"].ToString();
                    TimeOUT = AutoDTable.Rows[i]["TimeOUT"].ToString();
                    Emp_categ = AutoDTable.Rows[i]["CatName"].ToString();
                    Emp_Type = AutoDTable.Rows[i]["Wages"].ToString();

                    DataTable DT_EmpType = new DataTable();
                    SSQL = "";
                    SSQL = "select * from MstEmployeeType where EmpType ='" + Emp_Type + "'";
                    DT_EmpType = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_EmpType.Rows.Count != 0)
                    {
                        Emp_categ = DT_EmpType.Rows[0]["EmpCategory"].ToString();
                        Emp_Type = DT_EmpType.Rows[0]["EmpTypeCd"].ToString();
                    }
                    
                    OneDay = (Convert.ToDecimal(basesalary) / 26).ToString();
                    OneDay = (Math.Round(Convert.ToDecimal(OneDay), 0, MidpointRounding.AwayFromZero)).ToString();

                    if (Eligible == "Yes")
                    {                                       
                        TOT_hrs = AutoDTable.Rows[i]["Total_Hrs"].ToString();   
                        string[] sp = TOT_hrs.Split(':');
                        Tot1 = sp[0];
                        Tot2 = sp[1];
                        if (Convert.ToInt32(Tot2) > (45))
                        {
                            Tot1 = (Convert.ToInt32(Tot1) + 1).ToString();

                        }
                        if ((Dept == "STAFF MESS") || (Dept == "HOSTEL CANTEEN") || (Dept == "DRIVERS"))
                        {

                            if (Convert.ToInt32(Tot1) >= (12))
                            {
                                OT = (Convert.ToInt32(Tot1) - 14);
                            }
                            else
                            {
                                OT = 0;
                            }
                        }
                        if ((Dept != "STAFF MESS") || (Dept != "HOSTEL CANTEEN"))
                        {
                            if (Convert.ToInt32(Tot1) >= (10))
                            {
                                OT = (Convert.ToInt32(Tot1) - 8);

                            }
                            else
                            {
                                OT = 0;
                            }

                        }
                        else
                        {
                            OT = 0;
                        }
                        //Add increment hours  
                        
                        if ((Dept == "HOSTEL CANTEEN") || (Dept == "STAFF MESS") || Dept == "DRIVERS")
                        {
                            PerHr = (Convert.ToDecimal(OneDay) / 12).ToString();
                        }
                        else
                        {
                            PerHr = (Convert.ToDecimal(OneDay) / 8).ToString();
                        }
                        OTWages = (Convert.ToDecimal(OneDay) / 2).ToString();
                        OTWages = (Math.Round(Convert.ToDecimal(OTWages), 1, MidpointRounding.AwayFromZero)).ToString();   

                        if (Convert.ToDecimal(OT) != 0)
                        {
                            OT_Amt = (Convert.ToDecimal(PerHr) * (Convert.ToDecimal(OT))).ToString();
                        }
                        else
                        {
                            OT_Amt = "0";
                        }

                        OT_Amt = (Math.Round(Convert.ToDecimal(OT_Amt), 1, MidpointRounding.AwayFromZero)).ToString();
                    }

                    if (Convert.ToInt32(OT) >= (4) && (Convert.ToInt32(OT) < (8)))
                    {
                        FourHrs_Inc = "1";
                        Four_Inc = "1";
                    }
                    else
                    {
                        FourHrs_Inc = "0";
                        Four_Inc = "0";
                    }


                    if (Convert.ToInt32(OT) >= (8) && (Convert.ToInt32(OT) < (16)))
                    {
                        EightHrsOInc = "1";
                        Eight_Inc = "1";
                    }
                    else
                    {
                        EightHrsOInc = "0";
                        Eight_Inc = "0";
                    }

                    if (TimeIN != "")
                    {
                        DateIN = Convert.ToDateTime(TimeIN);
                    }
                    if (TimeOUT != "")
                    {
                        DateOUT = Convert.ToDateTime(TimeOUT);
                    }

                    if (OT >= 4)
                    {
                        

                        WH = AutoDTable.Rows[i]["Wh_Present_Count"].ToString();
                        if ((Convert.ToInt32(OT) > 4) && (Convert.ToInt32(OT) < 8) || (Convert.ToInt32(OT) == 4))
                        {
                            chkOT = "4-7";
                        }

                        if ((Convert.ToInt32(OT) == 8) || (Convert.ToInt32(OT) > 8))
                        {
                            chkOT = "7:30 and Above";
                        }
                        
                        DataTable DT_IncMaster = new DataTable();
                        SSQL = "";
                        SSQL = "Select * from [" + Session["SessionEpay"] + "]..OT_Inc_Mst where Category='" + Emp_categ + "' and EmployeeType='" + Emp_Type + "'and OT_Hrs='" + chkOT + "'";
                        DT_IncMaster = objdata.RptEmployeeMultipleDetails(SSQL);
                        if (DT_IncMaster.Rows.Count != 0)
                        {
                            for (int j = 0; j < DT_IncMaster.Rows.Count; j++)
                            {
                                string FixTimeIN1 = "";
                                string fixTimeEndIN1 = "";
                                string EndOutDays = "";

                                DateTime fixTimeIN = new DateTime();
                                DateTime fixTimeEndIN = new DateTime();
                                FixTimeIN1 = DT_IncMaster.Rows[j]["Start_IN"].ToString();
                                fixTimeEndIN1 = DT_IncMaster.Rows[j]["End_IN"].ToString();
                                EndOutDays = DT_IncMaster.Rows[j]["End_Out_Days"].ToString();

                                fixTimeIN = Convert.ToDateTime(DT_IncMaster.Rows[j]["Start_IN"].ToString());
                                fixTimeEndIN = Convert.ToDateTime(DT_IncMaster.Rows[j]["End_IN"].ToString());
                                if (EndOutDays == "1")
                                {
                                    fixTimeEndIN = System.Convert.ToDateTime(fixTimeEndIN).AddDays(1);
                                }

                                if ((Convert.ToDateTime(DateIN) > Convert.ToDateTime(fixTimeIN)) && (Convert.ToDateTime(DateIN) < Convert.ToDateTime(fixTimeEndIN)))
                                {
                                    
                                    Inc_Amt = DT_IncMaster.Rows[j]["Amount"].ToString();
                                    FixWHAmt = DT_IncMaster.Rows[j]["WH_Amt"].ToString();
                                    if (FixWHAmt == "")
                                    {
                                        FixWHAmt = "0";
                                    }
                                    
                                }
                            }
                        }
                        if (DT_IncMaster.Rows.Count != 0)                     
                        {
                           
                            if ((Convert.ToInt32(OT) > 4) && (Convert.ToInt32(OT) < 8) || (Convert.ToInt32(OT) == 4))
                            {                                                                                            
                                fourhrsamount = Inc_Amt;
                                
                            }
                            else
                            {
                                fourhrsamount = "0";
                            }                                                             
                            if ((Convert.ToInt32(OT) == 8) || (Convert.ToInt32(OT) > 8))
                            {                                                                  
                                Eighthrsamount = Inc_Amt;
                            }
                            else
                            {
                                Eighthrsamount = "0";
                            }
                          

                            FourAmt = (Convert.ToDecimal(Four_Inc) * Convert.ToDecimal(fourhrsamount)).ToString();      
                            EightAmt = (Convert.ToDecimal(Eight_Inc) * Convert.ToDecimal(Eighthrsamount)).ToString();
                        }
                       
                        WH_Amt = (Convert.ToDecimal(FixWHAmt) * Convert.ToDecimal(WH)).ToString();
                    }

                    DataTable OT_Check = new DataTable();
                    SSQL = "";
                    SSQL = "Select * from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where MachineID='" + MachineID + "'";
                    SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(AttnDate).ToString("dd/MM/yyyy") + "',103)";
                    OT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (OT_Check.Rows.Count != 0)
                    {
                        SSQL = "Delete from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where  MachineID='" + MachineID + "'";
                        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(AttnDate).ToString("dd/MM/yyyy") + "',103)";

                        objdata.RptEmployeeMultipleDetails(SSQL);
                    }


                    //OT_Hrs_Mst Value Insert
                    SSQL = "";
                    SSQL = "insert into [" + Session["SessionEpay"] + "].. OT_Hrs_Mst(Ccode,Lcode,MachineID,EmpName,Attn_Date_Str,Shift,TotalHrs,OT_Hrs,Wages,Four_Hrs_inc,Eight_Hrs_inc,Weekoff,Category,Month,Amount,Oneday_Wages,OT_Wages,Four_Hrs_inc_Amount,Eight_Hrs_inc_Amount,WH_Amt,TimeIN,TimeOUT,Dept)";
                    SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID + "',";
                    SSQL = SSQL + "'" + Name + "','" + AttnDate + "','" + shift + "', '" + TOT_hrs + "','" + OT + "','" + ddlEmployeeType.SelectedItem.Text + "','" + FourHrs_Inc + "','" + EightHrsOInc + "','" + WH + "','" + ddlCategory.SelectedItem.Text + "','" + ddlMonths.SelectedItem.Text + "',";
                    SSQL = SSQL + "'" + OT_Amt + "','" + OneDay + "','" + OTWages + "','" + FourAmt + "','" + EightAmt + "','" + WH_Amt + "','" + TimeIN + "','" + TimeOUT + "','" + Dept + "')";
                    objdata.RptEmployeeMultipleDetails(SSQL);


                    //}
                    WH = "0";
                    sno += 1;
                   // OT_Calculation(MachineID);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Weekly OT Calculation Completed');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('No Records Found for the Calculation');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Check the Fields');", true);
        }
    }
    private void OT_Calculation(string MachineID_get)
    {
        //  OT Calculatuion
        DataTable DT_Weekly = new DataTable();
        DataTable DT_Emp = new DataTable();
        DataTable DT_OTHrs = new DataTable();
        string OT_AttnDate = "";
        string WH = "";
        string WHAmount = "";

        SSQL = "";
        SSQL = "Select * from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where Wages ='" + ddlEmployeeType.SelectedItem.Text + "'";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(txtToDate.Text).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " and MachineID='" + MachineID_get + "'";
        DT_Weekly = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Weekly.Rows.Count != 0)
        {
            for (int i = 0; i < DT_Weekly.Rows.Count; i++)
            {
                string DeptName = "";
                PerHr = "";
                string MachineID = DT_Weekly.Rows[i]["MachineID"].ToString();
                Session["MachineID"] = DT_Weekly.Rows[i]["MachineID"].ToString();
                if (MachineID == "1501560")
                {
                    string Stop = "";
                }
                OT_AttnDate = DT_Weekly.Rows[i]["Attn_Date_Str"].ToString();
                WH = DT_Weekly.Rows[i]["Weekoff"].ToString();


                SSQL = "";
                SSQL = "Select * from Employee_Mst where MachineID ='" + MachineID + "'";

                DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);
                Name = DT_Emp.Rows[0]["FirstName"].ToString();
                basesalary = DT_Emp.Rows[0]["BaseSalary"].ToString();
                DeptName = DT_Emp.Rows[0]["DeptName"].ToString();

                OneDay = (Convert.ToDecimal(basesalary) / 26).ToString();
                OneDay = (Math.Round(Convert.ToDecimal(OneDay), 0, MidpointRounding.AwayFromZero)).ToString();

                if ((DeptName == "HOSTEL CANTEEN") || (DeptName == "STAFF MESS") || DeptName == "DRIVERS")
                {
                    PerHr = (Convert.ToDecimal(OneDay) / 12).ToString();
                }
                else
                {
                    PerHr = (Convert.ToDecimal(OneDay) / 8).ToString();
                }

                OTWages = (Convert.ToDecimal(OneDay) / 2).ToString();
                OTWages = (Math.Round(Convert.ToDecimal(OTWages), 1, MidpointRounding.AwayFromZero)).ToString();

                SSQL = "";
                SSQL = "select sum(cast(OT_Hrs as int)) as Tot_OT_Hrs from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where ";
                //SSQL = SSQL + " CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy") + "',103)";
                //SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(txtToDate.Text).ToString("dd/MM/yyyy") + "',103)  and MachineID ='" + MachineID + "'";


                SSQL = SSQL + " CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + OT_AttnDate + "',103)and MachineID ='" + MachineID + "'";

                DT_OTHrs = objdata.RptEmployeeMultipleDetails(SSQL);

                Tot_OT_Hrs = DT_OTHrs.Rows[0]["Tot_OT_Hrs"].ToString();

                if (Convert.ToDecimal(Tot_OT_Hrs) != 0)
                {
                    OT_Amt = (Convert.ToDecimal(PerHr) * (Convert.ToDecimal(Tot_OT_Hrs))).ToString();
                }
                else
                {
                    OT_Amt = "0";
                }

                OT_Amt = (Math.Round(Convert.ToDecimal(OT_Amt), 1, MidpointRounding.AwayFromZero)).ToString();
                SSQL = "";
                SSQL = "Delete from  [" + Session["SessionEpay"] + "].. OT_Amt where MachineID='" + MachineID + "' and OT_AttenDate ='" + OT_AttnDate + "'";

                objdata.RptEmployeeMultipleDetails(SSQL);

                if (Convert.ToDecimal(OT_Amt) != 0)
                {

                    SSQL = "";
                    SSQL = "insert into [" + Session["SessionEpay"] + "].. OT_Amt(Ccode,Lcode,MachineID,EmpName,OT_AttenDate,Tot_OTHrs,Amount,Oneday_Wages,OT_Wages,WH,Wages,Dept)";
                    SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID + "',";
                    SSQL = SSQL + "'" + Name + "','" + OT_AttnDate + "','" + Tot_OT_Hrs + "','" + OT_Amt + "','" + OneDay + "','" + OTWages + "','" + WH + "','" + ddlEmployeeType.SelectedItem.Text + "','" + DeptName + "') ";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                if (ddlEmployeeType.SelectedItem.Text != "VOUCHER")
                {
                    OT_Inc_Calculation();
                }
                Session.Remove("MachineID");
                Session.Remove("Name");
            }
        }
    }

    private void OT_Inc_Calculation()
    {

        //OT incentive Calculation
        DataTable DT_EmpType = new DataTable();
        DataTable DT_IncMaster = new DataTable();
        DataTable DT_Emp1 = new DataTable();
        DataTable DT_Four = new DataTable();
        DataTable DT_Eight = new DataTable();
        DataTable DT_WH = new DataTable();
        DataTable DT_Hrs = new DataTable();
        DataTable DT_InOut = new DataTable();
        DateTime DateIN = new DateTime();
        DateTime DateOUT = new DateTime();
        DataTable FixAmt = new DataTable();

        string Four_Inc = "0";
        string Eight_Inc = "0";
        string Emp_categ = "";
        string Emp_Type = "";
        string Inc_Amt = "";
        string FixWHAmt = "";
        string FourAmt = "";
        string EightAmt = "";
        string Name = "";
        string Wages1 = "";
        string fourhrsamount = "";
        string Eighthrsamount = "";
        string Shift = "";
        string OTHrs = "";
        string date1 = "";
        string TimeIN = "";
        string TimeOUT = "";
        DateTime fixTimeIN;
        DateTime fixTimeEndIN;
        string EndOutDays = "";


        MachineID = Session["MachineID"].ToString();
        SSQL = "Select * from Employee_Mst where MachineID ='" + MachineID + "'";
        DT_Emp1 = objdata.RptEmployeeMultipleDetails(SSQL);

        Wages1 = DT_Emp1.Rows[0]["Wages"].ToString();
        Name = DT_Emp1.Rows[0]["FirstName"].ToString();
        SSQL = "";
        SSQL = "select * from MstEmployeeType where EmpType ='" + Wages1 + "'";
        DT_EmpType = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_EmpType.Rows.Count != 0)
        {
            Emp_categ = DT_EmpType.Rows[0]["EmpCategory"].ToString();
            Emp_Type = DT_EmpType.Rows[0]["EmpTypeCd"].ToString();
        }

        SSQL = "";
        SSQL = "select * from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where OT_Hrs >=4 and ";
        SSQL = SSQL + " CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)<= CONVERT(DATETIME,'" + Convert.ToDateTime(txtToDate.Text).ToString("dd/MM/yyyy") + "',103)  and MachineID ='" + MachineID + "'";
        DT_Hrs = objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT_Hrs.Rows.Count != 0)
        {
            for (int k = 0; k < DT_Hrs.Rows.Count; k++)
            {
                date1 = DT_Hrs.Rows[k]["Attn_Date_Str"].ToString();
                Shift = DT_Hrs.Rows[k]["Shift"].ToString();
                OTHrs = DT_Hrs.Rows[k]["OT_Hrs"].ToString();
                string chkOT = "0";

                if ((Convert.ToInt32(OTHrs) > 4) && (Convert.ToInt32(OTHrs) < 8) || (Convert.ToInt32(OTHrs) == 4))
                {
                    chkOT = "4-7";
                }

                if ((Convert.ToInt32(OTHrs) == 8) || (Convert.ToInt32(OTHrs) > 8))
                {
                    chkOT = "7:30 and Above";
                }

                SSQL = "";
                SSQL = "Select * from LogTime_Days where MachineID='" + MachineID + "'and Attn_Date_Str='" + date1 + "'";
                DT_InOut = objdata.RptEmployeeMultipleDetails(SSQL);

                if (DT_InOut.Rows.Count != 0)
                {
                    TimeIN = DT_InOut.Rows[0]["TimeIN"].ToString();
                    TimeOUT = DT_InOut.Rows[0]["TimeOUT"].ToString();
                }
                else
                {
                    TimeIN = "00:00";
                    TimeOUT = "00:00";
                }

                DateIN = Convert.ToDateTime(TimeIN);
                DateOUT = Convert.ToDateTime(TimeOUT);

                //SSQL = "";
                //SSQL = "Select * from [Raajco_Epay]..OT_Inc_Mst where Category='" + Emp_categ + "' and EmployeeType='" + Emp_Type + "' and Shift='" + Shift + "' and OT_Hrs='" + chkOT + "'";
                //DT_IncMaster = objdata.RptEmployeeMultipleDetails(SSQL);

                SSQL = "";
                SSQL = "Select * from [" + Session["SessionEpay"] + "]..OT_Inc_Mst where Category='" + Emp_categ + "' and EmployeeType='" + Emp_Type + "'and OT_Hrs='" + chkOT + "'";
                DT_IncMaster = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_IncMaster.Rows.Count != 0)
                {
                    for (int i = 0; i < DT_IncMaster.Rows.Count; i++)
                    {
                        string FixTimeIN1 = "";
                        string fixTimeEndIN1 = "";
                        FixTimeIN1 = DT_IncMaster.Rows[i]["Start_IN"].ToString();
                        fixTimeEndIN1 = DT_IncMaster.Rows[i]["End_IN"].ToString();
                        EndOutDays = DT_IncMaster.Rows[i]["End_Out_Days"].ToString();

                        fixTimeIN = Convert.ToDateTime(DT_IncMaster.Rows[i]["Start_IN"].ToString());
                        fixTimeEndIN = Convert.ToDateTime(DT_IncMaster.Rows[i]["End_IN"].ToString());
                        if (EndOutDays == "1")
                        {
                            fixTimeEndIN = System.Convert.ToDateTime(fixTimeEndIN).AddDays(1);
                        }

                        if ((Convert.ToDateTime(fixTimeIN) < Convert.ToDateTime(DateIN)) && (Convert.ToDateTime(DateIN) < Convert.ToDateTime(fixTimeEndIN)))
                        {

                            SSQL = "";
                            SSQL = "Select * from [" + Session["SessionEpay"] + "]..OT_Inc_Mst where Category='" + Emp_categ + "' and EmployeeType='" + Emp_Type + "'and OT_Hrs='" + chkOT + "' and Start_IN='" + FixTimeIN1 + "' and End_IN='" + fixTimeEndIN1 + "'";
                            FixAmt = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (FixAmt.Rows.Count != 0)
                            {
                                Inc_Amt = FixAmt.Rows[0]["Amount"].ToString();
                                FixWHAmt = FixAmt.Rows[0]["WH_Amt"].ToString();
                                if (FixWHAmt == "")
                                {
                                    FixWHAmt = "0";
                                }
                            }
                            else
                            {
                                Inc_Amt = "0";
                                FixWHAmt = "0";
                            }
                        }
                    }
                }

                if (DT_IncMaster.Rows.Count != 0)
                // for (int i = 0; i < DT_IncMaster.Rows.Count; i++)
                {
                    //Inc_Amt = DT_IncMaster.Rows[i]["Amount"].ToString();

                    if ((Convert.ToInt32(OTHrs) > 4) && (Convert.ToInt32(OTHrs) < 8) || (Convert.ToInt32(OTHrs) == 4))
                    {
                        //fourhrsamount = DT_IncMaster.Rows[0]["Amount"].ToString();
                        fourhrsamount = Inc_Amt;
                    }
                    else
                    {
                        fourhrsamount = "0";
                    }
                    //fourhrsamount = DT_IncMaster.Rows[0]["Amount"].ToString();
                    if ((Convert.ToInt32(OTHrs) == 8) || (Convert.ToInt32(OTHrs) > 8))
                    {
                        // Eighthrsamount = DT_IncMaster.Rows[0]["Amount"].ToString();
                        Eighthrsamount = Inc_Amt;
                    }
                    else
                    {
                        Eighthrsamount = "0";
                    }

                    SSQL = "";
                    SSQL = "select Count(Four_Hrs_Inc) as Four from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where Four_Hrs_inc='1' and ";
                    //  SSQL = SSQL + " CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + "CONVERT(DATETIME,Attn_Date_Str,103) = CONVERT(DATETIME,'" + date1 + "',103)  and MachineID ='" + MachineID + "'";
                    DT_Four = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (DT_Four.Rows.Count != 0)
                    {
                        Four_Inc = DT_Four.Rows[0]["Four"].ToString();
                    }
                    else
                    {
                        Four_Inc = "0";
                    }

                    FourAmt = (Convert.ToDecimal(Four_Inc) * Convert.ToDecimal(fourhrsamount)).ToString();

                    //FourAmt = (Convert.ToDecimal(Four_Inc) * 80).ToString();

                    SSQL = "";
                    SSQL = "select Count(Eight_Hrs_Inc) as Eight from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where Eight_Hrs_inc ='1' and ";
                    //SSQL = SSQL + " CONVERT(DATETIME,Attn_Date_Str,103)>= CONVERT(DATETIME,'" + Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy") + "',103)";
                    SSQL = SSQL + "CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + date1 + "',103)  and MachineID ='" + MachineID + "'";
                    DT_Eight = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_Eight.Rows.Count != 0)
                    {
                        Eight_Inc = DT_Eight.Rows[0]["Eight"].ToString();
                    }
                    else
                    {
                        Eight_Inc = "0";
                    }
                    EightAmt = (Convert.ToDecimal(Eight_Inc) * Convert.ToDecimal(Eighthrsamount)).ToString();
                }
                //Weekoff Inc

                SSQL = "";
                SSQL = "select Count(Weekoff) as WH from [" + Session["SessionEpay"] + "]..OT_Hrs_Mst where Weekoff ='1' and ";
                SSQL = SSQL + "CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + date1 + "',103)  and MachineID ='" + MachineID + "'";
                DT_WH = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_WH.Rows.Count != 0)
                {
                    WH = DT_WH.Rows[0]["WH"].ToString();
                    if (WH == "")
                    {
                        WH = "0";
                    }
                }

                //if(MachineID== "1501560" && date1 == "11/10/2020")
                //{
                //    string Stop = "";
                //}


                WH_Amt = (Convert.ToDecimal(FixWHAmt) * Convert.ToDecimal(WH)).ToString();
                DataTable DT_Check = new DataTable();
                SSQL = "";
                SSQL = "Select * from [" + Session["SessionEpay"] + "].. OT_Inc_Amt where ";
                SSQL = SSQL + "CONVERT(DATETIME,OT_AttenDate,103)= CONVERT(DATETIME,'" + date1 + "',103)  and MachineID ='" + MachineID + "'";
                DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_Check.Rows.Count != 0)
                {
                    SSQL = "Delete from [" + Session["SessionEpay"] + "].. OT_Inc_Amt where ";
                    SSQL = SSQL + "CONVERT(DATETIME,OT_AttenDate,103)= CONVERT(DATETIME,'" + date1 + "',103)  and MachineID ='" + MachineID + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
                SSQL = "";
                SSQL = "insert into [" + Session["SessionEpay"] + "].. OT_Inc_Amt(Ccode,Lcode,MachineID,EmpName,OT_AttenDate,Four_Hrs,Eight_Hrs,Four_Hrs_inc_Amount,Eight_Hrs_inc_Amount,WH,WH_Amt,OT_Hrs,TimeIN,TimeOUT)";
                SSQL = SSQL + "values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID + "',";
                SSQL = SSQL + "'" + Name + "','" + date1 + "', '" + Four_Inc + "','" + Eight_Inc + "','" + FourAmt + "','" + EightAmt + "','" + WH + "','" + WH_Amt + "','" + OTHrs + "','" + TimeIN + "','" + TimeOUT + "' ) ";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }
        }
    }
    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }

    protected void ddlMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMachineID.SelectedItem.Text != "-Select-")
        {
            SSQL = "";
            SSQL = "Select * from Employee_Mst where compCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " and (isActive='Yes') and machineID='" + ddlMachineID.SelectedItem.Text + "'";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                txtName.Text = dt.Rows[0]["FirstName"].ToString();
                txtTokenNo.Text = dt.Rows[0]["ExistingCode"].ToString();
            }
            else
            {
                txtTokenNo.Text = "";
                txtName.Text = "";
            }
        }
        else
        {
            txtTokenNo.Text = "";
            txtName.Text = "";
        }
    }
}