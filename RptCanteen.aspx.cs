﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class RptCanteen : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL = "";
    string FDate_Str = "";
    string ToDate_Str = "";
    string TknNo = "";
    string ReportName = "";

    DataTable dt = new DataTable();

    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            FDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();
            TknNo = Request.QueryString["TknNo"].ToString();
            ReportName = Request.QueryString["ReportName"].ToString();

            if (ReportName.ToString() == "Canteen Cumulative Report")
            {
                GetReport_Canteen_Cumulative();
            }
            if(ReportName.ToString()=="Canteen Employee Wise")
            {
                GetReport_Canteen_EmpWise();
            }
        }
    }

    private void GetReport_Canteen_EmpWise()
    {
        SSQL = "";
        SSQL = "Select EM.MachineID as EmpNo,EM.ExistingCode as Excode,EM.FirstName as EmpName,EM.DeptName,Convert(varchar,MD.Dates,103) as Date";
        SSQL = SSQL + ",SUM(CASE when MD.MealsType='BreakFast' then (isnull(MD.NoOfPerson,'0')) else '0' end) as BreakCnt";
        SSQL = SSQL + ",SUM(CASE when MD.MealsType='BreakFast' then (isnull(MD.Amount,'0')) else '0' end) as BreakAmt";
        SSQL = SSQL + ",SUM(CASE when MD.MealsType='Lunch' then (isnull(MD.NoOfPerson,'0')) else '0' end) as LunchCnt";
        SSQL = SSQL + ",SUM(CASE when MD.MealsType='Lunch' then (isnull(MD.Amount,'0')) else '0' end) as LunchAmt";
        SSQL = SSQL + ",SUM(CASE when MD.MealsType='Dinner' then (isnull(MD.NoOfPerson,'0')) else '0' end) as DinnerCnt";
        SSQL = SSQL + ",SUM(CASE when MD.MealsType='Dinner' then (isnull(MD.Amount,'0')) else '0' end) as DinnerAmt";
        SSQL = SSQL + ",SUM(isnull(MD.Amount,'0')) as TotalAmt";
        SSQL = SSQL + " from Employee_Mst EM inner join MealsDetails MD on EM.ExistingCode=MD.TknNo";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and Convert(datetime,Dates,103)>=Convert(datetime,'" + FDate_Str + "',103) and Convert(datetime,Dates,103)<=Convert(datetime,'" + ToDate_Str + "',103)";
        if (TknNo.ToString() != "")
        {
            SSQL = SSQL + " and EM.ExistingCode='" + TknNo + "'";
        }
        SSQL = SSQL + " group by EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DeptName,MD.Dates";
        SSQL = SSQL + " order by CAST(EM.ExistingCode as int),MD.dates asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            report.Load(Server.MapPath("crystal/Canteen_EmpWise.rpt"));
            SSQL = "Select CompName,(Add1+'-'+Add2+'-'+Pincode) as Address from Company_Mst Where CompCode='" + SessionCcode + "'";
            DataTable DT_For = new DataTable();
            DT_For = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_For.Rows.Count != 0)
            {
                report.DataDefinition.FormulaFields["Company"].Text = "'" + DT_For.Rows[0]["CompName"].ToString() + "'";
                report.DataDefinition.FormulaFields["Address"].Text = "'" + DT_For.Rows[0]["Address"].ToString() + "'";
            }

          
            report.Database.Tables[0].SetDataSource(dt);
            report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FDate_Str + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate_Str + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
        }
    }

    private void GetReport_Canteen_Cumulative()
    {
       

        SSQL = "";
        SSQL = "Select EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DeptName";
        SSQL = SSQL + ",(CASE when MD.MealsType='BreakFast' then SUM(isnull(MD.NoOfPerson,'0')) else '0' end) as BreakFast";
        SSQL = SSQL + ",(CASE when MD.MealsType='BreakFast' then SUM(isnull(MD.Amount,'0')) else '0' end) as BreakFastAmt";
        SSQL = SSQL + ",(CASE when MD.MealsType='Lunch' then SUM(isnull(MD.NoOfPerson,'0')) else '0' end) as Lunch";
        SSQL = SSQL + ",(CASE when MD.MealsType='Lunch' then SUM(isnull(MD.Amount,'0')) else '0' end) as LunchAmt";
        SSQL = SSQL + ",(CASE when MD.MealsType='Dinner' then SUM(isnull(MD.NoOfPerson,'0')) else '0' end) as Dinner";
        SSQL = SSQL + ",(CASE when MD.MealsType='Dinner' then SUM(isnull(MD.Amount,'0')) else '0' end) as DinnerAmt";
        SSQL = SSQL + " ,SUM(isnull(MD.Amount,'0')) as TotalAmt";
        SSQL = SSQL + " from Employee_Mst EM inner join MealsDetails MD on EM.ExistingCode=MD.TknNo";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " and Convert(datetime,Dates,103)>=Convert(datetime,'" + FDate_Str + "',103) and Convert(datetime,Dates,103)<=Convert(datetime,'" + ToDate_Str + "',103)";
        if (TknNo.ToString() != "")
        {
            SSQL = SSQL + " and EM.ExistingCode='" + TknNo + "'";
        }
        SSQL = SSQL + " group by EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DeptName,MD.MealsType";
        SSQL = SSQL + " order by CAST(EM.ExistingCode as int) asc";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            grid.DataSource = dt;
            grid.DataBind();

            string attachment = "attachment;filename=CANTEEN REPORT BETWEEN DAYS.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + dt.Columns.Count + "'>");
            Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + dt.Columns.Count + "'>");
            Response.Write("<a style=\"font-weight:bold\">CANTEEN REPORT CUMULATIVE-" + Session["Lcode"].ToString() + "-" + FDate_Str + "-" + ToDate_Str + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
        }
    }
}