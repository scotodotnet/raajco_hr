<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="EmployeeDetails.aspx.cs" Inherits="EmployeeDetails" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="assets/plugins/ModalPopup/CSS/Popup.css" rel="stylesheet" type="text/css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css" rel="stylesheet"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker3.css" rel="stylesheet"/>--%>

    <style>
        .well1 {
            padding: 15px;
            background: #eeeeee;
            box-shadow: none;
            -webkit-box-shadow: none;
        }

        .LabelColor {
            color: #116dca;
        }

        .BorderStyle {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }

        .select2 {
            border: 1px solid #5f656b;
            color: #020202;
            font-weight: bold;
        }
    </style>

    <!-- begin #content -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Employee Profile</a></li>
            <li class="active">Employee Details</li>
        </ol>

        <!-- end breadcrumb -->

        <!-- begin page-header -->
        <h1 class="page-header">Employee History</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <div>
                    <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-primary" ><i class="fa fa-search"></i></a>
                            </div>
                            <h4 class="panel-title">Employee Details</h4>
                        </div>
                        <div class="panel-body">
                            <div id="wizard">
                                <ol>
                                    <li>Basic
                                    </li>
                                    <li>Salary
                                    </li>
                                    <li>Personal
                                    </li>
                                    <li>General 
                                    </li>

                                    <li>Adolescent 
                                    </li>
                                    <li>Documents
                                    </li>
                                    <li>Others
                                    </li>
                                </ol>
                                <!-- begin wizard step-1 -->
                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">Basic</legend>
                                                <div class="col-md-12">
                                                    <div class="col-md-9">
                                                        <!-- begin row -->
                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Machine ID</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtMachineID" runat="server" class="form-control  BorderStyle"
                                                                        AutoPostBack="true" OnTextChanged="txtMachineID_TextChanged"></asp:TextBox>
                                                                    <%--  <asp:RequiredFieldValidator ControlToValidate="txtMachineID" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <%-- <label class="LabelColor">Existing Number</label><span class="mandatory">*</span>--%>
                                                                    <label class="LabelColor">Emp.Code</label><span class="mandatory">*</span>

                                                                    <asp:TextBox ID="txtExistingCode" runat="server" class="form-control  BorderStyle" OnTextChanged="txtExistingCode_TextChanged" AutoPostBack="true"></asp:TextBox>
                                                                    <%--  <asp:RequiredFieldValidator ControlToValidate="txtExistingCode" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Division</label>
                                                                    <asp:DropDownList ID="ddlDivision" runat="server" class="form-control  BorderStyle select2" Style="width: 100%">
                                                                    </asp:DropDownList>
                                                                    <%--  <asp:RequiredFieldValidator ControlToValidate="ddlDivision" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator30" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->

                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4" runat="server" id="divTicketNo" visible="false">
                                                                <div class="form-group block1">
                                                                    <label class="LabelColor">Ticket Number</label><span class="mandatory">*</span>
                                                                    <asp:TextBox ID="txtTokenID" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>
                                                        <div class="row">
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Category</label><span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="STAFF" Text="STAFF"></asp:ListItem>
                                                                        <asp:ListItem Value="LABOUR" Text="LABOUR"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--  <asp:RequiredFieldValidator ControlToValidate="ddlCategory" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Sub Category</label><span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlSubCategory" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="INSIDER" Text="INSIDER"></asp:ListItem>
                                                                        <asp:ListItem Value="OUTSIDER" Text="OUTSIDER"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--   <asp:RequiredFieldValidator ControlToValidate="ddlSubCategory" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                            <!-- begin col-4 -->
                                                            <div class="col-md-4">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Shift</label><span class="mandatory">*</span>
                                                                    <asp:DropDownList ID="ddlShift" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                        <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                        <asp:ListItem Value="GENERAL" Text="GENERAL"></asp:ListItem>
                                                                        <asp:ListItem Value="SHIFT" Text="SHIFT"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                    <%--  <asp:RequiredFieldValidator ControlToValidate="ddlShift" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                </div>
                                                            </div>
                                                            <!-- end col-4 -->
                                                        </div>

                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="media" style="margin-top: -19px;">
                                                            <a class="media-right" href="javascript:;" style="float: right;">
                                                                <asp:Image ID="Image3" runat="server" class="media-object" Style="width: 158px; height: 161px;" Visible="false" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                                <img id="img1" alt="" height="100%" width="100%" />
                                                            </a>
                                                            <asp:FileUpload ID="FileUpload1" runat="server" onchange="showimagepreview(this)" />
                                                            <%--<asp:Button ID="btnUpload" Text="Upload" runat="server" OnClick="Upload" Style="display: none" />--%>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group block1">
                                                                <label class="LabelColor">First Name</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtFirstName" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                <%--  <asp:RequiredFieldValidator ControlToValidate="txtFirstName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Initial /Last Name</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtLastName" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                <%--<asp:RequiredFieldValidator ControlToValidate="txtLastName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Date of Birth</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtDOB" runat="server" class="form-control BorderStyle datepicker"
                                                                    placeholder="dd/MM/YYYY" AutoPostBack="true"
                                                                    OnTextChanged="txtDOB_TextChanged"></asp:TextBox>
                                                                <%--  <asp:RequiredFieldValidator ControlToValidate="txtDOB" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                            </asp:RequiredFieldValidator>--%>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtDOB" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>
                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Age</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtAge" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group block1">
                                                                <label class="LabelColor">Gender</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlGender" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                                                                    <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%-- <asp:RequiredFieldValidator ControlToValidate="ddlGender" InitialValue="-Select-" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Date of Join</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtDOJ" runat="server" class="form-control  BorderStyle datepicker" AutoPostBack="true" OnTextChanged="txtDOJ_TextChanged" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                <%--  <asp:RequiredFieldValidator ControlToValidate="txtDOJ" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator10" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtDOJ" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Department</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlDepartment" runat="server"
                                                                    class="form-control  BorderStyle select2" Style="width: 100%;"
                                                                    OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged" AutoPostBack="true">
                                                                </asp:DropDownList>
                                                                <%-- <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" Display="Dynamic" InitialValue="0" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator11" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>

                                                        </div>
                                                        <!-- end col-4 -->

                                                    </div>
                                                    <div class="row">

                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Designation</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlDesignation" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                </asp:DropDownList>
                                                                <%--<asp:RequiredFieldValidator ControlToValidate="ddlDesignation" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->

                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <div class="form-group">
                                                                    <label class="LabelColor">Qualification</label><span class="mandatory">*</span>
                                                                    <%--<asp:TextBox ID="txtQulification" runat="server" class="form-control  BorderStyle">
                                                            </asp:TextBox>--%>
                                                                    <asp:DropDownList ID="txtQulification" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Emp MobileNo</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtEmpMobileNo" runat="server" class="form-control  BorderStyle" MaxLength="10"></asp:TextBox>
                                                                <%-- <asp:RequiredFieldValidator ControlToValidate="txtEmpMobileNo" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator13" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtEmpMobileNo" ValidChars="0123456789">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">OT Eligible</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlOTEligible" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="Yes" Text="Yes"></asp:ListItem>
                                                                    <asp:ListItem Value="No" Text="No"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%-- <asp:RequiredFieldValidator ControlToValidate="ddlOTEligible" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator14" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Wages Type</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlWagesType" runat="server" class="form-control  BorderStyle select2"
                                                                    Style="width: 100%;" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlWagesType_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:RequiredFieldValidator ControlToValidate="ddlWagesType" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator15" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                                </asp:RequiredFieldValidator>
                                                            </div>

                                                        </div>
                                                        <!-- end col-2 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Employee Level</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlEmpLevel" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                    <asp:ListItem Value="Trainee">Trainee</asp:ListItem>
                                                                    <asp:ListItem Value="SemiSkilled">SemiSkilled</asp:ListItem>
                                                                    <asp:ListItem Value="Skilled">Skilled</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%--<asp:RequiredFieldValidator ControlToValidate="ddlEmpLevel" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator32" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>

                                                        </div>
                                                        <!-- end col-2 -->
                                                    </div>

                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4" runat="server" id="IF_PF_Eligible">
                                                            <div class="form-group">
                                                                <label class="LabelColor">PF Eligible</label><span class="mandatory">*</span>
                                                                <asp:RadioButtonList ID="RdbPFEligible" runat="server" class="form-control  BorderStyle"
                                                                    RepeatColumns="2" AutoPostBack="true" OnSelectedIndexChanged="RdbPFEligible_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>

                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">PF No</label>
                                                                <asp:TextBox ID="txtPFNo" runat="server" class="form-control  BorderStyle" Enabled="false"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender30" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtPFNo" ValidChars="0123456789">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group block1">
                                                                <label class="LabelColor">PF Date</label>
                                                                <asp:TextBox ID="txtPFDate" runat="server" class="form-control  BorderStyle datepicker" Enabled="false" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtPFDate" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>
                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4" runat="server" id="IF_ESI_Eligible">
                                                            <div class="form-group">
                                                                <label class="LabelColor">ESI Eligible</label><span class="mandatory">*</span>
                                                                <asp:RadioButtonList ID="RdbESIEligible" runat="server" class="form-control  BorderStyle"
                                                                    RepeatColumns="2" AutoPostBack="true" OnSelectedIndexChanged="RdbESIEligible_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                </asp:RadioButtonList>

                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">ESI No</label>
                                                                <asp:TextBox ID="txtESINo" runat="server" class="form-control  BorderStyle" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">ESI Date</label>
                                                                <asp:TextBox ID="txtESIDate" runat="server" class="form-control  BorderStyle datepicker" Enabled="false" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtESIDate" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>
                                                    <div class="row">

                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">UAN</label>
                                                                <asp:TextBox ID="txtUAN" runat="server" class="form-control  BorderStyle"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">PF Code</label>
                                                                <asp:DropDownList ID="ddlPFCode" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                    <asp:ListItem Value="0" Text="MDMDU0029249"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">ESI Code</label>
                                                                <asp:DropDownList ID="ddlESICode" runat="server" class="form-control  BorderStyle select2" Style="width: 100%;">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>
                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Hostel Room No</label>
                                                                <asp:TextBox ID="txtHostelRoom" runat="server" class="form-control  BorderStyle"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->

                                                        <!-- begin col-4 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Vehicle Type</label>
                                                                <asp:DropDownList ID="ddlVehicleType" runat="server"
                                                                    class="form-control  BorderStyle select2" Style="width: 100%" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlVehicleType_SelectedIndexChanged">
                                                                    <%--   <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								                       <asp:ListItem Value="Company Bus">Company Bus</asp:ListItem>
								                       <asp:ListItem Value="Hired Vechile">Hired Vechile</asp:ListItem>
								                       <asp:ListItem Value="On Their Own">On Their Own</asp:ListItem>
								                       <asp:ListItem Value="By Walk">By Walk</asp:ListItem>--%>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Bus No</label>
                                                                <asp:DropDownList ID="txtBusNo" runat="server" class="form-control  BorderStyle select2" Style="width: 100%"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="txtBusNo_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Route</label>
                                                                <asp:DropDownList ID="txtVillage" runat="server" class="form-control  BorderStyle select2"
                                                                    Style="width: 100%">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->


                                                    </div>

                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Skill Level</label>
                                                                <asp:DropDownList ID="ddlSkill" runat="server" class="form-control  BorderStyle select2"
                                                                    Style="width: 100%">
                                                                    <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="SKILLED" Text="SKILLED"></asp:ListItem>
                                                                    <asp:ListItem Value="UN SKILLED" Text="UN SKILLED"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Active Mode</label>
                                                                <asp:RadioButtonList ID="dbtnActive" runat="server" class="form-control  BorderStyle"
                                                                    RepeatDirection="Horizontal" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="dbtnActive_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px" Selected="True"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="No"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Relieved Date</label>
                                                                <asp:TextBox ID="txtReliveDate" runat="server" class="form-control  BorderStyle datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtReliveDate" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Reason for Relieving</label>
                                                                <asp:TextBox ID="txtReason" runat="server" class="form-control BorderStyle" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>

                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">480 Days Completed</label>
                                                                <asp:TextBox ID="txt480Days" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Week - Off</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlWeekOff" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="Sunday" Text="Sunday"></asp:ListItem>
                                                                    <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                                                                    <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                                                                    <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                                                                    <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                                                                    <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                                                                    <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                                                                    <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%--  <asp:RequiredFieldValidator ControlToValidate="ddlWeekOff" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator19" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Form I Obtained Date  </label>
                                                                <asp:CheckBox ID="chkExment" runat="server"
                                                                    AutoPostBack="true" OnCheckedChanged="chkExment_CheckedChanged" />
                                                                <span class="LabelColor">Exempted Staff </span>
                                                                <asp:TextBox ID="txtFormIDate" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY" Enabled="false"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender26" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtFormIDate" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>
                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Work Type</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlWorkType" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="1" Text="Experience"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="Fresher"></asp:ListItem>
                                                                </asp:DropDownList>

                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Level</label>
                                                                <asp:DropDownList ID="ddlHostelExp" runat="server"
                                                                    class="form-control BorderStyle select2" Style="width: 100%">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Insurance Join Date </label>
                                                                <asp:CheckBox ID="ChkInsurance_Elgbl" runat="server"
                                                                    AutoPostBack="true" OnCheckedChanged="ChkInsurance_Elgbl_CheckedChanged" />
                                                                <span class="LabelColor">Eligible Insurance</span>
                                                                <asp:TextBox ID="txtInssurance_Joining_Date" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY" Enabled="false"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender31" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtInssurance_Joining_Date" ValidChars="0123456789/">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Shift Category<span class="mandatory">*</span></label>
                                                                <asp:DropDownList ID="ddlShiftCat" runat="server" class="form-control  BorderStyle select2" Style="width: 100%"
                                                                    AutoPostBack="true" OnSelectedIndexChanged="ddlShiftCat_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                    </div>

                                                </div>

                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- end wizard step-1 -->

                                <!-- begin wizard step-2 -->
                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">Bank/Cash</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-3 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Salary Through</label><span class="mandatory">*</span>
                                                            <asp:RadioButtonList ID="rbtnSalaryThrough" runat="server"
                                                                RepeatDirection="Horizontal" class="form-control BorderStyle" AutoPostBack="true"
                                                                OnSelectedIndexChanged="rbtnSalaryThrough_SelectedIndexChanged">
                                                                <asp:ListItem Value="1" Text="Cash" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Value="2" Text="Bank"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-3 -->
                                                    <!-- begin col-3 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Bank Name</label>
                                                            <asp:DropDownList ID="ddlBankName" Enabled="false" runat="server" class="form-control BorderStyle select2" Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlBankName_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-3 -->
                                                    <!-- begin col-3 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">IFSC Code</label>
                                                            <asp:TextBox ID="txtIFSC" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-3 -->
                                                    <!-- begin col-3 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Branch</label>
                                                            <asp:TextBox ID="txtBranch" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <!-- end col-3 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-3 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Account Number</label><span class="mandatory">*</span>
                                                            <asp:TextBox ID="txtAccNo" runat="server" Enabled="false" class="form-control BorderStyle"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <!-- end col-3 -->
                                                </div>
                                                <!-- end row -->
                                                <legend class="pull-left width-full">Fixed Salary</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Based On</label>
                                                            <asp:RadioButtonList ID="rbtnBasedOn" RepeatColumns="2" RepeatDirection="Horizontal" class="form-control BorderStyle" runat="server" AutoPostBack="true" OnSelectedIndexChanged="rbtnBasedOn_SelectedIndexChanged">
                                                                <asp:ListItem Text="DA" Value="0" style="padding-right: 40px" Selected="True"></asp:ListItem>
                                                                <asp:ListItem Text="VDA" Value="1"></asp:ListItem>
                                                            </asp:RadioButtonList>
                                                        </div>
                                                    </div>
                                                    <!-- End col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">DA Point</label>
                                                            <asp:TextBox ID="txtDaPoint" runat="server" class="form-control BorderStyle" Text="0"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- End col-4 -->
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Basic Salary</label>
                                                            <asp:TextBox ID="txtBasic" runat="server" class="form-control BorderStyle" Text="0.0" Enabled="false"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtBasic" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Fixed DA</label>
                                                            <asp:TextBox ID="txtFixed" runat="server" class="form-control BorderStyle" Text="0"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- End col-4 -->
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">VPF</label>
                                                            <asp:TextBox ID="txtVPF" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtVPF" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2" runat="server" id="divAll1" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Allowance 1</label>
                                                            <asp:TextBox ID="txtAllowance1" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtAllowance1" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                   
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2" runat="server" id="divDed1" visible="true">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Deduction 1</label>
                                                            <asp:TextBox ID="txtDeduction1" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDeduction1" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                     <!-- begin col-2 -->
                                                    <div class="col-md-2" runat="server" id="div1" visible="true">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Basic</label>
                                                            <asp:TextBox ID="txtFBasic" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender32" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtFBasic" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" runat="server" id="div2" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">DA</label>
                                                            <asp:TextBox ID="txtDA" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDA" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                     <div class="col-md-2" runat="server" id="div3" visible="true">
                                                        <div class="form-group">
                                                            <label class="LabelColor">HRA</label>
                                                            <asp:TextBox ID="txtHRA" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender34" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtHRA" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" runat="server" id="div4" visible="true">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Conveyance</label>
                                                            <asp:TextBox ID="txtConveyance" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender35" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtConveyance" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" runat="server" id="div5" visible="true">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Washing Allowance</label>
                                                            <asp:TextBox ID="txtWashing" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender36" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtWashing" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2" runat="server" id="div6" visible="true">
                                                        <div class="form-group">
                                                            <label class="LabelColor">LTA</label>
                                                            <asp:TextBox ID="txtLTA" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender37" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLTA" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>  
                                                     <!-- begin col-2 -->
                                                    <div class="col-md-2" runat="server" id="divAll2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Other Allowance (All2)</label>
                                                            <asp:TextBox ID="txtAllowance2" runat="server" class="form-control BorderStyle" Text="0.0" ></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtAllowance2" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                    <!-- end col-2 -->
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2" runat="server" id="divDed2" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Deduction 2</label>
                                                            <asp:TextBox ID="txtDeduction2" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtDeduction2" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2" runat="server" id="divOTSal" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">OT Salary</label>
                                                            <asp:TextBox ID="txtOTSal" runat="server" class="form-control BorderStyle" Text="0.0"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtOTSal" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-2 -->
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- end wizard step-2 -->
                                <!-- begin wizard step-3 -->
                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">Personal</legend>
                                                <div class="col-md-9">

                                                    <!-- begin row -->
                                                    <div class="row">
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Martial Status</label><span class="mandatory">*</span>
                                                                <asp:DropDownList ID="ddlMartialStatus" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="UN-MARRIED" Text="UN-MARRIED"></asp:ListItem>
                                                                    <asp:ListItem Value="MARRIED" Text="MARRIED"></asp:ListItem>
                                                                    <asp:ListItem Value="Divorced" Text="Divorced"></asp:ListItem>
                                                                    <asp:ListItem Value="Widowed" Text="Widowed"></asp:ListItem>
                                                                    <asp:ListItem Value="Separated" Text="Separated"></asp:ListItem>
                                                                    <asp:ListItem Value="None" Text="None"></asp:ListItem>
                                                                </asp:DropDownList>
                                                                <%-- <asp:RequiredFieldValidator ControlToValidate="ddlMartialStatus" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator16" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>--%>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Nationality</label>
                                                                <asp:TextBox ID="txtNationality" runat="server" Text="INDIAN" class="form-control BorderStyle">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-4 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Religion</label>
                                                                <asp:TextBox ID="txtReligion" runat="server" class="form-control BorderStyle">
                                                                </asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-4 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Height</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Cms</span>
                                                                    <asp:TextBox ID="txtHeight" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtHeight" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- end col-2 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Weight</label>
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Kg</span>
                                                                    <asp:TextBox ID="txtWeight" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                        TargetControlID="txtWeight" ValidChars="0123456789.">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>


                                                    <!-- end row -->
                                                    <!-- begin row -->
                                                    <div class="row">
                                                        <!-- begin col-3 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Community</label>
                                                                <asp:DropDownList ID="ddlCommunity" runat="server" class="form-control BorderStyle select2" Style="width: 100%;">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <!-- end col-3 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Std Working Hrs</label><span class="mandatory">*</span>
                                                                <asp:TextBox ID="txtStdWorkingHrs" runat="server" Text="0" class="form-control BorderStyle"></asp:TextBox>
                                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                    TargetControlID="txtStdWorkingHrs" ValidChars="0123456789">
                                                                </cc1:FilteredTextBoxExtender>
                                                            </div>
                                                        </div>
                                                        <!-- end col-2 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Physically Challenged</label>
                                                                <asp:RadioButtonList ID="rbtnPhysically" runat="server" class="form-control BorderStyle"
                                                                    RepeatDirection="Horizontal" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="rbtnPhysically_SelectedIndexChanged">
                                                                    <asp:ListItem Value="1" Text="Yes" style="padding-right: 40px"></asp:ListItem>
                                                                    <asp:ListItem Value="2" Text="No" Selected="True"></asp:ListItem>
                                                                </asp:RadioButtonList>

                                                            </div>
                                                        </div>
                                                        <!-- end col-2 -->
                                                        <!-- begin col-2 -->
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Physically Reason</label>
                                                                <asp:TextBox ID="txtPhyReason" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <!-- end col-2 -->
                                                    </div>
                                                    <!-- end row -->
                                                </div>
                                                <div class="col-md-3" id="PersonImg" runat="server" visible="false">
                                                    <div class="media" style="margin-top: -19px;">
                                                        <a class="media-right" href="javascript:;" style="float: right;">
                                                            <asp:Image ID="Image1" runat="server" class="media-object" Style="width: 158px; height: 161px;" Visible="false" ImageUrl="~/assets/img/login-bg/man-user-50.png" />
                                                            <img id="img2" alt="" height="100%" width="100%" />
                                                        </a>
                                                        <asp:FileUpload ID="FileUpload2" runat="server" onchange="showimagepreview2(this)" />
                                                    </div>
                                                </div>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Blood Group</label><span class="mandatory">*</span><br />
                                                            <asp:DropDownList ID="ddlBloodGrp" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                <asp:ListItem Value="-Select-" Text="-Select-"></asp:ListItem>
                                                                <asp:ListItem Value="A+" Text="A+"></asp:ListItem>
                                                                <asp:ListItem Value="A-" Text="A-"></asp:ListItem>
                                                                <asp:ListItem Value="B+" Text="B+"></asp:ListItem>
                                                                <asp:ListItem Value="B-" Text="B-"></asp:ListItem>
                                                                <asp:ListItem Value="AB+" Text="AB+"></asp:ListItem>
                                                                <asp:ListItem Value="AB-" Text="AB-"></asp:ListItem>
                                                                <asp:ListItem Value="O+" Text="O+"></asp:ListItem>
                                                                <asp:ListItem Value="O-" Text="O-"></asp:ListItem>
                                                                <asp:ListItem Value="NONE" Text="NONE"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="ddlBloodGrp" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator17" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Recruitment Through</label><span class="mandatory">*</span>
                                                            <asp:DropDownList ID="txtRecruitThrg" runat="server"
                                                                class="form-control BorderStyle select2" Style="width: 100%" AutoPostBack="true"
                                                                OnSelectedIndexChanged="txtRecruitThrg_SelectedIndexChanged">
                                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                <asp:ListItem Value="Recruitment Officer">Recruitment Officer</asp:ListItem>
                                                                <asp:ListItem Value="Existing Employee">Existing Employee</asp:ListItem>
                                                                <asp:ListItem Value="Direct">Direct</asp:ListItem>
                                                                <asp:ListItem Value="ReJoin">ReJoin</asp:ListItem>
                                                                <asp:ListItem Value="Transfer">Transfer</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="ddlBloodGrp" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator18" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label runat="server" id="lblRecruit">Recruiter Name</label>
                                                            <label runat="server" id="lblAgent" visible="false">Agent Name</label>
                                                            <asp:DropDownList ID="txtRecruitmentName" runat="server"
                                                                class="form-control BorderStyle select2" AutoPostBack="true" Style="width: 100%"
                                                                OnSelectedIndexChanged="txtRecruitmentName_SelectedIndexChanged">
                                                            </asp:DropDownList>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->

                                                <!-- begin row -->
                                                <div class="row">

                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Unit</label>
                                                            <asp:DropDownList ID="ddlUnit" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Existing Employee No</label>
                                                            <asp:TextBox ID="txtExistingEmpNo" runat="server" class="form-control BorderStyle" AutoPostBack="true"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Recruiter Mobile</label>
                                                            <asp:TextBox ID="txtRecruitMobile" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Existing Employee Name</label>
                                                            <asp:TextBox ID="txtExistingEmpName" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Referal Type</label>
                                                            <asp:DropDownList ID="txtRefType" runat="server" class="form-control BorderStyle select2"
                                                                Style="width: 100%" AutoPostBack="true"
                                                                OnSelectedIndexChanged="txtRefType_SelectedIndexChanged">
                                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                <asp:ListItem Value="Agent">Agent</asp:ListItem>
                                                                <asp:ListItem Value="Parent">Parent</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">


                                                    <!-- begin col-3 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Agent Name</label>
                                                            <asp:DropDownList ID="txtAgentName" runat="server" class="form-control BorderStyle select2"
                                                                Style="width: 100%" Enabled="false" AutoPostBack="true"
                                                                OnSelectedIndexChanged="txtAgentName_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-3 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Parents Name</label>
                                                            <asp:TextBox ID="txtRefParentsName" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Mobile No</label>
                                                            <asp:TextBox ID="txtRefMobileNo" runat="server" class="form-control BorderStyle" MaxLength="10" Enabled="false"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtRefMobileNo" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Commission Amount</label>
                                                            <asp:TextBox ID="txtCommissionAmt" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender29" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtCommissionAmt" ValidChars="0123456789.">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Working Unit</label>
                                                            <asp:DropDownList ID="ddlWorkingUnit" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Salary Unit</label>
                                                            <asp:DropDownList ID="ddlSalaryUnit" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Grade</label>
                                                            <asp:DropDownList ID="ddlGrade" runat="server" class="form-control BorderStyle select2" Style="width: 100%">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->

                                                <!-- begin row -->
                                                <div class="row">


                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Leave From1</label>
                                                            <asp:TextBox ID="txtLeaveFrom" runat="server" class="form-control BorderStyle datepicker"
                                                                placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLeaveFrom" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Leave To1</label>
                                                            <asp:TextBox ID="txtLeaveTo" runat="server" class="form-control BorderStyle datepicker"
                                                                placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLeaveTo" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Festival</label>
                                                            <asp:TextBox ID="txtFestival1" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">LeaveDays</label>
                                                            <asp:TextBox ID="txtLeaveDays1" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->

                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Leave From2</label>
                                                            <asp:TextBox ID="txtLeaveFrom2" runat="server" class="form-control BorderStyle datepicker"
                                                                placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender27" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLeaveFrom2" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Leave To2</label>
                                                            <asp:TextBox ID="txtLeaveTo2" runat="server" class="form-control BorderStyle datepicker"
                                                                placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtLeaveTo2" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Festival</label>
                                                            <asp:TextBox ID="txtFestival2" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">LeaveDays</label>
                                                            <asp:TextBox ID="txtLeaveDays2" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->

                                                <div class="row">

                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Certificate</label>
                                                            <asp:TextBox ID="txtCertificate" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Eye Sight</label>

                                                        </div>
                                                        <%-- </div>--%>
                                                        <%-- <div class="col-md-2">--%>
                                                        <%--  <div class="form-group">--%>
                                                        <asp:CheckBox ID="chkNormal" runat="server" AutoPostBack="True" OnCheckedChanged="chkNormal_CheckedChanged" />
                                                        Normal
                                                  
                                                        <%-- </div>--%>
                                                        <%--   <div class="form-group">--%>
                                                        <asp:CheckBox ID="ChkShort" runat="server" AutoPostBack="True" OnCheckedChanged="ChkShort_CheckedChanged" />
                                                        Short Sight
                                                  
                                                        <%-- </div>
                                                    <div class="form-group">--%>
                                                        <asp:CheckBox ID="chkLong" runat="server" AutoPostBack="True" OnCheckedChanged="chkLong_CheckedChanged" />
                                                        Long Sight
                                                   
                                                        <%--</div>--%>
                                                    </div>

                                                </div>

                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- end wizard step-3 -->
                                <!-- begin wizard step-4 -->
                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">General</legend>
                                                <!-- begin row -->
                                                <div class="row">

                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Nominee</label>
                                                            <asp:TextBox ID="txtNominee" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="txtNominee" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator31" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>
                                                            --%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Nominee Relationship</label>
                                                            <asp:TextBox ID="txtNomineeRelation" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtNominee" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator32" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                   </asp:RequiredFieldValidator>
                                                            --%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Father Name/Spouse Name</label>
                                                            <asp:TextBox ID="txtFatherName" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%--<asp:RequiredFieldValidator ControlToValidate="txtFatherName" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator34" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Parents Mobile1</label>
                                                            <asp:TextBox ID="txtParentMob1" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="txtParentMob1" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator20" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtParentMob1" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">

                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Mother Name</label>
                                                            <asp:TextBox ID="txtMotherName" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtMotherName" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator35" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Parents Mobile2</label>
                                                            <asp:TextBox ID="txtParentMob2" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtParentMob2" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Guardian Name</label>
                                                            <asp:TextBox ID="txtGuardianName" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->


                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">

                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Guardian Mobile</label>
                                                            <asp:TextBox ID="txtGuardianMobile" runat="server" class="form-control BorderStyle" MaxLength="10"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtGuardianMobile" ValidChars="0123456789">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Permanent Address</label>
                                                            <asp:TextBox ID="txtPermAddr" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="txtPermAddr" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator21" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Permanent Taluk</label>
                                                            <asp:DropDownList ID="txtPermTaluk" runat="server" class="form-control BorderStyle select2" Style="width: 100%"></asp:DropDownList>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtPermTaluk" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator22" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">

                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Permanent District</label>
                                                            <asp:DropDownList ID="txtPermDist" runat="server" class="form-control BorderStyle select2" Style="width: 100%"></asp:DropDownList>
                                                            <%--  <asp:RequiredFieldValidator ControlToValidate="txtPermDist" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator23" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">State  </label>
                                                            <asp:CheckBox ID="chkOtherState" runat="server" Visible="false" />
                                                            <%--OtherState--%>
                                                            <asp:DropDownList ID="ddlState" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                <%--<asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                        <asp:ListItem Value="1" Text="Andhra Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="2" Text="Arunachal Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="3" Text="Assam"></asp:ListItem>
                                                        <asp:ListItem Value="4" Text="Bihar"></asp:ListItem>
                                                        <asp:ListItem Value="5" Text="Chhattisgarh"></asp:ListItem>
                                                        <asp:ListItem Value="6" Text="Goa"></asp:ListItem>
                                                        <asp:ListItem Value="7" Text="Gujarat"></asp:ListItem>
                                                        <asp:ListItem Value="8" Text="Haryana"></asp:ListItem>
                                                        <asp:ListItem Value="9" Text="Himachal Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="10" Text="Jammu and Kashmir"></asp:ListItem>
                                                        <asp:ListItem Value="11 Text="Jharkhand"></asp:ListItem>
                                                        <asp:ListItem Value="12" Text="Karnataka"></asp:ListItem>
                                                        <asp:ListItem Value="13" Text="Kerala"></asp:ListItem>
                                                        <asp:ListItem Value="14" Text="Madhya Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="15" Text="Maharashtra"></asp:ListItem>
                                                        <asp:ListItem Value="16" Text="Manipur"></asp:ListItem>
                                                        <asp:ListItem Value="17" Text="Meghalaya"></asp:ListItem>
                                                        <asp:ListItem Value="18 Text="Mizoram"></asp:ListItem>
                                                        <asp:ListItem Value="19" Text="Nagaland"></asp:ListItem>
                                                        <asp:ListItem Value="20" Text="Orissa"></asp:ListItem>
                                                        <asp:ListItem Value="21" Text="Punjab"></asp:ListItem>
                                                        <asp:ListItem Value="22" Text="Rajasthan"></asp:ListItem>
                                                        <asp:ListItem Value="23" Text="Sikkim"></asp:ListItem>
                                                        <asp:ListItem Value="24" Text="Tamil Nadu"></asp:ListItem>
                                                        <asp:ListItem Value="25 Text="Telangana"></asp:ListItem>
                                                        <asp:ListItem Value="26" Text="Tripura"></asp:ListItem>
                                                        <asp:ListItem Value="27" Text="Uttar Pradesh"></asp:ListItem>
                                                        <asp:ListItem Value="28" Text="Uttarakhand"></asp:ListItem>
                                                        <asp:ListItem Value="29" Text="West Bengal"></asp:ListItem>--%>
                                                            </asp:DropDownList>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="ddlState" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator24" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- begin col-4 -->
                                                    <%--   <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="LabelColor">Temp Taluk</label>
                                                    <asp:DropDownList ID="txtTempTaluk" runat="server" class="form-control BorderStyle select2" style="width:100%"></asp:DropDownList>
                                                   <%-- <asp:RequiredFieldValidator ControlToValidate="txtTempTaluk" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator25" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                    <%--     </div>
                                            </div>--%>
                                                    <!-- end col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Stay</label>
                                                            <asp:DropDownList ID="ddlStay" runat="server" class="form-control BorderStyle select2"
                                                                Style="width: 100%">
                                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                <asp:ListItem Value="Own House">Own House</asp:ListItem>
                                                                <asp:ListItem Value="Rented House">Rented house</asp:ListItem>
                                                                <asp:ListItem Value="Hostel">Hostel</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>


                                                </div>
                                                <!-- end row -->

                                                <!-- begin row -->
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Temp Address</label>
                                                            <asp:CheckBox ID="chkSame" runat="server"
                                                                AutoPostBack="true" OnCheckedChanged="chkSame_CheckedChanged" />Same as Permanent
                                                   
                                                            <asp:TextBox ID="txtTempAddr" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtTempAddr" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator27" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Temp Taluk</label>
                                                            <asp:DropDownList ID="txtTempTaluk" runat="server" class="form-control BorderStyle select2" Style="width: 100%"></asp:DropDownList>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtTempTaluk" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator25" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Temp District</label>
                                                            <asp:DropDownList ID="txtTempDist" runat="server" class="form-control BorderStyle select2" Style="width: 100%"></asp:DropDownList>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtTempDist" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator26" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <%-- <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="LabelColor">Temp Address</label>  <asp:CheckBox ID="chkSame" runat="server" 
                                                        AutoPostBack="true" oncheckedchanged="chkSame_CheckedChanged" />Same as Permanent
                                                    <asp:TextBox ID="txtTempAddr" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ControlToValidate="txtTempAddr" InitialValue="-Select-" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator27" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                    <%--      </div>
                                            </div>--%>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <%-- <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="LabelColor">Identification Mark1</label>
                                                    <asp:TextBox ID="txtIdenMark1" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                    <%-- <asp:RequiredFieldValidator ControlToValidate="txtIdenMark1" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator28" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                    <%--</div>
                                            </div>--%>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Identification Mark1</label>
                                                            <asp:TextBox ID="txtIdenMark1" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%-- <asp:RequiredFieldValidator ControlToValidate="txtIdenMark1" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator28" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Identification Mark2</label>
                                                            <asp:TextBox ID="txtIdenMark2" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%--<asp:RequiredFieldValidator ControlToValidate="txtIdenMark2" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator29" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                    </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Dependant Name</label>
                                                            <asp:TextBox ID="txtDependant" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Relationship</label>
                                                            <asp:TextBox ID="txtRelation" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">DOB</label>
                                                            <asp:TextBox ID="txtDOB1" runat="server" class="form-control BorderStyle datepicker" OnTextChanged="txtDOB1_TextChanged" AutoPostBack="true"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Age</label>
                                                            <asp:TextBox ID="txtAge1" runat="server" class="form-control BorderStyle" Enabled="false"></asp:TextBox>

                                                        </div>
                                                    </div>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Profession</label>
                                                            <asp:TextBox ID="txtProfession" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnFamily" runat="server" class="btn btn-success" Style="margin-top: 16%;" Text="ADD" OnClick="btnFamily_Click" />
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater4" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="exampleExp" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>Depandant Name</th>
                                                                            <th>Realtionship</th>
                                                                            <th>DOB</th>
                                                                            <th>Age</th>
                                                                            <th>Profession</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("DepandantName")%></td>
                                                                    <td><%# Eval("RelationShip")%></td>
                                                                    <td><%# Eval("DOB")%></td>
                                                                    <td><%# Eval("age")%></td>
                                                                    <td><%# Eval("Profession")%></td>


                                                                    <td>
                                                                        <asp:LinkButton ID="btnEdit_family" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                            Text="" OnCommand="btnEdit_family_Command" CommandArgument='<%# Eval("Machineid")%>' CommandName='<%# Eval("DepandantName")%>'>
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="btnDelete_family" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="btnDelete_family_Command" CommandArgument='Delete' CommandName='<%# Eval("DepandantName")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this  details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- end row -->
                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- end wizard step-4 -->
                                <!-- begin wizard step-5 -->
                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">Adolescent</legend>
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:CheckBox ID="chkAdolescent" runat="server" AutoPostBack="true"
                                                                OnCheckedChanged="chkAdolescent_CheckedChanged" />
                                                            Adolescent 
                                                   
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Status</label>
                                                            <asp:DropDownList ID="txtStatus" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:CheckBox ID="chkAge18Complete" runat="server" AutoPostBack="true"
                                                                OnCheckedChanged="chkAge18Complete_CheckedChanged" />
                                                            Adolescent Completed 
                                                   
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <asp:TextBox ID="txtAge18Comp_Date" runat="server" class="form-control BorderStyle datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtAge18Comp_Date" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Certificate No</label>
                                                            <asp:TextBox ID="txtCertificate_No" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            <%--   <asp:RequiredFieldValidator ControlToValidate="txtCertificate_No"  Display="Dynamic" ValidationGroup="ValidateAdl_Field" class="form_error" ID="RequiredFieldValidator33" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Certificate Date</label>
                                                            <asp:TextBox ID="txtCertificate_Date" runat="server" class="form-control BorderStyle datepicker"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtCertificate_Date" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Next Due Date</label>
                                                            <asp:TextBox ID="txtAdoles_Due_Date" runat="server" class="form-control BorderStyle datepicker"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender25" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                                TargetControlID="txtAdoles_Due_Date" ValidChars="0123456789/">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Type of certificate</label>
                                                            <asp:DropDownList ID="txtAdols_Type" runat="server" class="form-control BorderStyle select2" Width="100%">
                                                                <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                                <asp:ListItem Value="Adolescent">Adolescent</asp:ListItem>
                                                                <asp:ListItem Value="Noise Testing">Noise Testing</asp:ListItem>
                                                                <asp:ListItem Value="Canteen">Canteen</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <%--<asp:RequiredFieldValidator ControlToValidate="txtAdols_Type" InitialValue="-Select-"  Display="Dynamic" ValidationGroup="ValidateAdl_Field" class="form_error" ID="RequiredFieldValidator36" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                        </asp:RequiredFieldValidator>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Remarks</label>
                                                            <asp:TextBox ID="txtAdols_Remarks" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:Button ID="BtnAdolcent_Add" runat="server" class="btn btn-success" Style="margin-top: 16%;" ValidationGroup="ValidateAdl_Field" Text="ADD" OnClick="BtnAdolcent_Add_Click" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- end row -->
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater2" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>CertificateNo</th>
                                                                            <th>CertificateDate</th>
                                                                            <th>Due Date</th>
                                                                            <th>Type of certificate</th>
                                                                            <th>Remarks</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("Certificate_No")%></td>
                                                                    <td><%# Eval("Certificate_Date_Str")%></td>
                                                                    <td><%# Eval("Next_Due_Date_Str")%></td>
                                                                    <td><%# Eval("Certificate_Type")%></td>
                                                                    <td><%# Eval("Remarks")%></td>


                                                                    <td>
                                                                        <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteClick_Certificate" CommandArgument='Delete' CommandName='<%# Eval("Certificate_No")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Certificate No details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                                <!-- table End -->
                                            </fieldset>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- end wizard step-4 -->
                                <!-- begin wizard step-5 -->
                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">Documents</legend>

                                                <!-- begin row -->
                                                <div class="row">
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Document Type</label>
                                                            <asp:DropDownList ID="ddlDocType" runat="server" class="form-control BorderStyle select2"
                                                                Width="100%" AutoPostBack="true"
                                                                OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                <asp:ListItem Value="Adhar Card" Text="Adhar Card"></asp:ListItem>
                                                                <asp:ListItem Value="Voter Card" Text="Voter Card"></asp:ListItem>
                                                                <asp:ListItem Value="Ration Card" Text="Ration Card"></asp:ListItem>
                                                                <asp:ListItem Value="Pan Card" Text="Pan Card"></asp:ListItem>
                                                                <asp:ListItem Value="Driving Licence" Text="Driving Licence"></asp:ListItem>
                                                                <asp:ListItem Value="Smart Card" Text="Smart Card"></asp:ListItem>
                                                                <asp:ListItem Value="Bank Pass Book" Text="Bank Pass Book"></asp:ListItem>
                                                                <asp:ListItem Value="Passport" Text="Passport"></asp:ListItem>
                                                                <asp:ListItem Value="Others" Text="Others"></asp:ListItem>

                                                            </asp:DropDownList>

                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Document No</label>
                                                            <asp:TextBox ID="txtDocNo" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->

                                                    <!-- begin col-4 -->
                                                    <%--<div class="col-md-3">
                                                <div class="form-group">
                                                    <label class="LabelColor">Document Description</label>
                                                    <asp:TextBox ID="txtDocDesc" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                    
                                                </div>
                                            </div>--%>
                                                    <!-- end col-4 -->

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnDocAdd" runat="server" class="btn btn-success" Style="margin-top: 16%;" Text="ADD" OnClick="btnDocAdd_Click" />
                                                        </div>
                                                    </div>
                                                    <!-- begin col-4 -->
                                                    <div class="col-md-1">
                                                        <div class="form-group">
                                                            <label runat="server" id="txtDigit" style="margin-top: 30px; margin-left: -78px;"></label>
                                                            <%--<asp:Label ID="txtDigit" runat="server"></asp:Label>--%>
                                                        </div>
                                                    </div>
                                                    <!-- end col-4 -->
                                                    <div class="col-md-2"></div>
                                                    <div class="col-md-2" id="docImg" runat="server" visible="false">
                                                        <div class="media" style="margin-top: -19px;">
                                                            <a class="media-right" href="javascript:;" style="float: right;">
                                                                <asp:Image ID="Image2" runat="server" class="media-object rounded-corner" Style="width: 158px; height: 120px;" ImageUrl="~/assets/img/login-bg/man-user-50.png" />

                                                            </a>

                                                        </div>
                                                    </div>

                                                </div>
                                                <!-- end row -->
                                                <!-- begin row -->
                                                <div class="row">
                                                    <div class="col-md-2">

                                                        <cc1:AsyncFileUpload runat="server" ID="filUpload" CssClass="hideupload"
                                                            UploaderStyle="Modern" CompleteBackColor="White" UploadingBackColor="#CCFFFF"
                                                            ThrobberID="imgLoader" OnUploadedComplete="FileUploadComplete" />
                                                        <asp:Image ID="imgLoader" runat="server" ImageUrl="~/Images/2.gif" /><br />
                                                        <br />
                                                        <img id="imgDisplay" alt="" src="" style="display: none" />
                                                    </div>
                                                </div>
                                                <!-- end row -->
                                                <div class="row"></div>
                                                <!-- table start -->
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                                            <HeaderTemplate>
                                                                <table id="example1" class="display table">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>S.No</th>
                                                                            <th>DocType</th>
                                                                            <th>DocNo</th>
                                                                            <th>Images</th>
                                                                            <th>Mode</th>
                                                                        </tr>
                                                                    </thead>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                                    <td><%# Eval("DocType")%></td>
                                                                    <td><%# Eval("DocNo")%></td>
                                                                    <td>
                                                                        <asp:ImageButton ID="Image1" ImageUrl='<%# Eval("imgurl") %>' runat="server" Height="50px"
                                                                            Width="50px" Style="cursor: pointer" OnClientClick="Javascript:return loadm(this.src);" />
                                                                    </td>
                                                                    <td>
                                                                        <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                            Text="" OnCommand="GridEditClick" CommandArgument='<%# Eval("DocType")%>' CommandName='<%# Eval("DocNo")%>'>
                                                                        </asp:LinkButton>
                                                                        <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                            Text="" OnCommand="GridDeleteClick" CommandArgument='Delete' CommandName='<%# Eval("DocNo")%>'
                                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this DocNo details?');">
                                                                        </asp:LinkButton>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate></table></FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>

                                                <div id="divBackground" class="modal">
                                                </div>
                                                <div id="divImage">
                                                    <table style="height: 100%; width: 100%">
                                                        <tr>
                                                            <td valign="middle" align="center">
                                                                <img id="imgLoader" alt="" src="images/loader.gif" />
                                                                <img id="imgFull" alt="" src="" style="display: none; height: 500px; width: 590px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center" valign="bottom">
                                                                <input id="btnClose" type="button" value="close" onclick="HideDiv()" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>


                                                <!-- table End -->
                                                <style>
                                                    .hideupload #ctl00_ContentPlaceHolder1_filUpload_ctl04 {
                                                        display: none;
                                                    }
                                                </style>
                                                <%-- <!-- begin row -->
                                        <div class="row">
                                            <label class="control-label col-md-5 col-sm-5"></label>
                                            <div class="col-md-3 col-sm-3">
                                               <asp:Button runat="server" id="btnBack" Text="Back" class="btn btn-primary" 
                                                    onclick="btnBack_Click" Visible="false"/>
                                                <asp:Button runat="server" id="btnEmpSave" Text="Save" ValidationGroup="Validate_Field" class="btn btn-success" 
                                                    onclick="btnEmpSave_Click"/>
									           <asp:Button runat="server" id="btnEmpClear" Text="Clear" class="btn btn-danger" 
                                                    onclick="btnEmpClear_Click" />
                                            </div>
                                            <div class="col-md-3 col-sm-3">

                                            </div>
                                        </div>
                                        <!-- end row -->--%>
                                            </fieldset>

                                        </ContentTemplate>
                                        <%--   <Triggers>
                                 <asp:PostBackTrigger ControlID="btnEmpSave" />--%>
                                        <%--<asp:PostBackTrigger ControlID="btnDocAdd" />--%>
                                        <%--<asp:AsyncPostBackTrigger ControlID="btnDocAdd" />--%>
                                        <%-- </Triggers>--%>
                                    </asp:UpdatePanel>
                                </div>
                                <!-- end wizard step-5 -->

                                <div class="well1">
                                    <asp:UpdatePanel ID="UpdatePanel7" runat="server">
                                        <ContentTemplate>
                                            <fieldset>
                                                <legend class="pull-left width-full">Education Details</legend>
                                                <div class="row">
                                                    <div>
                                                        <asp:CheckBox ID="chkbelow" runat="server" AutoPostBack="true"
                                                            OnCheckedChanged="chkbelow_CheckedChanged" />
                                                        BelowSSLC
                                                   
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <!-- begin col-2 -->
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Qualification</label>

                                                            <asp:DropDownList ID="ddlQualifi" runat="server" class="form-control BorderStyle select2"
                                                                Width="100%" OnSelectedIndexChanged="ddlQualifi_SelectedIndexChanged" AutoPostBack="true">


                                                                <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                <asp:ListItem Value="Below SSLC" Text="Below SSLC"></asp:ListItem>
                                                                <asp:ListItem Value="SSLC" Text="SSLC"></asp:ListItem>
                                                                <asp:ListItem Value="HSC" Text="HSC"></asp:ListItem>
                                                                <asp:ListItem Value="Degree" Text="Degree"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <%-- </div>--%>

                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Standard</label>
                                                            <asp:TextBox ID="TxtStandard" runat="server" class="form-control BorderStyle" enable="false"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Degree</label>
                                                            <asp:TextBox ID="txtCourse" runat="server" class="form-control BorderStyle" enable="false"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">School/College</label>
                                                            <asp:TextBox ID="txtSclClg" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Year Of Passing</label>
                                                            <asp:TextBox ID="txtYearPass" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Marks Obtained In %</label>
                                                            <asp:TextBox ID="txtMarks" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                        </div>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <div class="form-group">
                                                            <asp:LinkButton ID="btnEduAdd" runat="server" class="btn btn-success" Style="margin-top: 16%;" Text="ADD" OnClick="btnEduAdd_Click" />
                                                        </div>
                                                    </div>


                                                </div>
                                                <div class="row">
                                                    <div>
                                                        <legend class="pull-left width-full">Language Known</legend>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Languages</label>
                                                                <asp:DropDownList ID="ddlLanguage" runat="server" class="form-control BorderStyle select2"
                                                                    Width="100%" AutoPostBack="true" OnSelectedIndexChanged="ddlLanguage_SelectedIndexChanged">

                                                                    <asp:ListItem Value="0" Text="-Select-"></asp:ListItem>
                                                                    <asp:ListItem Value="Tamil" Text="Tamil"></asp:ListItem>
                                                                    <asp:ListItem Value="English" Text="English"></asp:ListItem>
                                                                    <asp:ListItem Value="Others" Text="Others"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <%--  <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label class="LabelColor">Other Language</label>
                                                       <asp:TextBox ID="TextBox1" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                    </div>
                                                </div>--%>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <asp:CheckBox ID="chkRead" runat="server" AutoPostBack="true"
                                                                    OnCheckedChanged="chkAge18Complete_CheckedChanged" />
                                                                Read
                                                   
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:CheckBox ID="chkwrite" runat="server" AutoPostBack="true" />
                                                                Write
                                                   
                                                            </div>
                                                            <div class="form-group">
                                                                <asp:CheckBox ID="chkspeak" runat="server" AutoPostBack="true"
                                                                    OnCheckedChanged="chkAge18Complete_CheckedChanged" />
                                                                Speak
                                                   
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnaddLang" runat="server" class="btn btn-success" Style="margin-top: 16%;" Text="ADD" OnClick="btnaddLang_Click" />
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label class="LabelColor">Extracurricular Activities</label>
                                                            <asp:TextBox ID="txtExtra" runat="server" class="form-control BorderStyle" TextMode="MultiLine"></asp:TextBox>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="row">
                                                    <div>
                                                        <legend class="pull-left width-full">Previous Experience Details</legend>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Company Name</label>
                                                                <asp:TextBox ID="txtCompnyName" runat="server" class="form-control BorderStyle"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">DOJ</label>
                                                                <asp:TextBox ID="txtDoj1" runat="server" class="form-control BorderStyle datepicker"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">DOL</label>
                                                                <asp:TextBox ID="txtDol1" runat="server" class="form-control BorderStyle datepicker"></asp:TextBox>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Last Drawn Wages</label>
                                                                <asp:TextBox ID="txtDrawnWages" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <label class="LabelColor">Reason For Leaving</label>
                                                                <asp:TextBox ID="txtReason1" runat="server" class="form-control BorderStyle"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <div class="form-group">
                                                                <asp:LinkButton ID="btnExp" runat="server" class="btn btn-success" Style="margin-top: 16%;" Text="ADD" OnClick="btnExp_Click" />

                                                                <asp:LinkButton ID="btnClear" runat="server" class="btn btn-danger" Style="margin-top: 16%;" Text="Clear" OnClick="btnClear_Click" />


                                                            </div>
                                                        </div>

                                                    </div>
                                                    <%--</div>--%>

                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <asp:Repeater ID="Repeater3" runat="server" EnableViewState="false">
                                                                <HeaderTemplate>
                                                                    <table id="exampleExp" class="display table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>S.No</th>
                                                                                <th>Company Name</th>
                                                                                <th>DOJ</th>
                                                                                <th>DOL</th>
                                                                                <th>Last Drawn Wages</th>
                                                                                <th>Remarks</th>
                                                                                <th>Mode</th>
                                                                            </tr>
                                                                        </thead>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td><%# Container.ItemIndex + 1 %></td>
                                                                        <td><%# Eval("CompanyName")%></td>
                                                                        <td><%# Eval("Doj")%></td>
                                                                        <td><%# Eval("Dol")%></td>
                                                                        <td><%# Eval("DrawnWages")%></td>
                                                                        <td><%# Eval("Reason")%></td>


                                                                        <td>
                                                                            <asp:LinkButton ID="LinkButton1" class="btn btn-success btn-sm fa fa-pencil" runat="server"
                                                                                Text="" OnCommand="btnEditGrid_CommandEXP" CommandArgument='<%# Eval("Machineid")%>' CommandName='<%# Eval("CompanyName")%>'>
                                                                            </asp:LinkButton>
                                                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                                                Text="" OnCommand="btnDeleteGrid_CommandEXP" CommandArgument='Delete' CommandName='<%# Eval("CompanyName")%>'
                                                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this details?');">
                                                                            </asp:LinkButton>
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate></table></FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                    <!-- table End -->
                                            </fieldset>

                                        </ContentTemplate>
                                        <Triggers>
                                            <asp:PostBackTrigger ControlID="btnEmpSave" />
                                            <%--<asp:PostBackTrigger ControlID="btnDocAdd" />--%>
                                            <%--<asp:AsyncPostBackTrigger ControlID="btnDocAdd" />--%>
                                        </Triggers>
                                    </asp:UpdatePanel>
                                    <%--     </div>--%>

                                    <!-- begin row -->
                                    <asp:Panel ID="Approve_Cancel_panel" runat="server" Visible="false">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label class="LabelColor">Employee Cancel Reason</label>
                                                    <asp:TextBox ID="txtCanecel_Reason_Approve" runat="server" TextMode="MultiLine" class="form-control BorderStyle"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="row">
                                        <label class="control-label col-md-5 col-sm-5"></label>
                                        <div class="col-md-3 col-sm-3">
                                            <asp:Button runat="server" ID="btnBack" Text="Back" class="btn btn-primary"
                                                OnClick="btnBack_Click" Visible="false" />
                                            <asp:Button runat="server" ID="btnApprove" Text="Approve" class="btn btn-success"
                                                OnClick="btnApprove_Click" Visible="false" />
                                            <asp:Button runat="server" ID="btnCancel_Approve" Text="Cancel" class="btn btn-danger"
                                                OnClick="btnCancel_Approve_Click" Visible="false" />
                                            <asp:Button runat="server" ID="btnEmpSave" Text="Save" ValidationGroup="Validate_Field" class="btn btn-success"
                                                OnClick="btnEmpSave_Click" />
                                            <asp:Button runat="server" ID="btnEmpClear" Text="Clear" class="btn btn-danger"
                                                OnClick="btnEmpClear_Click" />
                                        </div>
                                        <div class="col-md-3 col-sm-3">
                                        </div>
                                    </div>
                                    <!-- end row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>


    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>--%>

    <script>
        $(document).ready(function () {
            //alert('hi');
            $('#example').dataTable();
            $('#example1').dataTable();

            $('#exampleExp').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
         
            $('.doj').datepicker({
                endDate: new Date(),
                format: "dd/mm/yyyy",
                autoclose: true,
                todayBtn: "linked"
            });
        });
    </script>
    <script type="text/javascript">
        function showimagepreview(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img1').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>

    <script type="text/javascript">
        function showimagepreview2(input) {
            if (input.files && input.files[0]) {
                var filerdr = new FileReader();
                filerdr.onload = function (e) {
                    $('#img2').attr('src', e.target.result);
                }
                filerdr.readAsDataURL(input.files[0]);
            }
        }
    </script>
    <%--<script type="text/javascript">
    function UploadFile(fileUpload) {
        if (fileUpload.value != '') {
            document.getElementById("<%=btnUpload.ClientID %>").click();
        }
    }
</script>--%>
    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('#example1').dataTable(); exampleExp
                    $('#exampleExp').dataTable();
                    $("#wizard").bwizard();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });

                    // var tab = document.getElementById('<%= txtDOB.ClientID%>').value;
                // alert(tab);
                //                $('#myTab a[href="' + tab + '"]').tab('show');
            }
        });
    };
    </script>

    <script type="text/javascript">

        function loadm(url) {
            var img = new Image();
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            var imgLoader = document.getElementById("imgLoader");
            imgLoader.style.display = "block";
            img.onload = function () {
                imgFull.src = img.src;
                imgFull.style.display = "block";
                imgLoader.style.display = "none";
            };
            img.src = url;
            var width = document.body.clientWidth;
            if (document.body.clientHeight > document.body.scrollHeight) {
                bcgDiv.style.height = document.body.clientHeight + "px";
            }
            else {
                bcgDiv.style.height = document.body.scrollHeight + "px";
            }
            imgDiv.style.left = (width - 650) / 2 + "px";
            imgDiv.style.top = "100px";
            bcgDiv.style.width = "100%";

            bcgDiv.style.display = "block";
            imgDiv.style.display = "block";
            return false;
        }
        function HideDiv() {
            var bcgDiv = document.getElementById("divBackground");
            var imgDiv = document.getElementById("divImage");
            var imgFull = document.getElementById("imgFull");
            if (bcgDiv != null) {
                bcgDiv.style.display = "none";
                imgDiv.style.display = "none";
                imgFull.style.display = "none";
            }
        }

    </script>
    <script type="text/javascript">
        //    function uploadStarted() {
        //        $get("imgDisplay").style.display = "none";
        //    }
        //    function uploadComplete(sender, args) {
        //        var imgDisplay = $get("imgDisplay");
        //        imgDisplay.src = "images/loader.gif";
        //        imgDisplay.style.cssText = "";
        //        var img = new Image();
        //        img.onload = function() {
        //            imgDisplay.style.cssText = "height:100px;width:100px";
        //            imgDisplay.src = img.src;
        //        };
        //        img.src = "<%=ResolveUrl(UploadFolderPath) %>" + args.get_fileName();
        //    }
    </script>
</asp:Content>

