﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class LeaveApprove : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Leave Approval";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

            
        }
        Load_Data();

    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Leave_Register_Mst where CompCode='" + SessionCcode + "'";
        query = query + " And LocCode='" + SessionLcode + "' And LeaveStatus='N'";
        
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridApproveEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT=new DataTable();
        DataTable DT_Check=new DataTable();
        string[] LeaveDate;
        LeaveDate = e.CommandArgument.ToString().Split(',');

        string FromDate = LeaveDate[0].ToString();
        string ToDate = LeaveDate[1].ToString();

        SSQL = "Select * from Leave_Register_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
        SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "' And LeaveStatus='N'";
        DT=objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
           

            //CL leave
            if (DT.Rows[0]["LeaveType"].ToString() == "Casual Leave (CL)")
            {
                SSQL = "Update Leave_Register_Mst set LeaveStatus='1'";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
                SSQL = SSQL + " and LeaveType='Casual Leave (CL)'";
                objdata.RptEmployeeMultipleDetails(SSQL);

                //SSQL = "";
                //SSQL = "Select * from CLMst where EmpNo='" + e.CommandName.ToString() + "'";
                //DataTable dt_CLleave = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_CLleave.Rows.Count > 0)
                //{
                //    decimal cldays = Convert.ToDecimal(dt_CLleave.Rows[0]["CLMinDays"]);
                //    decimal totalLeaveDays = Convert.ToDecimal(DT.Rows[0]["TotalDays"]);

                //    decimal ClRemaning_days = Convert.ToDecimal(cldays) - Convert.ToDecimal(totalLeaveDays);
                //    SSQL = "";
                //    SSQL = "update CLMst set CLMinDays='" + ClRemaning_days + "'";
                //    SSQL = SSQL + "where EmpNo='" + e.CommandName.ToString() + "'";
                //    objdata.RptEmployeeMultipleDetails(SSQL);
                //}
            }else
            //EL leave
            //if (DT.Rows[0]["LeaveType"].ToString() == "Emergency Leave (EL)")
                if (DT.Rows[0]["LeaveType"].ToString() == "Earned Leave (EL)")
                {

                SSQL = "Update Leave_Register_Mst set LeaveStatus='1'";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
                SSQL = SSQL + " and LeaveType='Earned Leave (EL)'";
                objdata.RptEmployeeMultipleDetails(SSQL);
                //SSQL = "";
                //SSQL = "Select * from CLMst where EmpNo='" + e.CommandName.ToString() + "'";
                //DataTable dt_CLleave = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (dt_CLleave.Rows.Count > 0)
                //{
                //    decimal eldays = Convert.ToDecimal(dt_CLleave.Rows[0]["ELMinDays"]);
                //    decimal totalLeaveDays= Convert.ToDecimal(DT.Rows[0]["TotalDays"]);

                //    decimal elRemaning_days = Convert.ToDecimal(eldays) - Convert.ToDecimal(totalLeaveDays);
                //    SSQL = "";
                //    SSQL = "update CLMst set ELMinDays='" + elRemaning_days + "'";
                //    SSQL = SSQL + "where EmpNo='" + e.CommandName.ToString() + "'";
                //    objdata.RptEmployeeMultipleDetails(SSQL);
                //}

                
              

            }else
            //Compensation days Enter
            if (DT.Rows[0]["LeaveType"].ToString() == "Compensation Leave")

            {
                SSQL = "Update Leave_Register_Mst set LeaveStatus='1'";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
                SSQL = SSQL + " and LeaveType='Compensation Leave'";
                objdata.RptEmployeeMultipleDetails(SSQL);


                DataTable DT_Emp = new DataTable();
                string EmpName = "";
                DataTable DT_Tot = new DataTable();
                string totalDays = "0";
                SSQL = "";
                SSQL = "select FirstName from Employee_Mst where MachineID='"+ e.CommandName.ToString() + "'";
                DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);
                if(DT_Emp.Rows.Count !=0)
                {
                    EmpName = DT_Emp.Rows[0]["FirstName"].ToString();
                }

                SSQL = "select TotalDays from  Leave_Register_Mst where EmpNo='" + e.CommandName.ToString() + "' and FromDate='" + FromDate + "' and ToDate ='" + ToDate + "'";
                DT_Tot = objdata.RptEmployeeMultipleDetails(SSQL);
                if(DT_Tot.Rows.Count !=0)
                {
                    totalDays = DT_Tot.Rows[0]["TotalDays"].ToString();
                }




                SSQL = "Insert Into Compensation_Details(CompCode,LocCode,MachineID,EmpName,CompDate_From,CompDate_To,Tot_CompDays)";
                SSQL = SSQL + " Values('" + SessionCcode + "'";
                SSQL = SSQL + ",'" + SessionLcode + "','" + e.CommandName.ToString() + "','" + EmpName + "','" + FromDate + "'";
                SSQL = SSQL + ",'" + ToDate + "','"+ totalDays + "')";
                objdata.RptEmployeeMultipleDetails(SSQL);




            }
            else
            {
                SSQL = "Update Leave_Register_Mst set LeaveStatus='1'";
                SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
                SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
                SSQL = SSQL + " and (LeaveType='Sick Leave' or LeaveType='Medical Leave' or LeaveType='Long Leave')";
                objdata.RptEmployeeMultipleDetails(SSQL);
            }

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details Approved Succesfully..!');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check the Employee Leave Details.!');", true);
        }
        Load_Data();
    }

    protected void GridCancelEnquiryClick(object sender, CommandEventArgs e)
    {
        DataTable DT = new DataTable();
        DataTable DT_Check = new DataTable();
        string[] LeaveDate;
        LeaveDate = e.CommandArgument.ToString().Split(',');

        string FromDate = LeaveDate[0].ToString();
        string ToDate = LeaveDate[1].ToString();

        SSQL = "Select * from Leave_Register_Mst Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
        SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "' And LeaveStatus='N'";
        DT = objdata.RptEmployeeMultipleDetails(SSQL);
        if (DT.Rows.Count != 0)
        {
            SSQL = "Update Leave_Register_Mst set LeaveStatus='2'";
            SSQL = SSQL + " Where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And EmpNo='" + e.CommandName.ToString() + "'";
            SSQL = SSQL + " And FromDate='" + FromDate + "' And ToDate='" + ToDate + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Leave Details Cancelled Succesfully..!');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Check the Employee Leave Details.!');", true);
        }
        Load_Data();
    }

}
