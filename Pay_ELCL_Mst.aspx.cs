﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Pay_ELCL_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string SSQL = "";
    DataTable dt = new DataTable();
    String CurrentYear1;
    static int CurrentYear;
    string SessionPayroll;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_EmpType();
            Load_Module_Form_Details();
            chkAll_CheckedChanged(sender, e);
        }
        Load_data1();
    }


    private void Load_Module_Form_Details()
    {

        string query = "";
        DataTable qry_dt = new DataTable();
        bool ErrFlag = false;

       
        
        DataTable DT = new DataTable();
        query = "Select  DeptCode,DeptName from Department_Mst";
        // query = query + " And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' order by FormID Asc";
        DT = objdata.RptEmployeeMultipleDetails(query);
        GVModule.DataSource = DT;
        GVModule.DataBind();

        if (GVModule.Rows.Count != 0)
        {
            GVPanel.Visible = true;
        }
        else
        {
            GVPanel.Visible = false;
        }
        
    }


   public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }

    public void Load_EmpType()
    {
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        //ddlEmployeeType_SelectedIndexChanged(sender, e);

    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_EmpType();
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Load_data1();
    }

    protected void chkAll_CheckedChanged(object sender, EventArgs e)
    {
        foreach (GridViewRow gvsal in GVModule.Rows)
        {
            //User Rights Update in Grid
            if (chkAll.Checked == true)
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = true;
                
            }
            else
            {
                ((CheckBox)gvsal.FindControl("chkSelect")).Checked = false;
               
            }
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
         try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;

            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }
            string Form_ID = "0";
            string Form_Name = "0";
            string query = "";

            if (!ErrFlag)
            {
                foreach (GridViewRow gvsal in GVModule.Rows)
                {
                    CheckBox ChkSelect_chk = (CheckBox)gvsal.FindControl("chkSelect");
                    string AddCheck = "0";

                    Label FormID_lbl = (Label)gvsal.FindControl("DeptCode");
                    Form_ID = FormID_lbl.Text.ToString();
                    Form_Name = gvsal.Cells[1].Text.ToString();

                    if (ChkSelect_chk.Checked == true)
                    {


                        query = "Select * from [" + SessionPayroll + "]..MstELCLMaster where DeptCode='" + Form_ID + "' And DeptName='" + Form_Name + "'";
                        query = query + " And Category='" + ddlCategory.SelectedValue.ToString() + "' And EmployeeType='" + ddlEmployeeType.SelectedValue.ToString() + "' "; 
                        query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count != 0)
                        {
                            query = "delete from [" + SessionPayroll + "]..MstELCLMaster where DeptCode='" + Form_ID + "' And DeptName='" + Form_Name + "'";
                            query = query + " And Category='" + ddlCategory.SelectedValue.ToString() + "' And EmployeeType='" + ddlEmployeeType.SelectedValue.ToString() + "' ";
                            query = query + " And Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                            objdata.RptEmployeeMultipleDetails(query);
                        }

                        query = "Insert Into [" + SessionPayroll + "]..MstELCLMaster (Ccode,Lcode,Category,EmployeeType,DaysFrom,";
                        query = query + " DaysTo,ELDays,CLDays,DeptCode,DeptName)";
                        query = query + " values ('" + SessionCcode + "','" + SessionLcode + "','" + ddlCategory.SelectedValue.ToString() + "','" + ddlEmployeeType.SelectedValue.ToString() + "',";
                        query = query + "'" + txtDaysFrom.Text + "','" + txtDaysTo.Text + "','" + txtELDays.Text + "','" + txtCLValue.Text + "',";
                        query = query + "'" + Form_ID + "','" + Form_Name + "') ";
                        objdata.RptEmployeeMultipleDetails(query);
         
                    }
                   
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                        
                btnClear_Click(sender, e);
            }
         }
        catch (Exception ex)
        {
        }
    }
    public void Load_data1()
    {
        Query = "Select BM.Ccode,BM.Lcode,BM.Category,(case BM.Category when '1' then 'STAFF' else 'LABOUR' end) as CategoryName,";
        Query = Query + "BM.EmployeeType,EM.EmpType,BM.DaysFrom,BM.DaysTo,BM.ELDays,";
        Query = Query + " BM.CLDays,BM.DeptCode,BM.DeptName from[" + SessionPayroll + "].. MstELCLMaster BM ";
        Query = Query + " inner join MstEmployeeType EM on BM.EmployeeType=EM.EmpTypeCd";
        Query = Query + " where BM.Ccode='" + SessionCcode + "' and BM.Lcode='" + SessionLcode + "'";
        //And Category='" + ddlCategory.SelectedValue.ToString() + "' ";
        //Query = Query + " and EmployeeType='" + ddlEmployeeType.SelectedValue.ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(Query);

        Repeater1.DataSource = dt;
        Repeater1.DataBind();
    }
    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string CommArg = e.CommandArgument.ToString();
        string[] CommName = e.CommandName.ToString().Split(',');

        string Category = CommName[0].ToString();
        string EmployeeType = CommName[1].ToString();

        string DeptCode = CommArg.ToString();

        SSQL = "Select * from [" + SessionPayroll + "]..MstELCLMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
        SSQL = SSQL + " and Category='" + Category + "' And EmployeeType='" + EmployeeType + "' ";
        SSQL = SSQL + " and DeptCode='" + DeptCode + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);


        if (dt.Rows.Count != 0)
        {
            SSQL = "delete from [" + SessionPayroll + "]..MstELCLMaster where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'  ";
            SSQL = SSQL + " and Category='" + Category + "' And EmployeeType='" + EmployeeType + "' ";
            SSQL = SSQL + " and DeptCode='" + DeptCode + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);


            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Deleted Successfully');", true);

        }

        Load_data1();
    }
  
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clr1();
        chkAll.Checked = true;
        chkAll_CheckedChanged(sender, e);
       
    }
    private void clr1()
    {
        ddlCategory.SelectedValue = "0";
        ddlEmployeeType.SelectedValue = "0";
        txtCLValue.Text = "0.00";
        txtELDays.Text = "0.00";
        txtDaysFrom.Text = "0.00";
        txtDaysTo.Text = "0.00";
    }
}
