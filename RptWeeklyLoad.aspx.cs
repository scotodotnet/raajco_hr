﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;


public partial class RptWeeklyLoad : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    string Month = "";
    string EmpType = "";
    string Category = "";
    string Date1 = "";
    string date2 = "";
    string Pf = "1";

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Weekly Payment details for the Period From " + FromDate + "To" + ToDate + ")";
                SessionCcode = Session["Ccode"].ToString();
                SessionLcode = Session["Lcode"].ToString();
                SessionEpay = Session["SessionEpay"].ToString();
                //SessionUserName = Session["Usernmdisplay"].ToString();
                //SessionUserID = Session["UserId"].ToString();
                //SessionRights = Session["Rights"].ToString();
                SessionUserType = Session["Isadmin"].ToString();
                Category = Request.QueryString["Category"].ToString();
                EmpType = Request.QueryString["EmpType"].ToString();
                FromDate = Request.QueryString["fromdate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                Date1 = Request.QueryString["fromdate"].ToString();
                date2 = Request.QueryString["ToDate"].ToString();
                Pf = Request.QueryString["pf"].ToString();
                Month = Request.QueryString["Months"].ToString();
                GetData();
            }
        }
    }

    private void GetData()
    {
        WagesType = EmpType;
        string TableName = "";
        TableName = "Employee_Mst";

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();
        DataTable dtAmt = new DataTable();
        DataTable dtSal = new DataTable();
        DataTable DT_Emp = new DataTable();
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Emp.Code");
        //AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("DeptName");

        //AutoDTable.Columns.Add("ExistingCode");

        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("WH");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        string whDays = "0";
        string TotDays = "0";
        string salary = "0";
        string OTAmount = "0";
        string TotAmount = "0";
        string NetPay = "0";
        string TotDed = "0";
        // string oneday = "0";
        string WH_Amount = "0";
        string basesalary = "0";
        string DeptName = "";
        string OneDay = "0";
        string OTAdd = "0";
        string Round = "0";
        string BasicDA = "0";
        string OTDay = "0";
        string fourHrs = "0";
        string EightsHrs = "0";
        string TotalInc = "0";
        string FourAmt = "0";
        string EightAmt = "0";
        string ESI = "0";
        string ESI_Wages = "0";
        string Total = "0";
        string NetAmt = "0";
        string WHAmt = "0";

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.Day.ToString()));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }
        AutoDTable.Columns.Add("WH Days");
        AutoDTable.Columns.Add("Tot Hrs");
        AutoDTable.Columns.Add("R.of. Wage / Day");
        AutoDTable.Columns.Add("R.of (Basic+ DA)/Day ");
        AutoDTable.Columns.Add("OT Amount");
        AutoDTable.Columns.Add("4 Hrs");
        AutoDTable.Columns.Add("8 Hrs");
        AutoDTable.Columns.Add("OT Inc Amt");
        AutoDTable.Columns.Add("ESI Wages");
        AutoDTable.Columns.Add("ESI Amount");
        AutoDTable.Columns.Add("Days");
        AutoDTable.Columns.Add("Travel All");
        AutoDTable.Columns.Add("Total Amount");
        AutoDTable.Columns.Add("Total Ded");
        AutoDTable.Columns.Add("Total");
        AutoDTable.Columns.Add("R.Off");
        AutoDTable.Columns.Add("NetPay");
        AutoDTable.Columns.Add("Signature");

        string query = "";
        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,LEFT(EM.WeekOff,3) as WeekOff,DATENAME(dd, Attn_Date) as Day_V, ";
        query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 THEN 'WH/X' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
        query = query + " else (CASE WHEN LD.Present = 1.0 THEN 'X' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";

        query = query + " where LD.LocCode='" + SessionLcode + "' and LD.Present!='0' ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";

        if (WagesType != "-Select-")
        {
            query = query + " And EM.Wages='" + WagesType + "'";
            query = query + " And LD.Wages='" + WagesType + "'";
        }

        query = query + " And EM.Eligible_PF='" + Pf + "' And EM.LocCode='" + SessionLcode + "') as CV ";

        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";

        dt = objdata.RptEmployeeMultipleDetails(query);

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            //  AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            //AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["Emp.Code"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();

            //string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            AutoDTable.Rows[DT_Row]["WH"] = dt.Rows[i]["WeekOff"].ToString();

            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    DataTable DT_Shift = new DataTable();
                    string Shift = "";
                    string ShiftSort = "";


                    string date_str = dayy.ToString("dd/MM/yyyy HH:mm:ss");
                    string[] Split = date_str.Split(' ');
                    OTDay = Split[0];
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());


                    SSQL = "";
                    SSQL = "Select (Case when Shift!='No Shift' then Left(Shift,1)+Right(Shift,1) when Shift='No Shift' then Left(Shift,1) else '' end) as Shift from LogTime_Days where Attn_Date_Str='" + OTDay + "' and MachineID='" + MachineID + "' and Present !=0";
                    DT_Shift = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_Shift.Rows.Count != 0)
                    {
                        //Shift = DT_Shift.Rows[0]["Shift"].ToString();
                        if (DOJ_Date_Check_Emp <= Query_Date_Check)
                        {
                            AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X/" + DT_Shift.Rows[0]["Shift"].ToString() + "" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "H" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X" + DT_Shift.Rows[0]["Shift"].ToString() + "" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/H" + "</b></span>"; }
                            if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH/A" + "</b></span>"; }
                        }
                        else
                        {
                            AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "";
                        }
                    }
                    //if (DT_Shift.Rows.Count != 0)
                    //{
                    //    if (Shift != "")
                    //    {
                    //        if (Shift == "SHIFT 1")
                    //        {
                    //            ShiftSort = "S1";
                    //        }
                    //        if (Shift == "SHIFT 2")
                    //        {
                    //            ShiftSort = "S2";
                    //        }
                    //        if (Shift == "SHIFT 3")
                    //        {
                    //            ShiftSort = "S3";
                    //        }
                    //        if (Shift == "SHIFT3")
                    //        {
                    //            ShiftSort = "S3";
                    //        }

                    //        if (Shift == "SHIFT 4")
                    //        {
                    //            ShiftSort = "S4";
                    //        }
                    //        if (Shift == "No Shift")
                    //        {
                    //            ShiftSort = "N";
                    //        }

                    //    }
                    //    else
                    //    {
                    //        ShiftSort = "";
                    //    }



                    //}
                    else
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X/" + ShiftSort + "" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "H" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X" + ShiftSort + "" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/H" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH/A" + "</b></span>"; }
                    }

                    daycount -= 1;
                    daysAdded += 1;
                    DataTable DT_Hrs = new DataTable();
                    string OT_Hrs = "";

                    OT_Hrs = "";
                    SSQL = "";
                    SSQL = "Select isnull(OT_Hrs,'0') as OT_Hrs, Left(isnull(Shift,''),1)+Right(isnull(Shift,''),1) as Shift from [" + SessionEpay + "]..OT_Hrs_Mst where Attn_Date_Str='" + OTDay + "' and MachineID='" + MachineID + "' and Cast(OT_Hrs as decimal(18,2))>'0'";
                    DT_Hrs = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (DT_Hrs.Rows.Count != 0)
                    {
                        //OT_Hrs = DT_Hrs.Rows[0]["OT_Hrs"].ToString();
                        //Shift = DT_Hrs.Rows[0]["Shift"].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X/" + DT_Hrs.Rows[0]["OT_Hrs"].ToString() + "/" + DT_Hrs.Rows[0]["Shift"].ToString() + "" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X/" + DT_Hrs.Rows[0]["OT_Hrs"].ToString() + "/" + DT_Hrs.Rows[0]["Shift"].ToString() + "" + "</b></span>"; }
                    }
                    //if (DT_Hrs.Rows.Count != 0)
                    //{
                    //    if (OT_Hrs != "0")
                    //    {
                    //        if (Shift != "")
                    //        {
                    //            if (Shift == "SHIFT 1")
                    //            {
                    //                ShiftSort = "S1";
                    //            }
                    //            if (Shift == "SHIFT 2")
                    //            {
                    //                ShiftSort = "S2";
                    //            }
                    //            if (Shift == "SHIFT 3")
                    //            {
                    //                ShiftSort = "S3";
                    //            }
                    //            if (Shift == "SHIFT3")
                    //            {
                    //                ShiftSort = "S3";
                    //            }
                    //            if (Shift == "SHIFT 4")
                    //            {
                    //                ShiftSort = "S4";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            ShiftSort = "";
                    //        }

                    //        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X/" + OT_Hrs + "/" + ShiftSort + "" + "</b></span>"; }
                    //        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X/" + OT_Hrs + "/" + ShiftSort + "" + "</b></span>"; }
                    //    }
                    //}
                }
                SSQL = "";
                SSQL = "Select * from Employee_Mst where MachineID ='" + MachineID + "'";
                DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);

                basesalary = DT_Emp.Rows[0]["BaseSalary"].ToString();
                DeptName = DT_Emp.Rows[0]["DeptName"].ToString();
                OneDay = (Convert.ToDecimal(basesalary) / 26).ToString();
                OneDay = (Math.Round(Convert.ToDecimal(OneDay), 0, MidpointRounding.AwayFromZero)).ToString();

                BasicDA = (Convert.ToDecimal(OneDay) / 2).ToString();
                AutoDTable.Rows[DT_Row]["R.of. Wage / Day"] = OneDay;
                AutoDTable.Rows[DT_Row]["R.of (Basic+ DA)/Day "] = BasicDA;


                AutoDTable.Rows[DT_Row]["OT Amount"] = "0";
                AutoDTable.Rows[DT_Row]["Tot Hrs"] = "0";
                AutoDTable.Rows[DT_Row]["4 Hrs"] = "0";
                AutoDTable.Rows[DT_Row]["8 Hrs"] = "0";
                AutoDTable.Rows[DT_Row]["ESI Wages"] = "0";
                AutoDTable.Rows[DT_Row]["ESI Amount"] = "0";
                AutoDTable.Rows[DT_Row]["Total Ded"] = "0";
                AutoDTable.Rows[DT_Row]["Total Amount"] = "0";
                AutoDTable.Rows[DT_Row]["R.Off"] = "0";
                AutoDTable.Rows[DT_Row]["NetPay"] = "0";
                AutoDTable.Rows[DT_Row]["WH Days"] = "0";


                //SSQL = "";
                //SSQL = "select MachineID,Oneday_Wages,Sum(Cast(Tot_OTHrs as int)) as Tot_OTHrs,Sum(cast(Amount as decimal(18,2))) Amount,Sum(Cast(WH as int))WH   from [Raajco_Epay]..OT_Amt where MachineID='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                //SSQL = SSQL + " And LCode='" + SessionLcode + "'";
                //SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
                //SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) <= CONVERT(datetime,'" + ToDate + "',103) group by MachineID,Oneday_Wages";
                //dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (MachineID == "1501559")
                {
                    string stop = "";
                }

                SSQL = "";
                //SSQL = "select A.MachineID,A.EmpName,sum(Tot_OTHrs)Tot_OTHrs,Sum(Amount)Amount,sum(Four_Hrs_Inc)Four_Hrs," +
                //        " sum(Four_Hrs_inc_Amount)Four_Hrs_inc_Amount," +
                //        " Sum(Eight_Hrs_Inc)Eight_Hrs, sum(Eight_Hrs_inc_Amount)Eight_Hrs_inc_Amount,Sum(WH) as WH,Sum(WH_Amt)WHAmt from (" +
                //        " Select MachineID, EmpName, OT_AttenDate, cast(Tot_OTHrs as int)Tot_OTHrs, cast(Amount as decimal(18, 2)) Amount ," +
                //        " Oneday_Wages,OT_Wages,0 Four_Hrs_Inc, 0 Eight_Hrs_Inc,0 Four_Hrs_inc_Amount, " +
                //        " 0 Eight_Hrs_inc_Amount,0 WH,0 WH_Amt from  [" + SessionEpay + "]..OT_Amt " +
                //        " where CCode = '" + SessionCcode + "' and LCode = '" + SessionLcode + "' and  MachineID='" + MachineID + "' And " +
                //        " convert(DateTime,OT_AttenDate,103) >= Convert(Datetime,'" + FromDate + "',103) and " +
                //        " convert(DateTime, OT_AttenDate, 103) <= Convert(Datetime, '" + ToDate + "', 103) and Wages='" + EmpType + "'" +
                //        " Union All " +
                //        " Select MachineID,EmpName,OT_AttenDate,0 Tot_OTHrs,  0.0 Amount,0 Oneday_Wages,0 OT_Wages,Four_Hrs," +
                //        " Eight_Hrs,Four_Hrs_inc_Amount,Eight_Hrs_inc_Amount,WH,WH_Amt " +
                //        " from  [" + SessionEpay + "]..OT_Inc_Amt where  CCode = '" + SessionCcode + "' and LCode = '" + SessionLcode + "' " +
                //        " and MachineID='" + MachineID + "'  and  convert(DateTime,OT_AttenDate,103) >= Convert(Datetime,'" + FromDate + "',103) " +
                //        " and convert(DateTime,OT_AttenDate, 103) <= Convert(Datetime,'" + ToDate + "' , 103)  )A Group by A.MachineID,A.EmpName";

                SSQL = "Select MachineID,EmpName,sum(CAST(isnull(OT_Hrs,'0') as decimal(18,2))) as Tot_OTHrs,Sum(CAST(isnull(Amount,'0') as decimal(18,2))) as Amount,Sum(CAST(isnull(Four_Hrs_Inc,'0') as decimal(18,2))) as Four_Hrs,";
                SSQL = SSQL + "Sum(CAST(isnull(Eight_Hrs_Inc,'0') as decimal(18,2))) as Eight_Hrs,Sum(CAST(isnull(Four_Hrs_Inc_Amount,'0') as decimal(18,2))) as Four_Hrs_inc_Amount,Sum(CAST(isnull(Eight_Hrs_Inc_Amount,'0') as decimal(18,2))) as Eight_Hrs_inc_Amount,";
                SSQL = SSQL + "Sum(CAST(isnull(Weekoff,'0') as decimal(18,2))) as WH,Sum(CAST(Isnull(WH_Amt,'0') as decimal(18,2))) as WHAmt from [" + SessionEpay + "]..OT_Hrs_Mst";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MachineID='" + MachineID + "' and ";
                SSQL = SSQL + " Convert(date,Attn_Date_Str,103)>=Convert(date,'" + FromDate + "',103) and Convert(date,Attn_Date_Str,103)<=Convert(date,'" + ToDate + "',103)";
                SSQL = SSQL + " and Wages='" + EmpType + "'";
                SSQL = SSQL + " group by MachineID,EmpName order by MachineID asc";

                dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dtAmt.Rows.Count != 0)
                {
                    for (int j = 0; j < dtAmt.Rows.Count; j++)
                    {
                        if (dtAmt.Rows[0]["Tot_OTHrs"].ToString() != "")
                        {

                            AutoDTable.Rows[DT_Row]["Tot Hrs"] = dtAmt.Rows[j]["Tot_OTHrs"].ToString();
                        }
                        OTAdd = dtAmt.Rows[j]["Amount"].ToString();
                        fourHrs = dtAmt.Rows[j]["Four_Hrs"].ToString();
                        EightsHrs = dtAmt.Rows[j]["Eight_Hrs"].ToString();
                        FourAmt = dtAmt.Rows[j]["Four_Hrs_inc_Amount"].ToString();
                        EightAmt = dtAmt.Rows[j]["Eight_Hrs_inc_Amount"].ToString();
                        WHAmt = dtAmt.Rows[j]["WHAmt"].ToString();

                        TotalInc = (Convert.ToDecimal(FourAmt) + Convert.ToDecimal(EightAmt) + Convert.ToDecimal(WHAmt)).ToString();

                        //OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();
                        whDays = dtAmt.Rows[j]["WH"].ToString();


                        if (whDays != "0")
                        {
                            WH_Amount = (Convert.ToDecimal(OneDay) * Convert.ToDecimal(whDays)).ToString();

                        }
                        else
                        {
                            WH_Amount = "0";
                        }

                        OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();
                        AutoDTable.Rows[DT_Row]["WH Days"] = whDays;
                        AutoDTable.Rows[DT_Row]["OT Amount"] = OTAmount;
                        AutoDTable.Rows[DT_Row]["4 Hrs"] = fourHrs;
                        AutoDTable.Rows[DT_Row]["8 Hrs"] = EightsHrs;
                        AutoDTable.Rows[DT_Row]["ESI Wages"] = OTAmount;


                        if (OTAmount != "0")
                        {
                            ESI_Wages = OTAmount;
                            ESI = (Convert.ToDecimal(OTAmount) * Convert.ToDecimal(0.75)).ToString();
                            ESI = (Convert.ToDecimal(ESI) / Convert.ToDecimal(100)).ToString();
                            ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.AwayFromZero)).ToString();
                        }

                        SSQL = "";
                        SSQL = "Select EM.MachineID,EM.ExistingCode as TokenNo,EM.FirstName as EmpName,EM.DeptName as Department,EM.Vehicles_Type";
                        SSQL = SSQL + ",SUM(isnull(LD.Present,'0')) as Days,(SUM(isnull(LD.Present,'0')) * isnull(VD.Commission,'0')) as Amount";
                        SSQL = SSQL + " from Employee_mst EM inner join MstVehicle VD on VD.VehicleType=EM.Vehicles_Type and VD.Ccode=EM.CompCode and VD.Lcode=EM.LocCode";
                        SSQL = SSQL + " inner join LogTime_Days LD on LD.MachineID=EM.MachineID and LD.CompCode=EM.CompCode and LD.LocCOde=EM.LocCOde";
                        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";

                        SSQL = SSQL + " and Convert(datetime,LD.Attn_Date,103)>=Convert(datetime,'" + FromDate + "',103)";
                        SSQL = SSQL + " and Convert(datetime,LD.Attn_Date,103)<=Convert(datetime,'" + ToDate + "',103)";
                        SSQL = SSQL + " and LD.Present!='0.0' and EM.Vehicles_type is not null and  EM.Vehicles_type!='-Select-'";
                        SSQL = SSQL + " and EM.MachineID='" + MachineID + "'";
                        SSQL = SSQL + " group by EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Vehicles_Type,VD.Commission";
                        SSQL = SSQL + " order by EM.MachineID asc";

                        DataTable dt_TAll = new DataTable();
                        dt_TAll = objdata.RptEmployeeMultipleDetails(SSQL);
                        string Days = "0";
                        string TvAllo = "0";
                        if (dt_TAll.Rows.Count > 0)
                        {
                            Days = dt_TAll.Rows[0]["Days"].ToString();
                            TvAllo = dt_TAll.Rows[0]["Amount"].ToString();
                        }

                        NetAmt = (Convert.ToDecimal(OTAmount) + Convert.ToDecimal(TotalInc) + Convert.ToDecimal(TvAllo)).ToString();
                        Total = (Convert.ToDecimal(NetAmt) - Convert.ToDecimal(ESI)).ToString();
                        NetPay = (Math.Round(Convert.ToDecimal(Total), 0, MidpointRounding.AwayFromZero)).ToString();
                        Round = (Convert.ToDecimal(NetPay) - Convert.ToDecimal(Total)).ToString();

                        AutoDTable.Rows[DT_Row]["OT Inc Amt"] = TotalInc;
                        AutoDTable.Rows[DT_Row]["ESI Amount"] = ESI;

                        AutoDTable.Rows[DT_Row]["Days"] = Days;
                        AutoDTable.Rows[DT_Row]["Travel All"] = TvAllo;
                        AutoDTable.Rows[DT_Row]["Total Amount"] = NetAmt;
                        AutoDTable.Rows[DT_Row]["Total Ded"] = ESI;
                        AutoDTable.Rows[DT_Row]["Total"] = Total;
                        AutoDTable.Rows[DT_Row]["R.Off"] = Round;
                        AutoDTable.Rows[DT_Row]["NetPay"] = NetPay;


                        TotalInc = "0";
                        EightsHrs = "0";
                        fourHrs = "0";
                        OTAmount = "0";
                        Total = "0";
                        NetPay = "0";
                        Round = "0";
                        ESI = "0";
                        NetAmt = "0";
                        whDays = "0";
                    }
                }
            }
        }



        if (AutoDTable.Rows.Count != 0)

        {
            grid.DataSource = AutoDTable;
            grid.DataBind();
            string attachment = "attachment;filename=" + WagesType + " OT Report " + FromDate + " To " + ToDate + " .xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            int row = daysAdded + 6;
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + row + ">");
            Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
            //Response.Write("--");
            //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='Center'>");
            Response.Write("<td colspan=" + row + ">");
            Response.Write("<a style=\"font-weight:bold\">Weekly Payment details for the Period from -  " + FromDate + " - " + ToDate + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");
            //Response.Write("<tr Font-Bold='true' align='center'>");
            //Response.Write("<td colspan='10'>");
            //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
            //Response.Write("&nbsp;&nbsp;&nbsp;");
            //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
            //Response.Write("</td>");
            //Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
            Response.Clear();
        }
        else
        {
        }
    }
}

