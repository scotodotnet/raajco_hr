﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptELCL.aspx.cs" Inherits="RptELCL" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
 <!-- begin #content -->
		<div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Report</a></li>
            <li class="active">EL&CL Report</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">EL&CL Report</h1>
        <!-- end page-header -->

        <!-- begin row -->
        <div class="row">
            <!-- begin col-12 -->
            <div class="col-md-12">
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">EL&CL Master</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <!-- begin row -->
                            <div class="row">
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Category</label>
                                        <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" AutoPostBack="true"
                                            Style="width: 100%;" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                            <asp:ListItem Value="0">-Select-</asp:ListItem>
                                            <asp:ListItem Value="1">Staff</asp:ListItem>
                                            <asp:ListItem Value="2">Labour</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Employee Type</label>
                                        <asp:DropDownList runat="server" ID="ddlEmployeeType" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>EL CL Year</label>
                                        <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" Style="width: 100%;">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                            </div>
                            <!-- end row -->
                            <div class="row">
                                 <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                         <label>Days From</label>
                                        <asp:TextBox ID="txtDaysFrom" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                <!--end col2-->
                                 <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                         <label>Days To</label>
                                        <asp:TextBox ID="txtDaysTo" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                <!--end col2-->
                                 <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                         <label>EL Days</label>
                                        <asp:TextBox ID="txtELDays" class="form-control" runat="server"></asp:TextBox>
                                        </div>
                                    </div>
                                <!--end col2-->
                            </div>

                            <!-- begin row -->
                            <div class="row">
                                <div class="col-md-4"></div>
                                <!-- begin col-4 -->
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnSave" Text="Save"
                                            class="btn btn-success" OnClick="btnSave_Click" />
                                        <asp:Button runat="server" ID="btnCancel" Text="Cancel"
                                            class="btn btn-danger" OnClick="btnCancel_Click" />
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <div class="col-md-4"></div>
                            </div>
                            <!-- begin row -->
                            <div class="row">
                                 <div class="col-md-4">

                                     </div>
                                </div>

                            <div id="Download_loader" style="display: none" />
                        </div>
                        <!-- end row -->
                    </div>
                </div>

            </div>
            <!-- end panel -->
        </div>
        <!-- end col-12 -->
    </div>
            <!-- end row -->
    

</asp:Content>
