﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Data.OleDb;
using Payroll;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;

public partial class AttendanceBonusUpload : System.Web.UI.Page
{
    string SessionAdmin;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    BALDataAccess objdata = new BALDataAccess();
    AttenanceClass objatt = new AttenanceClass();
    string SessionCcode;
    string SessionLcode;
    string WagesType;
    string SessionUserType;
    string SessionEpay;
    string Query = "";

    string TempNFH = "0";
    string TempCL = "0";
    string TempThreeSide = "0";
    string TempFull = "0";
    string TempHome = "0";
    string TempOTNew = "";
    string TempWH_Work_Days = "0";
    string TempFixed_Days = "0";
    string TempNFH_Work_Days = "0";
    string TempNFH_Work_Manual = "0";
    string TempNFH_Work_Station = "0";
    string TempNFH_Count_Days = "0";
    string TempLBH_Count_Days = "0";
    string weekoff = "0";
    string totWeekoff = "0";
    string ELdays = "0";
    string CLdays = "0";
    string CompDays = "0";
    SqlConnection con;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        con = new SqlConnection(constr);
        if (!IsPostBack)
        {
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlfinance.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

        }
    }
    public void Load_TwoDates()
    {
        if (ddlMonths.SelectedValue != "-Select-" && ddlfinance.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;
            string FromDate = "";
            string ToDate = "";

            Temp_Years = ddlfinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "February")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "February") || (ddlMonths.SelectedValue == "March"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtFrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();
            txtdays.Text = ((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).Days + 1).ToString();
        }
        else
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }


    }
    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void ddlfinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
    protected void btnDownload_Click(object sender, EventArgs e)
    {
        try
        {

            if (ddlCategory.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }

            else if (ddlEmployeeType.SelectedValue == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {
                string staff_lab = "";
                string Employee_Type = "";
                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }

                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                DataTable dt = new DataTable();
                Query = "";
                Query = Query + "select EM.EmpNo,(EM.FirstName) as EmpName,(EM.MachineID) as MachineNo,(EM.ExistingCode) as ExisistingCode,";
                Query = Query + " CASE When (Sum(isnull(LD.Present,'0'))-(SUM(isnull(LD.WH_Count,'0'))+Sum(isnull(LD.NFH_Count,'0'))))<='0' then '0' else  (Sum(isnull(LD.Present,'0'))-(SUM(isnull(LD.WH_Present_Count,'0'))+Sum(isnull(LD.NFH_Present_Count,'0')))) end as Days,";
                Query = Query + " Sum(isnull(LD.NFH_Count,'0')) as NFH,Sum(isnull(LD.WH_Count,'0')) as WH,('" + txtdays.Text + "'-sum(isnull(LD.Present,'0'))) as LOP,'" + txtdays.Text + "' as TotalDays,EM.SkillLevel as SkillLevel,AB.SkillRate";
                Query = Query + " from Employee_Mst EM inner join Logtime_Days LD on LD.MachineID=EM.MachineID and LD.CompCode=EM.CompCode and LD.LocCode=EM.LocCode";
                Query = Query + " left join [" + SessionEpay + "]..MstAtten_Bonus AB on AB.SkillLevel COLLATE DATABASE_DEFAULT=EM.SkillLevel COLLATE DATABASE_DEFAULT";
                Query = Query + " where EM.LocCode='" + SessionLcode + "' and EM.CompCode='" + SessionCcode + "' and EM.IsActive='Yes' ";
                Query = Query + " and EM.CatName='" + ddlCategory.SelectedItem.Text.ToString() + "' and EM.Wages='" + ddlEmployeeType.SelectedItem.Text.ToString() + "'";
                Query = Query + " and Convert(datetime,Attn_Date,103)>=Convert(datetime,'" + txtFrom.Text + "',103) and Convert(datetime,Attn_Date,103)<=Convert(datetime,'" + txtTo.Text + "',103)";
                Query = Query + " group by EM.Empno,EM.FirstName,EM.MachineID,EM.ExistingCode,EM.SkillLevel,AB.SkillRate";
                Query = Query + " Order by EM.ExistingCode Asc";
                dt = objdata.RptEmployeeMultipleDetails(Query);
               
                if (dt.Rows.Count > 0)
                {
                    gvEmp.DataSource = dt;
                    gvEmp.DataBind();

                    string attachment = "attachment;filename=Attendance Bonus.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    gvEmp.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    gvEmp.RenderControl(htextw);
                    Response.Write(stw.ToString());
                    Response.End();
                    Response.Clear();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string SSQL = "";
        try
        {

            if (ddlMonths.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
                ErrFlag = true;
            }
            else if (txtFrom.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
                ErrFlag = true;
            }
            else if (txtTo.Text.Trim() == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
                ErrFlag = true;
            }

            if (FileUpload.HasFile)
            {
                FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

            }
            if (!ErrFlag)
            {
                int Month_Mid_Total_Days_Count = 0;
                Month_Mid_Total_Days_Count = (int)((Convert.ToDateTime(txtTo.Text) - Convert.ToDateTime(txtFrom.Text)).TotalDays);
                Month_Mid_Total_Days_Count = Month_Mid_Total_Days_Count + 1;
                txtdays.Text = Month_Mid_Total_Days_Count.ToString();
                decimal total = (Convert.ToDecimal(txtdays.Text.Trim()));
                int total_check = 0;
                decimal days = 0;
                string from_date_check = txtFrom.Text.ToString();
                string to_date_check = txtTo.Text.ToString();
                from_date_check = from_date_check.Replace("/", "-");
                to_date_check = to_date_check.Replace("/", "-");
                txtFrom.Text = from_date_check;
                txtTo.Text = to_date_check;

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                }

                if ((ddlMonths.SelectedValue == "January") || (ddlMonths.SelectedValue == "March") || (ddlMonths.SelectedValue == "May") || (ddlMonths.SelectedValue == "July") || (ddlMonths.SelectedValue == "August") || (ddlMonths.SelectedValue == "October") || (ddlMonths.SelectedValue == "December"))
                {
                    days = 31;
                }
                else if ((ddlMonths.SelectedValue == "April") || (ddlMonths.SelectedValue == "June") || (ddlMonths.SelectedValue == "September") || (ddlMonths.SelectedValue == "November"))
                {
                    days = 30;
                }

                else if (ddlMonths.SelectedValue == "February")
                {
                    int yrs = (Convert.ToInt32(ddlfinance.SelectedValue) + 1);
                    if ((yrs % 4) == 0)
                    {
                        days = 29;
                    }
                    else
                    {
                        days = 28;
                    }
                }
                if (total > days)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the days properly.');", true);
                    ErrFlag = true;
                }
                if (!ErrFlag)
                {
                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();
                    using (sSourceConnection)
                    {
                        sSourceConnection.Open();

                        OleDbCommand command = new OleDbCommand("Select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = sSourceConnection.CreateCommand())
                        {
                            command.CommandText = "Select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];

                        SSQL = "";
                        SSQL = "Delete from [" + SessionEpay + "]..Atten_Bonus_Details where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        SSQL = SSQL + " and month='" + ddlMonths.SelectedItem.Text + "' and Convert(datetime,FromDate,103)=Convert(datetime,'" + txtFrom.Text + "',103)";
                        SSQL = SSQL + " and Convert(datetime,ToDate,103)=Convert(datetime,'" + txtTo.Text + "',103)";
                        SSQL = SSQL + " and EmpType='" + ddlEmployeeType.SelectedItem.Text + "'";
                        objdata.RptEmployeeMultipleDetails(SSQL);

                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            string MachineID = dt.Rows[j][0].ToString();
                            string ExCode = dt.Rows[j][1].ToString();
                            string EmpDays = dt.Rows[j][3].ToString();
                            string NHDays = dt.Rows[j][4].ToString();
                            string WHDays = dt.Rows[j][5].ToString();
                            string LOPdays = dt.Rows[j][6].ToString();
                            string TotalDays = dt.Rows[j][7].ToString();
                            string TotalAbsentDays = dt.Rows[j][8].ToString();
                            string EligibleDays = dt.Rows[j][9].ToString();
                            string AbsentDays = dt.Rows[j][10].ToString();
                            string AttnSkill = dt.Rows[j][11].ToString();
                            string Rate = dt.Rows[j][12].ToString();
                            
                            string Name = "";
                            decimal paidamt = 0;
                            decimal ExpectWorkDays = 0;
                            decimal AmtproBase = 0;
                            decimal PerdaysRate = 0;
                            
                            SSQL = "";
                            SSQL = "Select * from Employee_Mst Where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and MachineID='" + MachineID + "'";
                            SSQL = SSQL + " and ExistingCode='" + ExCode + "'";
                            DataTable dt_Emp = new DataTable();
                            dt_Emp = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (dt_Emp.Rows.Count > 0)
                            {
                                Name = dt_Emp.Rows[0]["FirstName"].ToString();
                                ExpectWorkDays = (Convert.ToDecimal(TotalDays) - (Convert.ToDecimal(NHDays) + Convert.ToDecimal(WHDays)));
                                PerdaysRate = (Convert.ToDecimal(Rate) / Convert.ToDecimal(ExpectWorkDays));
                                PerdaysRate = Math.Round(PerdaysRate,0,MidpointRounding.AwayFromZero);

                                paidamt = (Convert.ToDecimal(EmpDays) * Convert.ToDecimal(PerdaysRate));
                                paidamt = Math.Round(paidamt, 0, MidpointRounding.AwayFromZero);

                                AmtproBase = paidamt;

                                SSQL = "Insert into [" + SessionEpay + "]..Atten_Bonus_Details(Ccode,Lcode,MachineID,ExistingCode,Name,Days,NH,WH,LOP,TotalDays,TotalAbsent,Eligible_Days,AbsentDays,Attn_Bonus_Skill,Rate,PaidAmt,ExpectWorkDays,AmtProBase,FromDate,ToDate,Month,FinYear,Year,Cate,EmpType,PerDayRate)";
                                SSQL = SSQL + " values('" + SessionCcode + "','" + SessionLcode + "','" + MachineID + "','" + ExCode + "','" + Name + "','" + EmpDays + "','" + NHDays + "','" + WHDays + "','" + LOPdays + "','" + TotalDays + "','" + TotalAbsentDays + "','" + EligibleDays + "','" + AbsentDays + "'";
                                SSQL = SSQL + ",'" + AttnSkill + "','" + Rate + "','" + paidamt + "','" + ExpectWorkDays + "','" + AmtproBase + "',Convert(datetime,'" + txtFrom.Text + "',103),Convert(datetime,'" + txtTo.Text + "',103),'" + ddlMonths.SelectedItem.Text + "','" + ddlfinance.SelectedItem.Text + "'";
                                SSQL = SSQL + ",'" + ddlfinance.SelectedValue + "','" + ddlCategory.SelectedItem.Text + "','" + ddlEmployeeType.SelectedItem.Text + "','" + PerdaysRate + "')";
                                objdata.RptEmployeeMultipleDetails(SSQL);

                            }
                            else
                            {
                                ErrFlag = true;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('The Employee Details Not Match in Employee Master. Then Token Number is " + ExCode + "');", true);
                                return;
                            }
                        }
                        if (!ErrFlag)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Calculation Completed');", true);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please check all Empty Fields or Upload Correct File Format...');", true);
            //System.Windows.Forms.MessageBox.Show("Please Upload Correct File Format", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCategory.SelectedItem.Text != "-Select-")
        {
            string SSQL = "";
            SSQL = "Select * from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
            ddlEmployeeType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
            ddlEmployeeType.DataTextField = "EmpType";
            ddlEmployeeType.DataValueField = "EmptypeCd";
            ddlEmployeeType.DataBind();
            ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        }
    }

    protected void btnReport_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string SSQL = "";

        if (ddlMonths.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
            ErrFlag = true;
        }
        else if (txtFrom.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
            ErrFlag = true;
        }
        else if (txtTo.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            SSQL = "";
            SSQL = "Select ROW_NUMBER() OVER (ORDER BY MachineID Asc) AS [S.No],ExistingCode as TkNo,Name,Days as Total,NH,WH,LOP,TotalDays as [TOT.DAYS]";
            SSQL = SSQL + ",TotalAbsent as [TOTAL ABSENT DAYS],Eligible_Days as [Eligi.Days],AbsentDays as [AbsentDays],Attn_Bonus_Skill as [Att.Bonus Skil Level]";
            SSQL = SSQL + ",Rate,PaidAmt as [Paid. Pro Bon. Amnt],ExpectWorkDays,AmtProBase as [Amount Pro. Rate Base]";
            SSQL = SSQL + " from [" + SessionEpay + "]..Atten_Bonus_Details where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
            SSQL = SSQL + " and Convert(datetime,FromDate,103)>=Convert(datetime,'" + txtFrom.Text + "',103) and Convert(datetime,ToDate,103)<=Convert(datetime,'" + txtTo.Text + "',103)";
            SSQL = SSQL + " and Month='" + ddlMonths.SelectedItem.Text + "' and FinYear='" + ddlfinance.SelectedItem.Text + "' and Year='" + ddlfinance.SelectedValue + "'";
            if (ddlCategory.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " and Cate='" + ddlCategory.SelectedItem.Text + "'";
            }
            if (ddlEmployeeType.SelectedItem.Text != "-Select-")
            {
                SSQL = SSQL + " and EmpType='" + ddlEmployeeType.SelectedItem.Text + "'";
            }
            SSQL = SSQL + " order by MachineID  asc";
            DataTable dt = new DataTable();
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dt.Rows.Count > 0)
            {
                grid.DataSource = dt;
                grid.DataBind();

                string attachment = "attachment;filename=Attendance Bonus Report.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + dt.Columns.Count + "'>");
                Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan='" + dt.Columns.Count + "'>");
                Response.Write("<a style=\"font-weight:bold\">ATTENDANCE BONUS REPORT FOR -" + Session["Lcode"].ToString() + "-" + txtFrom.Text + "-" + txtTo.Text + "</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.Write("<table border=1>");
                Response.Write("<tr>");
                Response.Write("<td colspan='4' align=center>");
                Response.Write("<a style=\"font-weight:bold\">TOTAL</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(E4:E" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(F4:F" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(G4:G" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(H4:H" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(I4:I" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(J4:J" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(K4:K" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(M4:M" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(N4:N" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(O4:O" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("<td>");
                Response.Write("<a style=\"font-weight:bold\">=sum(P4:P" + (dt.Rows.Count + 3) + ")</a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");
                Response.End();
                Response.Clear();
            }
        }
    }

    protected void btnSignList_Click(object sender, EventArgs e)
    {
        bool ErrFlag = false;
        string SSQL = "";
        if (ddlMonths.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Month.');", true);
            ErrFlag = true;
        }
        else if (txtFrom.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the from Date Properly.');", true);
            ErrFlag = true;
        }
        else if (txtTo.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the To Date Properly.');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlfinance.SelectedValue + "&fromdate=" + txtFrom.Text + "&ToDate=" + txtTo.Text + "&Finance=" + ddlfinance.SelectedItem.Text + "&Wages=" + ddlEmployeeType.SelectedItem.Text + "&Report_Type=AttendanceBonusSignList&AttMonths=" + "" + "&EmpTypeCd=" + ddlEmployeeType.SelectedValue + "&PF_NonPF=" + "" , "_blank", "");
        }
    }
}