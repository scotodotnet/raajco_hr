﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="DownLoadClear.aspx.cs" Inherits="DownLoadClear" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
               }
        });
    };
</script>
 <script type="text/javascript">
     function SaveMsgAlert(msg) {
         swal(msg);
     }
</script>

<script type="text/javascript">
    function ProgressBarShow() {
        $('#Download_loader').show();
    }
</script>

<script type="text/javascript">
    function ProgressBarHide() {
        $('#Download_loader').hide();
    }
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Bio Metric</a></li>
				<li class="active">DownLoad/Clear</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">DownLoad/Clear </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">DownLoad/Clear</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>IP Address</label>
								  <asp:DropDownList runat="server" ID="ddlIPAddress" class="form-control select2" style="width:100%;">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="ddlIPAddress" Display="Dynamic" InitialValue="-Select-"  ValidationGroup="ValidateDept_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                          <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btndownload" Text="Download" class="btn btn-success" onclick="btndownload_Click" OnClientClick="ProgressBarShow();"/>
									<asp:Button runat="server" id="btndownloadClear" Text="Download/Clear" class="btn btn-danger" onclick="btndownloadClear_Click" OnClientClick="ProgressBarShow();"/>
								<asp:Button runat="server" id="btnDocumnet" Text="Document" class="btn btn-danger" onclick="btnDocumnet_Click" />
							
								 </div>
                               </div>
                              <!-- end col-4 -->
                             
                             
                             
                             
                              </div>
                        <!-- end row -->
                        
                        
                          <!-- begin row -->
                          <div class="row">
                          <asp:Label ID="lblDwnCmpltd" runat="server" Font-Bold="True" Font-Italic="True" 
                                    Font-Size="Medium" ForeColor="Red"></asp:Label>
                          </div>
                          <!-- end row -->
                           <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>Token Number</label>
								 <asp:TextBox runat="server" ID="txtTokenNumber" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4" runat="server" visible="false" id="Div_Wages">
							    <div class="form-group">
								 <label>Wages Type</label>
								    <asp:DropDownList runat="server" ID="ddlWages" Visible="True" class="form-control select2" style="width:100%; ">
							 	    </asp:DropDownList>
								</div>
                               </div>
                           </div>
                          <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>From Date</label>
								 <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                               <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								  <label>To Date</label>
								 <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button ID="btnConversion" class="btn btn-success"  runat="server" 
                         Text="Convert" ValidationGroup="Validate_Field" OnClick="btnConversion_Click"
                          OnClientClick="ProgressBarShow();"/>
                         <asp:Button ID="Button1" class="btn btn-success"  runat="server" 
                         Text="IDConvert" onclick="Button1_Click" Visible="true" />
								 </div>
                               </div>
                              <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <div class="row">
                         <div class="col-md-4"></div>
                           
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                         <div id="Download_loader" style="display:none"/></div>
                      
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

