﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptWeekly_OT : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDTable = new DataTable();
    string SessionUserType;

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    string basesalary = "0";
    string PerHr = "0";
    string OTWages = "0";
    string OneDay = "0";
    string Tot_OT_Hrs = "0";
    string OT_Amt = "0";
    string Wages = "";
    string MachineID = "";
    string Emp_WH_Day = "";
    string WH = "0";
    string WH_Amt = "0";
    string PF = "0";
    string SessionEpay = "";
    Boolean ErrFlag = false;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Shift Incentive";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            //Load_WagesType();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        SSQL = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
        ddlEmployeeType.Items.Insert(1, new ListItem("ALL", "ALL", true));
        ddlEmployeeType_SelectedIndexChanged(sender, e);
        // Load_Data();
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {

        //Load_Data();

    }
    protected void btn_OT_Report_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }
                ResponseHelper.Redirect("RptWeeklyLoad.aspx?Category=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&pf=" + ddlpf.SelectedValue + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
    protected void btnOT_Monthly_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                ResponseHelper.Redirect("RptOTandOTInc.aspx?Category=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&pf=" + ddlpf.SelectedValue + "&RptType=OTMonthly" + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");


            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnOT_Inc_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                ResponseHelper.Redirect("RptOTandOTInc.aspx?Category=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&pf=" + ddlpf.SelectedValue + "&ToDate=" + txtToDate.Text + "&RptType=OT_Inc" + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");

            }
        }
        catch (Exception)
        {

            throw;
        }

    }

    protected void btnShiftIncentive_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                ResponseHelper.Redirect("ViewReport.aspx?ESICode=" + "" + "&PFTypePost=" + PF + "&Left_Emp=" + "" + "&Leftdate=" + "" + "&CashBank=" + "" + "&Report_Type=" + "ShiftIncentive" + "&Cate=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&pf=" + ddlpf.SelectedValue + "&ToDate=" + txtToDate.Text + "&RptType=ShiftIncentive" + "&PayslipType=" + "" + "&Salary=" + "" + "&EmpTypeCd=" + ddlEmployeeType.SelectedItem.Text + "&Depat=" + "" + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");

            }
        }
        catch (Exception)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Check the Input Field and try Again!!!');", true);
            return;
        }

    }

    protected void btnExcel_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                SSQL = "";
                SSQL = "Select ExistingCode,MachineID,FirstName,DeptName,SUM(Present) as PresentDays,(CAST(SUM(Rate) as decimal(18,2))) as Amount from ";
                SSQL = SSQL + "(select EM.ExistingCode,CAST(EM.MachineID as varchar(50)) as MachineID,EM.FirstName,EM.DeptName,LD.Shift, LD.Total_Hrs1,LD.Total_Hrs,LD.Present,(CAST(CAST(LD.Present as decimal(18,2)) * CAST(SI.Amount as decimal(18,2)) as decimal(18,2))) as Rate,LD.Attn_Date_Str from Employee_Mst EM inner join LogTime_Days LD on LD.MachineID=EM.MachineID and LD.CompCode COLLATE DATABASE_DEFAULT =EM.CompCode COLLATE DATABASE_DEFAULT  and LD.LocCode COLLATE DATABASE_DEFAULT =EM.LocCode COLLATE DATABASE_DEFAULT";
                SSQL = SSQL + " inner join MstEmployeeType ET on ET.EmpType=EM.Wages inner join Raajco_Epay..Shift_Inc_Mst SI on SI.Shift COLLATE DATABASE_DEFAULT=LD.Shift COLLATE DATABASE_DEFAULT and SI.EmployeeType=ET.EmpTypeCd and SI.Ccode COLLATE DATABASE_DEFAULT=LD.CompCode COLLATE DATABASE_DEFAULT and SI.Lcode COLLATE DATABASE_DEFAULT=LD.LocCode COLLATE DATABASE_DEFAULT ";
                SSQL = SSQL + " where   Convert(datetime,LD.Attn_Date,103)>=Convert(datetime,'" + txtFromDate.Text + "',103)";
                if (ddlEmployeeType.SelectedValue == "ALL")
                {
                    SSQL = SSQL + " and EM.Catname='" + ddlCategory.SelectedItem.Text + "' ";
                }
                else
                {
                    SSQL = SSQL + " and EM.Catname = '" + ddlCategory.SelectedItem.Text + "' and EM.Wages = '" + ddlEmployeeType.SelectedItem.Text + "'"; 
                }
                SSQL = SSQL + " and Convert(datetime,Attn_Date,103)<=Convert(datetime,'" + txtToDate.Text + "',103) and cast(datepart(hour, LD.Total_Hrs1) + datepart(minute, LD.Total_Hrs1) / 60.00 as decimal(5, 2))>7.75 and LD.Shift!='No Shift'";
                SSQL = SSQL + ") as table1 group by ExistingCode,MachineID,FirstName,DeptName order by CAST(ExistingCode as int) asc";

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_1.Rows.Count > 0)
                {
                    grid.DataSource = dt_1;
                    grid.DataBind();

                    DataTable dt_Cat = new DataTable();
                    string attachment = "attachment;filename=ShiftIncentive.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    SSQL = "";
                    SSQL = "Select CompName,(Add1+','+Add2+','+City+' '+Pincode) as Address,RegNo from Company_Mst where CompCode='" + SessionCcode + "'";
                    dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                    string CmpName = "";
                    string Cmpaddress = "";
                    if (dt_Cat.Rows.Count > 0)
                    {
                        CmpName = dt_Cat.Rows[0]["CompName"].ToString();
                        Cmpaddress = (dt_Cat.Rows[0]["Address"].ToString());
                    }

                    grid.HeaderStyle.Font.Bold = true;
                    System.IO.StringWriter stw = new System.IO.StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);
                    grid.RenderControl(htextw);

                    Response.Write("<table>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='" + dt_1.Columns.Count + "'>");
                    Response.Write("RAAJCO SPINNERS PRIVATE LIMITED");
                    Response.Write("</td>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='" + dt_1.Columns.Count + "'>");
                    Response.Write("SHIFT INCENTIVE REPORT FOR " + ddlEmployeeType.SelectedItem.Text + " FROM " + txtFromDate.Text + "-" + txtToDate.Text + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");
                    Response.Write("<table>");
                    Response.Write("<tr align='Center'>");
                    Response.Write(stw.ToString());
                    Response.Write("</tr></table>");
                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='4'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");
                    Int32 Grand_Tot_End = 0;
                    Grand_Tot_End = dt_1.Rows.Count + 4;
                    Response.Write("<td>=sum(E5:E" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("<td>=sum(F5:F" + Grand_Tot_End.ToString() + ")</td>");
                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.End();
                    Response.Clear();
                    // ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "ProgressBarHide();", true);


                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No records Found!!!');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
                    return;
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Check the Input Field and try Again!!!');", true);
            ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(), "script", "ProgressBarHide();", true);
            return;
        }
    }
}