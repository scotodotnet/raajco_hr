﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstLabourAllot : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;
    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Department();
            ManDayCost_Details();
            Load_Shift();
        }
        Load_Allotment_Details();
    }

    private void Load_Shift()
    {
        SSQL = "";
        SSQL = "select ShiftDesc from Shift_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        ddlShift.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
        ddlShift.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Department()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlDepartment.Items.Clear();
        query = "Select * from Department_Mst";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlDepartment.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["DeptName"] = "-Select-";
        dr["DeptName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlDepartment.DataTextField = "DeptName";
        ddlDepartment.DataValueField = "DeptName";
        ddlDepartment.DataBind();
    }

    private void ManDayCost_Details()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();

        query = "Select *from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; ;
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        if (dtdsupp.Rows.Count != 0)
        {
            txtDayCost.Text = dtdsupp.Rows[0]["ManDayCost"].ToString();
            txtStaffAllot.Text = dtdsupp.Rows[0]["StaffAllot"].ToString();
        }
        else
        {
            txtDayCost.Text = "0";
            txtStaffAllot.Text = "0";
        }

    }

    private void Load_Allotment_Details()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And DeptName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And DeptName='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Allotment Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Labour Allotment Not Found');", true);
        }

        Load_Allotment_Details();
    }

    protected void btnCostSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (txtDayCost.Text == "" || txtDayCost.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Day cost');", true);
        }
        if (!ErrFlag)
        {
            query = "Select *from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; ;
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                query = "Delete from ManDayCost where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'"; ;
                objdata.RptEmployeeMultipleDetails(query);

            }
            SaveMode = "Insert";
            query = "Insert Into ManDayCost(CompCode,LocCode,ManDayCost,StaffAllot)";
            query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','" + txtDayCost.Text + "','" + txtStaffAllot.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);
        }
        if (!ErrFlag)
        {
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('ManDayCost Saved Successfully');", true);
            }
        }

        ManDayCost_Details();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        string SaveMode = "Insert";
        bool ErrFlag = false;

        if (txtLabourAllot.Text == "" || txtLabourAllot.Text == "0")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Labour Allotment');", true);
        }
        if (!ErrFlag)
        {
            query = "Select * from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And DeptName='" + ddlDepartment.SelectedValue + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                query = "Delete from Employee_Allotment_Mst_New where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                query = query + " And DeptName='" + ddlDepartment.SelectedValue + "'";
                objdata.RptEmployeeMultipleDetails(query);

            }
            SaveMode = "Insert";
            query = "Insert Into Employee_Allotment_Mst_New(CompCode,LocCode,EmpType,Emp_Count,DeptName,Shift,Allot_Date_Str,Allot_Date)";
            query = query + " Values ('" + SessionCcode + "','" + SessionLcode + "','LABOUR','" + txtLabourAllot.Text + "','" + ddlDepartment.SelectedValue + "','" + ddlShift.SelectedValue + "','" + txtAllotedDate.Text + "',Convert(Date,'" + txtAllotedDate.Text + "',103))";
            objdata.RptEmployeeMultipleDetails(query);
        }
        if (!ErrFlag)
        {
            if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Allotment Saved Successfully');", true);
            }
        }

        Load_Allotment_Details();
        Clear_All_Field();
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtLabourAllot.Text = "";
        ddlDepartment.SelectedValue = "-Select-";
        txtAllotedDate.Text = "";
        txtAvailLabour.Text = "0";
        txtLabourAllot.Text = "0";
        ddlShift.ClearSelection();

        btnSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {

    }

    protected void ddlDepartment_SelectedIndexChanged(object sender, EventArgs e)
    {
        SSQL = "Select Count(*) as Count from  Employee_Mst where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and DeptName='" + ddlDepartment.SelectedItem.Text + "' and IsActive='Yes'";
        con = new SqlConnection(constr);
        con.Open();
        SqlCommand cmd = new SqlCommand(SSQL, con);
        txtAvailLabour.Text = (cmd.ExecuteScalar()).ToString();
    }

    protected void txtLabourAllot_TextChanged(object sender, EventArgs e)
    {
        if (Convert.ToDecimal(txtLabourAllot.Text) > Convert.ToDecimal(txtAvailLabour.Text))
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Labour Allotment Should be Less than Available!!!');", true);
            txtLabourAllot.Text = "0";
            return;
        }
    }
}
