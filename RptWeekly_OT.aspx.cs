﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class RptWeekly_OT : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDTable = new DataTable();
    string SessionUserType;

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] Time_Minus_Value_Check;
    string basesalary = "0";
    string PerHr = "0";
    string OTWages = "0";
    string OneDay = "0";
    string Tot_OT_Hrs = "0";
    string OT_Amt = "0";
    string Wages = "";
    string MachineID = "";
    string Emp_WH_Day = "";
    string WH = "0";
    string WH_Amt = "0";
    string PF = "0";
    Boolean ErrFlag = false;
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Weekly OT Salary";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            //Load_WagesType();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
        }
    }
    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {

        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        SSQL = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
        // Load_Data();
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {

        //Load_Data();

    }
    protected void btn_OT_Report_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }
                ResponseHelper.Redirect("RptWeeklyLoad.aspx?Category=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&pf=" + ddlpf.SelectedValue + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");
            }
        }
        catch (Exception)
        {
            throw;
        }
    }
    protected void btnOT_Monthly_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                ResponseHelper.Redirect("RptOTandOTInc.aspx?Category=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&ToDate=" + txtToDate.Text + "&pf=" + ddlpf.SelectedValue + "&RptType=OTMonthly" + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");


            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnOT_Inc_Click(object sender, EventArgs e)
    {
        try
        {
            string Stafflabour = "";
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            if (ddlFinance.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Fin.Year');", true);
                ErrFlag = true;
            }
            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Month');", true);
                ErrFlag = true;
            }
            if (txtFromDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }
            if (txtToDate.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Please Enter the Date Properly');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {
                if (ddlCategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlCategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }




                ResponseHelper.Redirect("RptOTandOTInc.aspx?Category=" + ddlCategory.SelectedItem.Text + "&Months=" + ddlMonths.SelectedItem.Text + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtFromDate.Text + "&pf=" + ddlpf.SelectedValue + "&ToDate=" + txtToDate.Text + "&RptType=OT_Inc" + "&EmpType=" + ddlEmployeeType.SelectedItem.Text, "_blank", "");


            }
        }
        catch (Exception)
        {

            throw;
        }

    }
}