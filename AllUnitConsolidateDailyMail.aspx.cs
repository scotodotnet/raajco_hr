﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic;
using System.IO;
using System.Net.Mail;

public partial class AllUnitConsolidateDailyMail : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUnit;
    string FromDate;
    string AttachfileName_Invoice_Bill = "";
    string SSQL;

    DataTable mDataSet = new DataTable();
    DataTable dt = new DataTable();
    DataTable presentDT = new DataTable();
    DataTable improperDT = new DataTable();
    DataTable MancostDT = new DataTable();
    BALDataAccess objdata = new BALDataAccess();
    DataTable AutoDataTable = new DataTable();
    DataSet ds = new DataSet();
    string Division;

    string Rpt_To_Date = "";
    string Rpt_From_Date = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        SessionCcode = "ESM";
        SessionLcode = "UNIT I";
        SessionAdmin = "1";
        SessionUserType = "1";


        DataTable DT = new DataTable();
        string query = "Select convert(varchar,getdate(),103) as ServerDate";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            string DBDate = "";
            string ToDate_Month_Get = "";
            string ToDate_Year_Get = "";
            DBDate = DT.Rows[0]["ServerDate"].ToString();
            //DBDate = "13/11/2018"; //DT.Rows[0]["ServerDate"].ToString();
            
            //FromDate = "13/11/2018";
            //FromDate = Convert.ToDateTime(DBDate).AddDays(-1).ToString("dd/MM/yyyy");
            
            Rpt_To_Date = Convert.ToDateTime(DBDate).AddDays(-1).ToString("dd/MM/yyyy");

            ToDate_Month_Get = Convert.ToDateTime(DBDate).AddDays(-1).Month.ToString();
            ToDate_Year_Get = Convert.ToDateTime(DBDate).AddDays(-1).Year.ToString();
            if (ToDate_Month_Get.Length == 1)
            {
                ToDate_Month_Get = "0" + ToDate_Month_Get;
            }

            if (Convert.ToDateTime(DBDate).AddDays(-1).Day.ToString() == "1" || Convert.ToDateTime(DBDate).AddDays(-1).Day.ToString() == "01")
            {
                Rpt_From_Date = Convert.ToDateTime(DBDate).AddDays(-1).ToString("dd/MM/yyyy");
            }
            else
            {
                Rpt_From_Date = "01/" + ToDate_Month_Get + "/" + ToDate_Year_Get;
            }
            NEW_MD_Report_GetAttdTable_Allotment();
            
            //FromDate = Convert.ToDateTime(DBDate).AddDays(-1).ToString("dd/MM/yyyy");



            //Report();
            
            InvoiceMail_AttachandSend();
            txtDisplay.Text = "Mail sent Successfully...";
        }
        
    }

    public void Report()
    {
        AutoDataTable.Columns.Add("CompanyName");
        AutoDataTable.Columns.Add("LocationName");
        AutoDataTable.Columns.Add("UnitII");

        AutoDataTable.Columns.Add("SNo");
        AutoDataTable.Columns.Add("Dept");
        AutoDataTable.Columns.Add("UnitI");
        AutoDataTable.Columns.Add("BasicSalary");



        SSQL = " Select LocCode,Sum(Emp_Count) as Emp_Count from  Employee_Allotment_Mst_New ";
        SSQL = SSQL + " group by LocCode";

        mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);
        if (mDataSet.Rows.Count != 0)
        {
            for (int i = 0; i < mDataSet.Rows.Count; i++)
            {
                //Present Count
                SSQL = "Select Count(Present) as Present from LogTime_Days ";
                SSQL = SSQL + " where  LocCode='" + mDataSet.Rows[i]["LocCode"].ToString() + "' and CompCode='" + SessionCcode + "' and Present !='0.0' ";

                if (FromDate != "")
                {
                    SSQL = SSQL + " and Attn_Date_Str='" + FromDate + "' ";
                }
                presentDT = objdata.RptEmployeeMultipleDetails(SSQL);

                //Improper Count
                SSQL = "select Count(MachineID) as Improper";
                SSQL = SSQL + " from LogTime_Days   ";
                SSQL = SSQL + " where CompCode='" + SessionCcode.ToString() + "' ANd LocCode='" + mDataSet.Rows[i]["LocCode"].ToString() + "'";

                if (FromDate != "")
                {
                    SSQL = SSQL + " and Attn_Date_Str='" + FromDate + "' ";
                }
                SSQL = SSQL + " And TypeName='Improper'";
                improperDT = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Select ManDayCost from ManDayCost  where CompCode ='" + SessionCcode.ToString() + "' ANd LocCode='" + mDataSet.Rows[i]["LocCode"].ToString() + "'";
                MancostDT = objdata.RptEmployeeMultipleDetails(SSQL);


                SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
                dt = objdata.RptEmployeeMultipleDetails(SSQL);
                string name = dt.Rows[0]["CompName"].ToString();

                AutoDataTable.NewRow();
                AutoDataTable.Rows.Add();
                AutoDataTable.Rows[i]["CompanyName"] = name;
                AutoDataTable.Rows[i]["LocationName"] = mDataSet.Rows[i]["LocCode"].ToString();
                AutoDataTable.Rows[i]["UnitII"] = mDataSet.Rows[i]["Emp_Count"].ToString();
                AutoDataTable.Rows[i]["SNo"] = i + 1;
                AutoDataTable.Rows[i]["Dept"] = presentDT.Rows[0]["Present"].ToString();
                AutoDataTable.Rows[i]["UnitI"] = improperDT.Rows[0]["Improper"].ToString();
                AutoDataTable.Rows[i]["BasicSalary"] = MancostDT.Rows[0]["ManDayCost"].ToString();

            }
            ds.Tables.Add(AutoDataTable);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/UnitwiseConsolidate.rpt"));

            report.DataDefinition.FormulaFields["Date_Str"].Text = "'" + FromDate + "'";

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;


            //string Server_Path = Server.MapPath("~");

            //string Invoice_No_Str = FromDate.Replace("/", "_");
            //string[] Check_Val = FromDate.Split(new[] { "/", "_" }, StringSplitOptions.RemoveEmptyEntries);
            //AttachfileName_Invoice_Bill = Server_Path + "/Daily_Report/All_UNIT_Daily_Attendance_" + Check_Val[0] + Check_Val[1] + Check_Val[2] + ".pdf";

            //if (File.Exists(AttachfileName_Invoice_Bill))
            //{
            //    File.Delete(AttachfileName_Invoice_Bill);
            //}

            //report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Invoice_Bill);

        }
    }

    public void InvoiceMail_AttachandSend()
    {
        string query = "";
        string To_Address = "";
        string CC_Mail_Address = "";
        string BCC_Mail_Address = "";
        DataTable Order_Mail_DT = new DataTable();
        DataTable DT_T = new DataTable();
        DataTable Party_DT = new DataTable();

        query = "Select * from MailAdd_Mst where CompCode='ESM' And LocCode='UNIT I'";
        DT_T = objdata.RptEmployeeMultipleDetails(query);

        if (DT_T.Rows.Count != 0)
        {
            To_Address = DT_T.Rows[0]["ToAdd"].ToString();
            //CC Address Add
            if (DT_T.Rows[0]["CCAdd"].ToString() != "")
            {
                CC_Mail_Address = DT_T.Rows[0]["CCAdd"].ToString() + "," + "aatm2005@gmail.com";// "kalyan.r@scoto.in";
            }
            else
            {
                CC_Mail_Address = "aatm2005@gmail.com";// "kalyan.r@scoto.in";
            }
            //BCC Address Add
            if (DT_T.Rows[0]["BCCAdd"].ToString() != "")
            {
                BCC_Mail_Address = DT_T.Rows[0]["BCCAdd"].ToString();// "kalyan.r@scoto.in";
            }

            MailMessage mm = new MailMessage("ALL UNIT MAN POWER ENGAGED REPORT DETAILS<aatm2005@gmail.com>", To_Address);
            mm.Subject = "ALL UNIT MAN POWER ENGAGED REPORT DETAILS";
            mm.Body = "Find the attachment for All Unit Man Power Engaged Report Details";
            mm.Attachments.Add(new Attachment(AttachfileName_Invoice_Bill));
            if (CC_Mail_Address != "") { mm.CC.Add(CC_Mail_Address); }
            if (BCC_Mail_Address != "") { mm.Bcc.Add(BCC_Mail_Address); }
            mm.IsBodyHtml = false;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";   // We use gmail as our smtp client
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius@2005");

            smtp.Send(mm);
            mm.Attachments.Dispose();
        }
    }

    private void NEW_MD_Report_GetAttdTable_Allotment()
    {
        string SSQL = "";
        string ss = "";
        DataTable mDataset = new DataTable();
        DataTable dtRow = new DataTable();

        DataTable table = new DataTable();

        table.Columns.Add("CompanyName");
        table.Columns.Add("LocationName");
        table.Columns.Add("Date1");
        table.Columns.Add("Date2");
        table.Columns.Add("Unit");
        table.Columns.Add("OnRoll");
        table.Columns.Add("Alloted");
        table.Columns.Add("Engaged");
        table.Columns.Add("Excess");
        table.Columns.Add("CostManday");
        table.Columns.Add("OnDate");
        table.Columns.Add("FromToDate");
        table.Columns.Add("TotStrength");

        DataTable Emp_DS = new DataTable();
        DataTable OnRoll_DS = new DataTable();
        DataTable Enaged_DS = new DataTable();
        DataTable Cost_DS = new DataTable();
        string Date_Value_Str = "";
        
        SSQL = "Select Distinct EM.LocCode as LocCode,Count(EM.EmpNo) as EmpCount from Employee_Mst EM";
        SSQL = SSQL + " inner Join Employee_Allotment_Mst_New EA on EM.DeptName=EA.DeptName";
        SSQL = SSQL + " And EM.LocCode=EA.LocCode where (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR, 103)>=CONVERT(DATETIME,'" + Rpt_To_Date + "',103))";
        SSQL = SSQL + " And EM.CatName = 'LABOUR'";
        SSQL = SSQL + " Group By EM.LocCode";
        SSQL = SSQL + " Order by EM.LocCode Asc";
        Emp_DS = objdata.RptEmployeeMultipleDetails(SSQL);
        for (int i = 0; i < Emp_DS.Rows.Count; i++)
        {
            table.NewRow();
            table.Rows.Add();
            double Allot = 0.0;
            double Engaged = 0.0;
            double Excess = 0.0;
            double CostMan = 0.0;
            double On_Cost = 0.0;

            table.Rows[i]["CompanyName"] = "Eveready Spinning Mills (P) Ltd";
            table.Rows[i]["Unit"] = Emp_DS.Rows[i]["LocCode"].ToString();
            table.Rows[i]["OnRoll"] = Emp_DS.Rows[i]["EmpCount"].ToString();
            table.Rows[i]["Date1"] = Rpt_From_Date;
            table.Rows[i]["Date2"] = Rpt_To_Date;

            //OnRoll_DS
            ss = "Select sum(EA.Emp_Count) As Emp_Count from Employee_Allotment_Mst_New EA where EA.LocCode='" + Emp_DS.Rows[i]["LocCode"].ToString() + "'";

            OnRoll_DS = objdata.RptEmployeeMultipleDetails(ss);

            if (OnRoll_DS.Rows.Count != 0)
            {
                if (OnRoll_DS.Rows[0]["Emp_Count"].ToString() != "")
                {
                    table.Rows[i]["Alloted"] = OnRoll_DS.Rows[0]["Emp_Count"].ToString();
                    Allot = Convert.ToDouble(OnRoll_DS.Rows[0]["Emp_Count"].ToString());
                }
                else
                {
                    table.Rows[i]["Alloted"] = "0.0";
                    Allot = 0.0;
                }
            }
            else
            {
                table.Rows[i]["Alloted"] = "0.0";
                Allot = 0.0;
            }
            //Enaged_DS
            ss = "Select sum(LD.Present) as Present from LogTime_Days LD inner Join Employee_Mst EM on LD.DeptName=EM.DeptName And EM.LocCode=LD.LocCode And EM.ExistingCode=LD.ExistingCode";
            ss = ss + " inner Join Employee_Allotment_Mst_New EA on EM.DeptName=EA.DeptName And LD.DeptName=EA.DeptName And EA.LocCode=LD.LocCode And EA.LocCode=EM.LocCode";
            ss = ss + " Where LD.Present<>'0.0' And LD.LocCode='" + Emp_DS.Rows[i]["LocCode"].ToString() + "' And (CONVERT(DATETIME,LD.Attn_Date, 103)=CONVERT(DATETIME,'" + Rpt_To_Date + "',103))";
            ss = ss + " And EM.CatName='LABOUR' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR, 103)>=CONVERT(DATETIME,'" + Rpt_To_Date + "',103))";
            Enaged_DS = objdata.RptEmployeeMultipleDetails(ss);
            if (Enaged_DS.Rows.Count != 0)
            {
                if (Enaged_DS.Rows[0]["Present"].ToString() != "")
                {
                    table.Rows[i]["Engaged"] = Enaged_DS.Rows[0]["Present"].ToString();
                    Engaged = Convert.ToDouble(Enaged_DS.Rows[0]["Present"].ToString());
                }
                else
                {
                    table.Rows[i]["Engaged"] = "0.0";
                    Engaged = 0.0;
                }
            }
            else
            {
                table.Rows[i]["Engaged"] = "0.0";
                Engaged = 0.0;
            }

            Excess = Engaged - Allot;
            table.Rows[i]["Excess"] = Excess;

            ss = "Select ManDayCost from ManDayCost Where LocCode='" + Emp_DS.Rows[i]["LocCode"].ToString() + "'";
            Cost_DS = objdata.RptEmployeeMultipleDetails(ss);
            if (Cost_DS.Rows.Count != 0)
            {
                table.Rows[i]["CostManday"] = Cost_DS.Rows[0]["ManDayCost"].ToString();
                CostMan = Convert.ToDouble(Cost_DS.Rows[0]["ManDayCost"].ToString());
            }
            else
            {
                table.Rows[i]["CostManday"] = "0";
                CostMan = 0;
            }

            if (Excess > 0)
            {
                On_Cost = Excess * CostMan;
                table.Rows[i]["OnDate"] = On_Cost;
            }
            else
            {
                On_Cost = 0;
                table.Rows[i]["OnDate"] = On_Cost;
            }

            DateTime EndDate = Convert.ToDateTime(Rpt_To_Date);
            DateTime StartDate = Convert.ToDateTime(Rpt_From_Date);
            TimeSpan t = EndDate - StartDate;
            int dayCount = Convert.ToInt32(t.TotalDays);
            int daysAdded = 0;
            int FromTo_val = 0;
            double FromTo_Exc = 0.0;
            for (int intCol = 0; intCol <= dayCount - 1; intCol++)
            {
                double Allot1 = 0.0;
                double Engaged1 = 0.0;
                double Excess1 = 0.0;
                double CostMan1 = 0.0;
                double On_Cost1 = 0.0;

                Date_Value_Str = Convert.ToDateTime(Rpt_From_Date).AddDays(intCol).ToString("dd/MM/yyyy");
                ss = "Select sum(EA.Emp_Count) As Emp_Count from Employee_Allotment_Mst_New EA where EA.LocCode='" + Emp_DS.Rows[i]["LocCode"].ToString() + "'";
                OnRoll_DS = objdata.RptEmployeeMultipleDetails(ss);
                if (OnRoll_DS.Rows.Count != 0)
                {
                    if (OnRoll_DS.Rows[0]["Emp_Count"].ToString() != "")
                    {
                        Allot1 = Convert.ToDouble(OnRoll_DS.Rows[0]["Emp_Count"].ToString());
                    }
                    else
                    {
                        Allot1 = 0.0;
                    }
                }
                else
                {
                    Allot1 = 0.0;
                }

                ss = "Select sum(LD.Present) as Present from LogTime_Days LD inner Join Employee_Mst EM on LD.DeptName=EM.DeptName And EM.LocCode=LD.LocCode And EM.ExistingCode=LD.ExistingCode";
                ss = ss + " inner Join Employee_Allotment_Mst_New EA on EM.DeptName=EA.DeptName And LD.DeptName=EA.DeptName And EA.LocCode=LD.LocCode And EA.LocCode=EM.LocCode";
                ss = ss + " Where LD.Present<>'0.0' And LD.LocCode='" + Emp_DS.Rows[i]["LocCode"].ToString() + "' And (CONVERT(DATETIME,LD.Attn_Date, 103)=CONVERT(DATETIME,'" + Date_Value_Str + "',103))";
                ss = ss + " And EM.CatName='LABOUR' And (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR, 103)>=CONVERT(DATETIME,'" + Rpt_To_Date + "',103))";
                Enaged_DS = objdata.RptEmployeeMultipleDetails(ss);
                if (Enaged_DS.Rows.Count != 0)
                {
                    if (Enaged_DS.Rows[0]["Present"].ToString() != "")
                    {
                        Engaged1 = Convert.ToDouble(Enaged_DS.Rows[0]["Present"].ToString());
                    }
                    else
                    {
                        Engaged1 = 0.0;
                    }
                }
                else
                {
                    Engaged1 = 0.0;
                }
                Excess1 = Engaged1 - Allot1;
                if (Excess > 0)
                {
                    On_Cost1 = Excess1 * CostMan;
                }
                else
                {
                    On_Cost = 0;
                }
                FromTo_val = FromTo_val + Convert.ToInt32(On_Cost1);
            }
            table.Rows[i]["FromToDate"] = FromTo_val;
        }

        if (table.Rows.Count != 0)
        {
            ds.Tables.Add(table);
            ReportDocument report = new ReportDocument();
            report.Load(Server.MapPath("crystal/UnitwiseConsolidate_Mail_Final.rpt"));

            //report.DataDefinition.FormulaFields["Date_Str"].Text = "'" + FromDate + "'";

            report.Database.Tables[0].SetDataSource(ds.Tables[0]);
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;
            //report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.ReportSource = report;


            string Server_Path = Server.MapPath("~");

            string Invoice_No_Str = Rpt_To_Date.Replace("/", "_");
            string[] Check_Val = Rpt_To_Date.Split(new[] { "/", "_" }, StringSplitOptions.RemoveEmptyEntries);
            AttachfileName_Invoice_Bill = Server_Path + "/Daily_Report/ALL_Unit_Man_Power_Engaged_" + Check_Val[0] + Check_Val[1] + Check_Val[2] + ".pdf";

            if (File.Exists(AttachfileName_Invoice_Bill))
            {
                File.Delete(AttachfileName_Invoice_Bill);
            }

            report.ExportToDisk(ExportFormatType.PortableDocFormat, AttachfileName_Invoice_Bill);
        }
    }
}
