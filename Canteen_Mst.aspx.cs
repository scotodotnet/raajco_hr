﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Canteen_Mst : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;

    string Query = "";
    string basic = "0";
    string HRA = "0";
    string Conv = "0";
    string Spl = "0";
    string WH = "0";
    string Medical = "0";
    string Edu = "0";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {

            LoadShift();


        }

        Load_Data();
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadShift();
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);



    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        // LoadShift();

    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string Errflag = "False";

        if (ddlCategory.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Category...');", true);
        }
        if ((ddlEmployeeType.SelectedItem.Text == "-Select-") || (ddlEmployeeType.SelectedItem.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter Employee Type');", true);
        }




        if ( (txtAmount.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Enter Amount...');", true);
        }




        if ((ddlFoodtype.SelectedItem.Text == "-Select-") || (ddlEmployeeType.SelectedItem.Text == ""))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Food Type');", true);
        }

        if (Errflag != "true")
        {
            try
            {
                String MsgFlag = "Insert";
                DataTable dt = new DataTable();
                Query = "select * from [Raajco_Epay]..Mst_Canteen where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FoodType='" + ddlFoodtype.SelectedItem.Text + "' ";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                   Query = "delete from [Raajco_Epay]..Mst_Canteen where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Compcode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' and FoodType='" + ddlFoodtype.SelectedItem.Text + "' ";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MsgFlag = "Update";
                }

                Query = "";
                Query = Query + "Insert into [Raajco_Epay]..Mst_Canteen(Compcode,LocCode,Category,EmployeeType,FoodType,Amount,CreatedDate";
                Query = Query + ")Values('" + SessionCcode + "','" + SessionLcode + "',";
               Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + ddlFoodtype.SelectedItem.Text + "','" + txtAmount.Text + "','" + DateTime.Now.ToString("dd/MM/yyyy") + "')";

                objdata.RptEmployeeMultipleDetails(Query);
                Clear();
                if (MsgFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
                Load_Data();
            }
            catch (Exception Ex)
            {

            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {

        ddlCategory.SelectedValue = "0";
        LoadShift();
        txtAmount.Text = "0";
        ddlFoodtype.SelectedValue = "-Select-";
        loadEmp();

    }
    private void loadEmp()
    {
        DataTable dtdsupp1 = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp1 = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp1;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        //ddlEmployeeType_SelectedIndexChanged(sender, e);

    }




    private void LoadShift()
    {
        //DataTable dtdsupp2 = new DataTable();
        //ddlEmployeeType.Items.Clear();
        //Query = "Select ShiftDesc from Shift_Mst";
        //dtdsupp2 = objdata.RptEmployeeMultipleDetails(Query);
        //ddlshift.DataSource = dtdsupp2;
        //DataRow dr = dtdsupp2.NewRow();
        //dr["ShiftDesc"] = "-Select-";
        ////dr["EmpTypeCd"] = "-Select-";
        //dtdsupp2.Rows.InsertAt(dr, 0);
        //ddlshift.DataTextField = "ShiftDesc";
        ////  ddlEmployeeType.DataValueField = "EmpTypeCd";
        //ddlshift.DataBind();
    }

    private void LoadData()
    {
        DataTable dt1 = new DataTable();

        Query = "";
        Query = "select * from [Raajco_Epay]..Mst_Canteen where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FoodType='" + ddlFoodtype.SelectedItem.Text + "' ";
        dt1 = objdata.RptEmployeeMultipleDetails(Query);
        if (dt1.Rows.Count != 0)
        {

            txtAmount.Text = dt1.Rows[0]["Amount"].ToString();
        }
    }

    protected void ddlshift_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadData();
    }


    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "select * from [Raajco_Epay]..Mst_Canteen OT inner join [Raajco_Spay]..MstEmployeeType  MSt on OT.EmployeeType = MSt.EmpTypeCd";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }
    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string EmpType = "";
        string FoodType = "";
       
        {
            string[] D1 = e.CommandArgument.ToString().Split('|');
            EmpType = D1[0];
            FoodType = D1[1];
        
        }



        string query = "";
        DataTable DT = new DataTable();
        query = "select EmpType, * from [Raajco_Epay]..Mst_Canteen OT inner join [Raajco_Spay]..MstEmployeeType  MSt on OT.EmployeeType = MSt.EmpTypeCd";
        query = query + " where EmployeeType='" + EmpType + "' and FoodType='" + FoodType + "' ";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            ddlCategory.SelectedValue = DT.Rows[0]["Category"].ToString();
            Session["Cate"] = DT.Rows[0]["Category"].ToString();
            Session["Emp"] = DT.Rows[0]["EmployeeType"].ToString();
            Edit_EmpLoad(sender, e);
            ddlFoodtype.SelectedItem.Text= DT.Rows[0]["FoodType"].ToString();
            txtAmount.Text = DT.Rows[0]["Amount"].ToString();
            //ddlOTHrs.SelectedValue = DT.Rows[0]["OT_Hrs"].ToString();
            //TxtWH_Amt.Text = DT.Rows[0]["WH_Amt"].ToString();

        }
        else
        {
        }
    }
    private void Edit_EmpLoad(object sender, CommandEventArgs e)
    {
        string emp = "";
        string cate = "";
        cate = Session["Cate"].ToString();
        emp = Session["Emp"].ToString();
        LoadShift();
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + cate + "' and EmpTypeCD='" + emp + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
        Session.Remove("Emp");
        Session.Remove("Cate");


    }
    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string EmpType = "";
        string FoodType = "";
       
        {
            string[] D1 = e.CommandArgument.ToString().Split('|');
            EmpType = D1[0];
            FoodType = D1[1];
          
        }



        string query = "";
        DataTable DT = new DataTable();
        query = "select EmpType, * from [Raajco_Epay]..Mst_Canteen OT inner join [Raajco_Spay]..MstEmployeeType  MSt on OT.EmployeeType = MSt.EmpTypeCd";
        query = query + " where EmployeeType='" + EmpType + "' and FoodType='" + FoodType + "' ";

        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count > 0)
        {
            Query = "delete from [Raajco_Epay]..OT_Inc_Mst where EmployeeType='" + EmpType + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and FoodType='" + FoodType + "'";
            objdata.RptEmployeeMultipleDetails(Query);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Details are Deleted...');", true);
            Load_Data();
        }
    }




}


