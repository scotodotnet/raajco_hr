﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="MstDACalc.aspx.cs" Inherits="MstDACalc" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            $('#example').dataTable();

        });
    </script>

    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('.select2').select2();
                    $('#example').dataTable();
                }
            });
        };
    </script>


    <!-- begin #content -->
    <div id="content" class="content">
        <ol class="breadcrumb pull-right">
            <li class="active">Incentive Master</li>
        </ol>
        <h1 class="page-header">DA MASTER</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                        </div>
                        <h4 class="panel-title">DA CALCULATION</h4>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>DA Point</label>
                                        <asp:TextBox runat="server" ID="txtDaScore" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Form Date</label>
                                        <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker"></asp:TextBox>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                <!-- begin col-4 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>To Date</label>
                                        <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker"></asp:TextBox>
                                    </div>
                                </div>
                                <!-- end col-4 -->
                                 <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Less Point</label>
                                        <asp:TextBox runat="server" ID="txtLessPoint" class="form-control" AutoPostBack="true" OnTextChanged="txtLessPoint_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Payable Point</label>
                                        <asp:TextBox runat="server" ID="txtPayablePoint" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Paisa Rate</label>
                                        <asp:TextBox runat="server" ID="txtPaisaRate" class="form-control" AutoPostBack="true" OnTextChanged="txtPaisaRate_TextChanged"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="col-md-2">
                                    <div class="form-group">
                                        <label>DA Amount</label>
                                        <asp:TextBox runat="server" ID="txtDAAmount" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row" runat="server" visible="false">
                                <h4>Type I</h4>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>From</label>
                                        <asp:TextBox runat="server" ID="txtTypeIFrom" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>To</label>
                                        <asp:TextBox runat="server" ID="txtTypeITo" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Type I Point</label>
                                        <asp:TextBox runat="server" ID="txtMin1Point" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                            </div>


                            <div class="row" runat="server" visible="false">
                                <h4>Type II</h4>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>From</label>
                                        <asp:TextBox runat="server" ID="txtTypeIIFrom" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>To</label>
                                        <asp:TextBox runat="server" ID="txtTypeIITo" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Type II Point</label>
                                        <asp:TextBox runat="server" ID="txtMin2Point" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Type II Fixed DA</label>
                                        <asp:TextBox runat="server" ID="txtTypeIIFixedDA" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>

                            </div>

                            <div class="row" runat="server" visible="false">
                                <h4>Type III</h4>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>From</label>
                                        <asp:TextBox runat="server" ID="txtTypeIIIFrom" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>To</label>
                                        <asp:TextBox runat="server" ID="txtTypeIIITo" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Type III Point</label>
                                        <asp:TextBox runat="server" ID="txtMin3Point" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Type III Fixed DA</label>
                                        <asp:TextBox runat="server" ID="txtTypeIIIFixedDA" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>


                            <div class="row" runat="server" visible="false">
                                <h4>Type IV</h4>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>ADDITIONAL DA RATE 1</label>
                                        <asp:TextBox runat="server" ID="txtDaRate1" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>ADDITIONAL DA Point</label>
                                        <asp:TextBox runat="server" ID="Add1DaPoint" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>ADDITIONAL DA RATE 2</label>
                                        <asp:TextBox runat="server" ID="txtDaRate2" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>ADDITIONAL DA Point</label>
                                        <asp:TextBox runat="server" ID="Add2DaPoint" Text="0" class="form-control"></asp:TextBox>
                                    </div>
                                </div>


                            </div>

                            <div class="row">

                                <div class="col-md-4">
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button runat="server" ID="btnSave" Text="Calculate" class="btn btn-success"
                                            OnClick="btnSave_Click" />
                                        <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger" />

                                    </div>
                                </div>

                            </div>
                            <div class="row" runat="server" visible="false">
                                <div class="col-md-4">
                                    <label>DA Point</label>
                                    <asp:DropDownList ID="ddlDascore" class="form-control  select2" runat="server">
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-4">
                                    <br />
                                    <asp:Button runat="server" ID="btnReport" Text="View" class="btn btn-info"
                                        OnClick="btnReport_Click" />
                                </div>
                            </div>


                            <%-- <div class="panel">
                         
                        <h5>Hostel / Regular Basic Incentive Text</h5>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <asp:RadioButtonList ID="txtRdpHos_Rg_Incent_Txt" runat="server" RepeatColumns="3" class="form-control" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="1" Text="Attendance" style="padding:5px;"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Production" style="padding:5px;"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Punctuality" style="padding:5px;"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <br />
									<asp:Button runat="server" id="BtnHos_Rg_Incen" Text="Save" class="btn btn-success" />
						    	 </div>
                            </div>
                        </div>
                       
                       
                         <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Department Incentive Amount</label>
								    <asp:TextBox runat="server" ID="txtDeptIncAmt" Text="0.00" class="form-control" style="width:100%;"></asp:TextBox>
								</div>
                            </div>
                             <div class="col-md-4">
								<div class="form-group">
								    <br />
									<asp:Button runat="server" id="BtnDeptIncAmt" Text="Save" class="btn btn-success" />
						    	 </div>
                            </div>
                        </div>
                        </div>--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

