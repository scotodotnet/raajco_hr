﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;
public partial class ActiveEmpList : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    string ReportType = "";
    string FromDate = "";
    string ToDate = "";
    DataTable mDataSet = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Day Attendance Shift Wise";

            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();

            Status = Request.QueryString["Status"].ToString();
            //ShiftType1 = Request.QueryString["Shift"].ToString();
            //  Date = Request.QueryString["Date"].ToString();
            // ReportType = Request.QueryString["Report"].ToString();
            ReportType= Request.QueryString["ReportType"].ToString();
            if (ReportType != "EMPLOYEE LIST")
            {
                FromDate= Request.QueryString["FromDate"].ToString();
                ToDate= Request.QueryString["ToDate"].ToString();
            }


            Get_Emp();
        }
    }
    private void Get_Emp()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("MachineID");
        DataCell.Columns.Add("Emp.Code");
        // DataCell.Columns.Add("ExCode");
        DataCell.Columns.Add("Name");
        DataCell.Columns.Add("Gender");
        DataCell.Columns.Add("Category");
        DataCell.Columns.Add("SubCategory");
        DataCell.Columns.Add("Dept");
        DataCell.Columns.Add("Designation");
      //  DataCell.Columns.Add("Shift");
        DataCell.Columns.Add("DOJ");
        if (ReportType == "EMPLOYEE RESIGN")
        {
            DataCell.Columns.Add("DOR");
        }
            DataCell.Columns.Add("DOB");
        DataCell.Columns.Add("Age");
        DataCell.Columns.Add("Wages");
        DataCell.Columns.Add("BaseSalary");
        DataCell.Columns.Add("WeekOff");
        DataCell.Columns.Add("Eligible_PF");
        DataCell.Columns.Add("PFNo");
        DataCell.Columns.Add("PFDOJ");
        DataCell.Columns.Add("Eligible_ESI");
        DataCell.Columns.Add("ESICode");
        DataCell.Columns.Add("ESIDOJ");
        DataCell.Columns.Add("BankName");
        DataCell.Columns.Add("Account_No");
        DataCell.Columns.Add("IFSC_Code");
        DataCell.Columns.Add("Adhar_No");
         DataCell.Columns.Add("UAN");
        DataCell.Columns.Add("EmployeeMobile");
        DataCell.Columns.Add("Address");



        SSQL = "";
        //SSQL = "select ('&nbsp;'+ESICode) as ESICode,('&nbsp;'+UAN) as UAN,('&nbsp;'+Adhar_No) as Adhar_No,* from ";
        SSQL = "select ('&nbsp;'+AccountNo) as Account_No,('&nbsp;'+ESINo) as ESICode,('&nbsp;'+UAN) as UAN,('&nbsp;'+Adhar_No) as Adhar_No,";
        SSQL = SSQL + " ('&nbsp;'+Address1)Address, * from ";
        if (Status == "Approval")
        {
            SSQL = SSQL + "Employee_Mst EM";
        }
        if (Status == "Pending")
        {
            SSQL = SSQL + "Employee_Mst_New_Emp EM";
        }
        SSQL = SSQL + " Where EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
        if ((Status == "Approval")&&(ReportType != "EMPLOYEE RESIGN"))
        {
            SSQL = SSQL + "and IsActive ='Yes'";
        }
        if (ReportType == "EMPLOYEE NEW JOINING")
        {
            SSQL = SSQL + "  and CONVERT(datetime,DOJ,103) >= CONVERT(datetime,'" + FromDate+"',103)";
            SSQL = SSQL + "   and CONVERT(datetime, DOJ,103) <= CONVERT(datetime, '" +ToDate +"', 103)";
        }
        if (ReportType == "EMPLOYEE RESIGN")
        {
            SSQL = SSQL + "  and CONVERT(datetime,DOR,103) >= CONVERT(datetime,'" + FromDate + "',103)";
            SSQL = SSQL + "   and CONVERT(datetime, DOR,103) <= CONVERT(datetime, '" + ToDate + "', 103)";
        }


        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);
        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                DataCell.NewRow();
                DataCell.Rows.Add(); 
                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["MachineID"] = AutoDTable.Rows[i]["MachineID"].ToString();
                DataCell.Rows[i]["Emp.Code"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                DataCell.Rows[i]["Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                DataCell.Rows[i]["Category"] = AutoDTable.Rows[i]["CatName"].ToString();
                DataCell.Rows[i]["Dept"] = AutoDTable.Rows[i]["DeptName"].ToString();
                DataCell.Rows[i]["SubCategory"] = AutoDTable.Rows[i]["SubCatName"].ToString();
                DataCell.Rows[i]["Designation"] = AutoDTable.Rows[i]["Designation"].ToString();

                string DOJ= AutoDTable.Rows[i]["DOJ"].ToString();
                string DOJ1 = "";
                if (DOJ != "")
                {
                    string[] DojSplit = DOJ.Split(' ');
                    DOJ1 = DojSplit[0];
                }
                else
                {
                    DOJ1 = "";
                }
                //   DataCell.Rows[i]["DOJ"] = AutoDTable.Rows[i]["DOJ"].ToString();
                DataCell.Rows[i]["DOJ"] = DOJ1;
                if (ReportType == "EMPLOYEE RESIGN")
                {
                    DataCell.Rows[i]["DOR"] = AutoDTable.Rows[i]["DOR"].ToString();
                }
                //DataCell.Rows[i]["DOB"] = AutoDTable.Rows[i]["BirthDate"].ToString();

                string DOB = AutoDTable.Rows[i]["DOJ"].ToString();
                string DOB1 = "";
                if (DOB != "")
                {
                    string[] DobSplit = DOB.Split(' ');
                    DOB1 = DobSplit[0];
                }
                else
                {
                    DOB1 = "";
                }
                //   DataCell.Rows[i]["DOJ"] = AutoDTable.Rows[i]["DOJ"].ToString();
                DataCell.Rows[i]["DOB"] = DOB1;



                DataCell.Rows[i]["Age"] = AutoDTable.Rows[i]["Age"].ToString();
                DataCell.Rows[i]["Wages"] = AutoDTable.Rows[i]["Wages"].ToString();
                DataCell.Rows[i]["WeekOff"] = AutoDTable.Rows[i]["WeekOff"].ToString();
                DataCell.Rows[i]["BaseSalary"] = AutoDTable.Rows[i]["BaseSalary"].ToString();
                DataCell.Rows[i]["PFNo"] = AutoDTable.Rows[i]["PFNo"].ToString();
                DataCell.Rows[i]["PFDOJ"] = AutoDTable.Rows[i]["PFDOJ"].ToString();
                DataCell.Rows[i]["Eligible_PF"] = AutoDTable.Rows[i]["Eligible_PF"].ToString();
                DataCell.Rows[i]["Eligible_ESI"] = AutoDTable.Rows[i]["Eligible_ESI"].ToString();
                DataCell.Rows[i]["ESICode"] = AutoDTable.Rows[i]["ESICode"].ToString();
                DataCell.Rows[i]["ESIDOJ"] = AutoDTable.Rows[i]["ESIDOJ"].ToString();
                DataCell.Rows[i]["BankName"] = AutoDTable.Rows[i]["BankName"].ToString();
                DataCell.Rows[i]["Account_No"] = AutoDTable.Rows[i]["Account_No"].ToString();
                DataCell.Rows[i]["IFSC_Code"] = AutoDTable.Rows[i]["IFSC_Code"].ToString();
                DataCell.Rows[i]["Adhar_No"] = AutoDTable.Rows[i]["Adhar_No"].ToString();
                DataCell.Rows[i]["UAN"] = AutoDTable.Rows[i]["UAN"].ToString();
                DataCell.Rows[i]["EmployeeMobile"] = AutoDTable.Rows[i]["EmployeeMobile"].ToString();
                DataCell.Rows[i]["Address"] = AutoDTable.Rows[i]["Address"].ToString();
                DataCell.Rows[i]["Gender"] = AutoDTable.Rows[i]["Gender"].ToString();
                sno += 1;

            }
            grid.DataSource = DataCell;
            grid.DataBind();
            if((Status != "Pending") && (ReportType == "EMPLOYEE LIST"))
            {
               
                string attachment = "attachment;filename=Active Employee.xls";
                
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

            }
            if ((Status == "Pending") && (ReportType == "EMPLOYEE LIST"))
            {
                string attachment = "attachment;filename=Pending Employee List.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";

            }
            if (ReportType == "EMPLOYEE NEW JOINING")
            {
                string attachment = "attachment;filename=Employee New Joining List.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
            }
            if (ReportType == "EMPLOYEE RESIGN")
            {
                string attachment = "attachment;filename=Left EmployeeList.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
            }


            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");

            //Response.Write("<tr Font-Bold='true' align='left'>");
            if (ReportType == "EMPLOYEE LIST")
            {
                Response.Write("<tr Font-Bold='true' align='Center'>");
                Response.Write("<td colspan='10'>");
                if (Status != "Pending")
                {
                    Response.Write("<a style=\"font-weight:bold\"> RAAJCO SPINNERS ACTIVE EMPLOYEE LIST </a>");
                }
                if (Status == "Pending")
                {
                    Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PENDING EMPLOYEE LIST </a>");
                }
            }
            if (ReportType == "EMPLOYEE NEW JOINING")
            {

                Response.Write("<tr Font-Bold='true' align='Center'>");
                Response.Write("<td colspan='10'>");
          
                Response.Write("<a style=\"font-weight:bold\"> RAAJCO SPINNERS NEW JOINING EMPLOYEE LIST </a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='Center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> From Date &nbsp&nbsp- &nbsp&nbsp " + FromDate + " To Date&nbsp- &nbsp&nbsp " + ToDate +" </a>");
                Response.Write("</td>");
                Response.Write("</tr>");





            }
            if (ReportType == "EMPLOYEE RESIGN")
            {

                Response.Write("<tr Font-Bold='true' align='Center'>");
                Response.Write("<td colspan='10'>");
              
                Response.Write("<a style=\"font-weight:bold\"> RAAJCO SPINNERS LEFT EMPLOYEE LIST </a>");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='Center'>");
                Response.Write("<td colspan='10'>");
                Response.Write("<a style=\"font-weight:bold\"> From Date&nbsp&nbsp- &nbsp&nbsp " + FromDate + " To Date&nbsp&nbsp- &nbsp&nbsp " + ToDate + " </a>");
                Response.Write("</td>");
                Response.Write("</tr>");

            }


            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
        }
    }
}