﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="RptPercentageReport.aspx.cs" Inherits="RptPercentageReport" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<script runat="server">

    protected void btnReport_Click(object sender, EventArgs e)
    {

    }
</script>




<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="SalPay" runat="server">
  <ContentTemplate>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Report</a></li>
				<li class="active">Salary Percentage Report</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Salary Percentage Report</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Salary Percentage Report</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                           
                        <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2" 
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;">
                                    
								 </asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                        
                        </div>
                    <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                         <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_FromDate_Hide">
								<div class="form-group">
								 <%--<label>FromDate</label>--%>
								  <asp:TextBox ID="txtfrom" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY" Visible="false"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtfrom" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                           <!-- end col-4 -->
                           <!-- begin col-4 -->
                              <div class="col-md-4" runat="server" id="IF_ToDate_Hide">
								<div class="form-group">
								 <%--<label>ToDate</label>--%>
								 <asp:TextBox ID="txtTo" runat="server" class="form-control datepicker" placeholder="dd/MM/YYYY" Visible="false"></asp:TextBox>
                                   <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                        TargetControlID="txtTo" ValidChars="0123456789/">
                                   </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                           <!-- end col-4 -->
                        </div>
                       <!-- end row -->
                      
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnReport" Text="Report View" class="btn btn-success"  onclick="btnReport_Click1"/>
							     </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                       
              
                       
 </ContentTemplate>
                 <Triggers>
                    
                    <asp:PostBackTrigger ControlID="btnReport" />
                    
                 </Triggers>
              </asp:UpdatePanel>  
</asp:Content>




