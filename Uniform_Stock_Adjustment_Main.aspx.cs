﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class Uniform_Stock_Adjustment_Main : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SessionAdmin;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        if (!IsPostBack)
        {
            Page.Title = "HR | Uniform Stock Adjustment";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Menu-EmployeeProfile"));
            //li.Attributes.Add("class", "has-sub active open");
        }
        LoadDataDisplay();
    }

    public void LoadDataDisplay()
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select TransID,TransDate,Remarks,Qty_Total,Minus_Qty_Total from Uniform_Stock_Adjustment_Main Where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        string status_str = "0";
        DataTable DT = new DataTable();
        if (SessionAdmin == "1")
        {
            Session["TransID"] = e.CommandName.ToString();
            Response.Redirect("Uniform_Stock_Adjustment.aspx");
        }
        else
        {
            query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                status_str = DT.Rows[0]["App_Status"].ToString();
            }
            if (status_str == "1")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Approved you can not edit this stock adjustment details..!');", true);
            }
            else
            {
                Session["TransID"] = e.CommandName.ToString();
                Response.Redirect("Uniform_Stock_Adjustment.aspx");
            }
        }
    }

    protected void GridApproveEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        string status_str = "0";
        DataTable DT = new DataTable();

        query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            status_str = DT.Rows[0]["App_Status"].ToString();
        }
        if (status_str == "1")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Approved you can not edit this stock adjustment details..!');", true);
        }
        else
        {
            Stock_Add(e.CommandName.ToString());
            //Update Status
            query = "update Uniform_Stock_Adjustment_Main set App_Status='1' where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
            objdata.RptEmployeeMultipleDetails(query);
            //here Stock Add
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Approved..!');", true);
        }
    }

    protected void GridDeleteEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        string status_str = "0";
        DataTable DT = new DataTable();
        if (SessionAdmin == "1")
        {
            query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                query = "Delete from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Uniform_Stock_Adjustment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
                objdata.RptEmployeeMultipleDetails(query);
                query = "Delete from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "' And TransType='Stock Adjustment'";
                objdata.RptEmployeeMultipleDetails(query);
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Details Deleted Successfully..!');", true);
                LoadDataDisplay();

            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Transaction No not fount..!');", true);
            }
        }
        else
        {
            query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                status_str = DT.Rows[0]["App_Status"].ToString();
            }
            if (status_str == "1")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Approved you can not edit this stock adjustment details..!');", true);
            }
            else
            {
                query = "Select * from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);
                if (DT.Rows.Count != 0)
                {
                    query = "Delete from Uniform_Stock_Adjustment_Main where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    query = "Delete from Uniform_Stock_Adjustment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "'";
                    objdata.RptEmployeeMultipleDetails(query);
                    query = "Delete from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + e.CommandName + "' And TransType='Stock Adjustment'";
                    objdata.RptEmployeeMultipleDetails(query);
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Stock Adjustment Details Deleted Successfully..!');", true);
                    LoadDataDisplay();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Transaction No not fount..!');", true);
                }
            }
        }
    }

    protected void lbtnAdd_Click(object sender, EventArgs e)
    {
        Session.Remove("TransID");
        Response.Redirect("Uniform_Stock_Adjustment.aspx");
    }

    private void Stock_Add(string Transaction_No)
    {
        string query = "";
        DataTable DT = new DataTable();
        DataTable DT_CH = new DataTable();
        query = "Select * from Uniform_Stock_Adjustment_Main_Sub where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            query = "Select * from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "' And TransType='Stock Adjustment'";
            DT_CH = objdata.RptEmployeeMultipleDetails(query);
            if (DT_CH.Rows.Count != 0)
            {
                query = "Delete from Uniform_Stock_Ledger_All where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And TransID='" + Transaction_No + "' And TransType='Stock Adjustment'";
                DT_CH = objdata.RptEmployeeMultipleDetails(query);
            }
            //Insert Stock Ledger
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string ItemDesc_Full = DT.Rows[i]["ItemName"].ToString() + " Size:" + DT.Rows[i]["SizeName"].ToString();
                if (DT.Rows[i]["Add_Type"].ToString() == "Add")
                {
                    query = "Insert Into Uniform_Stock_Ledger_All(Ccode,Lcode,TransID,TransDate,TransDate_Str,TransType,ItemDesc,ItemName,SizeName,Add_Qty,Add_Rate,";
                    query = query + " Add_Value,Minus_Qty,Minus_Rate,Minus_Value,Party_Name,Token_No,Wages_Type) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + Transaction_No + "',Convert(Datetime,'" + DT.Rows[i]["TransDate"].ToString() + "',103),";
                    query = query + " '" + DT.Rows[i]["TransDate"].ToString() + "','Stock Adjustment','" + ItemDesc_Full + "','" + DT.Rows[i]["ItemName"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["SizeName"].ToString() + "','" + DT.Rows[i]["Qty"].ToString() + "','" + DT.Rows[i]["ItemRate"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["LineTotal"].ToString() + "','0.00','0.00','0.00','Stock Adjustment Mill','','')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
                else
                {
                    query = "Insert Into Uniform_Stock_Ledger_All(Ccode,Lcode,TransID,TransDate,TransDate_Str,TransType,ItemDesc,ItemName,SizeName,Add_Qty,Add_Rate,";
                    query = query + " Add_Value,Minus_Qty,Minus_Rate,Minus_Value,Party_Name,Token_No,Wages_Type) Values('" + SessionCcode + "',";
                    query = query + " '" + SessionLcode + "','" + Transaction_No + "',Convert(Datetime,'" + DT.Rows[i]["TransDate"].ToString() + "',103),";
                    query = query + " '" + DT.Rows[i]["TransDate"].ToString() + "','Stock Adjustment','" + ItemDesc_Full + "','" + DT.Rows[i]["ItemName"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["SizeName"].ToString() + "','0.00','0.00','0.00','" + DT.Rows[i]["Qty"].ToString() + "','" + DT.Rows[i]["ItemRate"].ToString() + "',";
                    query = query + " '" + DT.Rows[i]["LineTotal"].ToString() + "','Stock Adjustment Mill','','')";
                    objdata.RptEmployeeMultipleDetails(query);
                }
            }
        }
    }
}

