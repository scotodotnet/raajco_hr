﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;

public partial class RptPF_Esi_Online : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    string SessionEpay;

    ReportDocument report = new ReportDocument();

    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();


    string Datestr = "";
    string Datestr1 = "";
    string ShiftType = "";
    DataTable mDataSet = new DataTable();
    int Fin_Year_DB = 0;
    string month = "";
    string year = "";
    string pf_Esi = "";
    string rbt = "";
    string doj = "";
    string newdoj = "";
        string Uan ="";
    string ESINetAmount ="";
    string EsiAdd = "";
    string ESIAmount_Act = "";
    string Esi_Act = "";
    System.Web.UI.WebControls.DataGrid grid =
                           new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();
        month = Request.QueryString["month"].ToString();
        year = Request.QueryString["year"].ToString();
        rbt= Request.QueryString["rbt"].ToString();

        if (rbt == "PF ONLINE")
        {
            getPFDetails();
        }
        if (rbt == "ESI ONLINE")
        {
            getEsiDetails();
        }

    }

    private void getPFDetails()
    {
        DataCell.Columns.Add("SNo");
        DataCell.Columns.Add("Emp.Code");
        DataCell.Columns.Add("UAN");
        DataCell.Columns.Add("Member Name");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("GrossWages");
        DataCell.Columns.Add("EPF Wages");
        DataCell.Columns.Add("EPS Wages");
        DataCell.Columns.Add("EDLI Wages");
        DataCell.Columns.Add("EPF Contribution remitted");
        DataCell.Columns.Add("EPS Contribution remitted");
        DataCell.Columns.Add("EPF and EPS Diff remitted");
        DataCell.Columns.Add("NCP Days");
        DataCell.Columns.Add("Refund of Advances");


        SSQL = "";
        SSQL = "Select Em.ExistingCode,em.UAN,em.FirstName,em.PFDOJ,sd.PF_Sal,sd.Emp_PF,sd.EmployeerPFone,LOPDays from";
        SSQL = SSQL + " Employee_Mst em inner join[" + SessionEpay + "]..SalaryDetails sd on em.MachineID = sd.MachineNo where Month = '" + month + "' and FinancialYear='" + year + "' and em.Eligible_PF = '1' order by ExisistingCode asc";
        AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

        SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        string name = dt.Rows[0]["CompName"].ToString();
        if (AutoDTable.Rows.Count != 0)
        {
            int sno = 1;
            for (int i = 0; i < AutoDTable.Rows.Count; i++)
            {
                string Epf = "0";
                string Eps = "0";
                string diff = "0";
                Epf = AutoDTable.Rows[i]["Emp_PF"].ToString();
                Eps = AutoDTable.Rows[i]["EmployeerPFone"].ToString();

                diff = (Convert.ToDecimal(Epf) - Convert.ToDecimal(Eps)).ToString();

                DataCell.NewRow();
                DataCell.Rows.Add();
                DataCell.Rows[i]["SNo"] = sno;
                DataCell.Rows[i]["Emp.Code"] = AutoDTable.Rows[i]["ExistingCode"].ToString();

                Uan = AutoDTable.Rows[i]["UAN"].ToString();
                DataCell.Rows[i]["UAN"] = Uan;
                DataCell.Rows[i]["Member Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                doj = AutoDTable.Rows[i]["PFDOJ"].ToString();
                DataCell.Rows[i]["DOJ"] = doj;
                DataCell.Rows[i]["GrossWages"] = AutoDTable.Rows[i]["PF_Sal"].ToString();
                DataCell.Rows[i]["EPF Wages"] = AutoDTable.Rows[i]["PF_Sal"].ToString();
                DataCell.Rows[i]["EPS Wages"] = AutoDTable.Rows[i]["PF_Sal"].ToString();
                DataCell.Rows[i]["EDLI Wages"] = AutoDTable.Rows[i]["PF_Sal"].ToString();

                DataCell.Rows[i]["EPF Contribution remitted"] = AutoDTable.Rows[i]["Emp_PF"].ToString();
                DataCell.Rows[i]["EPS Contribution remitted"] = AutoDTable.Rows[i]["EmployeerPFone"].ToString();
                DataCell.Rows[i]["EPF and EPS Diff remitted"] = diff;
                DataCell.Rows[i]["NCP Days"] = AutoDTable.Rows[i]["LOPDays"].ToString();
                DataCell.Rows[i]["Refund of Advances"] = "0";

                sno += 1;
            }
        }
        grid.DataSource = DataCell;
        grid.DataBind();
        string attachment = "attachment;filename=Online PF Statement.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);
        Response.Write("<table>");

        Response.Write("<tr Font-Bold='true' align='center'>");
        Response.Write("<td colspan=" + 12 + ">");

        Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
        Response.Write("<tr Font-Bold='true' align='Center'>");
        Response.Write("<td colspan='12'>");
        //Response.Write("<a style=\"font-weight:bold\">DAILY ATTENDANCE -" + ShiftType1 +"-" + Session["Lcode"].ToString() + "-" + Date + "</a>");
        Response.Write("<a style=\"font-weight:bold\">PF Online  Statement(ECR)- " + month + "-" + year + "</a>");

        Response.Write("</td>");
        Response.Write("</tr>");

        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();

    }


    private void getEsiDetails()
    {
        {
            DataCell.Columns.Add("SNo");
            DataCell.Columns.Add("Emp.Code");
            DataCell.Columns.Add("ESI No");
            DataCell.Columns.Add("Emp Name");
            DataCell.Columns.Add("DOJ");
            DataCell.Columns.Add("Payable Days");
            DataCell.Columns.Add("Wages");
            DataCell.Columns.Add("ESI Emp Contribution");
            DataCell.Columns.Add("ESI Empr Contribution");
            DataCell.Columns.Add("Total Cont.");
            


            SSQL = "";
            SSQL = "Select Em.ExistingCode,em.ESINo,em.FirstName,em.ESIDOJ,sd.ESI_Sal,sd.EmployeerESI,sd.ESI,sd.WorkedDays from ";
            SSQL = SSQL + " Employee_Mst em inner join[" + SessionEpay + "]..SalaryDetails sd on em.MachineID = sd.MachineNo where Month = '" + month + "' and FinancialYear='" + year + "' and em.Eligible_ESI = '1' order by ExisistingCode asc";
            AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Select * from Company_Mst where CompCode='" + SessionCcode + "' ";
            dt = objdata.RptEmployeeMultipleDetails(SSQL);
            string name = dt.Rows[0]["CompName"].ToString();
            if (AutoDTable.Rows.Count != 0)
            {
                int sno = 1;
                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                { 
                    string machineID= AutoDTable.Rows[i]["ExistingCode"].ToString();
                    string Esi = "0";
                    string EmpEsi = "0";
                    string Tot = "0";
                    Esi = AutoDTable.Rows[i]["ESI"].ToString();
                    EmpEsi = AutoDTable.Rows[i]["EmployeerESI"].ToString();
                    EmpEsi = (Math.Round(Convert.ToDecimal(EmpEsi), 0, MidpointRounding.AwayFromZero)).ToString();

                   

                    DataCell.NewRow();
                    DataCell.Rows.Add();
                    DataCell.Rows[i]["SNo"] = sno; 
                    DataCell.Rows[i]["Emp.Code"] = AutoDTable.Rows[i]["ExistingCode"].ToString();
                    DataCell.Rows[i]["ESI No"] = AutoDTable.Rows[i]["ESINO"].ToString();
                    DataCell.Rows[i]["Emp Name"] = AutoDTable.Rows[i]["FirstName"].ToString();
                    DataCell.Rows[i]["DOJ"] = AutoDTable.Rows[i]["ESIDOJ"].ToString();
                    DataCell.Rows[i]["Payable Days"] = AutoDTable.Rows[i]["WorkedDays"].ToString();

                    //OT ESI Amount add (27/07/2020)
                    DataTable DT_OTEsi = new DataTable();
                    string OT_EsiAmount = "";
                    string OT_Esi = "";
                    SSQL = "";
                    SSQL = "Select * from ["+SessionEpay+"]..OT_ESI_Amt where MachineID='" + machineID + "' and Months='" + month + "' and FinYear='" + year + "'";
                    DT_OTEsi= objdata.RptEmployeeMultipleDetails(SSQL);
                    if(DT_OTEsi.Rows.Count !=0)
                    {
                        OT_EsiAmount = DT_OTEsi.Rows[0]["ESI_Amount"].ToString();
                        OT_Esi = DT_OTEsi.Rows[0]["ESI"].ToString();
                    }
                    else
                    {
                        OT_EsiAmount = "0";
                        OT_Esi = "0";
                    }
                    //Total ESI Amount
                    ESIAmount_Act = AutoDTable.Rows[i]["ESI_Sal"].ToString();
                    Esi_Act = AutoDTable.Rows[i]["ESI"].ToString();
                    ESINetAmount = ((Convert.ToDecimal(ESIAmount_Act) + (Convert.ToDecimal(OT_EsiAmount))).ToString());
                    ESINetAmount = (Math.Round(Convert.ToDecimal(ESINetAmount), 0, MidpointRounding.AwayFromZero)).ToString();
                    //Total DedESI Amt
                    EsiAdd = ((Convert.ToDecimal(Esi_Act) + (Convert.ToDecimal(OT_Esi))).ToString());
                    EsiAdd = (Math.Round(Convert.ToDecimal(EsiAdd), 0, MidpointRounding.AwayFromZero)).ToString();
                    DataCell.Rows[i]["Wages"] = ESINetAmount;
                    Tot = (Convert.ToDecimal(Esi) + Convert.ToDecimal(EmpEsi)+ Convert.ToDecimal(OT_Esi)).ToString();
                    Tot = (Math.Round(Convert.ToDecimal(Tot), 0, MidpointRounding.AwayFromZero)).ToString();

                    DataCell.Rows[i]["ESI Emp Contribution"] = EsiAdd;
                    DataCell.Rows[i]["ESI Empr Contribution"] = EmpEsi;
                    DataCell.Rows[i]["Total Cont."] = Tot;
                 
                    sno += 1;
                }
            }
            grid.DataSource = DataCell;
            grid.DataBind();
            string attachment = "attachment;filename=Online ESI Statement.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");

            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan=" + 12 + ">");

            Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
            Response.Write("<tr Font-Bold='true' align='Center'>");
            Response.Write("<td colspan='12'>");
            //Response.Write("<a style=\"font-weight:bold\">DAILY ATTENDANCE -" + ShiftType1 +"-" + Session["Lcode"].ToString() + "-" + Date + "</a>");
            Response.Write("<a style=\"font-weight:bold\">ESi Online  Statement(ECR)- " + month + "-" + year + "</a>");

            Response.Write("</td>");
            Response.Write("</tr>");

            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.End();
        }
    }
}