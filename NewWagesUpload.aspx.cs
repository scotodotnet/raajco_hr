﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Net.Mail;
using System.Net;


public partial class NewWagesUpload : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    DateTime mydate;
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    string FromBankACno, FromBankName, FromBranch, ProvisionalTax = "0";
    SqlConnection con;
    string DepartmentCode;
    string Name_Upload;
    string Name_Upload1;
    string Cash;

    SalaryClass objSal = new SalaryClass();
    string CCA = "0";

    string SessionUserType;
    string SessionEpay;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionEpay = Session["SessionEpay"].ToString();

        con = new SqlConnection(constr);
        if (!IsPostBack)
        {

        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string StaffLabour = "";

        if (ddlCategory.SelectedValue == "1")
        {
            StaffLabour = "1";
        }
        else if (ddlCategory.SelectedValue == "2")
        {
            StaffLabour = "2";
        }
        else
        {
            StaffLabour = "0";
        }


        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        query = "select EmpType from MstEmployeeType where EmpCategory='" + StaffLabour + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlEmployeeType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpType";
        ddlEmployeeType.DataBind();
    }

    protected void btnSalaryDownload_Click(object sender, EventArgs e)
    {
        string staff_lab = "S";
        string Employee_Type = "";
        DataTable DT_S = new DataTable();
        try
        {
            if (ddlCategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Category');", true);
            }
            else if (ddlEmployeeType.SelectedValue == "" || ddlEmployeeType.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee Type');", true);
            }
            else
            {

                if (ddlCategory.SelectedValue == "1")
                {
                    staff_lab = "S";
                }
                else if (ddlCategory.SelectedValue == "2")
                {
                    staff_lab = "L";
                }
                Employee_Type = ddlEmployeeType.SelectedValue.ToString();
                string query = "";
                query = "Select EmpNo,MachineID,ExistingCode,DeptName,FirstName,BaseSalary from Employee_Mst where CompCode='" + SessionCcode + "'";
                query = query + " And LocCode='" + SessionLcode + "' And IsActive='Yes' And Wages='" + ddlEmployeeType.SelectedItem.Text + "'";
                DT_S = objdata.RptEmployeeMultipleDetails(query);
                if (DT_S.Rows.Count != 0)
                {
                    GVWages.DataSource = DT_S;
                    GVWages.DataBind();

                    if (DT_S.Rows.Count > 0)
                    {
                        string attachment = "attachment;filename=Wages.xls";
                        Response.ClearContent();
                        Response.AddHeader("content-disposition", attachment);
                        Response.ContentType = "application/ms-excel";

                        StringWriter stw = new StringWriter();
                        HtmlTextWriter htextw = new HtmlTextWriter(stw);
                        GVWages.RenderControl(htextw);


                        //gvDownload.RenderControl(htextw);
                        //Response.Write("Contract Details");
                        Response.Write(stw.ToString());
                        Response.End();
                        Response.Clear();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Downloaded Successfully');", true);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }

    protected void btnUpload_Click(object sender, EventArgs e)
    {
        string Token_No = "";
        string New_Wages = "";
        string Hos_Total_W_Days = "0";
        string Days_Below = "0";
        string Days_Above = "0";
        string OLD_Wages = "";
        string EmpNo = "";
        string MachineNo = "";
        bool ErrFlag = false;
        DataTable Emp_Det_DT = new DataTable();
        DataTable SM_Det_DT = new DataTable();
        DataTable Update_Query_DT = new DataTable();
        DataTable SQLServer_Date_DT = new DataTable();
        double Emp_Count = 0;
        bool saved_Employee = true;
        string query = "";
        try
        {

            if (ddlCategory.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((ddlEmployeeType.SelectedValue == "") || (ddlEmployeeType.SelectedValue == "-Select-"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            if (!ErrFlag)
            {

                if (FileUpload.HasFile)
                {
                    FileUpload.SaveAs(Server.MapPath("Upload/" + FileUpload.FileName));

                    string connectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath("Upload/" + FileUpload.FileName) + ";" + "Extended Properties=Excel 8.0;";
                    OleDbConnection sSourceConnection = new OleDbConnection(connectionString);
                    DataTable dts = new DataTable();

                    using (sSourceConnection)
                    {

                        sSourceConnection.Open();
                        OleDbCommand command = new OleDbCommand("select * FROM [Sheet1$];", sSourceConnection);
                        sSourceConnection.Close();

                        using (OleDbCommand cmd = new OleDbCommand())
                        {
                            command.CommandText = "select * FROM [Sheet1$];";
                            sSourceConnection.Open();
                        }
                        using (OleDbDataReader dr = command.ExecuteReader())
                        {

                            if (dr.HasRows)
                            {

                            }

                        }
                        OleDbDataAdapter objDataAdapter = new OleDbDataAdapter();
                        objDataAdapter.SelectCommand = command;
                        DataSet ds = new DataSet();

                        objDataAdapter.Fill(ds);
                        DataTable dt = new DataTable();
                        dt = ds.Tables[0];
                        for (int i = 0; i < dt.Columns.Count; i++)
                        {
                            dt.Columns[i].ColumnName = dt.Columns[i].ColumnName.Replace(" ", string.Empty).ToString();

                        }
                        sSourceConnection.Close();
                        string constr = ConfigurationManager.AppSettings["ConnectionString"];
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            SqlConnection cn = new SqlConnection(constr);
                            EmpNo = dt.Rows[j][0].ToString();
                            MachineNo = dt.Rows[j][1].ToString();
                            Token_No = dt.Rows[j][2].ToString();
                            OLD_Wages = dt.Rows[j][5].ToString();
                            New_Wages = dt.Rows[j][6].ToString();
                            Hos_Total_W_Days = "0";
                            
                            if (OLD_Wages == "") { OLD_Wages = "0"; }
                            if (New_Wages == "") { New_Wages = "0"; }

                            //Get Employee Details
                            if (Convert.ToDecimal(New_Wages.ToString()) > 0)
                            {
                                query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                                query = query + " And ExistingCode='" + Token_No + "'";
                                Emp_Det_DT = objdata.RptEmployeeMultipleDetails(query);
                                if (Emp_Det_DT.Rows.Count != 0)
                                {
                                    //Get Salary Master

                                    if (Convert.ToDecimal(Emp_Det_DT.Rows[0]["BaseSalary"].ToString()) != Convert.ToDecimal(New_Wages.ToString()))
                                    {
                                        OLD_Wages = Emp_Det_DT.Rows[0]["BaseSalary"].ToString();
                                        //Emp_Count = Emp_Count + 1;
                                        saved_Employee = false;
                                        //Salary Master Update
                                        New_Wages = (Math.Round(Convert.ToDecimal(New_Wages), 2, MidpointRounding.AwayFromZero)).ToString();
                                        OLD_Wages = (Math.Round(Convert.ToDecimal(OLD_Wages), 2, MidpointRounding.AwayFromZero)).ToString();
                                        query = "Update Employee_Mst set BaseSalary='" + New_Wages + "' where CompCode='" + SessionCcode + "'";
                                        query = query + " And LocCode='" + SessionLcode + "' And ExistingCode='" + Token_No + "'";
                                        Update_Query_DT = objdata.RptEmployeeMultipleDetails(query);

                                        //Get SQL Server Date
                                        query = "Select Convert(Varchar(10),GetDate(),103) as Server_Date,DATENAME(month, GetDate()) as Month_Name,Year(GetDate()) as Year_Int";
                                        SQLServer_Date_DT = objdata.RptEmployeeMultipleDetails(query);

                                        //Check Already Update or Not
                                        query = "Select * from [Raajco_Epay]..Wages_Increment_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                        query = query + " And EmpNo='" + EmpNo + "' And DATENAME(month, Increment_Date)='" + SQLServer_Date_DT.Rows[0]["Month_Name"].ToString() + "'";
                                        query = query + " And Year(Increment_Date)='" + SQLServer_Date_DT.Rows[0]["Year_Int"].ToString() + "'";
                                        Update_Query_DT = objdata.RptEmployeeMultipleDetails(query);
                                        if (Update_Query_DT.Rows.Count != 0)
                                        {
                                            query = "Delete from [Raajco_Epay]..Wages_Increment_Det where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                                            query = query + " And EmpNo='" + EmpNo + "' And DATENAME(month, Increment_Date)='" + SQLServer_Date_DT.Rows[0]["Month_Name"].ToString() + "'";
                                            query = query + " And Year(Increment_Date)='" + SQLServer_Date_DT.Rows[0]["Year_Int"].ToString() + "'";
                                            Update_Query_DT = objdata.RptEmployeeMultipleDetails(query);
                                        }

                                        //Get Days Below And Days Above
                                        DataTable Hos_Det_DT = new DataTable();
                                        Days_Below = "0";
                                        Days_Above = "0";
                                        //Insert Wages Increement Table
                                        query = "Insert Into [Raajco_Epay]..Wages_Increment_Det(Ccode,Lcode,EmpNo,BiometricID,Token_No,EmpName,Employee_Type,";
                                        query = query + " Department,OLD_Wages,New_Wages,Increment_Date,Increment_Date_Str,Month,Year,Days_Below,Days_Above,Hos_Tot_W_Days) Values('" + SessionCcode + "',";
                                        query = query + " '" + SessionLcode + "','" + EmpNo + "','" + Emp_Det_DT.Rows[0]["MachineID"] + "','" + Emp_Det_DT.Rows[0]["ExistingCode"] + "',";
                                        query = query + " '" + Emp_Det_DT.Rows[0]["FirstName"] + "','" + Emp_Det_DT.Rows[0]["Wages"] + "','" + Emp_Det_DT.Rows[0]["DeptName"] + "','" + OLD_Wages + "',";
                                        query = query + " '" + New_Wages + "',convert(datetime, '" + SQLServer_Date_DT.Rows[0]["Server_Date"] + "', 105),'" + SQLServer_Date_DT.Rows[0]["Server_Date"] + "','" + SQLServer_Date_DT.Rows[0]["Month_Name"] + "',";
                                        query = query + " '" + SQLServer_Date_DT.Rows[0]["Year_Int"] + "','" + Days_Below + "','" + Days_Above + "','" + Hos_Total_W_Days + "')";
                                        Update_Query_DT = objdata.RptEmployeeMultipleDetails(query);
                                    }

                                }
                            }

                        }
                        //string Emp_Count_Display = (Math.Round(Convert.ToDecimal(Emp_Count), 0, MidpointRounding.AwayFromZero)).ToString();
                        if (!saved_Employee)
                        {
                            SendMail();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Wages Updated Success Fully... ');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Wages Updated... ');", true);
                        }

                    }

                }
            }
        }
        catch
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Upload Correct File Format...');", true);
        }
    }

    //Mail Code Start
    public void SendMail()
    {
        string result = "0";
        string temptext = "";
        DataTable dt = new DataTable();
        DataTable SQLServer_Date_DT = new DataTable();
        string query = "";
        //Get SQL Server Date
        query = "Select Convert(Varchar(10),GetDate(),103) as Server_Date,DATENAME(month, GetDate()) as Month_Name,Year(GetDate()) as Year_Int";
        SQLServer_Date_DT = objdata.RptEmployeeMultipleDetails(query);

        //Check Already Update or Not
        query = "Select WD.Token_No,WD.EmpName as [Name],WD.Department as Department,WD.OLD_Wages,WD.New_Wages,WD.Increment_Date_Str as Upload_Date";
        query = query + " from [Raajco_Epay]..Wages_Increment_Det WD where WD.Ccode='" + SessionCcode + "' And WD.Lcode='" + SessionLcode + "'";
        query = query + " And DATENAME(month, WD.Increment_Date)='" + SQLServer_Date_DT.Rows[0]["Month_Name"].ToString() + "'";
        query = query + " And Year(WD.Increment_Date)='" + SQLServer_Date_DT.Rows[0]["Year_Int"].ToString() + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        GridView gv = new GridView();
        gv.DataSource = dt;
        gv.DataBind();
        AttachandSend(gv);


    }
    public void AttachandSend(GridView gv)
    {
        string query = "";
        DataTable Wages_Mail_DT = new DataTable();
        //Get Mail Address Details
        query = "Select * from [Raajco_Epay]..Wages_Mail where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
        Wages_Mail_DT = objdata.RptEmployeeMultipleDetails(query);

        StringWriter stw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(stw);

        gv.RenderControl(hw);

        System.Text.Encoding Enc = System.Text.Encoding.ASCII;
        byte[] mBArray = Enc.GetBytes(stw.ToString());
        System.IO.MemoryStream mAtt = new System.IO.MemoryStream(mBArray, false);

        MailMessage mm = new MailMessage("ESM<aatm2005@gmail.com>", Wages_Mail_DT.Rows[0]["AddTo"].ToString());
        mm.Subject = SessionLcode.ToUpper().ToString() + " " + Wages_Mail_DT.Rows[0]["Subject"].ToString();
        mm.Body = "Find the attachment for " + SessionLcode.ToUpper().ToString() + "Wages Increment Details";
        mm.Attachments.Add(new Attachment(mAtt, SessionLcode + "_Wages_Increment.xls"));

        //mm.From.Address("sabapathy.m@altius.co.in");
        //mm.To.Add(Wages_Mail_DT.Rows[0]["AddTo"].ToString());
        mm.CC.Add(Wages_Mail_DT.Rows[0]["AddCC"].ToString());
        mm.Bcc.Add(Wages_Mail_DT.Rows[0]["AddBCC"].ToString());

        mm.IsBodyHtml = false;
        SmtpClient smtp = new SmtpClient();



        smtp.Host = "smtp.gmail.com";   // We use gmail as our smtp client
        smtp.Port = 587;
        smtp.EnableSsl = true;
        smtp.UseDefaultCredentials = true;
        smtp.Credentials = new System.Net.NetworkCredential("aatm2005@gmail.com", "Altius@2005");

        smtp.Send(mm);







        //smtp.Host = "smtp.gmail.com";
        //smtp.EnableSsl = true;
        //NetworkCredential NetworkCred = new NetworkCredential("aatm2005@gmail.com", "Altius@2005");
        //smtp.UseDefaultCredentials = true;
        //smtp.Credentials = NetworkCred;
        //smtp.Port = 587;
        //smtp.Send(mm);
        ////ClientScript.RegisterStartupScript(GetType(), "alert", "alert('Email sent.');", true);


    }
    //Mail Cone End
}
