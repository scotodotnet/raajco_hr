﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class TrainingStatus : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Employee Approval";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

        }
        Load_Data();
    }

    private void Load_Data()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select ENJ.EmpNo,ENJ.FirstName,ENJ.DeptName,ENJ.Designation,ENJ.EmpLevel";
        query = query + " from Employee_Mst ENJ inner join Training_Level_Change TC on ENJ.EmpNo=TC.EmpNo";
        query = query + " where ENJ.CompCode='" + SessionCcode + "'";
        query = query + " And ENJ.LocCode='" + SessionLcode + "' And ENJ.IsActive='Yes'";
        query = query + " And TC.Ccode='" + SessionCcode + "' AND TC.Lcode='" + SessionLcode + "' And TC.Training_Level='Semi-Exp'";
        query = query + " And DATEDIFF(day, convert(datetime,TC.Level_Date, 105) , GETDATE()) < 60";
        DT = objdata.RptEmployeeMultipleDetails(query);

        Repeater1.DataSource = DT;
        Repeater1.DataBind();

    }

    protected void GridViewEnquiryClick(object sender, CommandEventArgs e)
    {
        lblEmpNo.Text = e.CommandName.ToString();
        lblEmpName.Text = e.CommandArgument.ToString();
    }

    protected void btnChange_Click(object sender, EventArgs e)
    {
        string query = "";
        if (lblEmpNo.Text != "")
        {
            if (chkReTrainee.Checked == true)
            {
                query = "Update Training_Level_Change set Training_Level='Re-Trainee',Level_Date=convert(varchar, GETDATE(), 103),Reason='" + txtReason.Text + "'";
                query = query + " where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "'";
                query = query + " And EmpNo='" + lblEmpNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                query = "Update Employee_Mst set EmpLevel='Trainee',Training_Status='4',EmpLevel_Reason='" + txtReason.Text + "'";
                query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                query = query + " And EmpNo='" + lblEmpNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(query);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Employee Level changed Successfully..!');", true);

                lblEmpNo.Text = "";
                lblEmpName.Text = "";
                chkReTrainee.Checked = false;
                txtReason.Text = "";
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Re-Trainee checkbox to be check.!');", true);
            }
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Employee No!');", true);
        }
        Load_Data();
    }
}
