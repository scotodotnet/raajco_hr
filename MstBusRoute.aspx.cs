﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstBusRoute : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Route Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Shift();
            Load_Vehicle();
        }
     
        Load_Data_Route();
       
    }

    private void Load_Vehicle()
    {
        SSQL = "";
        SSQL = "select * from MstVehicle where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        ddlVehicleType.DataSource = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlVehicleType.DataTextField = "Vehicletype";
        ddlVehicleType.DataValueField = "Vehicletype";
        ddlVehicleType.DataBind();
        ddlVehicleType.Items.Insert(0, new ListItem("-Select-", "-Select-", true));
    }

    private void Load_Shift()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlShift.Items.Clear();
        query = "Select *from Shift_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlShift.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ShiftDesc"] = "-Select-";
        dr["ShiftDesc"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlShift.DataTextField = "ShiftDesc";
        ddlShift.DataValueField = "ShiftDesc";
        ddlShift.DataBind();
    }

    private void Load_Data_Route()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from RouteMst";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        string[] Route_Str;
        Route_Str = e.CommandArgument.ToString().Split(',');
        string shift = Route_Str[1];
        string RouteNo=Route_Str[0];

        query = "select * from RouteMst where Type='" + e.CommandName.ToString() + "' and RouteNo='" + RouteNo + "' And ShiftName='" + shift + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count > 0)
        {
            query = "delete from RouteMst where Type='" + e.CommandName.ToString() + "' and RouteNo='" + RouteNo + "' And ShiftName='" + shift + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Route Deleted Successfully...!');", true);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('No Data Found..!');", true);
        }
        Load_Data_Route();
    }

    //protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    //{
    //}
    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT=new DataTable();

        query = "select * from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "' and RouteNo='" + txtRouteNo.Text + "' And ShiftName='" + ddlShift.SelectedItem.Text + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if(DT.Rows.Count>0)
        {
            query = "delete from RouteMst where Type='" + ddlVehicleType.SelectedItem.Text + "' and RouteNo='" + txtRouteNo.Text + "' And ShiftName='" + ddlShift.SelectedItem.Text + "'";
            objdata.RptEmployeeMultipleDetails(query);
        }

        query = "Insert into RouteMst (Type,RouteNo,ShiftName,RouteName,BusNo)";
        query = query + "values('" + ddlVehicleType.SelectedItem.Text + "','" + txtRouteNo.Text + "','" + ddlShift.SelectedItem.Text + "','" + txtRouteName.Text + "','" + txtBusNo.Text + "')";
        objdata.RptEmployeeMultipleDetails(query);
        
        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
        Load_Data_Route();
        Clear_All_Field();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        ddlShift.SelectedValue = "-Select-";
        txtRouteNo.Text = ""; txtRouteName.Text = "";
        ddlVehicleType.SelectedValue = "-Select-";
        txtBusNo.Text = "";
        btnSave.Text = "Save";
    }
}
