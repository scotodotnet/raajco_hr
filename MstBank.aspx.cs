﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;


public partial class MstBank : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Bank Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            Load_Bank_Name_DropDown();
        }
        Load_Data_Bank();
    }

    private void Load_Bank_Name_DropDown()
    {
        string query = "";
        DataTable DIV_DT = new DataTable();
        txtBankName.Items.Clear();
        query = "Select BankName from MstBank_New";
        query = query + " Order by BankName Asc";
        DIV_DT = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = DIV_DT;
        DataRow dr = DIV_DT.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        DIV_DT.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();

    }

    private void Load_Data_Bank()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank where BankName='" + e.CommandName.ToString() + "' And Branch='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
            txtBranch.Text = DT.Rows[0]["Branch"].ToString();
            txtIFSCCode.Text = DT.Rows[0]["IFSCCode"].ToString();

            if (DT.Rows[0]["Default_Bank"].ToString() == "Yes")
            {
                chkDefault.Checked = true;
            }
            else
            {
                chkDefault.Checked = false;
            }

            btnSave.Text = "Update";
        }
        else
        {
            txtBankName.SelectedIndex = 0;
            txtBranch.Text = "";
            txtIFSCCode.Text = "";
            chkDefault.Checked = false;
        }

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank where BankName='" + e.CommandName.ToString() + "' And Branch='" + e.CommandArgument.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstBank where BankName='" + e.CommandName.ToString() + "' And Branch='" + e.CommandArgument.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Not Found');", true);
        }
        Load_Data_Bank();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

        if (chkDefault.Checked == true)
        {
            Def_Chk = "Yes";
        }
        else
        {
            Def_Chk = "No";
        }

        if (Def_Chk == "Yes")
        {
            query = "select * from MstBank where Default_Bank='Yes'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Already Default Bank exist.. Check it.!');", true);
            }
            else
            {
                ErrFlag = false;
            }
        }
        if (!ErrFlag)
        {
            query = "select * from MstBank where BankName='" + txtBankName.SelectedValue.ToUpper() + "' and Branch='" + txtBranch.Text.ToUpper() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from MstBank where BankName='" + txtBankName.SelectedValue.ToUpper() + "' and Branch='" + txtBranch.Text.ToUpper() + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into MstBank (BankName,Branch,IFSCCode,Default_Bank)";
            query = query + "values('" + txtBankName.SelectedValue.ToUpper() + "','" + txtBranch.Text.ToUpper() + "','" + txtIFSCCode.Text + "','" + Def_Chk + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
        Load_Data_Bank();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtBankName.SelectedIndex = 0;
        txtIFSCCode.Text = "";
        txtBranch.Text = "";
        chkDefault.Checked = false;
        btnSave.Text = "Save";
    }
}
