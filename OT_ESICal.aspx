﻿<%@ Page Language="C#"MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="OT_ESICal.aspx.cs" Inherits="OT_ESICal" Title="" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script src="assets/js/master_list_jquery.min.js"></script>
    <script src="assets/js/master_list_jquery-ui.min.js"></script>
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
    <script>
        $(document).ready(function () {
            $('#example1').dataTable();
            $('.datepicker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });
        });
    </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example1').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
    <script type="text/javascript">
        function SaveMsgAlert(msg) {
            swal(msg);
        }
    </script>
    <script type="text/javascript">
        function ProgressBarShow() {
            $('#Download_loader').show();
        }
    </script>

    <script type="text/javascript">
        function ProgressBarHide() {
            $('#Download_loader').hide();
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">OT</a></li>
                    <li class="active">OT ESI Calculation</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">OT ESI Calculation</h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">OT ESI Calculation</h4>
                            </div>
                            <div class="panel-body">
                                <div class="form-group">

                                    <!-- begin row -->
                                    <div class="row">

                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Category</label>
                                                <asp:DropDownList runat="server" ID="ddlCategory" class="form-control select2" Enabled="true"
                                                    Style="width: 100%;" AutoPostBack="true"
                                                    OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                                                    <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
                                                    <asp:ListItem Value="1">STAFF</asp:ListItem>
                                                    <asp:ListItem Value="2">LABOUR</asp:ListItem>
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Employee Type</label>
                                                <asp:DropDownList ID="ddlEmployeeType" runat="server" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlEmployeeType_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlEmployeeType" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Machine ID</label>
                                                <asp:DropDownList ID="ddlMachineID" runat="server" class="form-control select2" AutoPostBack="true" OnSelectedIndexChanged="ddlMachineID_SelectedIndexChanged">
                                                </asp:DropDownList>

                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Token No</label>
                                                <asp:TextBox runat="server" ID="txtTokenNo" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <div class="row">
                                        <!-- begin col-4 -->
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <label>Name</label>
                                                <asp:TextBox runat="server" ID="txtName" class="form-control"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Fin. Year</label>
                                                <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>Month</label>
                                                <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" Style="width: 100%;">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic" ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>From Date</label>
                                                <asp:TextBox runat="server" ID="txtFromDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtFromDate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ControlToValidate="txtFromDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <!-- begin col-4 -->
                                        <div class="col-md-2">
                                            <div class="form-group">
                                                <label>To Date</label>
                                                <asp:TextBox runat="server" ID="txtToDate" class="form-control datepicker" placeholder="dd/MM/YYYY"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                    TargetControlID="txtToDate" ValidChars="0123456789/">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ControlToValidate="txtToDate" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                                </asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                    </div>
                                    <!-- begin row -->
                                    <div class="row">
                                    </div>
                                    <!-- end row -->
                                    <!-- begin row -->
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <!-- begin col-4 -->
                                        <div class="col-md-2"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <br />
                                                <asp:Button runat="server" ID="ESI_Cal" Text="ESI_Calculation"
                                                    class="btn btn-success" ValidationGroup="Validate_Field" OnClick="ESI_Cal_Click" />
                                            </div>
                                        </div>
                                        <!-- end col-4 -->
                                        <div class="col-md-4"></div>
                                    </div>
                                    <!-- end row -->
                                    <div id="Download_loader" style="display: none" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ESI_Cal" />

        </Triggers>
    </asp:UpdatePanel>
</asp:Content>


