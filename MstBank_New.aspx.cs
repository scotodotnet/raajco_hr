﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class MstBank_New : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Bank Master New";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");

        }
        Load_Data_Bank();
    }

    private void Load_Data_Bank()
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank_New";
        DT = objdata.RptEmployeeMultipleDetails(query);
        Repeater1.DataSource = DT;
        Repeater1.DataBind();
    }

    protected void GridEditEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank_New where BankName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtBankName.Text = DT.Rows[0]["BankName"].ToString();
            RdpBankType.SelectedValue = DT.Rows[0]["BankType"].ToString();
            txtBelow_Amt.Text = DT.Rows[0]["Min_Amt"].ToString();
            txtBelow_Rate.Text = DT.Rows[0]["Below_Rate"].ToString();
            txtAbove_Rate.Text = DT.Rows[0]["Above_Rate"].ToString();

            

            btnSave.Text = "Update";
        }
        else
        {
            txtBankName.Text = "";
            txtBelow_Amt.Text = "0";
            txtBelow_Rate.Text = "0";
            txtAbove_Rate.Text = "0";
        }

    }

    protected void GridDeleteEnquiryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();
        query = "Select * from MstBank_New where BankName='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            query = "Delete from MstBank_New where BankName='" + e.CommandName.ToString() + "'";
            objdata.RptEmployeeMultipleDetails(query);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Deleted Successfully');", true);

        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Not Found');", true);
        }
        Load_Data_Bank();
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "Insert";
        string query = "";
        DataTable DT = new DataTable();
        string Def_Chk = "";
        bool ErrFlag = false;

        if (RdpBankType.SelectedValue == "OWN")
        {
            if (txtBelow_Amt.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Below Amount.!');", true);
            }
            if (txtBelow_Rate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Below Rate.!');", true);
            }
            if (txtAbove_Rate.Text == "")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Above Rate.!');", true);
            }
        }
        else
        {
            if (txtBelow_Amt.Text == "" || txtBelow_Amt.Text == "0" || txtBelow_Amt.Text == "0.0" || txtBelow_Amt.Text == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Below Amount.!');", true);
            }
            if (txtBelow_Rate.Text == "" || txtBelow_Rate.Text == "0" || txtBelow_Rate.Text == "0.0" || txtBelow_Rate.Text == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Below Rate.!');", true);
            }
            if (txtAbove_Rate.Text == "" || txtAbove_Rate.Text == "0" || txtAbove_Rate.Text == "0.0" || txtAbove_Rate.Text == "0.00")
            {
                ErrFlag = true;
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Above Rate.!');", true);
            }
        }

        if (!ErrFlag)
        {
            query = "select * from MstBank_New where BankName='" + txtBankName.Text.ToUpper() + "'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count > 0)
            {
                SaveMode = "Update";
                query = "delete from MstBank_New where BankName='" + txtBankName.Text.ToUpper() + "'";
                objdata.RptEmployeeMultipleDetails(query);
            }
            else
            {
                SaveMode = "Insert";
            }

            query = "Insert into MstBank_New (BankName,BankType,Min_Amt,Below_Rate,Above_Rate)";
            query = query + "values('" + txtBankName.Text.ToUpper() + "','" + RdpBankType.SelectedValue + "','" + txtBelow_Amt.Text + "','" + txtBelow_Rate.Text + "','" + txtAbove_Rate.Text + "')";
            objdata.RptEmployeeMultipleDetails(query);

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Updated Successfully...!');", true);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Saved Successfully...!');", true);
            }
            Clear_All_Field();
        }
        Load_Data_Bank();
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear_All_Field();
    }
    private void Clear_All_Field()
    {
        txtBankName.Text = "";
        txtBelow_Amt.Text = "0";
        txtBelow_Rate.Text = "0";
        txtAbove_Rate.Text = "0";
        
        btnSave.Text = "Save";
    }
}
