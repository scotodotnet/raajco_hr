﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstAgent.aspx.cs" Inherits="MstAgent" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
 </script>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('.select2').select2();
                $('#example').dataTable();
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Agent</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Agent </h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Agent</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Agent Name</label>
								  <asp:TextBox runat="server" ID="txtAgentName" class="form-control" Style="text-transform: uppercase">
								  </asp:TextBox>
								  <asp:HiddenField ID="txtAgentID" runat="server" />
								  <asp:RequiredFieldValidator ControlToValidate="txtAgentName" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Mobile No</label>
								  <asp:TextBox runat="server" ID="txtMobileNo" class="form-control" MaxLength="10"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtMobileNo" Display="Dynamic"   ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                             <!-- begin col-4 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Location</label>
								  <asp:TextBox runat="server" ID="txtLocation" class="form-control"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtLocation" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                               <!-- begin col-4 -->
                                <div class="col-md-3" runat="server" id="divComm">
								<div class="form-group">
								  <label>Commission</label>
								  <asp:TextBox runat="server" ID="txtCommission" class="form-control" Text="0"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtCommission" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                            
                              </div>
                        <!-- end row -->
                        <!-- begin row -->
                          <div class="row">
                          <!-- begin col-4 -->
                                <div class="col-md-3">
								<div class="form-group">
								  <label>Address</label>
								  <asp:TextBox runat="server" ID="txtAddress" class="form-control" TextMode="MultiLine"></asp:TextBox>
								  <asp:RequiredFieldValidator ControlToValidate="txtAddress" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                           <!-- end col-4 -->
                          </div>
                        <!-- end row -->
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                         ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click"/>
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                            <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>AgentName</th>
                                                <th>Mobile No</th>
                                                <th>Location</th>
                                                <th>Commission</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Eval("AgentName")%></td>
                                        <td><%# Eval("Mobile")%></td>
                                        <td><%# Eval("Location")%></td>
                                        <td><%# Eval("Commission")%></td>
                                        <td>
                                                   <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEnquiryClick" CommandArgument="Edit" CommandName='<%# Eval("AgentID")%>'>
                                                    </asp:LinkButton>
                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument="Delete" CommandName='<%# Eval("AgentID")%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Agent details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                            <div class="row">
                                <div class="col-md-3" runat="server" id="div1">
								    <div class="form-group">
								      <label>Parent Commission</label>
								      <asp:TextBox runat="server" ID="txtParent_Commission" class="form-control" Text="0"></asp:TextBox>
								    </div>
                                   </div>
                                   <div class="col-md-4">
								     <div class="form-group">
									    <br />
									    <asp:Button runat="server" id="BtnParentComm_Save" Text="Save" class="btn btn-success" 
                                             onclick="BtnParentComm_Save_Click"  />
								     </div>
                                   </div>
                            </div>
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->

</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

