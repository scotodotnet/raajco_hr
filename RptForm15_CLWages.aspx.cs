﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class RptFrom15_CLWages : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";
    string Status = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionPayroll;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string Adolescent_Shift;
    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();

    string FromDate = "";
    string ToDate = "";
    string EmpType = "";
    string year = "";
    string EmpTypeName = "";


    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | FORM-15 CL WAGES";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionPayroll = Session["SessionEpay"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            EmpType = Request.QueryString["WagesType"].ToString();
            year = Request.QueryString["year"].ToString();
            // FromDate = Request.QueryString["FromDate"].ToString();
            //ToDate = Request.QueryString["ToDate"].ToString();

            
            if(EmpType.ToString().ToUpper()== "STAFF")
            {
                EmpTypeName = "1";
            }
            if(EmpType.ToString().ToUpper()== "MONTHLY WORKERS")
            {
                EmpTypeName = "2";
            }
            if (EmpType.ToString().ToUpper() == "MONTHLY FN")
            {
                EmpTypeName = "3";
            }
            if (EmpType.ToString().ToUpper() == "FN MONTHLY II ATM")
            {
                EmpTypeName = "4";
            }
            if (EmpType.ToString().ToUpper() == "FN MONTHLY I")
            {
                EmpTypeName = "5";
            }
            if (EmpType.ToString().ToUpper() == "SCHEDULED STAFF")
            {
                EmpTypeName = "6";
            }
            Get_Fprm15_CL_wage();
        }
    }

    private void Get_Fprm15_CL_wage()
    {
        DataTable DT = new DataTable();
        SSQL = "";
        SSQL = "Select ROW_NUMBER() Over (Order by MachineID) As [SN],MachineID,EmpName,FatherName,Department,DOJ,";
        SSQL = SSQL + "Worked_Days,Final_Bonus_Amt,BaseSalary from [" + SessionPayroll + "]..ELCL_Details";
        SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        SSQL = SSQL + " and Bonus_Year='" + year + "' and Employee_type='" + EmpTypeName + "'";
        DT=objdata.RptEmployeeMultipleDetails(SSQL);

        if (DT != null && DT.Rows.Count > 0)
        {
            report.Load(Server.MapPath("Payslip/Mill_Form-15_CL_Wages.rpt"));
            report.Database.Tables[0].SetDataSource(DT);
            report.DataDefinition.FormulaFields["Year"].Text = "'" + year + "'";
            report.DataDefinition.FormulaFields["FromDate"].Text = "'01/01/" + year + "'";
            report.DataDefinition.FormulaFields["ToDate"].Text = "'31/12/" + year + "'";
            report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            CrystalReportViewer1.ReportSource = report;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
        }
    }
}