﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="MstLabourAllot.aspx.cs" Inherits="MstLabourAllot" Title="" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css" />
   
    <asp:UpdatePanel ID="UpdatePanel5" runat="server">
        <ContentTemplate>
            <!-- begin #content -->
            <div id="content" class="content">
                <!-- begin breadcrumb -->
                <ol class="breadcrumb pull-right">
                    <li><a href="javascript:;">Master</a></li>
                    <li class="active">Employee Allotment</li>
                </ol>
                <!-- end breadcrumb -->
                <!-- begin page-header -->
                <h1 class="page-header">Employee Allotment </h1>
                <!-- end page-header -->

                <!-- begin row -->
                <div class="row">
                    <!-- begin col-12 -->
                    <div class="col-md-12">
                        <!-- begin panel -->
                        <div class="panel panel-inverse">
                            <div class="panel-heading">
                                <div class="panel-heading-btn">
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                    <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                                </div>
                                <h4 class="panel-title">Employee Allotment</h4>
                            </div>
                            <div class="panel-body">

                                <!-- begin row -->
                                <div class="row" runat="server" visible="false">
                                    <!-- begin col-2 -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>ManDay Cost</label>
                                            <asp:TextBox runat="server" ID="txtDayCost" class="form-control"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtDayCost" ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                    <!-- end col-2 -->
                                    <!-- begin col-2 -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Staff Allotment</label>
                                            <asp:TextBox runat="server" ID="txtStaffAllot" class="form-control"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtStaffAllot" ValidChars="0123456789">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                    <!-- end col-2 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button runat="server" ID="btnCostSave" Text="Save" class="btn btn-success" OnClick="btnCostSave_Click" />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                </div>
                                <!-- end row -->
                                <!-- begin row -->
                                <div class="row">
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Department Name</label>
                                            <asp:DropDownList runat="server" ID="ddlDepartment" class="form-control select2" Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="ddlDepartment_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ControlToValidate="ddlDepartment" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator2" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                      <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Labour Available</label>
                                           <asp:Label runat="server" ID="txtAvailLabour" Text="0" CssClass="form-control" Enabled="false" style="font-weight:bold;color:black;font-size:15px"></asp:Label>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Labour Allotment</label>
                                            <asp:TextBox runat="server" ID="txtLabourAllot" class="form-control" Text="0" AutoPostBack="true" OnTextChanged="txtLabourAllot_TextChanged"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtLabourAllot"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                                TargetControlID="txtLabourAllot" ValidChars="0123456789">
                                            </cc1:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label>Shift Name</label>
                                            <asp:DropDownList runat="server" ID="ddlShift" class="form-control select2" Style="width: 100%;">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ControlToValidate="ddlShift" InitialValue="-Select-" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <!-- begin col-4 -->
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Alloted Date</label>
                                            <asp:TextBox runat="server" ID="txtAllotedDate" class="form-control datepicker"></asp:TextBox>
                                            <asp:RequiredFieldValidator ControlToValidate="txtAllotedDate" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                </div>
                                <!-- end row -->

                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <!-- begin col-4 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <br />
                                            <asp:Button runat="server" ID="btnSave" Text="Save" class="btn btn-success" ValidationGroup="Validate_Field" OnClick="btnSave_Click" />
                                            <asp:Button runat="server" ID="btnClear" Text="Clear" class="btn btn-danger" OnClick="btnClear_Click" />
                                        </div>
                                    </div>
                                    <!-- end col-4 -->
                                    <div class="col-md-4"></div>
                                </div>
                                <!-- end row -->

                                <!-- table start -->
                                <div class="col-md-12">
                                    <div class="row">
                                        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
                                            <HeaderTemplate>
                                                <table id="example" class="display table table-bordered table-hover">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>DeptName</th>
                                                            <th>Labour Allotment</th>
                                                            <th>Shift Name</th>
                                                            <th>Alloted Date</th>
                                                            <th>Mode</th>
                                                        </tr>
                                                    </thead>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td><%# Eval("DeptName")%></td>
                                                    <td><%# Eval("Emp_Count")%></td>
                                                    <td><%# Eval("Shift")%></td>
                                                    <td><%# Eval("Allot_Date_Str")%></td>
                                                    <td>
                                                        <%--<asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-primery btn-sm fa fa-pencil" runat="server"
                                                            Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument='<%# Eval("Shift")+","+ Eval("Allot_Date_Str") %>' CommandName='<%# Eval("DeptName")%>'
                                                            CausesValidation="true">
                                                        </asp:LinkButton>--%>
                                                        <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o" runat="server"
                                                            Text="" OnCommand="GridDeleteEnquiryClick" CommandArgument='<%# Eval("Shift")+","+ Eval("Allot_Date_Str") %>' CommandName='<%# Eval("DeptName")%>'
                                                            CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Labour Allotment details?');">
                                                        </asp:LinkButton>
                                                    </td>

                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate></table></FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <!-- table End -->

                            </div>
                        </div>
                        <!-- end panel -->
                    </div>
                    <!-- end col-12 -->
                </div>
                <!-- end row -->
            </div>
            <!-- end #content -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript" src="assets/js/master_list_jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/master_list_jquery-ui.min.js"></script>
     <script type="text/javascript">
         $(document).ready(function () {
             $('#example').dataTable();
             $('.select2').select2();
             $('.datepicker').datepicker({
                 format: "dd/mm/yyyy",
                 autoclose: true
             });
         });
     </script>


    <script type="text/javascript">
        //On UpdatePanel Refresh
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        if (prm != null) {
            prm.add_endRequest(function (sender, e) {
                if (sender._postBackSettings.panelsToUpdate != null) {
                    $('#example').dataTable();
                    $('.select2').select2();
                    $('.datepicker').datepicker({
                        format: "dd/mm/yyyy",
                        autoclose: true
                    });
                }
            });
        };
    </script>
</asp:Content>

