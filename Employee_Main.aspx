﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Employee_Main.aspx.cs" Inherits="Employee_Main" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
            }
        });
    };
</script>
<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                                <ContentTemplate>
<!-- begin #content -->
<div id="content" class="content">
 <!-- begin breadcrumb -->
    <ol class="breadcrumb pull-right">
        <li><a href="javascript:;">Master</a></li>
        <li><a href="javascript:;">EmployeeProfile</a></li>
        <li class="active">EmployeeProfile</li>
    </ol>
    <!-- end breadcrumb -->
     <!-- begin page-header -->
    <h1 class="page-header">Employee Details</h1>
    <!-- end page-header -->
    
    <!-- begin row -->
    <div class="row">
        <!-- begin col-12 -->
        <div class="col-md-12">
            <div>
                <!-- begin panel -->
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        </div>
                        <h4 class="panel-title">Employee Details</h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-sm-3">
                                <asp:LinkButton ID="lbtnAdd" runat="server" class="btn btn-success" 
                                    onclick="lbtnAdd_Click" >Add New Employee</asp:LinkButton>
                                
                            </div>
                             <!-- begin col-2 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                       <%-- <label>Adhar No</label>--%>
                                        <asp:TextBox ID="txtAdharNo" runat="server" class="form-control" MaxLength="12" Visible="false" ></asp:TextBox>
                                       
                                   </div>
                                </div>
                             <!-- end col-2 -->
                             <div class="col-md-2">
                             <div class="form-group">
                            
                             <asp:Button runat="server" id="btnSearch" Visible="false" Text="Search" style="margin-top: 16%;" class="btn btn-primary" onclick="btnSearch_Click" />
                              </div>
                             </div>
                             <!-- begin col-2 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                       <%-- <label>Token No</label>--%>
                                        <asp:TextBox ID="txtTokenNo" runat="server" class="form-control" Enabled="false" visible="false"></asp:TextBox>
                                   </div>
                                </div>
                             <!-- end col-2 -->
                              <!-- begin col-2 -->
                                <div class="col-md-2">
                                    <div class="form-group">
                                      <%--  <label>Location</label>--%>
                                        <asp:TextBox ID="txtLocation" runat="server" class="form-control" Enabled="false" Visible="false" ></asp:TextBox>
                                   </div>
                                </div>
                             <!-- end col-2 -->
                        </div>
                          
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <label>Employee ActiveMode</label>
                        <asp:RadioButtonList ID="rbtnIsActive" runat="server" AutoPostBack="true"
                          RepeatDirection="Horizontal" 
                              onselectedindexchanged="rbtnIsActive_SelectedIndexChanged">
                        <asp:ListItem Text="Yes" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Pending" Value="3"></asp:ListItem>
                        <asp:ListItem Text="Cancel" Value="4"></asp:ListItem>
                        </asp:RadioButtonList>
                   </div>
                </div>
                        <div class="row">
                               <div class="form-group">
                               <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
					                    <HeaderTemplate>
                                            <table id="example" class="display table">
                                                <thead>
                                                    <tr>
                                                       <th>Token ID</th>
                                                        <th>Name</th>
                                                        <th>Category</th>
                                                        <th>Gender</th>
                                                        <th>DeptName</th>
                                                        <th>Designation</th>
                                                        <th>DateOfBirth</th>
                                                        <th>Edit</th>
                                                    </tr>
                                                </thead>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Eval("ExistingCode")%></td>
                                                <td><%# Eval("FirstName")%></td>
                                                <td><%# Eval("CatName")%></td>
                                                 <td><%# Eval("Gender")%></td>
                                                <td><%# Eval("DeptName")%></td>
                                                <td><%# Eval("Designation")%></td>
                                                <td><%# Eval("DOB")%></td>
                                                <td>
                                                    <asp:LinkButton ID="btnEditIssueEntry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                        Text="" OnCommand="GridEditEntryClick" CommandArgument="Edit" CommandName='<%# Eval("MachineID")%>'>
                                                    </asp:LinkButton>
                                                    </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate></table></FooterTemplate>                                
					                </asp:Repeater>
                               </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

