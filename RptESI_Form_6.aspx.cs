﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;

using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;


public partial class RptESI_Form_6 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string str_month = "";
    string str_yr = "";
    string str_cate;
    string str_dept;
    string AgentName;
    string SessionCcode;
    string SessionLcode;
    string FinYearVal = "";
    string Month;
    string SessionAdmin; string SessionUserType;
    static string CmpName;
    static string Cmpaddress;
    string query;
    string fromdate = "";
    string ToDate = "";
    string salaryType;
    string Emp_ESI_Code;
    string SessionPayroll;
    string EmployeeType; string EmployeeTypeCd;
    string PayslipType;
    string Report_Types;

    string PFTypeGet;

    string Left_Employee = "0";
    string Left_Date = "";
    string Get_Report_Type = "0";
    string Salary_CashOrBank;
    string Get_Division_Name = "";

    string EmpType = "";
    string Other_state = "";
    string Non_Other_state = "";
    string Basic_Report_Date = "";
    string Basic_Report_Type = "OLD";
    string ExemptedStaff = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        string date_1 = "";
        date_1 = ("01".ToString() + "-" + "12".ToString() + "-" + "2018".ToString()).ToString();


        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        SessionAdmin = Session["Isadmin"].ToString();
        //string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        str_cate = Request.QueryString["Cate"].ToString();
        str_dept = Request.QueryString["DeptName"].ToString();

        FinYearVal = Request.QueryString["yr"].ToString();

        str_month = Request.QueryString["Months"].ToString();
        str_yr = Request.QueryString["yr"].ToString();
        fromdate = Request.QueryString["fromdate"].ToString();
        ToDate = Request.QueryString["ToDate"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();
        // EmpType= Request.QueryString["EmpType"].ToString();
        //Report_Types = Request.QueryString["ReportFormat"].ToString();
        //AgentName = Request.QueryString["AgentName"].ToString();

        //salaryType = Request.QueryString["Salary"].ToString();
        //EmployeeTypeCd = Request.QueryString["EmpTypeCd"].ToString();
        EmployeeType = Request.QueryString["Wages"].ToString();

        Load_Report();
        //Load_ZeroWorkDays();
    }

    private void Load_Report()
    {
        DateTime From, To;

        From = Convert.ToDateTime(fromdate.ToString());
        To = Convert.ToDateTime(ToDate.ToString());

        DataTable dt1 = new DataTable();
   
        SSQL = "";
        SSQL = "Select EmpDet.ESINo,(SalDet.ExisistingCode) as ExistingCode,EmpDet.FirstName,'DINDIGUL' as Dispensary,EmpDet.Designation,isnull(Saldet.DeptName ,'') as deptShift";
        //SSQL = SSQL + ",'' as emptycol,cast(SalDet.WorkedDays as decimal(18,0)) as WorkedDays,(CASE when EmpDet.CatName='STAFF' then (NULLIF(SalDet.GrossEarnings,'0') + SalDet.HRAAmt + SalDet.TAAmt) else NULLIF(SalDet.GrossEarnings,'0') end) as GrossEarnings ,(CASE WHEN EmpDet.CatName='STAFF' then CAST(CAST(NULLIF((SalDet.GrossEarnings + SalDet.HRAAmt + SalDet.TAAmt),'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2)) else CAST(CAST(NULLIF(SalDet.GrossEarnings,'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2))end) as dailywages";
        SSQL = SSQL + ",'' as emptycol,cast(SalDet.WorkedDays as decimal(18,0)) as WorkedDays, as GrossEarnings,";
        SSQL = SSQL + " (CASE WHEN EmpDet.CatName='STAFF' then CAST(CAST(NULLIF((SalDet.GrossEarnings),'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2))  ";
        //SSQL = SSQL + " WHEN EmpDet.Wages='MONTHLY WORKERS' then CAST(CAST(NULLIF(((SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) + SalDet.DayIncentive ),'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2)) else CAST(CAST(NULLIF(SalDet.GrossEarnings,'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2))end) as dailywages,";
        SSQL = SSQL + " WHEN EmpDet.Wages='MONTHLY WORKERS' then CAST(CAST(NULLIF((SalDet.GrossEarnings),'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2))  when EmpDet.Wages='FN MONTHLY I' then CAST(CAST(NULLIF(SalDet.GrossEarnings,0)  as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2)) else CAST(CAST(NULLIF(SalDet.GrossEarnings,'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2))end) as dailywages,";
        SSQL = SSQL + " SalDet.ESI as EmpShareContribution";
        SSQL = SSQL + " from Employee_Mst EmpDet inner join [" + SessionPayroll + "]..SalaryDetails SalDet on SalDet.EmpNo=EmpDet.EmpNo where";
        SSQL = SSQL + " SalDet.FromDate>=Convert(datetime,'" + From.ToString("dd/MM/yyyy") + "',103) and SalDet.ToDate<=Convert(datetime,'" + To.ToString("dd/MM/yyyy") + "',103) and EmpDet.IsActive='Yes'";
        SSQL = SSQL + " and SalDet.FinancialYear='" + FinYearVal + "' and SalDet.Month='" + str_month + "' and EmpDet.Eligible_ESI='1' and  (CASE When EmpDet.CatName='STAFF' then (Cast((SalDet.TotalBasicAmt+SalDet.HRAAmt+SalDet.TAAmt) as decimal(18,0)))  when EmpDet.Wages='MONTHLY WORKERS' then (Cast((SalDet.TotalBasicAmt+SalDet.DayIncentive) as decimal(18,0))) else (SalDet.TotalBasicAmt) end) <'21000' and SalDet.WorkedDays >'0'";
        if (str_dept.ToString() != "-Select-")
        {
            SSQL = SSQL + " and SalDet.DeptName='" + str_dept + "'";
        }
        if (str_cate.ToString() != "")
        {
            SSQL = SSQL + " and SalDet.CatName='" + str_cate + "'";
        }
        if (EmployeeType.ToString() != "" && EmployeeType.ToString() != "-Select-" && EmployeeType.ToString() != "0")
        {
            SSQL = SSQL + " and SalDet.Wages='" + EmployeeType + "'";
        }
        else
        {
            SSQL = SSQL + " and Saldet.Wages!='CONTRACTCLEANING I' and SalDet.Wages!='CONTRACTCLEANING II'";
        }
        //}
        SSQL = SSQL + " order by SalDet.ExisistingCode asc";
        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count > 0)
        {
            string attachment = "attachment;filename=ESI_Form6.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            DataTable dt = new DataTable();
            
            Response.Write("<table>");
            if (EmployeeType != "CONTRACTCLEANING I" && EmployeeType != "CONTRACTCLEANING II")
            {
                Response.Write("<tr>");
                Response.Write("<td align='Left' colspan='4'>");

                Response.Write("</td>");
                Response.Write("<td  align='Center' colspan='4'>");
                Response.Write("<b>RAAJCO SPINNERS PRIVATE LIMITED ,DINDIGUL</b>");
                Response.Write("</td>");
                Response.Write("<td align='Right' colspan='4'>");
                Response.Write("REG.FORM-6");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='4' align='Left'>");
                Response.Write("REGISTER OF EMPLOYEES, EMPLOYEES STATE INSURANCE CORPORATION, (REGULATION 32)");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='2' align='Left'>");
                Response.Write("CONTRIBUTION PERIOD :");
                Response.Write("</td>");
                Response.Write("<td colspan='2' align='Center'>");
                Response.Write("FROM : " + fromdate.ToString() + "");
                Response.Write("</td>");
                Response.Write("<td colspan='2' align='Center'>");
                Response.Write("TO : " + ToDate.ToString() + "");
                Response.Write("</td>");
                Response.Write("<td colspan='2' align='Center'>");
                Response.Write("MONTH : " + str_month.ToString().ToUpper() + " " + FinYearVal.ToString());
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            Response.Write("<td colspan='12'>");
            Response.Write("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td>");
            Response.Write("SL.No");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("INSURANCE NUMBER");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("CODE NO");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("NAME OF THE INSURED PERSON");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("NAME OF THE DISPENSARY TO WHICH ATTACHED");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("OCCUPATION");
            Response.Write("</td>");
            Response.Write("<td aligng='center'>");
            Response.Write("DEPARTMENT AND SHIFT IF ANY");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("IF APPNT. OR LEFT SERVICE DURING THE CONTRBN.PERIOD, DATE OF APPOINTMENT / LEAVING");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("NO OF DAYS FOR WHICH WAGES PAID / PAYABLE");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("TOTAL AMOUNT OF WAGES PAID");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("EMPLOYEE'S SHARE OF CONTRBN.");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("DAILY WAGES");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='12'>");
            Response.Write("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("<table>");

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                Response.Write("<tr style='font-weight: bold;'>");
                Response.Write("<td>" + Convert.ToInt32(i + 1).ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["ESINo"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["ExistingCode"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["FirstName"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["Dispensary"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["Designation"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["deptShift"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["emptycol"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["WorkedDays"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["GrossEarnings"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["EmpShareContribution"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["dailywages"].ToString() + "</td>");
                Response.Write("</tr>");
            }

            Response.Write("<tr>");
            Response.Write("<td colspan='9'>");
            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("--------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='8'>");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("TOTAL");
            Response.Write("</td>");

            if (dt1.Rows.Count > 0)
            {

                if (EmployeeType != "CONTRACTCLEANING I" && EmployeeType != "CONTRACTCLEANING II")
                {
                    Response.Write("<td style='font-weight:bold;'>=Sum(J7:J" + Convert.ToDecimal(dt1.Rows.Count + 6) + ")");
                    Response.Write("<td style='font-weight:bold;'>=Sum(K7:K" + Convert.ToDecimal(dt1.Rows.Count + 6) + ")");
                }
                else
                {
                    Response.Write("<td style='font-weight:bold;'>=Sum(J4:J" + Convert.ToDecimal(dt1.Rows.Count + 3) + ")");
                    Response.Write("<td style='font-weight:bold;'>=Sum(K4:K" + Convert.ToDecimal(dt1.Rows.Count + 3) + ")");
                }
            }

            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='9'>");
            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("--------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records found');", true);

        }
    }
    public void Load_ZeroWorkDays()
    {
        DateTime From, To;

        From = Convert.ToDateTime(fromdate.ToString());
        To = Convert.ToDateTime(ToDate.ToString());

        DataTable dt1 = new DataTable();

        SSQL = "";
        SSQL = "Select EmpDet.ESINo,EmpDet.ExistingCode,EmpDet.FirstName,'ARUPPUKOTTAI' as Dispensary,EmpDet.Designation,isnull((EmpDet.DeptName +'-'+EmpDet.ShiftName),'') as deptShift";
        SSQL = SSQL + ",'' as emptycol,SalDet.WorkedDays, SalDet.GrossEarnings,CAST(CAST(NULLIF(SalDet.GrossEarnings,'0') as decimal(18,2))/CAST(NULLIF(SalDet.WorkedDays,'0') as decimal(18,2)) as decimal(18,2)) as dailywages";
        SSQL = SSQL + ",SalDet.ESI as EmpShareContribution";
        SSQL = SSQL + " from Employee_Mst EmpDet inner join Ramalinga_Epay..SalaryDetails SalDet on SalDet.EmpNo=EmpDet.EmpNo where";
        SSQL = SSQL + " SalDet.FromDate>=Convert(datetime,'" + From.ToString("dd/MM/yyyy") + "',103) and SalDet.ToDate<=Convert(datetime,'" + To.ToString("dd/MM/yyyy") + "',103)";
        SSQL = SSQL + " and SalDet.FinancialYear='" + FinYearVal + "' and SalDet.Month='" + str_month + "' and EmpDet.Eligible_ESI='1' and SalDet.WorkedDays=0";
        if (str_dept.ToString() != "-Select-")
        {
            SSQL = SSQL + " and EmpDet.DeptName='" + str_dept + "'";
        }
        if (str_cate.ToString() != "")
        {
            SSQL = SSQL + " and EmpDet.CatName='" + str_cate + "'";
        }
        if (EmployeeType.ToString() != "" && EmployeeType.ToString() != "-Select-" && EmployeeType.ToString() != "0")
        {
            SSQL = SSQL + " and EmpDet.Wages='" + EmployeeType + "'";
        }
        else
        {
            SSQL = SSQL + " and EmpDet.Wages!='CONTRACTCLEANING I' and EmpDet.Wages!='CONTRACTCLEANING II'";
        }

        dt1 = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dt1.Rows.Count > 0)
        {
            string attachment = "attachment;filename=ESI_Form6_ZeroDays.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            StringWriter stw = new StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            DataTable dt = new DataTable();
           
            Response.Write("<table>");
            if (EmployeeType != "CONTRACTCLEANING I" && EmployeeType != "CONTRACTCLEANING II")
            {
                Response.Write("<tr>");
                Response.Write("<td align='Left' colspan='4'>");

                Response.Write("</td>");
                Response.Write("<td  align='Center' colspan='4'>");
                Response.Write("<b>SHRI RAMALINGA MILLS LIMITED ,ARUPPUKOTTAI</b>");
                Response.Write("</td>");
                Response.Write("<td align='Right' colspan='4'>");
                Response.Write("REG.FORM-6");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='4' align='Left'>");
                Response.Write("REGISTER OF EMPLOYEES, EMPLOYEES STATE INSURANCE CORPORATION, (REGULATION 32)");
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr>");
                Response.Write("<td colspan='2' align='Left'>");
                Response.Write("CONTRIBUTION PERIOD :");
                Response.Write("</td>");
                Response.Write("<td colspan='2' align='Center'>");
                Response.Write("FROM : " + fromdate.ToString() + "");
                Response.Write("</td>");
                Response.Write("<td colspan='2' align='Center'>");
                Response.Write("TO : " + ToDate.ToString() + "");
                Response.Write("</td>");
                Response.Write("<td colspan='2' align='Center'>");
                Response.Write("MONTH : " + str_month.ToString().ToUpper() + " " + FinYearVal.ToString());
                Response.Write("</td>");
                Response.Write("</tr>");
            }
            Response.Write("<tr>");
            Response.Write("<td colspan='12'>");
            Response.Write("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td>");
            Response.Write("SL.No");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("INSURANCE NUMBER");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("CODE NO");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("NAME OF THE INSURED PERSON");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("NAME OF THE DISPENSARY TO WHICH ATTACHED");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("OCCUPATION");
            Response.Write("</td>");
            Response.Write("<td aligng='center'>");
            Response.Write("DEPARTMENT AND SHIFT IF ANY");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("IF APPNT. OR LEFT SERVICE DURING THE CONTRBN.PERIOD, DATE OF APPOINTMENT / LEAVING");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("NO OF DAYS FOR WHICH WAGES PAID / PAYABLE");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("TOTAL AMOUNT OF WAGES PAID");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("EMPLOYEE'S SHARE OF CONTRBN.");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("DAILY WAGES");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='12'>");
            Response.Write("---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write("<table>");


            //DataTable dt1 = new DataTable();

            //grid.DataSource = dt1; //Your DataTable value here
            //grid.DataBind();
            //grid.ShowHeader = false;
            //grid.RenderControl(htextw);
            // Response.Write(stw.ToString());

            for (int i = 0; i < dt1.Rows.Count; i++)
            {
                Response.Write("<tr style='font-weight: bold;' align='center'>");
                Response.Write("<td>" + Convert.ToInt32(i + 1).ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["ESINo"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["ExistingCode"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["FirstName"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["Dispensary"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["Designation"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["deptShift"].ToString() + "</td>");
                Response.Write("<td>" + dt1.Rows[i]["emptycol"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["WorkedDays"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["GrossEarnings"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["EmpShareContribution"].ToString() + "</td>");
                Response.Write("<td align='right'>" + dt1.Rows[i]["dailywages"].ToString() + "</td>");
                Response.Write("</tr>");
            }

            Response.Write("<tr>");
            Response.Write("<td colspan='9'>");
            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("--------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='8'>");
            Response.Write("</td>");
            Response.Write("<td align='center'>");
            Response.Write("TOTAL");
            Response.Write("</td>");

            if (dt1.Rows.Count > 0)
            {
                if (EmployeeType != "CONTRACTCLEANING I" && EmployeeType != "CONTRACTCLEANING II")
                {
                    Response.Write("<td style='font-weight:bold;'>=Sum(J7:J" + Convert.ToDecimal(dt1.Rows.Count + 6) + ")");
                    Response.Write("<td style='font-weight:bold;'>=Sum(K7:K" + Convert.ToDecimal(dt1.Rows.Count + 6) + ")");
                }
                else
                {
                    Response.Write("<td style='font-weight:bold;'>=Sum(J4:J" + Convert.ToDecimal(dt1.Rows.Count + 3) + ")");
                    Response.Write("<td style='font-weight:bold;'>=Sum(K4:K" + Convert.ToDecimal(dt1.Rows.Count + 3) + ")");
                }

            }

            Response.Write("</tr>");
            Response.Write("<tr>");
            Response.Write("<td colspan='9'>");
            Response.Write("</td>");
            Response.Write("<td colspan='3'>");
            Response.Write("--------------------------------------------------------------------------------------------------");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");

            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records found');", true);

        }
    }
}