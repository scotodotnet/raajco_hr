﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Globalization;


public partial class RptTravelAllowance : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;

    string SSQL = "";
    string FDate_Str = "";
    string ToDate_Str = "";
    string vehicletype = "";

    BALDataAccess objdata = new BALDataAccess();
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            FDate_Str = Request.QueryString["FromDate"].ToString();
            ToDate_Str = Request.QueryString["ToDate"].ToString();
            vehicletype = Request.QueryString["VehicleType"].ToString();

            GetReport();
        }
    }
    private void GetReport()
    {
        DataTable dt = new DataTable();

        SSQL = "";
        SSQL = "Select EM.MachineID,EM.ExistingCode as TokenNo,EM.FirstName as EmpName,EM.DeptName as Department,EM.Vehicles_Type";
        SSQL = SSQL + ",SUM(isnull(LD.Present,'0')) as Days,(SUM(isnull(LD.Present,'0')) * isnull(VD.Commission,'0')) as Amount";
        SSQL = SSQL + " from Employee_mst EM inner join MstVehicle VD on VD.VehicleType=EM.Vehicles_Type and VD.Ccode=EM.CompCode and VD.Lcode=EM.LocCode";
        SSQL = SSQL + " inner join LogTime_Days LD on LD.MachineID=EM.MachineID and LD.CompCode=EM.CompCode and LD.LocCOde=EM.LocCOde";
        SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "'";
        if (vehicletype != "-Select-")
        {
            SSQL = SSQL + " and EM.Vehicles_type='" + vehicletype + "'";
        }
        SSQL = SSQL + " and Convert(datetime,LD.Attn_Date,103)>=Convert(datetime,'" + FDate_Str + "',103)";
        SSQL = SSQL + " and Convert(datetime,LD.Attn_Date,103)<=Convert(datetime,'" + ToDate_Str + "',103)";
        SSQL = SSQL + " and LD.Present!='0.0' and EM.Vehicles_type is not null and  EM.Vehicles_type!='-Select-'";
        SSQL = SSQL + " group by EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DeptName,EM.Vehicles_Type,VD.Commission";
        SSQL = SSQL + " order by EM.MachineID asc";

        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt.Rows.Count > 0)
        {
            grid.DataSource = dt;
            grid.DataBind();

            string attachment = "attachment;filename=TRAVEL ALLOWANCE REPORT.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/ms-excel";
            grid.HeaderStyle.Font.Bold = true;
            System.IO.StringWriter stw = new System.IO.StringWriter();
            HtmlTextWriter htextw = new HtmlTextWriter(stw);
            grid.RenderControl(htextw);
            Response.Write("<table>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + dt.Columns.Count + "'>");
            Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("<tr Font-Bold='true' align='center'>");
            Response.Write("<td colspan='" + dt.Columns.Count + "'>");
            Response.Write("<a style=\"font-weight:bold\">TRAVEL ALLOWANCE-" + Session["Lcode"].ToString() + "-" + FDate_Str + "-" + ToDate_Str + "</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.Write(stw.ToString());
            Response.Write("<table border=1>");
            Response.Write("<tr>");
            Response.Write("<td colspan='5' align='center'>");
            Response.Write("<a style=\"font-weight:bold\">Total</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">=Sum(F4:F" + (dt.Rows.Count + 3) + ")</a>");
            Response.Write("</td>");
            Response.Write("<td>");
            Response.Write("<a style=\"font-weight:bold\">=Sum(G4:G" + (dt.Rows.Count + 3) + ")</a>");
            Response.Write("</td>");
            Response.Write("</tr>");
            Response.Write("</table>");
            Response.End();
            Response.Clear();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No Records Found')", true);
        }
    }
}