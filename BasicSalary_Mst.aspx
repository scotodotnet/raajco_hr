﻿<%@ Page Language="C#"MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="BasicSalary_Mst.aspx.cs" Inherits="BasicSalary_Mst" Title="BasicSalary Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:UpdatePanel ID="SalPay" runat="server">
                 <ContentTemplate>

<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
        if (sender._postBackSettings.panelsToUpdate != null) {
            $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>


 <!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Master</a></li>
				<li class="active">Basic Salary Master</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Basic Salary Master</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Basic Salary Master</h4>
                        </div>
                        <div class="panel-body">
                        <div class="form-group">
                        
                        <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Category</label>
								 <asp:DropDownList runat="server" ID="ddlcategory" class="form-control select2"  
                                        style="width:100%;" AutoPostBack="true" 
                                        onselectedindexchanged="ddlcategory_SelectedIndexChanged">
								 <asp:ListItem Value="-Select-">-Select-</asp:ListItem>
								 <asp:ListItem Value="STAFF">STAFF</asp:ListItem>
								 <asp:ListItem Value="LABOUR">LABOUR</asp:ListItem>
								</asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                            <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Employee Type</label>
								 <asp:DropDownList runat="server" ID="txtEmployeeType" class="form-control select2" style="width:100%;">
                                    
								 </asp:DropDownList>
								
								</div>
                               </div>
                           <!-- end col-4 -->
                             <div class="col-md-4">
								<div class="form-group">
								 <label>Fin. Year</label>
								 <asp:DropDownList runat="server" ID="ddlFinance" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
								</div>
                               </div>
                        
                        </div>
                    <!-- end row -->
                       <!-- begin row -->
                        <div class="row">
                         <!-- begin col-4 -->
                              <div class="col-md-4">
								<div class="form-group">
								 <label>Month</label>
								 <asp:DropDownList runat="server" ID="ddlMonths" class="form-control select2" style="width:100%;">
							 	 </asp:DropDownList>
							 	 <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_PayField" class="form_error" ID="RequiredFieldValidator3" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ExpField" class="form_error" ID="RequiredFieldValidator4" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_LeaveField" class="form_error" ID="RequiredFieldValidator9" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ControlToValidate="ddlMonths" InitialValue="0" Display="Dynamic"  ValidationGroup="Validate_ManField" class="form_error" ID="RequiredFieldValidator12" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                </asp:RequiredFieldValidator>
								</div>
                               </div>


                                  <div class="col-md-4">
								<div class="form-group">
								    <label>Salary Percentage(%)</label>
								 	  <asp:TextBox runat="server" ID="txtPercentage" class="form-control" Text=""></asp:TextBox>
                                    <%--<asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPercentage" ErrorMessage="Enter the Valid Percentage" MaximumValue="100" MinimumValue="0" Type="Integer" ForeColor="Red"></asp:RangeValidator>--%>
                                   
								</div>
                            </div>
                            </div>
                            		  <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
                                    <asp:Button runat="server" id="btnConform" Text="Confirm" class="btn btn-warning" 
                                        onclick="btnConform_Click" />
						    	</div>
                            </div>
                           
                        </div>

                        <div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                 <th>S.No</th>
                                                <th>Employee Type</th>
                                                <th>Fin.Year</th>
                                                <th>Month</th>
                                                <th>Percentage(%)</th>
                                                <th>Create Date</th>
                                                <th>Status</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                          <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("EmployeeType")%></td>
                                        <td><%# Eval("FinYear")%></td>
                                        <td><%# Eval("Months")%></td>
                                        <td><%# Eval("Percentage")%></td>
                                        <td><%# Eval("CreatedDate")%></td>
                                        <td><%# Eval("conform")%></td>
                                        
                                      <td>
                                                    <asp:LinkButton ID="btnEditEnquiry_Grid" class="btn btn-success btn-sm fa fa-pencil"  runat="server" 
                                                    Text="" OnCommand="btnEditEnquiry_Grid_Command" CommandArgument='<%#Eval("EmployeeType") + "|" +Eval("FinYear")+"|" +"|"+Eval("Months") +"|"%>'
                                           CausesValidation="true"  OnClientClick="return confirm('Are you sure you want to Edit this details? ');">
                                                          </asp:LinkButton>



                                                    <asp:LinkButton ID="btnDeleteEnquiry_Grid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                        Text="" OnCommand="btnDeleteEnquiry_Grid_Command" CommandArgument='<%#Eval("EmployeeType") + "|" +Eval("FinYear")+"|" +"|"+Eval("Months") +"|"%>' 
                                                        CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this  details?');">
                                                    </asp:LinkButton>
                                                </td>
                                       
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

