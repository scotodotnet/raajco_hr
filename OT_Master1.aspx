﻿<%@ Page Language="C#"MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="OT_Master1.aspx.cs" Inherits="OT_Master1" Title="OT_Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="UpdatePanel25" runat="server">
   <ContentTemplate>
     <!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Salary Category</li>
	</ol>
	<h1 class="page-header">OT Master</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">OT Master</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <h5></h5>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                        <div class="row">
                            <%--<div class="col-md-3">
							    <div class="form-group">--%>
								    <label> </label>
								
                                    <asp:CheckBox ID="chkbasic" runat="server"  /><label>Basic and DA </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                                    <asp:CheckBox ID="ChkHRA" runat="server" /><label>HRA</label>&nbsp&nbsp&nbsp&nbsp&nbsp
                            <asp:CheckBox ID="ChkConv" runat="server" /><label>Conv.Allowance </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                                     <asp:CheckBox ID="ChkEdu" runat="server" /><label>Edu.Allowance </label>&nbsp&nbsp&nbsp&nbsp&nbsp        
                                    <asp:CheckBox ID="ChkMedi" runat="server" /><label>Medical Allowance </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                            <asp:CheckBox ID="chkWH" runat="server" /><label>Washing Allowance </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                            <asp:CheckBox ID="ChkSpl" runat="server" /><label>Special Allowance </label>&nbsp&nbsp&nbsp&nbsp&nbsp
                               </div>
                            <%--</div>
                            </div>--%>
                            
                                                   <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click1" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   </ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

