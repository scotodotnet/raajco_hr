﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;

public partial class RptWeekly : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionEpay;
    string SessionUserType;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    string EmpType = "";

    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    string RptType = "";
    string Payslip_Folder = "Payslip";
    string Month = "";
    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- Weekly Worker Payslip";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
                SessionCcode = Session["Ccode"].ToString();
                SessionLcode = Session["Lcode"].ToString();
                //SessionAdmin = Session["Isadmin"].ToString();
                //SessionCompanyName = Session["CompanyName"].ToString();
                //SessionLocationName = Session["LocationName"].ToString();
                SessionUserType = Session["Isadmin"].ToString();
                SessionEpay = Session["SessionEpay"].ToString();
               // Status = Request.QueryString["Status"].ToString();
               // Division = Request.QueryString["Division"].ToString();
                string TempWages = Request.QueryString["Wages"].ToString();
                WagesType = TempWages.Replace("_", "&");
                //WagesType = Request.QueryString["Wages"].ToString();
             //   TokenNo = Request.QueryString["TokenNo"].ToString();
                FromDate = Request.QueryString["FromDate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                string SessionUserType_1 = SessionUserType;
                RptType= Request.QueryString["Report_Type"].ToString();

                if (RptType != "Payslip_Pdf")
                {
                    Weekly();
                }
                else
                {
                    
                        Month = Request.QueryString["AttMonths"].ToString();
                    GetData();
                    if (AutoDTable.Rows.Count != 0)
                    {
                        DataSet ds = new DataSet();
                        ds.Tables.Add(AutoDTable);

                        ReportDocument report = new ReportDocument();    
                        report.Load(Server.MapPath(Payslip_Folder + "/Weekly.rpt"));
                        



                        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                        report.DataDefinition.FormulaFields["Wages"].Text = "'" + EmpType + "'";
                        report.DataDefinition.FormulaFields["Months"].Text = "'" + Month + "'";
                        // report.DataDefinition.FormulaFields["Year"].Text = "'" + str_yr + "'";
                        report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                        report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";

                        report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                        CrystalReportViewer1.ReportSource = report;
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
                    }  
                }
            }
        }
    }

    private void GetData()
    {
        string TableName = "";
        TableName = "Employee_Mst";

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();
        DataTable dtAmt = new DataTable();
        DataTable dtSal = new DataTable();
        DataTable DT_Emp = new DataTable();
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        //AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("Emp.Code");
        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("WH");



        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();
        string Query_header = "";
        string Query_Pivot = "";
        string whDays = "0";
        string TotDays = "0";
        string salary = "0";
        string OTAmount = "0";
        string TotAmount = "0";
        string NetPay = "0";
        string TotDed = "0";
        // string oneday = "0";
        string WH_Amount = "0";
        string basesalary = "0";
        string DeptName = "";
        string OneDay = "0";
        string OTAdd = "0";
        string Round = "0";
        string OTDay;

        AutoDTable.Columns.Add("Total_Days");
        AutoDTable.Columns.Add("OTDays");
        AutoDTable.Columns.Add("OTHrs");
        AutoDTable.Columns.Add("RatePerDay");
        AutoDTable.Columns.Add("Amount");
        AutoDTable.Columns.Add("OT Amount");
        AutoDTable.Columns.Add("Total");
        AutoDTable.Columns.Add("Ded");
        AutoDTable.Columns.Add("Earned Amount");
        AutoDTable.Columns.Add("R.Off");
        AutoDTable.Columns.Add("NetPay");
        AutoDTable.Columns.Add("Signature");

        string query = "";
        query = "Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff, ";
        query = query + "sum(LD.Present )Present  from Employee_Mst EM inner join";
        query = query + " LogTime_Days LD on EM.MachineID=LD.MachineID ";
        query = query + "  Where EM.Wages='" + WagesType + "'";
        query = query + "  And CONVERT(datetime, LD.Attn_Date,103) >= CONVERT(datetime, '" + FromDate + " ', 103)";
        query = query + " And CONVERT(datetime, LD.Attn_Date,103) <= CONVERT(datetime, '" + ToDate + "', 103)";

        query = query + " group by EM.MachineID,EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);
        for (int i = 0; i < dt.Rows.Count; i++)
        {

            string Sumdays = "";
            Sumdays = dt.Rows[i]["Present"].ToString();
            if (Sumdays != "0.0")
            {
                AutoDTable.NewRow();
                AutoDTable.Rows.Add();
                string MachineID = dt.Rows[i]["MachineID"].ToString();
                int DT_Row = AutoDTable.Rows.Count - 1;
                AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
                AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                //AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[DT_Row]["Emp.Code"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();

                //string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
                AutoDTable.Rows[DT_Row]["WH"] = dt.Rows[i]["WeekOff"].ToString();

                string DOJ_Date_Str = "";
                if (dt.Rows[i]["DOJ"].ToString() != "")
                {
                    DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                }
                else
                {
                    DOJ_Date_Str = "";
                }

                DataTable DT_Hrs = new DataTable();
                string OT_Hrs = "";
                //string date_str = dayy.ToString("dd/MM/yyyy HH:mm:ss");
                //string[] Split = date_str.Split(' ');
                //OTDay = Split[0];
                //OT_Hrs = "";


                //SSQL = "";
                //SSQL = "Select * from [Raajco_Epay]..OT_Hrs_Mst where Attn_Date_Str='" + OTDay + "' and MachineID='" + MachineID + "'";
                //DT_Hrs = objdata.RptEmployeeMultipleDetails(SSQL);
                //if (DT_Hrs.Rows.Count != 0)
                //{

                //    OT_Hrs = DT_Hrs.Rows[0]["OT_Hrs"].ToString();
                //}
                //if (DT_Hrs.Rows.Count != 0)
                //{
                //    if (OT_Hrs != "0")
                //    {


                //    }
                //}



                SSQL = "";
                SSQL = "Select * from Employee_Mst where MachineID ='" + MachineID + "'";
                DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);

                basesalary = DT_Emp.Rows[0]["BaseSalary"].ToString();
                DeptName = DT_Emp.Rows[0]["DeptName"].ToString();
                OneDay = (Convert.ToDecimal(basesalary) / 26).ToString();
                OneDay = (Math.Round(Convert.ToDecimal(OneDay), 0, MidpointRounding.AwayFromZero)).ToString();
                AutoDTable.Rows[DT_Row]["RatePerDay"] = OneDay;
                SSQL = "";
                SSQL = "Select * from ["+SessionEpay+"]..SalaryDetails where MachineNo='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                SSQL = SSQL + " And LCode='" + SessionLcode + "'";
                SSQL = SSQL + " And (convert(datetime,FromDate, 105) >= convert(datetime, '" + FromDate + "', 105))";
                SSQL = SSQL + " And (convert(datetime,ToDate, 105) <= convert(datetime, '" + ToDate + "', 105))";
                dtSal = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtSal.Rows.Count != 0)
                {
                    string mess1 = "";
                    string TShirt = "";

                    AutoDTable.Rows[DT_Row]["Amount"] = dtSal.Rows[0]["GrossEarnings"].ToString();
                    salary = dtSal.Rows[0]["GrossEarnings"].ToString();
                    mess1 = dtSal.Rows[0]["Deduction4"].ToString();
                    TShirt = dtSal.Rows[0]["T_Shirt"].ToString();
                    TotDed = (Convert.ToDecimal(mess1) + Convert.ToDecimal(TShirt)).ToString();
                    AutoDTable.Rows[DT_Row]["Ded"] = TotDed;
                    whDays = dtSal.Rows[0]["WH_Work_Days"].ToString();
                    AutoDTable.Rows[DT_Row]["OTDays"] = whDays;

                }

                AutoDTable.Rows[DT_Row]["OT Amount"] = "0";
                AutoDTable.Rows[DT_Row]["OTHrs"] = "0";


                SSQL = "";
                SSQL = "select MachineID,Oneday_Wages,Sum(Cast(Tot_OTHrs as int)) as Tot_OTHrs,Sum(cast(Amount as decimal(18,2))) Amount,Sum(Cast(WH as int))WH   from ["+ SessionEpay + "]..OT_Amt where MachineID='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                SSQL = SSQL + " And LCode='" + SessionLcode + "'";
                SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
                SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) <= CONVERT(datetime,'" + ToDate + "',103) group by MachineID,Oneday_Wages";
                dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);
                if (dtAmt.Rows.Count != 0)
                {
                    if (dtAmt.Rows[0]["Tot_OTHrs"].ToString() != "")
                    {
                        AutoDTable.Rows[DT_Row]["OTHrs"] = dtAmt.Rows[0]["Tot_OTHrs"].ToString();
                    }
                    OTAdd = dtAmt.Rows[0]["Amount"].ToString();

                    //OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();

                }

                if (whDays != "0")
                {
                    WH_Amount = (Convert.ToDecimal(OneDay) * Convert.ToDecimal(whDays)).ToString();

                }
                else
                {
                    WH_Amount = "0";
                }

                OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();

                AutoDTable.Rows[DT_Row]["OT Amount"] = OTAmount;

                query = "Select isnull(sum(present),0) as Total_Days from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                query = query + " And LocCode='" + SessionLcode + "'";
                query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
                query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103)  ";
                dt1 = objdata.RptEmployeeMultipleDetails(query);
                if (dt1.Rows.Count != 0)
                {

                    if (whDays != "0")
                    {
                        TotDays = "0";
                        TotDays = dt1.Rows[0]["Total_Days"].ToString();

                        TotDays = (Convert.ToDecimal(TotDays) - Convert.ToDecimal(whDays)).ToString();
                        AutoDTable.Rows[DT_Row]["Total_Days"] = TotDays;
                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["Total_Days"] = dt1.Rows[0]["Total_Days"].ToString();
                    }
                }
                else
                {

                    AutoDTable.Rows[DT_Row]["Total_Days"] = "0";
                }

                TotAmount = (Convert.ToDecimal(salary) + Convert.ToDecimal(OTAmount)).ToString();
                AutoDTable.Rows[DT_Row]["Total"] = TotAmount;

                string Netpay1 = "0";
                NetPay = (Convert.ToDecimal(TotAmount) - Convert.ToDecimal(TotDed)).ToString();
                Netpay1 = (Convert.ToDecimal(TotAmount) - Convert.ToDecimal(TotDed)).ToString();
                AutoDTable.Rows[DT_Row]["Earned Amount"] = NetPay;
                NetPay = (Math.Round(Convert.ToDecimal(NetPay), 0, MidpointRounding.AwayFromZero)).ToString();
                AutoDTable.Rows[DT_Row]["NetPay"] = NetPay;
                Round = (Convert.ToDecimal(NetPay) - Convert.ToDecimal(Netpay1)).ToString();
                AutoDTable.Rows[DT_Row]["R.Off"] = Round;


                whDays = "0";
                TotDays = "0";
                salary = "0";
                OTAmount = "0";
                TotAmount = "0";
                NetPay = "0";
                TotDed = "0";
                WH_Amount = "0";
                OneDay = "0";
                OTAdd = "0";
            }
        }
    }



    private void Weekly()
    {

        string TableName = "";
        TableName = "Employee_Mst";

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();
        DataTable dtAmt = new DataTable();
        DataTable dtSal = new DataTable();
        DataTable DT_Emp = new DataTable();
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("DeptName");
        AutoDTable.Columns.Add("MachineID");
        //AutoDTable.Columns.Add("ExistingCode");
        AutoDTable.Columns.Add("Emp.Code");
        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("WH");
       


        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();

        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        string whDays = "0";
        string TotDays = "0";
        string salary = "0";
        string OTAmount = "0";
        string TotAmount = "0";
        string NetPay = "0";
        string TotDed = "0";
       // string oneday = "0";
        string WH_Amount = "0";
        string basesalary = "0";
        string DeptName = "";
        string OneDay = "0";
        string OTAdd = "0";
        string Round = "0";
        string OTDay ;

        while (daycount >= 0)
        {
            DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
            //string day1=string.Format("{MM/dd/yyyy}",date1.AddDays(daysAdded));
            AutoDTable.Columns.Add(Convert.ToString(dayy.Day.ToString()));

            if (daysAdded == 0)
            {
                Query_header = "isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = "[" + dayy.Day.ToString() + "]";
            }
            else
            {
                Query_header = Query_header + ",isnull([" + dayy.Day.ToString() + "],'A') as [" + dayy.Day.ToString() + "]";
                Query_Pivot = Query_Pivot + ",[" + dayy.Day.ToString() + "]";
            }
            daycount -= 1;
            daysAdded += 1;
        }

        AutoDTable.Columns.Add("Total_Days");
        AutoDTable.Columns.Add("OTDays");
        AutoDTable.Columns.Add("OTHrs");
        AutoDTable.Columns.Add("RatePerDay");
        AutoDTable.Columns.Add("Amount");
        AutoDTable.Columns.Add("OT Amount");
        AutoDTable.Columns.Add("Total");
        AutoDTable.Columns.Add("Ded");
        AutoDTable.Columns.Add("Earned Amount");
        AutoDTable.Columns.Add("R.Off");
        AutoDTable.Columns.Add("NetPay");
        AutoDTable.Columns.Add("Signature");
        string query = "";
        query = "Select DeptName,MachineID,ExistingCode,FirstName,DOJ,WeekOff," + Query_header + " from ( ";
        query = query + " Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff,DATENAME(dd, Attn_Date) as Day_V, ";
        query = query + " (Case when EM.DOJ > LD.Attn_Date then '' when Datename(weekday,LD.Attn_Date)=EM.WeekOff then (CASE WHEN LD.Present = 1.0 THEN 'WH/X' WHEN LD.Present = 0.5 THEN 'WH/H' else 'WH/A' End) ";
        query = query + " else (CASE WHEN LD.Present = 1.0 THEN 'X' WHEN LD.Present = 0.5 THEN 'H' else 'A' End) end) as Presents ";
        query = query + " from LogTime_Days LD inner join " + TableName + " EM on EM.LocCode=LD.LocCode And EM.CompCode=LD.CompCode And EM.ExistingCode=LD.ExistingCode ";
        query = query + " where LD.LocCode='" + SessionLcode + "' ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
        query = query + " And CONVERT(datetime,LD.Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103) ";
       
        if (WagesType != "-Select-")
        {
            query = query + " And EM.Wages='" + WagesType + "'";
            query = query + " And LD.Wages='" + WagesType + "'";
        }
       
        query = query + " And EM.LocCode='" + SessionLcode + "') as CV ";
        query = query + " pivot (max (Presents) for Day_V in (" + Query_Pivot + ")) as AvgIncomePerDay ";
        query = query + " order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            AutoDTable.NewRow();
            AutoDTable.Rows.Add();

            string MachineID = dt.Rows[i]["MachineID"].ToString();
            int DT_Row = AutoDTable.Rows.Count - 1;
            AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
            AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
            AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
            //AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["Emp.Code"] = dt.Rows[i]["ExistingCode"].ToString();
            AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();

            //string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
            AutoDTable.Rows[DT_Row]["WH"] = dt.Rows[i]["WeekOff"].ToString();

            string DOJ_Date_Str = "";
            if (dt.Rows[i]["DOJ"].ToString() != "")
            {
                DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
            }
            else
            {
                DOJ_Date_Str = "";
            }

            daycount = (int)((Date2 - date1).TotalDays);
            daysAdded = 0;

            while (daycount >= 0)
            {
                DateTime dayy = Convert.ToDateTime(date1.AddDays(daysAdded).ToShortDateString());
                if (DOJ_Date_Str != "")
                {
                    Query_Date_Check = Convert.ToDateTime(dayy.Date.ToString());
                    DOJ_Date_Check_Emp = Convert.ToDateTime(DOJ_Date_Str.ToString());
                    if (DOJ_Date_Check_Emp <= Query_Date_Check)
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "H" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/H" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH/A" + "</b></span>"; }
                    }
                    else
                    {
                        AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "";
                    }
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString();
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:black><b>" + "H" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "A" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/H") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/H" + "</b></span>"; }
                    if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/A") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:red><b>" + "WH/A" + "</b></span>"; }
                }

                daycount -= 1;
                daysAdded += 1;
                DataTable DT_Hrs = new DataTable();
                string OT_Hrs = "";
                string date_str = dayy.ToString("dd/MM/yyyy HH:mm:ss");
                string[] Split = date_str.Split(' ');
                OTDay = Split[0];
                OT_Hrs = "";




                SSQL = "";
                SSQL = "Select * from ["+ SessionEpay + "]..OT_Hrs_Mst where Attn_Date_Str='" + OTDay + "' and MachineID='" + MachineID + "'";
                DT_Hrs = objdata.RptEmployeeMultipleDetails(SSQL);
                if(DT_Hrs.Rows.Count !=0)
                {

                    OT_Hrs = DT_Hrs.Rows[0]["OT_Hrs"].ToString();
                }
                if (DT_Hrs.Rows.Count != 0)
                {
                    if (OT_Hrs != "0")
                    {


                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "X/" + OT_Hrs + "" + "</b></span>"; }
                        if (dt.Rows[i]["" + dayy.Day.ToString() + ""].ToString() == "WH/X") { AutoDTable.Rows[DT_Row]["" + dayy.Day.ToString() + ""] = "<span style=color:green><b>" + "WH/X/" + OT_Hrs + "" + "</b></span>"; }
                    }
                }
            }


            SSQL = "";
            SSQL = "Select * from Employee_Mst where MachineID ='" + MachineID + "'";
            DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);
         
            basesalary = DT_Emp.Rows[0]["BaseSalary"].ToString();
            DeptName = DT_Emp.Rows[0]["DeptName"].ToString();
            OneDay = (Convert.ToDecimal(basesalary) / 26).ToString();
            OneDay = (Math.Round(Convert.ToDecimal(OneDay), 0, MidpointRounding.AwayFromZero)).ToString();
            AutoDTable.Rows[DT_Row]["RatePerDay"] = OneDay;
            SSQL = "";
            SSQL = "Select * from ["+ SessionEpay + "]..SalaryDetails where MachineNo='" + dt.Rows[i]["ExistingCode"].ToString()+"'";
            SSQL = SSQL + " And LCode='" + SessionLcode + "'";
            SSQL = SSQL + " And (convert(datetime,FromDate, 105) >= convert(datetime, '" + FromDate + "', 105))";
            SSQL = SSQL + " And (convert(datetime,ToDate, 105) <= convert(datetime, '" + ToDate + "', 105))";
            dtSal = objdata.RptEmployeeMultipleDetails(SSQL);
            if(dtSal.Rows.Count !=0)
            {
                string mess1 = "";
                string TShirt = "";

                AutoDTable.Rows[DT_Row]["Amount"] = dtSal.Rows[0]["GrossEarnings"].ToString();
                salary= dtSal.Rows[0]["GrossEarnings"].ToString();
                mess1 = dtSal.Rows[0]["Deduction4"].ToString();
                TShirt= dtSal.Rows[0]["T_Shirt"].ToString();
                TotDed = (Convert.ToDecimal(mess1) + Convert.ToDecimal(TShirt)).ToString();
                AutoDTable.Rows[DT_Row]["Ded"] = TotDed;
                whDays = dtSal.Rows[0]["WH_Work_Days"].ToString();
                AutoDTable.Rows[DT_Row]["OTDays"] = whDays;

            }
            
                AutoDTable.Rows[DT_Row]["OT Amount"] = "0";
                 AutoDTable.Rows[DT_Row]["OTHrs"] = "0";


            SSQL = "";
            SSQL = "select MachineID,Oneday_Wages,Sum(Cast(Tot_OTHrs as int)) as Tot_OTHrs,Sum(cast(Amount as decimal(18,2))) Amount,Sum(Cast(WH as int))WH   from [Raajco_Epay]..OT_Amt where MachineID='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            SSQL = SSQL + " And LCode='" + SessionLcode + "'";
            SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) <= CONVERT(datetime,'" + ToDate + "',103) group by MachineID,Oneday_Wages";
            dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);
            if (dtAmt.Rows.Count != 0)
            {
              if (dtAmt.Rows[0]["Tot_OTHrs"].ToString() != "")
               {
                    AutoDTable.Rows[DT_Row]["OTHrs"] = dtAmt.Rows[0]["Tot_OTHrs"].ToString();
                }
                OTAdd = dtAmt.Rows[0]["Amount"].ToString();

                //OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();
            
            }

            if (whDays != "0")
            {
                WH_Amount = (Convert.ToDecimal(OneDay) * Convert.ToDecimal(whDays)).ToString();

            }
          else
           {
              WH_Amount = "0";
             }

        OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();

        AutoDTable.Rows[DT_Row]["OT Amount"] = OTAmount;

            query = "Select isnull(sum(present),0) as Total_Days from LogTime_Days where ExistingCode='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
            query = query + " And LocCode='" + SessionLcode + "'";
            query = query + " And CONVERT(datetime,Attn_Date,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
            query = query + " And CONVERT(datetime,Attn_Date,103) <= CONVERT(datetime,'" + ToDate + "',103)  ";
            dt1 = objdata.RptEmployeeMultipleDetails(query);
            if (dt1.Rows.Count != 0)
            {
              
                if (whDays != "0")
                {
                    TotDays = "0";
                       TotDays = dt1.Rows[0]["Total_Days"].ToString();

                    TotDays = (Convert.ToDecimal(TotDays) - Convert.ToDecimal(whDays)).ToString();
                    AutoDTable.Rows[DT_Row]["Total_Days"] = TotDays;
                }
                else
                {
                    AutoDTable.Rows[DT_Row]["Total_Days"] = dt1.Rows[0]["Total_Days"].ToString();
                }
            }
            else
            {

                AutoDTable.Rows[DT_Row]["Total_Days"] = "0";
            }

            TotAmount = (Convert.ToDecimal(salary) + Convert.ToDecimal(OTAmount)).ToString();
            AutoDTable.Rows[DT_Row]["Total"] = TotAmount;

            string Netpay1 = "0";
            NetPay = (Convert.ToDecimal(TotAmount) - Convert.ToDecimal(TotDed)).ToString();
            Netpay1 = (Convert.ToDecimal(TotAmount) - Convert.ToDecimal(TotDed)).ToString();
            AutoDTable.Rows[DT_Row]["Earned Amount"] = NetPay;
            NetPay = (Math.Round(Convert.ToDecimal(NetPay),0, MidpointRounding.AwayFromZero)).ToString();
            AutoDTable.Rows[DT_Row]["NetPay"] = NetPay;
            Round= (Convert.ToDecimal(NetPay) - Convert.ToDecimal(Netpay1)).ToString();
            AutoDTable.Rows[DT_Row]["R.Off"] = Round;
         

            whDays = "0";
             TotDays = "0";
            salary = "0";
            OTAmount = "0";
            TotAmount = "0";
            NetPay = "0";
            TotDed = "0";
            WH_Amount = "0";
            OneDay = "0";
            OTAdd = "0";

        }


        if (AutoDTable.Rows.Count != 0)
        {
            if (RptType != "Payslip_Pdf")
            {
                grid.DataSource = AutoDTable;
                grid.DataBind();
                string attachment = "attachment;filename=Weekly Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                grid.HeaderStyle.Font.Bold = true;
                System.IO.StringWriter stw = new System.IO.StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                grid.RenderControl(htextw);
                int row = daysAdded + 6;
                Response.Write("<table>");
                Response.Write("<tr Font-Bold='true' align='center'>");
                Response.Write("<td colspan=" + row + ">");
                Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PRIVATE LIMITED</a>");
                //Response.Write("--");
                //Response.Write("<a style=\"font-weight:bold\">" + Session["Lcode"].ToString() + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("<tr Font-Bold='true' align='Center'>");
                Response.Write("<td colspan=" + row + ">");
                Response.Write("<a style=\"font-weight:bold\">Voucher Weekly Payslip -  " + FromDate + " - " + ToDate + "</a>");

                Response.Write("</td>");
                Response.Write("</tr>");
                //Response.Write("<tr Font-Bold='true' align='center'>");
                //Response.Write("<td colspan='10'>");
                //Response.Write("<a style=\"font-weight:bold\"> FROM -" + FromDate + "</a>");
                //Response.Write("&nbsp;&nbsp;&nbsp;");
                //Response.Write("<a style=\"font-weight:bold\"> TO -" + ToDate + "</a>");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                Response.Write("</table>");
                Response.Write(stw.ToString());
                Response.End();
                Response.Clear();
            }
            else
            {

            }
        }
        else
        {
        }   
    }
}
