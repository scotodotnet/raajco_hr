﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Data.SqlClient;
using System.Web.UI;

public partial class ManualAttendanceReport : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Division;
    string Date1_str;
    string Date2_str;
    string Status;


    string SSQL = "";

    DataTable mDataSet = new DataTable();

    DataTable AutoDataTable = new DataTable();
    int shiftCount;
    DateTime fromdate;
    DateTime todate;
    int dayCount;
    System.DateTime iDate;
    int daysAdded = 0;
    DataTable Datacells = new DataTable();
    Boolean isPresent;
    string SessionUserType;
    int grand;
    DataTable mLocalDS = new DataTable();

    System.Web.UI.WebControls.DataGrid grid =
                            new System.Web.UI.WebControls.DataGrid();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("../Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Manual Attendance";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            //SessionUserType = Session["UserType"].ToString();
                 Division = Request.QueryString["Division"].ToString();
            Date1_str = Request.QueryString["FromDate"].ToString();
            Date2_str = Request.QueryString["ToDate"].ToString();
            Status = Request.QueryString["Status"].ToString();
            Fill_Manual_Day_Attd_Between_Dates();

            Manual_Attendance_writeAttend();

        }
    }
    public void Fill_Manual_Day_Attd_Between_Dates()
    {
        try
        {
            string TableName = "";

            if (Status == "Pending")
            {
                TableName = "Employee_Mst_New_Emp";
            }

            else
            {
                TableName = "Employee_Mst";
            }
            fromdate = Convert.ToDateTime(Date1_str);
            todate = Convert.ToDateTime(Date2_str);
            int dayCount = (int)((todate - fromdate).TotalDays);
            if (dayCount > 0)
            {
                //if (category == "STAFF")
                //{
                SSQL = "";
                SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
                SSQL = SSQL + " (EM.FirstName) as [FirstName] from " + TableName + " EM,ManAttn_Details MA where";
                SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
                SSQL = SSQL + " And EM.Compcode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "' And EM.IsActive='Yes'";
                SSQL = SSQL + " And MA.Compcode='" + SessionCcode + "' And MA.LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date1_str + "', 105)";
                SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date2_str + "', 105)";
             //   SSQL = SSQL + " And EM.EmpCatCode = '" + SessionDivision + "'";
                if (Division != "-Select-")
                {
                    SSQL = SSQL + " And EM.Division = '" + Division + "'";
                }
                if (SessionUserType == "2")
                {
                    SSQL = SSQL + " And EM.IsNonAdmin='1'";
                }
                SSQL = SSQL + " Order By  EM.MachineID";
                //}
                //else if (category == "LABOUR")
                //{
                //    SSQL = "";
                //    SSQL = "Select Distinct MA.EmpNo,EM.DeptName,EM.MachineID,EM.ExistingCode,";
                //    SSQL = SSQL + " (EM.FirstName + '.'+ EM.MiddleInitial) as [FirstName] from Employee_Mst EM,ManAttn_Details MA where";
                //    SSQL = SSQL + " EM.Compcode=MA.CompCode And EM.LocCode=MA.LocCode And EM.EmpNo=MA.EmpNo";
                //    SSQL = SSQL + " And EM.Compcode='" + CompanyCode.ToString() + "' And EM.LocCode='" + LocationCode.ToString() + "' And EM.IsActive='" + active + "'";
                //    SSQL = SSQL + " And MA.Compcode='" + CompanyCode.ToString() + "' And MA.LocCode='" + LocationCode.ToString() + "'";
                //    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) >= CONVERT(VARCHAR(10), '" + Date + "', 105)";
                //    SSQL = SSQL + " And CONVERT(VARCHAR(10), MA.AttnDate, 105) <= CONVERT(VARCHAR(10), '" + Date2 + "', 105)";
                //    SSQL = SSQL + " Order By EM.DeptName, EM.MachineID";
                //}


                DataTable dsEmployee = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dsEmployee.Rows.Count > 0)
                {
                 //   AutoDataTable.Columns.Add("EmpNo");
                    AutoDataTable.Columns.Add("DeptName");
                    AutoDataTable.Columns.Add("MachineID");
                    //AutoDataTable.Columns.Add("ExistingCode");
                    AutoDataTable.Columns.Add("Emp.Code");
                    AutoDataTable.Columns.Add("FirstName");

                    for (int i = 0; i < dsEmployee.Rows.Count; i++)
                    {
                        AutoDataTable.NewRow();
                        AutoDataTable.Rows.Add();

                  //      AutoDataTable.Rows[i]["EmpNo"] = dsEmployee.Rows[i]["EmpNo"];
                        AutoDataTable.Rows[i]["DeptName"] = dsEmployee.Rows[i]["DeptName"];
                        AutoDataTable.Rows[i]["MachineID"] = dsEmployee.Rows[i]["MachineID"];
                        AutoDataTable.Rows[i]["Emp.Code"] = dsEmployee.Rows[i]["ExistingCode"];
                        AutoDataTable.Rows[i]["FirstName"] = dsEmployee.Rows[i]["FirstName"];

                    }


                    SSQL = "";
                    SSQL = "select ShiftDesc,StartIN,StartIN_Days,EndIN,EndIn_Days,StartOut,StartOut_Days,EndOut,EndOut_Days";
                    SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And (ShiftDesc Like '%Shift%' Or ShiftDesc Like '%GENERAL%')";

                    mDataSet = null;
                    mDataSet = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mDataSet.Rows.Count > 0)
                    {

                        shiftCount = mDataSet.Rows.Count * 2;
                        long iColVal = 0;

                        fromdate = Convert.ToDateTime(Date1_str);
                        todate = Convert.ToDateTime(Date2_str);
                        dayCount = (int)((todate - fromdate).TotalDays);
                        iColVal = dayCount + 1;
                        iDate = Convert.ToDateTime(Date1_str.ToString());


                        //Datacells.Columns.Add("EmpNo");
                        Datacells.Columns.Add("MachineID");
                        Datacells.Columns.Add("Emp.Code");
                        Datacells.Columns.Add("DeptName");
                        Datacells.Columns.Add("FirstName");
                        do
                        {
                            DateTime dayy = Convert.ToDateTime(fromdate.AddDays(daysAdded).ToShortDateString());
                            AutoDataTable.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                            Datacells.Columns.Add(Convert.ToString(dayy.ToShortDateString()));
                            dayCount -= 1;
                            daysAdded += 1;
                            iDate = iDate.AddDays(1);
                            while (iDate > Convert.ToDateTime(Date2_str))
                            {
                                return;
                            }
                        }

                        while (true);
                    }
                }
            }


        }

        catch (Exception ex)
        {

        }
    }


    public void Manual_Attendance_writeAttend()
    {
        try
        {
            string Date_value_str1 = "";
            int intI = 1;
            int intK = 1;
            int intCol = 0;

            Datacells.Columns.Add("Total Days");

            for (int i = 0; i < AutoDataTable.Rows.Count; i++)
            {
                string Machine_ID_Str;
                string OT_Week_OFF_Machine_No;
                string Date_Value_Str;

                string Attn_Status;
                isPresent = false;

                Datacells.NewRow();
                Datacells.Rows.Add();
                Datacells.Rows[i]["MachineID"] = AutoDataTable.Rows[i]["MachineID"];
                Datacells.Rows[i]["Emp.Code"] = AutoDataTable.Rows[i]["Emp.Code"];
                //Datacells.Rows[i]["EmpNo"] = AutoDataTable.Rows[i]["EmpNo"];
                Datacells.Rows[i]["FirstName"] = AutoDataTable.Rows[i]["FirstName"];
                Datacells.Rows[i]["DeptName"] = AutoDataTable.Rows[i]["DeptName"];
                //Datacells.Rows[i]["MachineID"] = AutoDataTable.Rows[i]["MachineID"];
                
              




                OT_Week_OFF_Machine_No = AutoDataTable.Rows[i]["MachineID"].ToString();
                //Date_Value_Str = Datacells.Rows[3][intK].ToString();


                fromdate = Convert.ToDateTime(Date1_str);
                todate = Convert.ToDateTime(Date2_str);
                int dayCount3 = (int)((todate - fromdate).TotalDays);
                int daycol = 4;
                int counting = 0;
                int daysAdded = 0;

                while (dayCount3 >= 0)
                {
                    DateTime dayy = Convert.ToDateTime(fromdate.AddDays(daysAdded).ToShortDateString());

                    SSQL = "Select * from ManAttn_Details where Machine_No='" + OT_Week_OFF_Machine_No + "' And AttnDate ='" + dayy.ToShortDateString() + "' And CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";

                    mLocalDS = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mLocalDS.Rows.Count <= 0)
                    {
                        Attn_Status = "";
                        isPresent = true;
                    }
                    else
                    {

                        Attn_Status = mLocalDS.Rows[0]["AttnStatus"].ToString();
                        isPresent = true;
                    }

                    if (string.IsNullOrEmpty(Attn_Status))
                    {
                        Datacells.Rows[i][daycol] = "";
                    }
                    else if (Attn_Status == "H")
                    {


                        Datacells.Rows[i][daycol] = "<span style=color:Green><b>H</b></span>";


                    }
                    else
                    {
                        //  Datacells.Rows[i][daycol] = "<b><span style=color:red>" + Attn_Status + "</span></b>";

                        Datacells.Rows[i][daycol] = "<b>" + Attn_Status + "</b>";
                        counting += 1;
                    }

                    dayCount3 -= 1;
                    daysAdded += 1;
                    daycol += 1;
                }
                Datacells.Rows[i]["Total Days"] = counting;
            }

            int i1;
            int j;
            int daycol1 = 4;

            Datacells.NewRow();
            Datacells.Rows.Add();
            Datacells.Rows[Datacells.Rows.Count - 1][daycol1 - 1] = "<b>Grand Total</b>";

            for (i1 = 0; i1 < daysAdded; i1++)
            {
                grand = 0;
                for (j = 0; j < Datacells.Rows.Count - 1; j++)
                {
                    if (Datacells.Rows[j][daycol1] != "")
                    {
                        grand = grand + 1;
                    }
                }
                Datacells.Rows[Datacells.Rows.Count - 1][daycol1] = grand;
                daycol1 += 1;
            }





        }



        catch (Exception ex)
        {

        }





        grid.DataSource = Datacells;
        grid.DataBind();
        string attachment = "attachment;filename=MANUAL ATTENDANCE BETWEEN DATES.xls";
        Response.ClearContent();
        Response.AddHeader("content-disposition", attachment);
        Response.ContentType = "application/ms-excel";
        grid.HeaderStyle.Font.Bold = true;
        System.IO.StringWriter stw = new System.IO.StringWriter();
        HtmlTextWriter htextw = new HtmlTextWriter(stw);
        grid.RenderControl(htextw);

        Response.Write("<table>");
        Response.Write("<tr Font-Bold='true' align='Center'>");
        Response.Write("<td colspan='10'>");
            Response.Write("<a style=\"font-weight:bold\">RAAJCO SPINNERS PENDING EMPLOYEE LIST </a>");
        Response.Write("</td>");
        Response.Write("</tr>");
        Response.Write("<tr Font-Bold='true' align='Center'>");
        Response.Write("<td colspan='10'>");
        Response.Write("<a style=\"font-weight:bold\">MANUAL ATTENDANCE BETWEEN DATES &nbsp;&nbsp;&nbsp; -&nbsp;&nbsp;&nbsp;" + Date1_str + "&nbsp;&nbsp;&nbsp;TO&nbsp;&nbsp;&nbsp;" + Date2_str + "</a>");

        Response.Write("</td>");

        //Response.Write("</tr>");
        //Response.Write("<tr Font-Bold='true' align='center'>");
        //Response.Write("<td colspan='10'>");
        //Response.Write("<a style=\"font-weight:bold\"> FROM -" + Date1_str + "</a>");
        //Response.Write("&nbsp;&nbsp;&nbsp;");
        //Response.Write("<a style=\"font-weight:bold\"> TO -" + Date2_str + "</a>");
        //Response.Write("</td>");
        //Response.Write("</tr>");
        Response.Write("</table>");
        Response.Write(stw.ToString());
        Response.End();
        Response.Clear();





    }

}
