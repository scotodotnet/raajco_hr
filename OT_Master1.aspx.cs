﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class OT_Master1 : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string Query = "";
    string basic = "0";
    string HRA = "0";
    string Conv = "0";
    string Spl = "0";
    string WH = "0";
    string Medical = "0";
    string Edu = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        string ss = Session["UserId"].ToString();
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        if (!IsPostBack)
        {
            Load_Data();
        }
    }

    protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        clearcheck();
        DataTable dtdsupp = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        ddlEmployeeType_SelectedIndexChanged(sender, e);
        Load_Data();
    }
    protected void ddlEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        clearcheck();
        Load_Data();

    }
    protected void btnSave_Click1(object sender, EventArgs e)
    {
        string Errflag = "False";


        if ((chkbasic.Checked == false) && (ChkHRA.Checked == false) && (ChkConv.Checked == false) && (ChkEdu.Checked == false) && (ChkMedi.Checked == false) && (ChkSpl.Checked == false) && (chkWH.Checked == false))
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select any Value...');", true);
        }
        else
        {

        }
        if (ddlCategory.SelectedItem.Text == "-Select-")
        {
            Errflag = "true";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Please Select Category...');", true);
        }
        //if ((ddlEmployeeType.SelectedItem.Text == "-Select-") || (ddlEmployeeType.SelectedItem.Text == ""))
        //{
        //    Errflag = "true";
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
        //}

        if (Errflag != "true")
        {
            try
            {
                String MsgFlag = "Insert";
                DataTable dt = new DataTable();
                Query = "select * from [Raajco_Epay]..OT_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(Query);
                if (dt.Rows.Count > 0)
                {
                    Query = "delete from [Raajco_Epay]..OT_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(Query);
                    MsgFlag = "Update";
                }
                CheckedData();

                Query = "";
                Query = Query + "Insert into [Raajco_Epay]..OT_Mst(Ccode,Lcode,Category,EmployeeType,BasicAndDA,HRA,ConvAllow,EduAllow,MediAllow,";
                Query = Query + "WashingAllow,SplAllowance)Values('" + SessionCcode + "','" + SessionLcode + "',";
                Query = Query + "'" + ddlCategory.SelectedValue + "','" + ddlEmployeeType.SelectedValue + "','" + basic + "','" + HRA + "','" + Conv + "','" + Edu + "','" + Medical + "',";
                Query = Query + "'" + WH + "','" + Spl + "')";
                objdata.RptEmployeeMultipleDetails(Query);
                Clear();
                Load_Data();
                if (MsgFlag == "Insert")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully');", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Update Successfully');", true);
                }
            }
            catch (Exception Ex)
            {

            }
        }
    }
    private void CheckedData()

    {
        if (chkbasic.Checked == true)
        {
            basic = "1";
        }

        else
        {
            basic = "0";
        }
        if (ChkHRA.Checked == true)
        {
            HRA = "1";
        }
        else
        {
            HRA = "0";
        }
        if (ChkConv.Checked == true)
        {
            Conv = "1";
        }
        if (ChkSpl.Checked == true)
        {
            Conv = "1";
        }
        else
        {
            Spl = "0";
        }
        if (ChkEdu.Checked == true)
        {
            Edu = "1";
        }
        else
        {
            Edu = "0";
        }
        if (ChkMedi.Checked == true)
        {
            Medical = "1";
        }
        else
        {
            Medical = "0";
        }

        if (chkWH.Checked == true)
        {
            WH = "1";
        }
        else
        {
            WH = "0";
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        Clear();
    }
    public void Clear()
    {
        chkbasic.Checked = false;
        ChkHRA.Checked = false;
        ChkConv.Checked = false;
        ChkSpl.Checked = false;
        ChkEdu.Checked = false;
        ChkMedi.Checked = false;
        chkWH.Checked = false;
        ddlCategory.SelectedValue = "0";
        //ddlCategory_SelectedIndexChanged(sender, e);


        loadEmp();
        //ddlEmployeeType.SelectedValue = "";
    }
    private void loadEmp()
    {
        DataTable dtdsupp1 = new DataTable();
        ddlEmployeeType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + ddlCategory.SelectedValue + "'";
        dtdsupp1 = objdata.RptEmployeeMultipleDetails(Query);
        ddlEmployeeType.DataSource = dtdsupp1;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpType"] = "-Select-";
        //dr["EmpTypeCd"] = "-Select-";
        //dtdsupp.Rows.InsertAt(dr, 0);
        ddlEmployeeType.DataTextField = "EmpType";
        ddlEmployeeType.DataValueField = "EmpTypeCd";
        ddlEmployeeType.DataBind();
        //ddlEmployeeType_SelectedIndexChanged(sender, e);


    }
    private void clearcheck()
    {
        chkbasic.Checked = false;
        ChkHRA.Checked = false;
        ChkConv.Checked = false;
        ChkSpl.Checked = false;
        ChkEdu.Checked = false;
        ChkMedi.Checked = false;
        chkWH.Checked = false;


    }
    public void Load_Data()
    {
        DataTable dt1 = new DataTable();
        Query = "select * from [Raajco_Epay]..OT_Mst where Category='" + ddlCategory.SelectedValue + "' and EmployeeType='" + ddlEmployeeType.SelectedValue + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        dt1 = objdata.RptEmployeeMultipleDetails(Query);
        if (dt1.Rows.Count > 0)
        {
            if (dt1.Rows[0]["BasicAndDA"].ToString() == "1")
            {
                chkbasic.Checked = true;
            }
            if (dt1.Rows[0]["HRA"].ToString() == "1")
            {
                ChkHRA.Checked = true;
            }
            if (dt1.Rows[0]["ConvAllow"].ToString() == "1")
            {
                ChkConv.Checked = true;
            }
            if (dt1.Rows[0]["EduAllow"].ToString() == "1")
            {
                ChkEdu.Checked = true;
            }

            if (dt1.Rows[0]["MediAllow"].ToString() == "1")
            {
                ChkMedi.Checked = true;
            }
            if (dt1.Rows[0]["WashingAllow"].ToString() == "1")
            {
                chkWH.Checked = true;
            }



        }

    }


}