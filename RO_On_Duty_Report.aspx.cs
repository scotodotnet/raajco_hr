﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.IO;
using System.Data.SqlClient;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.Collections.Generic; 

public partial class RO_On_Duty_Report : System.Web.UI.Page
{
    string SessionLcode;
    string SessionCcode;
    string SSQL;
    string SessionAdmin;
    string SessionUserType;
    BALDataAccess objdata = new BALDataAccess();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();

            SessionUserType = Session["Isadmin"].ToString();
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | RO ON DUTY REPORT";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("SalaryProcess"));
                //li.Attributes.Add("class", "droplink active open");

                Load_RO_Name();
            }



        }
    }

    public void Load_RO_Name()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtRO_Name.Items.Clear();
        query = "Select ROName from MstRecruitOfficer";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtRO_Name.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ROName"] = "-Select-";
        dr["ROName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtRO_Name.DataTextField = "ROName";
        txtRO_Name.DataValueField = "ROName";
        txtRO_Name.DataBind();
    }
    
    protected void btnReport_Click(object sender, EventArgs e)
    {
        string query = "";
        string Cmpaddress = "";
        string CmpName = "";
        bool ErrFlag = false;
        DataTable DT_RO = new DataTable();

        if (txtRO_Name.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the RO NAME...');", true);
            ErrFlag = true;
        }
        if (txtFromDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the From Date...');", true);
            ErrFlag = true;
        }
        if (txtToDate.Text == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the To Date...');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            query = "Select ROW_NUMBER() OVER(ORDER BY Start_Date ASC) AS Row_No,Start_Date_Str,End_Date_Str,No_Of_Days,Place_Of_Visit,Vehicle_No,Driver_Name,Tot_KM_Run,Own_Fuel_Consumed,OutSide_Fuel_Consumed,";
            query = query + " Own_Fuel_Cost,OutSide_Fuel_Cost,Boarding_Lodging_Cost,Toll_Gate_Fee,Agent_Expenses,Agent_Name,Agent_Mobile_No,Total_Expenses";
            query = query + " from RO_OnDuty_Expenses where Ccode='" + SessionCcode + "' And Lcode='" + SessionLcode + "' And RO_Name='" + txtRO_Name.SelectedValue + "'";
            query = query + " And CONVERT(Datetime,Start_Date,103) >=CONVERT(datetime,'" + txtFromDate.Text + "',103)";
            query = query + " And CONVERT(Datetime,Start_Date,103) <=CONVERT(datetime,'" + txtToDate.Text + "',103)";
            query = query + " And CONVERT(Datetime,End_Date,103) >=CONVERT(datetime,'" + txtFromDate.Text + "',103)";
            query = query + " And CONVERT(Datetime,End_Date,103) <=CONVERT(datetime,'" + txtToDate.Text + "',103)";
            DT_RO = objdata.RptEmployeeMultipleDetails(query);

            if (DT_RO.Rows.Count != 0)
            {
                string attachment = "attachment;filename=RO_On_Duty_Report.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [Raajco_Epay]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                //dt = objdata.Company_retrive(SessionCcode, SessionLcode);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);
                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='18' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write(CmpName + " - " + SessionLcode);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='18' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write("WORKERS RECRUITMENT EXPENSES FOR THE PERIOD FROM " + txtFromDate.Text + " TO " + txtToDate.Text);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='18' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write("RO NAME : " + txtRO_Name.Text.ToString().ToUpper());
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("</table>");

                //Heading Add
                Response.Write("<table border='1' style='font-weight:bold;'><tr><td>S.No</td><td>Starting Date</td><td>Ending Date</td><td>No.Of.Days</td><td>Place Of Visit</td>");
                Response.Write("<td>Vehcile No</td><td>Driver Name</td><td>Total Kilometers Run</td>");
                Response.Write("<td>Own Fuel Consumed(Ltrs)</td><td>OutSide Fuel Consumed(Ltrs)</td><td>Fuel Cost Own</td>");
                Response.Write("<td>Fuel Cost OutSide</td><td>Boarding/Lodging Expenses</td><td>Toll Gate Fee</td>");
                Response.Write("<td>Agent Expenses</td><td>Agent Name</td><td>Agent Mobile No</td>");
                Response.Write("<td>Total Expenses</td></tr></table>");


                for (int i = 0; i < DT_RO.Rows.Count; i++)
                {
                    Response.Write("<table border='1'><tr>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Row_No"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Start_Date_Str"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["End_Date_Str"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["No_Of_Days"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Place_Of_Visit"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Vehicle_No"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Driver_Name"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Tot_KM_Run"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Own_Fuel_Consumed"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["OutSide_Fuel_Consumed"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Own_Fuel_Cost"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["OutSide_Fuel_Cost"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Boarding_Lodging_Cost"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Toll_Gate_Fee"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Agent_Expenses"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Agent_Name"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Agent_Mobile_No"].ToString() + "</td>");
                    Response.Write("<td>" + DT_RO.Rows[i]["Total_Expenses"].ToString() + "</td>");
                    Response.Write("</tr></table>");
                }

                Response.Write("<table><tr><td colspan='18'></td></tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='18' style='font-size:11.0pt;font-weight:bold;text-decoration:underline;''>");
                Response.Write("WORKER RECRUITMENT MADE FOR THE PERIOD FROM " + txtFromDate.Text + " TO " + txtToDate.Text);
                Response.Write("</td>");
                Response.Write("</tr></table>");

                //Heading Add
                Response.Write("<table border='1' style='font-weight:bold;'><tr>");
                Response.Write("<td>S.No</td><td>Emp.No</td><td>Token No</td><td>Name</td><td>DOB</td><td>Age</td><td>Qualification</td>");
                Response.Write("<td>Address1</td><td>DOJ</td><td>Department</td><td>Designation</td><td>Agent/Parent Name</td><td>Commission Paid to Whom</td><td>Commission Amount</td>");
                Response.Write("<td>Date of Follow up</td><td>Employee Working Status</td><td>DOR</td><td>Effective Working Days</td>");
                Response.Write("</tr></table>");

                query = "Select EM.MachineID,EM.ExistingCode,EM.FirstName,Convert(varchar(20),EM.BirthDate,105) as DOB,EM.Age,EM.Qualification,EM.Address1,";
                query = query + " Convert(varchar(20),EM.DOJ,105) as DOJ,EM.DeptName,EM.Designation,EM.AgentName,EM.IsActive,EM.DOR,'0' W_Days";
                query = query + " from Employee_Mst EM inner join RO_OnDuty_Expenses RO On EM.CompCode=RO.Ccode And EM.LocCode=RO.Lcode";
                query = query + " And EM.RecuriterName=RO.RO_Name where EM.CompCode='" + SessionCcode + "' And EM.LocCode='" + SessionLcode + "'";
                query = query + " And CONVERT(Datetime,EM.DOJ,103) >=CONVERT(datetime,'" + txtFromDate.Text + "',103)";
                query = query + " And CONVERT(Datetime,EM.DOJ,103) <=CONVERT(datetime,'" + txtToDate.Text + "',103) And EM.RecuriterName='" + txtRO_Name.SelectedValue + "'";
                query = query + " And RO.Ccode='" + SessionCcode + "' And RO.Lcode='" + SessionLcode + "' And RO.RO_Name='" + txtRO_Name.SelectedValue + "'";
                query = query + " And CONVERT(Datetime,RO.Start_Date,103) >=CONVERT(datetime,'" + txtFromDate.Text + "',103)";
                query = query + " And CONVERT(Datetime,RO.Start_Date,103) <=CONVERT(datetime,'" + txtToDate.Text + "',103)";
                query = query + " And CONVERT(Datetime,RO.End_Date,103) >=CONVERT(datetime,'" + txtFromDate.Text + "',103)";
                query = query + " And CONVERT(Datetime,RO.End_Date,103) <=CONVERT(datetime,'" + txtToDate.Text + "',103)";
                DataTable DT_EM = new DataTable();
                DT_EM = objdata.RptEmployeeMultipleDetails(query);
                if (DT_EM.Rows.Count != 0)
                {
                    for (int i = 0; i < DT_EM.Rows.Count; i++)
                    {
                        Response.Write("<table border='1'><tr>");
                        Response.Write("<td>" + (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["MachineID"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["ExistingCode"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["FirstName"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["DOB"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["Age"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["Qualification"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["Address1"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["DOJ"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["DeptName"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["Designation"].ToString() + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["AgentName"].ToString() + "</td>");
                        Response.Write("<td></td>");
                        Response.Write("<td></td>");
                        Response.Write("<td></td>");
                        string Emp_Status_Str = "No";
                        if (DT_EM.Rows[i]["IsActive"].ToString().ToUpper() == "YES")
                        {
                            Emp_Status_Str = "ACTIVE";
                        }
                        else
                        {
                            Emp_Status_Str = "LEFT";
                        }
                        Response.Write("<td>" + Emp_Status_Str + "</td>");
                        Response.Write("<td>" + DT_EM.Rows[i]["DOR"].ToString() + "</td>");
                        
                        //Get Working Days
                        string Working_Days_Str = "0";
                        query = "Select isnull(sum(Present),0) as W_Days from LogTime_Days where CompCode='" + SessionCcode + "'";
                        query = query + " And LocCode='" + SessionLcode + "' And ExistingCode='" + DT_EM.Rows[i]["ExistingCode"].ToString() + "'";
                        query = query + " And CONVERT(Datetime,Attn_Date,103) >=CONVERT(datetime,'" + txtFromDate.Text + "',103)";
                        query = query + " And CONVERT(Datetime,Attn_Date,103) <=CONVERT(datetime,'" + txtToDate.Text + "',103)";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        if (dt.Rows.Count != 0)
                        {
                            Working_Days_Str = dt.Rows[0]["W_Days"].ToString();
                        }
                        else
                        {
                            Working_Days_Str = "0";
                        }

                        Response.Write("<td>" + Working_Days_Str + "</td>");
                        Response.Write("</tr></table>");
                    }
                }

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);
            }
        }

    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Verifies that the control is rendered */
    }
}
