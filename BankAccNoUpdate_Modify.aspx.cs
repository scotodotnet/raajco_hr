﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class BankAccNoUpdate_Modify : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] iStr3;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Bank Account Number Modify";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            //Initial_Data_Referesh();

            //Load_WagesType();
            //Load_Data_EmpDet();
            Load_Bank();
            Load_Bank_Branch();
        }
        Load_Data();
    }

    private void Load_Bank()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlBankName.Items.Clear();
        query = "Select Distinct BankName from MstBank";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataValueField = "BankName";
        ddlBankName.DataBind();

        //query = "Select *from MstBank where Default_Bank='Yes'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count != 0)
        //{
        //    ddlBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
        //    txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
        //    txtBranch.Text = DT.Rows[0]["Branch"].ToString();
        //}
        //else
        //{
        //    ddlBankName.SelectedValue = "-Select-";
        //    txtIFSC.Text = "";
        //    txtBranch.Text = "";
        //}
    }

    private void Load_Bank_Branch()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtBranch.Items.Clear();
        query = "Select Branch  from MstBank where BankName='" + ddlBankName.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBranch.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Branch"] = "-Select-";
        dr["Branch"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBranch.DataTextField = "Branch";
        txtBranch.DataValueField = "Branch";
        txtBranch.DataBind();

    }

    protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Bank_Branch();
    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select * from BankAccountUpdate where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " And MachineID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);


        if (DT.Rows.Count != 0)
        {
            ddlWages.Text = DT.Rows[0]["Wages"].ToString();
            txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
            //Load_Data_EmpDet();
            txtMachineID.Text = DT.Rows[0]["MachineID"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
            ddlBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
            Load_Bank_Branch();
            txtBranch.SelectedValue = DT.Rows[0]["BranchCode"].ToString();
            txtAccNo.Text = DT.Rows[0]["AccountNo"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSC_Code"].ToString();
        }

        //Session.Remove("MachineID_Apprv");
        //Session["MachineID"] = e.CommandName.ToString();
        //Response.Redirect("EmployeeDetails.aspx");
    }

    public void Load_Data()
    {
        DataTable dtDisplay = new DataTable();
        string IsActive = "";
        string EmpStatus = "";
        string Query = "";


        Query = "select * from BankAccountUpdate where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And Approve_Status='1'";
        Query = Query + " Order by ExistingCode Asc";

        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

        //dtdDisplay = objdata.EmployeeDisplay(SessionCcode,SessionLcode);
        //if (dtDisplay.Rows.Count > 0)
        //{
        Repeater1.DataSource = dtDisplay;
        Repeater1.DataBind();
        //}
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        bool ErrFlag = false;
        DataTable DT_Check = new DataTable();

        if (ddlBankName.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the BankName..!');", true);
        }

        if (txtBranch.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the Branch Name..!');", true);
        }
        if (txtAccNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Account No..!');", true);
        }

        if (txtTokenNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Token No..!');", true);
        }
        if (txtMachineID.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Machine ID..!');", true);
        }


        if (!ErrFlag)
        {
            string Approve_Status = "0";
            //Check Token No
            SSQL = "";
            SSQL = "Select * from BankAccountUpdate Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID = '" + txtMachineID.Text + "' And ExistingCode = '" + txtTokenNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                SSQL = "Delete from BankAccountUpdate Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                SSQL = SSQL + " And MachineID = '" + txtMachineID.Text + "' And ExistingCode = '" + txtTokenNo.Text + "'";
                objdata.RptEmployeeMultipleDetails(SSQL);

            }

            SSQL = "";
            SSQL = "Insert into BankAccountUpdate(CompCode,LocCode,EmpNo,ExistingCode,MachineID,FirstName,DeptName,";
            SSQL = SSQL + "Designation,BankName,BranchCode,AccountNo,Wages,IFSC_Code,Approve_Status) Values ( ";
            SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.Text + "',";
            SSQL = SSQL + "'" + txtTokenNo.Text + "','" + txtMachineID.Text + "','" + txtEmpName.Text + "',";
            SSQL = SSQL + "'" + txtDeptName.Text + "','" + txtDesignation.Text + "','" + ddlBankName.SelectedItem.Text + "',";
            SSQL = SSQL + "'" + txtBranch.SelectedValue + "','" + txtAccNo.Text + "','" + ddlWages.Text + "','" + txtIFSC.Text + "','1')";
            objdata.RptEmployeeMultipleDetails(SSQL);


            SSQL = "Update Employee_Mst Set BankName='" + ddlBankName.SelectedItem.Text + "',BranchCode='" + txtBranch.SelectedValue + "',";
            SSQL = SSQL + "AccountNo='" + txtAccNo.Text + "',IFSC_Code='" + txtIFSC.Text + "'";
            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And Wages='" + ddlWages.Text + "' And EmpNo='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            SSQL = "Update Employee_Mst_New_Emp Set BankName='" + ddlBankName.SelectedItem.Text + "',BranchCode='" + txtBranch.SelectedValue + "',";
            SSQL = SSQL + "AccountNo='" + txtAccNo.Text + "',IFSC_Code='" + txtIFSC.Text + "'";
            SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And Wages='" + ddlWages.Text + "' And EmpNo='" + txtMachineID.Text + "'";
            objdata.RptEmployeeMultipleDetails(SSQL);

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Details Modify Successfully...');", true);

            Load_Data();
            btnClear_Click(sender, e);
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        ddlWages.Text = "";
        txtTokenNo.Text = "";
        txtMachineID.Text = "";
        txtEmpName.Text = "";
        txtDeptName.Text = "";
        txtDesignation.Text = "";
        Load_Bank();
        Load_Bank_Branch();
        txtIFSC.Text = "";
        txtAccNo.Text = "";

        Load_Data();
    }

    protected void txtBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select * from MstBank where BankName='" + ddlBankName.SelectedItem.Text + "' And Branch='" + txtBranch.SelectedValue + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            txtIFSC.Text = dt.Rows[0]["IFSCCode"].ToString();
            //txtBranch.Text = dt.Rows[0]["Branch"].ToString();
        }
    }
}
