﻿using System;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

using System.Security.Cryptography;
using System.Collections;
using System.Collections.Specialized;
using System.Text;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;

public partial class _Default : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    System.Globalization.CultureInfo culterInfo = new System.Globalization.CultureInfo("en-GB");
    BALDataAccess objdata = new BALDataAccess();
    string SessionCMS;
    string SessionRights;
    string SessionEpay;
    string SSQL = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Remove("UserId");
        Session.Remove("RoleCode");
        Session.Remove("Isadmin");
        Session.Remove("Usernmdisplay");
        Session.Remove("Ccode");
        Session.Remove("Lcode");
        Load_DB();
        if (!IsPostBack)
        {
            ErrorDisplay.Visible = false;
            Load_company();
            ddlCode_SelectedIndexChanged(sender, e);

            //Module_Login_Table(sender, e);
        }
    }

    public void Load_company()
    {

        DataTable dt = new DataTable();
        SSQL = "";
        SSQL = "Select CompCode as Ccode,CompCode + ' - ' + CompName as Cname From Company_Mst";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlCode.DataSource = dt;

        ddlCode.DataTextField = "Cname";
        ddlCode.DataValueField = "Ccode";
        ddlCode.DataBind();
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Raajco_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionCMS = dt_DB.Rows[0]["CMS"].ToString();
            SessionRights = dt_DB.Rows[0]["Rights"].ToString();
            SessionEpay = dt_DB.Rows[0]["Payroll"].ToString();
        }
    }
    protected void ddlCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtempty = new DataTable();
        ddlLocation.DataSource = dtempty;
        ddlLocation.DataBind();
        DataTable dt = new DataTable();
        SSQL = "Select LocCode as LCode,LocCode + ' - ' + LocName as Location from Location_Mst where CompCode='" + ddlCode.SelectedValue + "'";
        dt = objdata.RptEmployeeMultipleDetails(SSQL);
        ddlLocation.DataSource = dt;

        ddlLocation.DataTextField = "Location";
        ddlLocation.DataValueField = "LCode";
        ddlLocation.DataBind();

    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Session["SessionEpay"] = SessionEpay;
        string query = "";
        bool ErrFlag = false;
        ErrorDisplay.Visible = false;
        //string Verification = objdata.Verification_verify();
        UserRegistrationClass objuser = new UserRegistrationClass();
        if (txtusername.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
            ErrFlag = true;
        }
        else if (txtpassword.Text.Trim() == "")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt_v = new DataTable();
            Session["Rights"] = SessionRights;
            Session["SessionEpay"] = SessionEpay;

            if (txtusername.Text.Trim() == "Scoto")
            {
                Session["UserId"] = txtusername.Text.Trim();
                objuser.UserCode = txtusername.Text.Trim();
                objuser.Password = UTF8Encryption(txtpassword.Text.Trim());
                string pwd = UTF8Encryption(txtpassword.Text);
                DataTable dt = new DataTable();
                query = "Select UserCode,UserName,Password,IsAdmin,LocationCode,CompCode,FormID,FormName from [Raajco_Rights]..MstUsers where UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                //dt = objdata.AltiusLogin(objuser);
                if (dt.Rows.Count > 0)
                {

                    if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd))
                    {
                        Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                        Session["Usernmdisplay"] = txtusername.Text.Trim();
                        Session["Ccode"] = ddlCode.SelectedValue;
                        Session["Lcode"] = ddlLocation.SelectedValue;

                        //string formID = dt.Rows[0]["FormID"].ToString().Trim();
                        //string formName = dt.Rows[0]["FormName"].ToString().Trim();

                        //Session["FinYearCode"] = ddlFinYear.SelectedValue;
                        //Session["FinYear"] = ddlFinYear.SelectedItem.Text.ToString();

                        if (Session["Isadmin"].ToString() == "1")
                        {
                            Session["RoleCode"] = "1";
                        }
                        else
                        {
                            Session["RoleCode"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                        }

                        Response.Redirect("AttendDashBoard.aspx");

                    }
                    else
                    {
                        ErrorDisplay.Visible = true;
                        txtusername.Focus();
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                    }
                }
            }
            else
            {
                //    string date_1 = "";
                //    string dtserver = "";

                //    date_1 = (02 + "-" + 04 + "-" + 2020).ToString();


                //    string constr = ConfigurationManager.AppSettings["ConnectionString"];
                //    SqlConnection con = new SqlConnection(constr);
                //    string Query = "";

                //    con.Open();
                //    Query = "Select convert(varchar,getdate(),105) as ServerDate";
                //    SqlCommand cmd_Server_Date = new SqlCommand(Query, con);
                //    dtserver = (cmd_Server_Date.ExecuteScalar()).ToString();


                //    con.Close();


                string date_1 = "";
                string dtserver = "";
                string Entrypoint = "";

                Load_CheckerV1("ScotoAdminpnt");

                date_1 = (15 + "-" + 07 + "-" + 2021).ToString();

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);
                string Query = "";

                con.Open();

                Query = "Select convert(varchar,getdate(),103) as ServerDate";
                SqlCommand cmd_Server_Date = new SqlCommand(Query, con);
                dtserver = (cmd_Server_Date.ExecuteScalar()).ToString();

                con.Close();

                if (Convert.ToDateTime(dtserver) < Convert.ToDateTime(date_1))

                {
                    if (txtusername.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the User Name ');", true);
                        ErrFlag = true;
                    }
                    else if (txtpassword.Text.Trim() == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Enter the Password ');", true);
                        ErrFlag = true;
                    }
                    else if (ddlLocation.SelectedValue == "")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('Select the Location ');", true);
                        ErrFlag = true;
                    }

                    if (!ErrFlag)
                    {
                        Session["UserId"] = txtusername.Text.Trim();
                        objuser.UserCode = txtusername.Text.Trim();
                        objuser.Password = UTF8Encryption(txtpassword.Text.Trim());
                        string pwd = UTF8Encryption(txtpassword.Text);
                        objuser.Ccode = ddlCode.SelectedValue;
                        objuser.Lcode = ddlLocation.SelectedValue;
                        DataTable dt = new DataTable();

                        query = "Select UserCode,UserName,Password,IsAdmin,LocationCode,CompCode,FormID,FormName,Link_Url from [Raajco_Rights]..MstUsers where UserCode='" + objuser.UserCode + "' and Password='" + objuser.Password + "'";
                        query = query + " And CompCode='" + objuser.Ccode + "' And LocationCode='" + objuser.Lcode + "'";
                        dt = objdata.RptEmployeeMultipleDetails(query);
                        //dt = objdata.UserLogin(objuser);
                        if (dt.Rows.Count > 0)
                        {
                            if ((dt.Rows[0]["UserCode"].ToString().Trim() == txtusername.Text.Trim()) && (dt.Rows[0]["Password"].ToString().Trim() == pwd) && (dt.Rows[0]["CompCode"].ToString().Trim() == ddlCode.SelectedValue) && (dt.Rows[0]["LocationCode"].ToString().Trim() == ddlLocation.SelectedValue))
                            {
                                Session["Isadmin"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                Session["Usernmdisplay"] = txtusername.Text.Trim();
                                Session["Ccode"] = dt.Rows[0]["CompCode"].ToString().Trim();
                                Session["Lcode"] = dt.Rows[0]["LocationCode"].ToString().Trim();

                                DataTable dt_Cat = new DataTable();
                                SSQL = "";
                                SSQL = SSQL + "Select Cname,Location,Address1,Address2,Location,Pincode from [" + Session["SessionEpay"] + "]..AdminRights where Ccode='" + objuser.Ccode + "' and LCode='" + objuser.Lcode + "'";
                                dt_Cat = objdata.RptEmployeeMultipleDetails(SSQL);
                                string CmpName = "";
                                string Cmpaddress = "";
                                if (dt_Cat.Rows.Count > 0)
                                {
                                    CmpName = dt_Cat.Rows[0]["Cname"].ToString();
                                    Session["CompanyName"] = CmpName.ToString();
                                    Session["LocationName"] = dt_Cat.Rows[0]["Location"].ToString();
                                }


                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Session["RoleCode"] = "1";
                                }
                                else
                                {
                                    Session["RoleCode"] = dt.Rows[0]["IsAdmin"].ToString().Trim();
                                }

                                //get User After Login Default Page Load link Start
                                if (Session["Isadmin"].ToString() == "1")
                                {
                                    Response.Redirect("AttendDashBoard.aspx");
                                }
                                else if (Session["Isadmin"].ToString() == "5")
                                {
                                    Response.Redirect("MedicalDetails.aspx");
                                }
                                else if (Session["Isadmin"].ToString() == "4")
                                {
                                    Response.Redirect("MDDashBoard.aspx");
                                }
                                else
                                {
                                    string link_Url = "AttendDashBoard.aspx";
                                    //get User After Login Default Page Load link
                                    DataTable dt1 = new DataTable();
                                    DataTable dt2 = new DataTable();
                                    query = "select * from [Raajco_Rights]..Company_user_default_page where CompCode='" + objuser.Ccode + "' And LocCode='" + objuser.Lcode + "' And UserName='" + objuser.UserCode + "'";
                                    query = query + " And ModuleName='Stores'";
                                    dt1 = objdata.RptEmployeeMultipleDetails(query);

                                    if (dt1.Rows.Count > 0)
                                    {
                                        //Get Rights Check
                                        if (dt1.Rows[0]["FormLink"].ToString() != "")
                                        {
                                            query = "Select * from [Raajco_Rights]..Company_Module_User_Rights where CompCode='" + objuser.Ccode + "' And LocCode='" + objuser.Lcode + "'";
                                            query = query + " And ModuleName='Stores' And MenuName='" + dt1.Rows[0]["MenuName"].ToString() + "' And FormName='" + dt1.Rows[0]["FormName"].ToString() + "'";
                                            dt2 = objdata.RptEmployeeMultipleDetails(query);
                                            if (dt2.Rows.Count != 0)
                                            {
                                                if (dt2.Rows[0]["AddRights"].ToString() == "1" || dt2.Rows[0]["ModifyRights"].ToString() == "1" || dt2.Rows[0]["DeleteRights"].ToString() == "1" || dt2.Rows[0]["ViewRights"].ToString() == "1" || dt2.Rows[0]["ApproveRights"].ToString() == "1" || dt2.Rows[0]["PrintRights"].ToString() == "1")
                                                {
                                                    link_Url = dt1.Rows[0]["FormLink"].ToString();
                                                    Response.Redirect("" + link_Url + "");
                                                }
                                                else
                                                {
                                                    if (Session["Isadmin"].ToString() == "4")
                                                    {
                                                        Response.Redirect("MDDashBoard.aspx");
                                                    }
                                                    else if (Session["Isadmin"].ToString() == "5")
                                                    {
                                                        Response.Redirect("MedicalDetails.aspx");
                                                    }
                                                    else
                                                    {
                                                        Response.Redirect("AttendDashBoard.aspx");
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (Session["Isadmin"].ToString() == "4")
                                                {
                                                    Response.Redirect("MDDashBoard.aspx");
                                                }
                                                else if (Session["Isadmin"].ToString() == "5")
                                                {
                                                    Response.Redirect("MedicalDetails.aspx");
                                                }
                                                else
                                                {
                                                    Response.Redirect("AttendDashBoard.aspx");
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (Session["Isadmin"].ToString() == "4")
                                            {
                                                Response.Redirect("MDDashBoard.aspx");
                                            }
                                            else if (Session["Isadmin"].ToString() == "5")
                                            {
                                                Response.Redirect("MedicalDetails.aspx");
                                            }
                                            else
                                            {
                                                Response.Redirect("AttendDashBoard.aspx");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Session["Isadmin"].ToString() == "4")
                                        {
                                            Response.Redirect("MDDashBoard.aspx");
                                        }
                                        else if (Session["Isadmin"].ToString() == "5")
                                        {
                                            Response.Redirect("MedicalDetails.aspx");
                                        }
                                        else
                                        {
                                            Response.Redirect("AttendDashBoard.aspx");
                                        }
                                    }
                                }
                                //get User After Login Default Page Load link End
                                //Response.Redirect("Dashboard.aspx");
                            }
                        }
                        else
                        {
                            ErrorDisplay.Visible = true;
                            txtusername.Focus();
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                        }
                    }
                }
                else
                {
                    Write_check();
                    //objdata.Insert_verificationValue();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                }

                //Session["UserId"] = txtusername.Text;
                //objuser.UserCode = txtusername.Text;
                //objuser.Password = UTF8Encryption(txtpassword.Text);
                //objuser.Ccode = ddlCode.SelectedValue.Trim();
                //objuser.Lcode = ddlLocation.SelectedValue.Trim();
                //string pwd = UTF8Encryption(txtpassword.Text);
                //string UserName = objdata.username(objuser);
                //string password = objdata.UserPassword(objuser);

                //if (UserName == txtusername.Text && password == pwd)
                //{

                //    string UserNameDis = objdata.usernameDisplay(objuser);
                //    string isadmin = objdata.isAdmin(txtusername.Text);
                //    string see = isadmin;
                //    int dd = Convert.ToInt32(see);
                //    Session["Isadmin"] = Convert.ToString(dd);
                //    Session["Usernmdisplay"] = UserNameDis;
                //    if (see.Trim() == "3")
                //    {
                //        Response.Redirect("Administrator.aspx");
                //    }
                //    else
                //    {
                //        if (UserName == "Admin")
                //        {
                //            Session["RoleCode"] = "1";
                //        }
                //        else
                //        {
                //            Session["RoleCode"] = "2";
                //        }
                //        Response.Redirect("EmployeeRegistration.aspx");
                //    }
                //}
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('User Name and password Incorrect ');", true);
                //}
            }
        }
    }

    public string Load_CheckerV1(string pass)
    {
        string file_path = "Temp.txt";
        //string Date_Set_Str = "18-12-2020";
        string line = "";
        string enc = UTF8Encryption(pass).ToString();
        string Dec = UTF8Encryption("Cancel");
        // DateTime Date_Set = Convert.ToDateTime(Date_Set_Str);
        // DateTime date_now = DateTime.Now.Date;
        //if (date_now <= Date_Set)
        //{
        try
        {
            // System.IO.File.SetAttributes(file_path, File.GetAttributes(Server.MapPath(file_path)) | FileAttributes.Hidden);
            //Pass the file path and file name to the StreamReader constructor
            StreamReader sr = new StreamReader(Server.MapPath(file_path));
            //Read the first line of text
            line = sr.ReadLine();
            //Continue to read until you reach end of file
            if (line != null)
            {
                while (line != null)
                {
                    //write the lie to console window
                    // Console.WriteLine(line);
                    if ((UTF8Decryption(line) == pass) || UTF8Decryption(line).ToString() == ("Cancel").ToString())
                    {

                    }
                    else
                    {
                        throw new FormatException();
                    }
                    //Read the next line
                    line = sr.ReadLine();
                }
            }
            else
            {
                throw new FormatException();
            }
            //close the file
            sr.Close();
            //Console.ReadLine();
        }
        catch (Exception e)
        {
            throw (e);
        }

        return pass;
    }
    public void Write_check()
    {
        string file_path = "Temp.txt";
        try
        {
            //Open the File

            StreamWriter sw = new StreamWriter(Server.MapPath(file_path), true, Encoding.ASCII);
            //Writeout the numbers 1 to 10 on the same line.

            sw.Write(UTF8Encryption("Cancel"));

            //close the file
            sw.Close();
        }
        catch (Exception e)
        {
            throw (e);
        }
    }

    private static string UTF8Encryption(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = string.Empty;
        UTF8Encoding encodepwd = new UTF8Encoding();
        Decoder Decode = encodepwd.GetDecoder();
        byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        char[] decoded_char = new char[charCount];
        Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

}
