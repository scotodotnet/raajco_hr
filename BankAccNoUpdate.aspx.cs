﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class BankAccNoUpdate : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    string SSQL = "";
    string[] iStr3;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();


        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Bank Account Number Update";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
            //Initial_Data_Referesh();
            
            Load_WagesType();
            Load_Data_EmpDet();
            Load_Bank();
            Load_Bank_Branch();
        }
        Load_Data();
    }

    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlWages.Items.Clear();
        query = "Select *from MstEmployeeType";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlWages.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpType"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWages.DataTextField = "EmpType";
        ddlWages.DataValueField = "EmpType";
        ddlWages.DataBind();
    }

    private void Load_Data_EmpDet()
    {
        string query = "";
        DataTable DT = new DataTable();

        //Repeater1.DataSource = DT;
        //Repeater1.DataBind();

        txtMachineID.Items.Clear();
        query = "Select CONVERT(varchar(10), EmpNo) as EmpNo from ";
        if(rbtnIsActive.SelectedValue=="1")
        {
            query = query + " Employee_Mst";
        }
        else if (rbtnIsActive.SelectedValue == "2")
        {
            query = query + " Employee_Mst_New_Emp";
        }
        query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And Salary_Through='2'";
        DT = objdata.RptEmployeeMultipleDetails(query);
        txtMachineID.DataSource = DT;
        DataRow dr = DT.NewRow();
        dr["EmpNo"] = "-Select-";
        dr["EmpNo"] = "-Select-";
        DT.Rows.InsertAt(dr, 0);
        txtMachineID.DataTextField = "EmpNo";
        txtMachineID.DataValueField = "EmpNo";
        txtMachineID.DataBind();
    }

    private void Load_Bank()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        ddlBankName.Items.Clear();
        query = "Select Distinct BankName from MstBank";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlBankName.DataTextField = "BankName";
        ddlBankName.DataValueField = "BankName";
        ddlBankName.DataBind();

        //query = "Select *from MstBank where Default_Bank='Yes'";
        //DT = objdata.RptEmployeeMultipleDetails(query);

        //if (DT.Rows.Count != 0)
        //{
        //    ddlBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
        //    txtIFSC.Text = DT.Rows[0]["IFSCCode"].ToString();
        //    txtBranch.Text = DT.Rows[0]["Branch"].ToString();
        //}
        //else
        //{
        //    ddlBankName.SelectedValue = "-Select-";
        //    txtIFSC.Text = "";
        //    txtBranch.Text = "";
        //}
    }

    private void Load_Bank_Branch()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        DataTable DT = new DataTable();
        txtBranch.Items.Clear();
        query = "Select Branch  from MstBank where BankName='" + ddlBankName.SelectedValue + "'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBranch.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["Branch"] = "-Select-";
        dr["Branch"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBranch.DataTextField = "Branch";
        txtBranch.DataValueField = "Branch";
        txtBranch.DataBind();

    }

    protected void txtMachineID_SelectedIndexChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();
        if (txtMachineID.SelectedValue != "-Select-")
        {
            query = "Select * from ";
            if (rbtnIsActive.SelectedValue == "1")
            {
                query = query + " Employee_Mst";
            }
            else if (rbtnIsActive.SelectedValue == "2")
            {
                query = query + " Employee_Mst_New_Emp";
            }
            query = query + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
            query = query + " And Wages='" + ddlWages.SelectedItem.Text + "' And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
            query = query + " And Salary_Through='2'";
            DT = objdata.RptEmployeeMultipleDetails(query);

            if (DT.Rows.Count != 0)
            {
                txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
                txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
                txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();
                txtDesignation.Text = DT.Rows[0]["Designation"].ToString();

            }
            else
            {
                txtMachineID.SelectedValue = "-Select-";
                txtEmpName.Text = "";
                txtTokenNo.Text = "";
                txtDeptName.Text = "";
                txtDesignation.Text = "";
            }
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtTokenNo.Text = "";
            txtDeptName.Text = "";
            txtDesignation.Text = "";
        }
    }

    protected void ddlWages_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();
    }

    protected void txtTokenNo_TextChanged(object sender, EventArgs e)
    {
        string query;
        DataTable DT = new DataTable();

        query = "Select * from Employee_Mst where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
        query = query + " And IsActive='Yes' And Wages='" + ddlWages.SelectedItem.Text + "' And ExistingCode='" + txtTokenNo.Text + "'";
        query = query + " And Salary_Through='2'";
        DT = objdata.RptEmployeeMultipleDetails(query);

        if (DT.Rows.Count != 0)
        {
            txtMachineID.SelectedValue = DT.Rows[0]["EmpNo"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
        }
        else
        {
            txtMachineID.SelectedValue = "-Select-";
            txtEmpName.Text = "";
            txtDeptName.Text = "";
            txtDesignation.Text = "";
            //txtTokenNo.Text = "";
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No not Data found.!');", true);
        }
    }

    protected void ddlBankName_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Bank_Branch();
    }

    protected void GridEditEntryClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select *from BankAccountUpdate where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " And MachineID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);


        if (DT.Rows.Count != 0)
        {
            ddlWages.SelectedValue = DT.Rows[0]["Wages"].ToString();
            txtTokenNo.Text = DT.Rows[0]["ExistingCode"].ToString();
            Load_Data_EmpDet();
            txtMachineID.SelectedValue = DT.Rows[0]["MachineID"].ToString();
            txtEmpName.Text = DT.Rows[0]["FirstName"].ToString();
            txtDeptName.Text = DT.Rows[0]["DeptName"].ToString();
            txtDesignation.Text = DT.Rows[0]["Designation"].ToString();
            ddlBankName.SelectedValue = DT.Rows[0]["BankName"].ToString();
            Load_Bank_Branch();
            txtBranch.SelectedValue = DT.Rows[0]["BranchCode"].ToString();
            txtAccNo.Text = DT.Rows[0]["AccountNo"].ToString();
            txtIFSC.Text = DT.Rows[0]["IFSC_Code"].ToString();            
        }
        

        //Session.Remove("MachineID_Apprv");
        //Session["MachineID"] = e.CommandName.ToString();
        //Response.Redirect("EmployeeDetails.aspx");
    }

    protected void GridPrintClick(object sender, CommandEventArgs e)
    {
        string query = "";
        DataTable DT = new DataTable();

        query = "Select *from BankAccountUpdate where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        query = query + " And MachineID='" + e.CommandName.ToString() + "'";
        DT = objdata.RptEmployeeMultipleDetails(query);


        if (DT.Rows.Count != 0)
        {
            string EmployeeTypeCd = "0";
            string EmployeeType = DT.Rows[0]["Wages"].ToString();

            if (EmployeeType.ToUpper() == "STAFF".ToUpper()) { EmployeeTypeCd = "1"; }
            if (EmployeeType.ToUpper() == "SUB-STAFF".ToUpper()) { EmployeeTypeCd = "2"; }
            if (EmployeeType.ToUpper() == "REGULAR".ToUpper()) { EmployeeTypeCd = "3"; }
            if (EmployeeType.ToUpper() == "HOSTEL".ToUpper()) { EmployeeTypeCd = "4"; }
            if (EmployeeType.ToUpper() == "CIVIL".ToUpper()) { EmployeeTypeCd = "5"; }
            if (EmployeeType.ToUpper() == "Watch & Ward".ToUpper()) { EmployeeTypeCd = "6"; }
            if (EmployeeType.ToUpper() == "MANAGER".ToUpper()) { EmployeeTypeCd = "7"; }
            if (EmployeeType.ToUpper() == "Others".ToUpper()) { EmployeeTypeCd = "10"; }

            ResponseHelper.Redirect("ViewReport.aspx?TokenNo=" + DT.Rows[0]["ExistingCode"].ToString() + "&MachineID=" + DT.Rows[0]["MachineID"].ToString() + "&Cate=L&Depat=" + "" + "&Months=March&yr=2019&fromdate=" + "" + "&ToDate=" + "" + "&Salary=" + "" + "&CashBank=" + "" + "&ESICode=" + "" + "&EmpTypeCd=" + EmployeeTypeCd + "&EmpType=" + EmployeeType + "&PayslipType=" + "" + "&PFTypePost=" + "" + "&Left_Emp=" + "" + "&Leftdate=" + "" + "&Division=" + "" + "&Report_Type=BankAccNo_ToPrint", "_blank", "");
        }
        else
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Token No No Data found.!');", true);
        }
    }

    public void Load_Data()
    {
        DataTable dtDisplay = new DataTable();
        string IsActive = "";
        string EmpStatus = "";
        string Query = "";


        Query = "Select CompCode,LocCode,EmpNo,ExistingCode,MachineID,FirstName,DeptName,Designation,BankName,BranchCode,AccountNo,Wages,";
        Query = Query + " IFSC_Code,(CASE WHEN Approve_Status='1' THEN 'Approved' ELSE 'Pending' END) as App_Status from BankAccountUpdate";
        Query = Query + " where CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "'";
        Query = Query + " Order by ExistingCode Asc";
        dtDisplay = objdata.RptEmployeeMultipleDetails(Query);

        //dtdDisplay = objdata.EmployeeDisplay(SessionCcode,SessionLcode);
        //if (dtDisplay.Rows.Count > 0)
        //{
            Repeater1.DataSource = dtDisplay;
            Repeater1.DataBind();
        //}
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string query = "";
        bool ErrFlag = false;
        DataTable DT_Check = new DataTable();

        if (txtMachineID.SelectedItem.Text == "-Select-")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Select the MachineID..!');", true);
        }

        if (txtTokenNo.Text == "")
        {
            ErrFlag = true;
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Enter the Token No..!');", true);
        }


        if (!ErrFlag)
        {
            string Approve_Status = "0";
            //Check Token No
            SSQL = "";
            SSQL = "Select * from BankAccountUpdate Where CompCode='" + SessionCcode + "'";
            SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
            SSQL = SSQL + " And MachineID = '" + txtMachineID.SelectedItem.Text + "' And ExistingCode = '" + txtTokenNo.Text + "'";
            DT_Check = objdata.RptEmployeeMultipleDetails(SSQL);
            if (DT_Check.Rows.Count != 0)
            {
                Approve_Status = DT_Check.Rows[0]["Approve_Status"].ToString();
                if (Approve_Status != "1")
                {
                    SSQL = "Delete from BankAccountUpdate Where CompCode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                    SSQL = SSQL + " And MachineID = '" + txtMachineID.SelectedItem.Text + "' And ExistingCode = '" + txtTokenNo.Text + "'";
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }
            }
            if (Approve_Status != "1")
            {
                SSQL = "";
                SSQL = "Insert into BankAccountUpdate(CompCode,LocCode,EmpNo,ExistingCode,MachineID,FirstName,DeptName,";
                SSQL = SSQL + "Designation,BankName,BranchCode,AccountNo,Wages,IFSC_Code,Approve_Status) Values ( ";
                SSQL = SSQL + "'" + SessionCcode + "','" + SessionLcode + "','" + txtMachineID.SelectedItem.Text + "',";
                SSQL = SSQL + "'" + txtTokenNo.Text + "','" + txtMachineID.SelectedItem.Text + "','" + txtEmpName.Text + "',";
                SSQL = SSQL + "'" + txtDeptName.Text + "','" + txtDesignation.Text + "','" + ddlBankName.SelectedItem.Text + "',";
                SSQL = SSQL + "'" + txtBranch.SelectedValue + "','" + txtAccNo.Text + "','" + ddlWages.SelectedItem.Text + "','" + txtIFSC.Text + "','0')";
                objdata.RptEmployeeMultipleDetails(SSQL);

                //SSQL = "";
                //SSQL = "Update ";
                //if (rbtnIsActive.SelectedValue == "1")
                //{
                //    SSQL = SSQL + " Employee_Mst";
                //}
                //else if (rbtnIsActive.SelectedValue == "2")
                //{
                //    SSQL = SSQL + " Employee_Mst_New_Emp";
                //}
                //SSQL = SSQL + " Set BankName='" + ddlBankName.SelectedItem.Text + "',BranchCode='" + txtBranch.SelectedValue + "',";
                //SSQL = SSQL + "AccountNo='" + txtAccNo.Text + "',IFSC_Code='" + txtIFSC.Text + "'";
                //SSQL = SSQL + " where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "'";
                //SSQL = SSQL + " And Wages='" + ddlWages.SelectedItem.Text + "' And EmpNo='" + txtMachineID.SelectedItem.Text + "'";
                //objdata.RptEmployeeMultipleDetails(SSQL);

                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Bank Details Saved Successfully...');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('This Employee Bank Details Already Approved. You Do not have Rights to enter this Employee');", true);
            }
            Load_Data();
            btnClear_Click(sender, e);
        }

    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        Load_WagesType();
        Load_Data_EmpDet();
        Load_Bank();
        Load_Bank_Branch();
        txtTokenNo.Text = "";
        txtEmpName.Text = "";
        txtDeptName.Text = "";
        txtDesignation.Text = "";
        txtAccNo.Text = "";
        txtIFSC.Text = "";

        Load_Data();
    }

    protected void rbtnIsActive_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Data_EmpDet();

    }
    protected void txtBranch_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = new DataTable();
        string query = "";
        query = "Select * from MstBank where BankName='" + ddlBankName.SelectedItem.Text + "' And Branch='" + txtBranch.SelectedValue + "'";
        dt = objdata.RptEmployeeMultipleDetails(query);

        if (dt.Rows.Count != 0)
        {
            txtIFSC.Text = dt.Rows[0]["IFSCCode"].ToString();
            //txtBranch.Text = dt.Rows[0]["Branch"].ToString();
        }
    }
}
