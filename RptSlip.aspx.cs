﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
using Payroll;
using Payroll.Data;
using Payroll.Configuration;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
public partial class RptSlip : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();
    DateTime TransDate;
    string SessionAdmin;
    string Stafflabour;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string NetBase;
    string NetFDA;
    string NetVDA;
    string NetHRA;
    string Nettotal;
    string NetPFEarnings;
    string NetPF;
    string NetESI;
    string NetUnion;
    string NetAdvance;
    string NetAll1;
    string NetAll2;
    string NetAll3;
    string NetAll4;
    string NetAll5;
    string NetDed1;
    string NetDed2;
    string NetDed3;
    string NetDed4;
    string NetDed5;
    string HomeDays;
    string Halfnight;
    string FullNight;
    string DayIncentive;
    string Spinning;
    string ThreeSided;
    string NetLOP;
    string NetStamp;
    string NetTotalDeduction;
    string NetOT;
    string NetAmt;
    string Network;
    string totNFh;
    string totweekoff;
    string totCL;
    string totwork;
    string Roundoff;
    DateTime MyDate;
    DateTime MyDate1;
    DateTime MyDate2;
    static decimal AdvAmt;
    static string ID;
    static string Adv_id = "";
    static string Dec_mont = "0";
    static string Adv_BalanceAmt = "0";
    static string Adv_due = "0";
    static string Increment_mont = "0";
    static decimal val;
    static string EmployeeDays = "0";
    string MyMonth;
    static string cl = "0";
    string TempDate;
    static string Fixedsal = "0";
    static string FixedOT = "0";
    static string Tot_OThr = "0";
    static string Emp_ESI_Code = "";
    static string NetPay_Grand_Total = "0";
    static string NetPay_Grand_Total_Words = "";
    static bool isUK = false;
    string SessionPayroll;

    string SessionUserName;
    string SessionUserID;
    string SessionRights;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        Load_DB();
        if (!IsPostBack)
        {
            Load_Report_Type();
            Load_WagesType();
            Agent_load();
            //Load_Department();
            Load_BankName();
            Months_load();
            //ESICode_load();
            int currentYear = Utility.GetFinancialYear;
            for (int i = 0; i < 10; i++)
            {
                ddlFinance.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                //  ddlShowYear.Items.Add(new ListItem(currentYear.ToString() + "-" + Convert.ToString(currentYear + 1), currentYear.ToString()));
                currentYear = currentYear - 1;
            }

            currentYear = Utility.GetCurrentYearOnly;
            for (int i = 0; i < 10; i++)
            {
                //txtLCYear.Items.Add(new System.Web.UI.WebControls.ListItem(currentYear.ToString(), currentYear.ToString()));
                currentYear = currentYear - 1;
            }
            //Load_Division_Name();
            //Master.Visible = false;
            if (SessionUserType == "2")
            {
                IFUser_Fields_Hide();
            }
        }
    }
    public void IFUser_Fields_Hide()
    {
        //IF_Dept_Hide.Visible = false;
        IF_FromDate_Hide.Visible = false;
        IF_ToDate_Hide.Visible = false;
        rdbPayslipIFFormat.Visible = true;
        RdbCashBank.SelectedValue = "2";
        RdbPFNonPF.SelectedValue = "1";
        IF_Salary_Through_Hide.Visible = false;
        IF_PF_NON_PF_Hide.Visible = false;
        IF_Report_Type.Visible = true;
        //IF_rdbPayslipFormat_Hide.Visible = false;
    }
    public void Load_DB()
    {
        //Get Database Name
        string query = "";
        DataTable dt_DB = new DataTable();
        query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Raajco_Rights]..MstDBname";
        dt_DB = objdata.RptEmployeeMultipleDetails(query);
        if (dt_DB.Rows.Count > 0)
        {
            SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
            //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
        }
    }
    private void Load_BankName()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtBankName.Items.Clear();
        query = "Select distinct BankName from MstBank order by BankName Asc";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtBankName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["BankName"] = "-Select-";
        dr["BankName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        txtBankName.DataTextField = "BankName";
        txtBankName.DataValueField = "BankName";
        txtBankName.DataBind();
    }
    private void Load_WagesType()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        txtEmployeeType.Items.Clear();
        string Category_Str = "0";
        if (ddlcategory.SelectedItem.Text == "STAFF")
        {
            Category_Str = "1";
        }
        else if (ddlcategory.SelectedItem.Text == "LABOUR")
        {
            Category_Str = "2";
        }
        query = "select EmpTypeCd,EmpType from MstEmployeeType where EmpCategory='" + Category_Str + "'";

        //query = "select distinct Wages from Raajco_Epay..SalaryDetails where Wagestype ='" + Category_Str + "'";

        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        txtEmployeeType.DataSource = dtdsupp;
        //DataRow dr = dtdsupp.NewRow();
        //dr["EmpTypeCd"] = "0";
        //dr["EmpType"] = "-Select-";

        if ((ddlcategory.SelectedItem.Text == "STAFF") || (ddlcategory.SelectedItem.Text == "LABOUR"))

        {
            //dr["EmpTypeCd"] = "1";
            //dr["EmpType"] = "ALL";
        }
        //dtdsupp.Rows.InsertAt(dr, 0);

        txtEmployeeType.DataTextField = "EmpType";
        txtEmployeeType.DataValueField = "EmpTypeCd";
        txtEmployeeType.DataBind();
        txtEmployeeType.Items.Insert(0, new System.Web.UI.WebControls.ListItem("-Select-", "-Select-", true));
        txtEmployeeType.Items.Insert(1, new System.Web.UI.WebControls.ListItem("ALL", "ALL", true));
    }
    private void Agent_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlAgentName.Items.Clear();
        query = "Select AgentName from MstAgent order by AgentName ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlAgentName.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["AgentName"] = "0";
        dr["AgentName"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlAgentName.DataTextField = "AgentName";
        ddlAgentName.DataValueField = "AgentName";
        ddlAgentName.DataBind();
    }

    private void Months_load()
    {
        string query = "";
        DataTable dtdsupp = new DataTable();
        ddlMonths.Items.Clear();
        query = "Select ID,Months from MonthDetails order by ID ASC";
        dtdsupp = objdata.RptEmployeeMultipleDetails(query);
        ddlMonths.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ID"] = "0";
        dr["Months"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlMonths.DataTextField = "Months";
        ddlMonths.DataValueField = "ID";
        ddlMonths.DataBind();
    }
    protected void ddlcategory_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_WagesType();
        Load_TwoDates();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";
                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";


                if (SessionUserType == "2")
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " " + YR.ToString();
                }
                else
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + txtfrom.ToString() + " - " + txtTo.ToString();
                }

                Basic_Report_Date = "";
                Basic_Report_Type = "OLD";

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                //Check PF Category

                //if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5")) //Labour Worker
                //    if ( (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5")) //Labour Worker
                //    {
                //    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed, " +
                //           " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                //           " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                //           " (SalDet.GrossEarnings - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, " +
                //           " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.CommsAmt + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                //           " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5) as NetFinal " +
                //           " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //           " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //           " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //           " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //           " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //           " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //           " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //             //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                //             " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')  ";

                //    query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                //   " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                //   " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5  " +
                //   " Order by cast(EmpDet.ExistingCode as int) Asc";
                //}
                //else if ((txtEmployeeType.SelectedValue == "1") || (txtEmployeeType.SelectedValue == "2"))


                if (txtEmployeeType.SelectedItem.Text != "ALL")
                {
                    // query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed, " +
                    //         "EmpDet.ESINo,EmpDet.UAN,EmpDet.AccountNo,EmpDet.BranchCode,EmpDet.DOJ,EmpDet.ActualSalary,EmpDet.Designation," +


                    //     " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                    //           " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,(SalDet.NetPay + SalDet.HRAamt + SalDet.TAamt) as  NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                    //           " ((SalDet.GrossEarnings + SalDet.HRAamt + SalDet.TAamt) - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, " +
                    //           " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.CommsAmt + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5 + SalDet.ProvidentFund + SalDet.ESI ) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                    //           " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5) as NetFinal,SalDet.HRAamt,SalDet.TAamt,EmpDet.HRA,(EmpDet.BaseSalary +EmpDet.HRA) as TotFixed, (SalDet.TotalBasicAmt + SalDet.HRAamt) as GrossTotal  " +
                    //           " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    //           " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    //           " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //           " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                    //           " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                    //           " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    //           " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                    //            //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                    //            " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') ";

                    // query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                    //" SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                    //" SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HRAamt,SalDet.TAamt,EmpDet.HRA,EmpDet.BaseSalary,  " +
                    //"EmpDet.ESINo,EmpDet.UAN,EmpDet.AccountNo,EmpDet.BranchCode,EmpDet.DOJ,EmpDet.ActualSalary,EmpDet.Designation"+
                    //" Order by cast(EmpDet.ExistingCode as int) Asc";

                    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.Weekoff,SalDet.LOPDays,SalDet.NFh," +
                             "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                             "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3) as IT,(SalDet.Deduction4) as Mess,(SalDet.Deduction5) as Rent," +
                             "SalDet.Advance,SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays " +
                             ", SalDet.RoundOffNetpay as Netpaid,(Cast(SalDet.NetPay as decimal(18,2))-Cast(SalDet.RoundOffNetpay as decimal(18,2))) as Rnd" +


                              " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                              " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                              " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                              " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                              " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                              " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                              " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                               //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') ";

                    //query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                    //" SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                    //" SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HRAamt,SalDet.TAamt,EmpDet.HRA,EmpDet.BaseSalary,  " +
                    //"EmpDet.ESINo,EmpDet.UAN,EmpDet.AccountNo,EmpDet.BranchCode,EmpDet.DOJ,EmpDet.ActualSalary,EmpDet.Designation" +
                    //" Order by cast(EmpDet.ExistingCode as int) Asc";
                    //query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo " +
                    //     " Order by cast(EmpDet.ExistingCode as int) Asc";

                    query = query + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation," +
                   "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                    "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3),(SalDet.Deduction4),(SalDet.Deduction5) ,SalDet.Advance," +
                 " SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF, SalDet.WorkedDays,SalDet.Weekoff," +
                  "SalDet.Basic_SM,SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo ,SalDet.LOPDays,SalDet.NFh,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays, SalDet.RoundOffNetpay Order by cast(EmpDet.ExistingCode as int) Asc";





                }
                else
                {
                    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.Weekoff,SalDet.LOPDays,SalDet.NFh," +
                         "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                         "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3) as IT,(SalDet.Deduction4) as Mess,(SalDet.Deduction5) as Rent," +
                         "SalDet.Advance,SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays " +
                         ", SalDet.RoundOffNetpay as Netpaid,(Cast(SalDet.NetPay as decimal(18,2))-Cast(SalDet.RoundOffNetpay as decimal(18,2))) as Rnd" +


                          " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                          " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                          " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                          " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                          " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                          " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                          " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "'" +
                           //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                           " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') ";

                    query = query + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation," +
                     "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                      "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3),(SalDet.Deduction4),(SalDet.Deduction5) ,SalDet.Advance," +
                   " SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF, SalDet.WorkedDays,SalDet.Weekoff," +
                    "SalDet.Basic_SM,SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo ,SalDet.LOPDays,SalDet.NFh,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays,SalDet.RoundOffNetpay Order by cast(EmpDet.ExistingCode as int) Asc";

                }

                //if (Str_PFType.ToString() != "0")
                //{
                //    if (Str_PFType.ToString() == "1")
                //    {
                //        if ((txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8")) //Tamil Girls Worker
                //        {
                //            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                   " SalDet.LOPDays,(SalDet.FFDA) as Basic_SMM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Basic_SM," +
                //                   " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                //                   " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.Deduction3, SalDet.Deduction4 " +
                //                   " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                   " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                   " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                   " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                   " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                   " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //                   " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                   " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                //            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
                //           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                //           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.Deduction3,SalDet.Deduction4 " +
                //           " Order by cast(EmpDet.ExistingCode as int) Asc";
                //        }
                //        else if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13"))
                //        {
                //            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SMM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Basic_SM," +
                //                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                //                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.Deduction3, SalDet.Deduction4 " +
                //                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) And " +
                //                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' and convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) ";

                //            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
                //           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                //           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.Deduction3,SalDet.Deduction4 " +
                //           " Order by cast(EmpDet.ExistingCode as int) Asc";
                //        }
                //        else if ((txtEmployeeType.SelectedValue == "112") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //        {
                //            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3, SalDet.Deduction4, " +
                //                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                //                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                //                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                //            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
                //           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3,SalDet.Deduction4, " +
                //           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                //           " Order by cast(EmpDet.ExistingCode as int) Asc";
                //        }
                //    }
                //    else
                //    {
                //        //Non PF Employee
                //        query = query + " and (EmpDet.Eligible_PF='2')";
                //    }
                //}
                //else
                //{
                //    if ((txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //               " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                //               " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                //               " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                //               " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                //               " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                //               " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //               " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //               " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //               " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //               " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //               " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //               " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                //        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                //       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                //       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                //       " Order by cast(EmpDet.ExistingCode as int) Asc";
                //    }
                //    else if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13"))
                //    {
                //        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                  " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                //                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                //                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                //                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                //                  " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                //                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) And " +
                //                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) ";

                //        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                //       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                //       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                //       " Order by cast(EmpDet.ExistingCode as int) Asc";
                //    }
                //    else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //    {
                //        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Actual,(EmpDet.VPF) as Fixed," +
                //                  " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                //                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                //                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                //                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                //                  " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                //                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                //        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                //       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                //       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                //       " Order by cast(EmpDet.ExistingCode as int) Asc";
                //    }
                //}

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";

                //if ((txtEmployeeType.SelectedValue == "2"))
                {
                    //GvMonthWorker.DataSource = dt_1;
                    //GvMonthWorker.DataBind();
                    GvStaffCheck.DataSource = dt_1;
                    GvStaffCheck.DataBind();
                }
                //else if (txtEmployeeType.SelectedValue == "1" )
                //{
                //    GvStaffCheck.DataSource = dt_1;
                //    GvStaffCheck.DataBind();
                //}
                //else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    GvOthers.DataSource = dt_1;
                //    GvOthers.DataBind();
                //}


                ////Check PF Category
                //if (Str_PFType.ToString() == "1")
                // {
                //     //PF Employee List
                //     GVHostel.DataSource = dt_1;
                //     GVHostel.DataBind();
                // }
                // else
                // {
                //     //Non PF Employee List
                //     if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                //     {
                //         GVCivil.DataSource = dt_1;
                //         GVCivil.DataBind();
                //     }
                //     else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //     {
                //         gvStaffNonPF.DataSource = dt_1;
                //         gvStaffNonPF.DataBind();
                //     }
                // }



                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                NetBase = "0";
                NetFDA = "0";
                NetVDA = "0";
                Nettotal = "0";
                NetPFEarnings = "0";
                NetPF = "0";
                NetESI = "0";
                NetUnion = "0";
                NetAdvance = "0";
                NetAll1 = "0";
                NetAll2 = "0";
                NetAll3 = "0";
                NetAll4 = "0";
                NetDed1 = "0";
                NetDed2 = "0";
                NetDed3 = "0";
                NetDed4 = "0";
                NetAll5 = "0";
                NetDed5 = "0";
                NetLOP = "0";
                NetStamp = "0";
                NetTotalDeduction = "0";
                NetOT = "0";
                NetAmt = "0";
                Network = "0";
                totCL = "0";
                totNFh = "0";
                totweekoff = "0";
                Roundoff = "0";
                Fixedsal = "0";
                FixedOT = "0";
                Tot_OThr = "0";
                //HomeDays = "0";
                //Halfnight = "0";
                //FullNight = "0";
                //Spinning = "0";
                //DayIncentive = "0";
                //ThreeSided = "0";
                //totwork = "0";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                //if ((txtEmployeeType.SelectedValue == "2"))
                {
                    //GvMonthWorker.RenderControl(htextw);
                    GvStaffCheck.RenderControl(htextw);
                }
                //else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    GvOthers.RenderControl(htextw);
                //}
                //else if (txtEmployeeType.SelectedValue == "1")
                //{
                //    GvStaffCheck.RenderControl(htextw);
                //}

                ////Check PF Category
                //    if (Str_PFType.ToString() == "1")
                //    {
                //        //PF Employee
                //        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7"))
                //        {
                //            GVHostel.RenderControl(htextw);
                //        }
                //    }
                //    else
                //    {
                //        //Non PF Employee
                //        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                //        {
                //            GVCivil.RenderControl(htextw);
                //        }
                //        else if((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //        {
                //            gvStaffNonPF.RenderControl(htextw);
                //        }
                //    }

                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='42'>");
                //Response.Write("" + CmpName + " - PAYSLIP REPORT");
                Response.Write("RAAJCO SPINNERS PRIVATE LIMITED");

                Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr align='Center'>");
                //Response.Write("<td colspan='19'>");
                //Response.Write("" + SessionLcode + "");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr align='Center'>");
                //Response.Write("<td colspan='19'>");
                //Response.Write("" + Cmpaddress + "");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                string Salary_Head = "";

                //if ((txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11"))
                {
                    //   Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                    Salary_Head = " SALARY STATEMENT OF EMPLOYOEES IN THE MONTH OF &nbsp;&nbsp;&nbsp;" + txtfrom.Text + "&nbsp; - &nbsp;" + txtTo.Text;

                }
                string category = "";
                {
                    category = "CATEGORY &nbsp;-&nbsp;" + txtEmployeeType.SelectedItem.Text;
                }
                //else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                //}

                Response.Write("<tr align='Center'>");

                Response.Write("<td colspan='42'>");
                //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='42'>");
                Response.Write(category);
                Response.Write("</td>");
                Response.Write("</tr>");

                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());

                //Response.Write("<table border='1'>");
                //Response.Write("<tr Font-Bold='true'>");
                //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //Response.Write("Grand Total");
                //Response.Write("</td>");

                Int32 Grand_Tot_End = 0;

                //Check PF Category

                //if ((txtEmployeeType.SelectedValue == "2"))
                if ((txtEmployeeType.SelectedValue == "4"))
                {
                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='10'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = GvMonthWorker.Rows.Count + 5;

                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");


                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");



                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                }
                else if ((txtEmployeeType.SelectedValue == "1") || (txtEmployeeType.SelectedValue == "2"))
                {
                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='10'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = GvStaffCheck.Rows.Count + 5;

                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");


                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");



                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                }
                else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='9'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = GvOthers.Rows.Count + 5;

                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");


                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");

                }

                //if (Str_PFType.ToString() == "1")
                //{
                //    //PF Employee
                //    if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        Response.Write("<table border='1'>");
                //        Response.Write("<tr Font-Bold='true'>");
                //        Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //        Response.Write("Grand Total");
                //        Response.Write("</td>");

                //        Grand_Tot_End = GVHostel.Rows.Count + 5;
                //    }

                //    if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");

                //    }
                //}
                //else
                //{
                //    //Non PF Employee
                //    if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        Response.Write("<table border='1'>");
                //        Response.Write("<tr Font-Bold='true'>");
                //        Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //        Response.Write("Grand Total");
                //        Response.Write("</td>");

                //        Grand_Tot_End = GVCivil.Rows.Count + 5;

                //        Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                //    }
                //    else
                //    {
                //        Response.Write("<table border='1'>");
                //        Response.Write("<tr Font-Bold='true'>");
                //        Response.Write("<td font-Bold='true' align='right' colspan='6'>");
                //        Response.Write("Grand Total");
                //        Response.Write("</td>");

                //        Grand_Tot_End = gvStaffNonPF.Rows.Count + 5;

                //        //Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                //    }



                //    if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {

                //    }
                //    //want to put grand total


                //}




                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);



            }

        }
        catch (Exception Ex)
        {

            throw;
        }
    }
    protected void btnSearch_Click_old(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }

            if (ddlcategory.SelectedValue == "-Select-")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                ErrFlag = true;
            }
            else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                ErrFlag = true;
            }

            else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";
                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";


                if (SessionUserType == "2")
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + ddlMonths.SelectedItem.Text + " " + YR.ToString();
                }
                else
                {
                    report_head = "PAYSLIP FOR THE MONTH OF " + txtfrom.ToString() + " - " + txtTo.ToString();
                }

                Basic_Report_Date = "";
                Basic_Report_Type = "OLD";

                DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                //Check PF Category

                //if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5")) //Labour Worker
                //    if ( (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5")) //Labour Worker
                //    {
                //    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed, " +
                //           " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                //           " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                //           " (SalDet.GrossEarnings - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, " +
                //           " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.CommsAmt + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                //           " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5) as NetFinal " +
                //           " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //           " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //           " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //           " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //           " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //           " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //           " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //             //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                //             " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')  ";

                //    query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                //   " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                //   " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5  " +
                //   " Order by cast(EmpDet.ExistingCode as int) Asc";
                //}
                //else if ((txtEmployeeType.SelectedValue == "1") || (txtEmployeeType.SelectedValue == "2"))


                if (txtEmployeeType.SelectedItem.Text != "ALL")
                {
                    // query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed, " +
                    //         "EmpDet.ESINo,EmpDet.UAN,EmpDet.AccountNo,EmpDet.BranchCode,EmpDet.DOJ,EmpDet.ActualSalary,EmpDet.Designation," +


                    //     " SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4," +
                    //           " SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,(SalDet.NetPay + SalDet.HRAamt + SalDet.TAamt) as  NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,(SalDet.TotalBasicAmt - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as TotalOne,EmpDet.PFNo, " +
                    //           " ((SalDet.GrossEarnings + SalDet.HRAamt + SalDet.TAamt) - (SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax)) as GrossOne,SalDet.TempleAmt,SalDet.HouseAmt,SalDet.Hloan, " +
                    //           " (SalDet.TempleAmt + SalDet.HouseAmt + SalDet.Hloan + SalDet.Advace2 + SalDet.Advance3 + SalDet.CommsAmt + SalDet.Deduction3 + SalDet.Deduction4 +  SalDet.Deduction5 + SalDet.ProvidentFund + SalDet.ESI ) as TotalDed,(SalDet.ProvidentFund + SalDet.ESI + SalDet.IncomeTax) as TotalDedFirst, " +
                    //           " SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,(SalDet.NetPay + SalDet.allowances5) as NetFinal,SalDet.HRAamt,SalDet.TAamt,EmpDet.HRA,(EmpDet.BaseSalary +EmpDet.HRA) as TotFixed, (SalDet.TotalBasicAmt + SalDet.HRAamt) as GrossTotal  " +
                    //           " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    //           " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    //           " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //           " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                    //           " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                    //           " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    //           " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                    //            //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                    //            " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') ";

                    // query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                    //" SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                    //" SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HRAamt,SalDet.TAamt,EmpDet.HRA,EmpDet.BaseSalary,  " +
                    //"EmpDet.ESINo,EmpDet.UAN,EmpDet.AccountNo,EmpDet.BranchCode,EmpDet.DOJ,EmpDet.ActualSalary,EmpDet.Designation"+
                    //" Order by cast(EmpDet.ExistingCode as int) Asc";

                    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.Weekoff,SalDet.LOPDays,SalDet.NFh," +
                             "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                             "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3) as IT,(SalDet.Deduction4) as Mess,(SalDet.Deduction5) as Rent," +
                             "SalDet.Advance,SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays " +



                              " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                              " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                              " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                              " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                              " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                              " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                              " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                               //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') ";

                    //query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.Fbasic,SalDet.GrossEarnings,SalDet.DaAmt,SalDet.DaFixed,SalDet.DaRateFixed,SalDet.DaRateAmt,SalDet.ProvidentFund,SalDet.ESI,SalDet.IncomeTax,SalDet.TotalFixed,SalDet.TempleAmt,SalDet.HouseAmt,EmpDet.PFNo, " +
                    //" SalDet.DayIncentiveFixed,SalDet.DayIncentive,SalDet.CommsAmt,SalDet.TotalBasicAmt,SalDet.Advance,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Advace2,SalDet.Advance3,SalDet.Deduction3,SalDet.Deduction4,SalDet.Hloan," +
                    //" SalDet.Deduction5,SalDet.Deduction6,SalDet.Deduction7,SalDet.Deduction8,SalDet.CommsAmt,SalDet.NetPay,SalDet.Availabledays,SalDet.WorkedDays,SalDet.NFh,SalDet.allowances1,SalDet.allowances2,SalDet.allowances3,SalDet.allowances4,SalDet.allowances5,SalDet.HRAamt,SalDet.TAamt,EmpDet.HRA,EmpDet.BaseSalary,  " +
                    //"EmpDet.ESINo,EmpDet.UAN,EmpDet.AccountNo,EmpDet.BranchCode,EmpDet.DOJ,EmpDet.ActualSalary,EmpDet.Designation" +
                    //" Order by cast(EmpDet.ExistingCode as int) Asc";
                    //query = query + "group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,SalDet.Basic_SM,SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo " +
                    //     " Order by cast(EmpDet.ExistingCode as int) Asc";

                    query = query + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation," +
                   "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                    "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3),(SalDet.Deduction4),(SalDet.Deduction5) ,SalDet.Advance," +
                 " SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF, SalDet.WorkedDays,SalDet.Weekoff," +
                  "SalDet.Basic_SM,SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo ,SalDet.LOPDays,SalDet.NFh,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays Order by cast(EmpDet.ExistingCode as int) Asc";





                }
                else
                {
                    query = "select EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation,SalDet.WorkedDays,SalDet.Weekoff,SalDet.LOPDays,SalDet.NFh," +
                         "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                         "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3) as IT,(SalDet.Deduction4) as Mess,(SalDet.Deduction5) as Rent," +
                         "SalDet.Advance,SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays " +



                          " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                          " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                          " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                          " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                          " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                          " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                          " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "'" +
                           //" and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";
                           " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') ";

                    query = query + " group by EmpDet.ExistingCode,EmpDet.EmpNo,EmpDet.FirstName,EmpDet.DeptName,EmpDet.Designation," +
                     "SalDet.Basic_SM,SalDet.OTHoursNew,SalDet.PfSalary,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.EduAllow,SalDet.WashingAllow,SalDet.MediAllow,SalDet.OverTime," +
                      "SalDet.VDA,SalDet.ConvAllow,SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.ESI,(SalDet.Deduction3),(SalDet.Deduction4),(SalDet.Deduction5) ,SalDet.Advance," +
                   " SalDet.T_Shirt,SalDet.Water_and_Others,SalDet.Canteen,SalDet.LIC,SalDet.EB,SalDet.VPF, SalDet.WorkedDays,SalDet.Weekoff," +
                    "SalDet.Basic_SM,SalDet.GrossEarnings,SalDet.ESI,EmpDet.PFNo ,SalDet.LOPDays,SalDet.NFh,SalDet.TotalDeductions,SalDet.NetPay,SalDet.PF_Sal,SalDet.ESI_Sal,AttnDet.TotalWHdays Order by cast(EmpDet.ExistingCode as int) Asc";

                }

                //if (Str_PFType.ToString() != "0")
                //{
                //    if (Str_PFType.ToString() == "1")
                //    {
                //        if ((txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8")) //Tamil Girls Worker
                //        {
                //            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                   " SalDet.LOPDays,(SalDet.FFDA) as Basic_SMM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Basic_SM," +
                //                   " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                //                   " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.Deduction3, SalDet.Deduction4 " +
                //                   " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                   " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                   " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                   " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                   " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                   " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //                   " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                   " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                //            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
                //           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                //           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.Deduction3,SalDet.Deduction4 " +
                //           " Order by cast(EmpDet.ExistingCode as int) Asc";
                //        }
                //        else if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13"))
                //        {
                //            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SMM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Basic_SM," +
                //                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                //                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays,SalDet.Deduction3, SalDet.Deduction4 " +
                //                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) And " +
                //                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' and convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) ";

                //            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,SalDet.Basic_SM," +
                //           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA," +
                //           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.Deduction3,SalDet.Deduction4 " +
                //           " Order by cast(EmpDet.ExistingCode as int) Asc";
                //        }
                //        else if ((txtEmployeeType.SelectedValue == "112") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //        {
                //            query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3, SalDet.Deduction4, " +
                //                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4) as DedPF," +
                //                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance + SalDet.Deduction3 + SalDet.Deduction4)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                //                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Eligible_PF='1' ";

                //            query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA," +
                //           " EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,SalDet.Deduction3,SalDet.Deduction4, " +
                //           " SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                //           " Order by cast(EmpDet.ExistingCode as int) Asc";
                //        }
                //    }
                //    else
                //    {
                //        //Non PF Employee
                //        query = query + " and (EmpDet.Eligible_PF='2')";
                //    }
                //}
                //else
                //{
                //    if ((txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //               " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                //               " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                //               " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                //               " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                //               " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                //               " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //               " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //               " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //               " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //               " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //               " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //               " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //               " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                //        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                //       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                //       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                //       " Order by cast(EmpDet.ExistingCode as int) Asc";
                //    }
                //    else if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13"))
                //    {
                //        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm," +
                //                  " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA ) as NonPFWages,SalDet.Dayincentive," +
                //                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew + SalDet.Dayincentive) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI," +
                //                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                //                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                //                  " (SalDet.NetPay) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.New_Allowance_Amt " +
                //                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And convert(datetime,AttnDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,AttnDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) And " +
                //                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and convert(datetime,SalDet.FromDate,103)>=convert(datetime,'" + txtfrom.Text + "',103) and convert(datetime,SalDet.ToDate,103)>=convert(datetime,'" + txtTo.Text + "',103) ";

                //        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt,SalDet.Dayincentive," +
                //       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                //       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                //       " Order by cast(EmpDet.ExistingCode as int) Asc";
                //    }
                //    else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //    {
                //        query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,(EmpDet.BaseSalary + EmpDet.VPF) as Actual,(EmpDet.VPF) as Fixed," +
                //                  " SalDet.WorkedDays,EmpDet.DeptName as Department,SalDet.OTHoursNew,(SalDet.BasicandDA) as NonPFWages," +
                //                  " SalDet.OTHoursAmtNew,(SalDet.BasicandDA + SalDet.OTHoursAmtNew) as GrossEarningsOT,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt," +
                //                  " (SalDet.Advance + SalDet.Deduction3 + SalDet.Deduction4 + SalDet.DedOthers1 + SalDet.DedOthers2) as TotDed,SalDet.TotalDeductions," +
                //                  " SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.Basic_SM,SalDet.Advance, " +
                //                  " (SalDet.New_Cash_Amt) as NetPay,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt " +
                //                  " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                //                  " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                //                  " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                //                  " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                //                  " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                //                  " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                //                  " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                //                  " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";

                //        query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.TotalDeductions,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                //       " EmpDet.DeptName,SalDet.WorkedDays,SalDet.FDA,SalDet.OTHoursNew,SalDet.OTHoursAmtNew,SalDet.BasicandDA,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI,SalDet.New_Allowance_Amt,EmpDet.BaseSalary,EmpDet.VPF," +
                //       " SalDet.GrossEarnings,SalDet.Advance,SalDet.Deduction3,SalDet.Deduction4,SalDet.Deduction5,SalDet.DedOthers1,SalDet.DedOthers2,SalDet.NetPay,SalDet.Basic_SM,SalDet.Advance  " +
                //       " Order by cast(EmpDet.ExistingCode as int) Asc";
                //    }
                //}

                DataTable dt_1 = new DataTable();
                dt_1 = objdata.RptEmployeeMultipleDetails(query);

                Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";

                //if ((txtEmployeeType.SelectedValue == "2"))
                {
                    //GvMonthWorker.DataSource = dt_1;
                    //GvMonthWorker.DataBind();
                    GvStaffCheck.DataSource = dt_1;
                    GvStaffCheck.DataBind();
                }
                //else if (txtEmployeeType.SelectedValue == "1" )
                //{
                //    GvStaffCheck.DataSource = dt_1;
                //    GvStaffCheck.DataBind();
                //}
                //else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    GvOthers.DataSource = dt_1;
                //    GvOthers.DataBind();
                //}


                ////Check PF Category
                //if (Str_PFType.ToString() == "1")
                // {
                //     //PF Employee List
                //     GVHostel.DataSource = dt_1;
                //     GVHostel.DataBind();
                // }
                // else
                // {
                //     //Non PF Employee List
                //     if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                //     {
                //         GVCivil.DataSource = dt_1;
                //         GVCivil.DataBind();
                //     }
                //     else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //     {
                //         gvStaffNonPF.DataSource = dt_1;
                //         gvStaffNonPF.DataBind();
                //     }
                // }



                string attachment = "attachment;filename=Payslip.xls";
                Response.ClearContent();
                Response.AddHeader("content-disposition", attachment);
                Response.ContentType = "application/ms-excel";
                //DataTable dt = new DataTable();
                query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                dt = objdata.RptEmployeeMultipleDetails(query);
                if (dt.Rows.Count > 0)
                {
                    CmpName = dt.Rows[0]["Cname"].ToString();
                    Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                }

                NetBase = "0";
                NetFDA = "0";
                NetVDA = "0";
                Nettotal = "0";
                NetPFEarnings = "0";
                NetPF = "0";
                NetESI = "0";
                NetUnion = "0";
                NetAdvance = "0";
                NetAll1 = "0";
                NetAll2 = "0";
                NetAll3 = "0";
                NetAll4 = "0";
                NetDed1 = "0";
                NetDed2 = "0";
                NetDed3 = "0";
                NetDed4 = "0";
                NetAll5 = "0";
                NetDed5 = "0";
                NetLOP = "0";
                NetStamp = "0";
                NetTotalDeduction = "0";
                NetOT = "0";
                NetAmt = "0";
                Network = "0";
                totCL = "0";
                totNFh = "0";
                totweekoff = "0";
                Roundoff = "0";
                Fixedsal = "0";
                FixedOT = "0";
                Tot_OThr = "0";
                //HomeDays = "0";
                //Halfnight = "0";
                //FullNight = "0";
                //Spinning = "0";
                //DayIncentive = "0";
                //ThreeSided = "0";
                //totwork = "0";

                StringWriter stw = new StringWriter();
                HtmlTextWriter htextw = new HtmlTextWriter(stw);

                //if ((txtEmployeeType.SelectedValue == "2"))
                {
                    //GvMonthWorker.RenderControl(htextw);
                    GvStaffCheck.RenderControl(htextw);
                }
                //else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    GvOthers.RenderControl(htextw);
                //}
                //else if (txtEmployeeType.SelectedValue == "1")
                //{
                //    GvStaffCheck.RenderControl(htextw);
                //}

                ////Check PF Category
                //    if (Str_PFType.ToString() == "1")
                //    {
                //        //PF Employee
                //        if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7"))
                //        {
                //            GVHostel.RenderControl(htextw);
                //        }
                //    }
                //    else
                //    {
                //        //Non PF Employee
                //        if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "6"))
                //        {
                //            GVCivil.RenderControl(htextw);
                //        }
                //        else if((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //        {
                //            gvStaffNonPF.RenderControl(htextw);
                //        }
                //    }

                Response.Write("<table>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='40'>");
                //Response.Write("" + CmpName + " - PAYSLIP REPORT");
                Response.Write("RAAJCO SPINNERS PRIVATE LIMITED");

                Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr align='Center'>");
                //Response.Write("<td colspan='19'>");
                //Response.Write("" + SessionLcode + "");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                //Response.Write("<tr align='Center'>");
                //Response.Write("<td colspan='19'>");
                //Response.Write("" + Cmpaddress + "");
                //Response.Write("</td>");
                //Response.Write("</tr>");
                string Salary_Head = "";

                //if ((txtEmployeeType.SelectedValue == "8") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11"))
                {
                    //   Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                    Salary_Head = " SALARY STATEMENT OF EMPLOYOEES IN THE MONTH OF &nbsp;&nbsp;&nbsp;" + txtfrom.Text + "&nbsp; - &nbsp;" + txtTo.Text;

                }
                string category = "";
                {
                    category = "CATEGORY &nbsp;-&nbsp;" + txtEmployeeType.SelectedItem.Text;
                }
                //else if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                //{
                //    Salary_Head = "" + txtEmployeeType.SelectedItem.Text + " WORKERS REPORT FOR THE MONTH OF " + txtfrom.Text + " - " + txtTo.Text;
                //}

                Response.Write("<tr align='Center'>");

                Response.Write("<td colspan='40'>");
                //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                Response.Write(Salary_Head);
                Response.Write("</td>");
                Response.Write("</tr>");
                Response.Write("<tr align='Center'>");
                Response.Write("<td colspan='40'>");
                Response.Write(category);
                Response.Write("</td>");
                Response.Write("</tr>");

                //}
                Response.Write("</table>");

                Response.Write(stw.ToString());

                //Response.Write("<table border='1'>");
                //Response.Write("<tr Font-Bold='true'>");
                //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //Response.Write("Grand Total");
                //Response.Write("</td>");

                Int32 Grand_Tot_End = 0;

                //Check PF Category

                //if ((txtEmployeeType.SelectedValue == "2"))
                if ((txtEmployeeType.SelectedValue == "4"))
                {
                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='10'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = GvMonthWorker.Rows.Count + 5;

                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");


                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");



                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                }
                else if ((txtEmployeeType.SelectedValue == "1") || (txtEmployeeType.SelectedValue == "2"))
                {
                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='10'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = GvStaffCheck.Rows.Count + 5;

                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");


                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Z6:Z" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AA6:AA" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AB6:AB" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AC6:AC" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AD6:AD" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AE6:AE" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(AF6:AF" + Grand_Tot_End.ToString() + ")</td>");



                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                }
                else if ((txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5"))
                {
                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='9'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    //Grand_Tot_End = GvOthers.Rows.Count + 5;

                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                    //Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");


                    //Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(Y6:Y" + Grand_Tot_End.ToString() + ")</td>");

                }

                //if (Str_PFType.ToString() == "1")
                //{
                //    //PF Employee
                //    if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        Response.Write("<table border='1'>");
                //        Response.Write("<tr Font-Bold='true'>");
                //        Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //        Response.Write("Grand Total");
                //        Response.Write("</td>");

                //        Grand_Tot_End = GVHostel.Rows.Count + 5;
                //    }

                //    if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");

                //    }
                //}
                //else
                //{
                //    //Non PF Employee
                //    if ((txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {
                //        Response.Write("<table border='1'>");
                //        Response.Write("<tr Font-Bold='true'>");
                //        Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                //        Response.Write("Grand Total");
                //        Response.Write("</td>");

                //        Grand_Tot_End = GVCivil.Rows.Count + 5;

                //        Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                //    }
                //    else
                //    {
                //        Response.Write("<table border='1'>");
                //        Response.Write("<tr Font-Bold='true'>");
                //        Response.Write("<td font-Bold='true' align='right' colspan='6'>");
                //        Response.Write("Grand Total");
                //        Response.Write("</td>");

                //        Grand_Tot_End = gvStaffNonPF.Rows.Count + 5;

                //        //Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(I6:I" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(J6:J" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(K6:K" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(L6:L" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(M6:M" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(N6:N" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(O6:O" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(P6:P" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(Q6:Q" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(R6:R" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(S6:S" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(T6:T" + Grand_Tot_End.ToString() + ")</td>");

                //        Response.Write("<td>=sum(U6:U" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(V6:V" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(W6:W" + Grand_Tot_End.ToString() + ")</td>");
                //        Response.Write("<td>=sum(X6:X" + Grand_Tot_End.ToString() + ")</td>");
                //    }



                //    if ((txtEmployeeType.SelectedValue == "2") || (txtEmployeeType.SelectedValue == "3") || (txtEmployeeType.SelectedValue == "4") || (txtEmployeeType.SelectedValue == "5") || (txtEmployeeType.SelectedValue == "12") || (txtEmployeeType.SelectedValue == "13") || (txtEmployeeType.SelectedValue == "10") || (txtEmployeeType.SelectedValue == "11") || (txtEmployeeType.SelectedValue == "9") || (txtEmployeeType.SelectedValue == "6") || (txtEmployeeType.SelectedValue == "7") || (txtEmployeeType.SelectedValue == "8"))
                //    {

                //    }
                //    //want to put grand total


                //}




                Response.Write("</tr></table>");

                Response.Write("</tr>");
                Response.Write("</table>");

                Response.End();
                Response.Clear();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);



                //}
                //bool ErrFlag = false;
                //string CmpName = "";
                //string Cmpaddress = "";
                //int YR = 0;
                //string SalaryType = "";
                //string query = "";
                //string ExemptedStaff = "";
                //string report_head = "";
                //string Basic_Report_Date = "";
                //string Basic_Report_Type = "";

                if (ddlMonths.SelectedItem.Text == "January")
                {
                    YR = Convert.ToInt32(ddlFinance.SelectedValue);
                    YR = YR + 1;
                }
                else if (ddlMonths.SelectedItem.Text == "February")
                {
                    YR = Convert.ToInt32(ddlFinance.SelectedValue);
                    YR = YR + 1;
                }
                else if (ddlMonths.SelectedItem.Text == "March")
                {
                    YR = Convert.ToInt32(ddlFinance.SelectedValue);
                    YR = YR + 1;
                }
                else
                {
                    YR = Convert.ToInt32(ddlFinance.SelectedValue);
                }
                if (rbtnReportType.SelectedValue == "2")
                {
                    if (ddlAgentName.SelectedValue == "-Select-")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                        ErrFlag = true;
                    }
                }
                else
                {
                    if (ddlcategory.SelectedValue == "-Select-")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                        ErrFlag = true;
                    }
                    else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                        ErrFlag = true;
                    }
                }




                if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                    ErrFlag = true;
                }
                //string Other_State = "";
                //string Non_Other_State = "";

                if (!ErrFlag)
                {
                    if (ddlcategory.SelectedValue == "STAFF")
                    {
                        Stafflabour = "STAFF";
                    }
                    else if (ddlcategory.SelectedValue == "LABOUR")
                    {
                        Stafflabour = "LABOUR";
                    }

                    //string constr = ConfigurationManager.AppSettings["ConnectionString"];
                    //SqlConnection con = new SqlConnection(constr);

                    //string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                    //string Payslip_Format_Type = "";

                    if (SessionUserType == "2")
                    {
                        Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                    }
                    else
                    {
                        Payslip_Format_Type = "";
                    }

                    // string Str_PFType = "";
                    if (SessionUserType == "2")
                    {
                        Str_PFType = RdpIFPF.SelectedValue.ToString();
                    }
                    else
                    {
                        Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                    }


                    string Report_Types = rbtnReportType.SelectedValue;
                    string AgentName = ddlAgentName.SelectedItem.Text;


                    // string Str_ChkLeft = "";
                    //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                    Str_ChkLeft = "0";

                    if (txtEmployeeType.SelectedItem.Text == "VOUCHER")
                    {
                        ResponseHelper.Redirect("RptWeekly.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                    }
                    //ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                }

            }
        }
        catch (Exception Ex)
        {

            throw;
        }
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        /* Confirms that an HtmlForm control is rendered for the specified ASP.NET
          server control at run time. */
    }
    public string NumerictoNumber(int Number, bool isUK)
    {
        if (Number == 0)
            return "Zero";

        string and = isUK ? "and " : "";
        if (Number == -2147483648)
            return "Minus Two Billion One Hundred " + and +
        "Forty Seven Million Four Hundred " + and + "Eighty Three Thousand " +
        "Six Hundred " + and + "Forty Eight";

        int[] num = new int[4];
        int first = 0;
        int u, h, t = 0;
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        if (Number < 0)
        {
            sb.Append("Minus");
            Number = -Number;
        }

        string[] words0 = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        string[] words1 = { "Ten ", "Eleven ", "Twelve ", "Thirteen ", "Fourteen ", "Fifteen ", "Sixteen ", "Seventeen ", "Eighteen ", "Nineteen " };
        string[] words2 = { "Twenty ", "Thirty ", "Forty ", "Fifty ", "Sixty ", "Seventy ", "Eighty ", "Ninety " };
        string[] words3 = { "Thousand ", "Lak", "Crore", "Million ", "Billion " };
        num[0] = Number % 1000;
        num[1] = Number / 1000;
        num[2] = Number / 1000000;
        num[1] = num[1] - 1000 * num[2];  // thousands
        num[2] = num[2] - 100 * num[3];//laks
        num[3] = Number / 10000000;     // billions
        num[2] = num[2] - 1000 * num[3];  // millions

        for (int i = 3; i > 0; i--)
        {
            if (num[i] != 0)
            {
                first = i;
                break;
            }
        }
        for (int i = first; i >= 0; i--)
        {
            if (num[i] == 0) continue;
            u = num[i] % 10;
            t = num[i] / 10;
            h = num[i] / 100;
            t = t - 10 * h;

            if (h > 0)
                sb.Append(words0[h] + " Hundred ");
            if (u > 0 || t > 0)
            {
                if (h > 0 || i < first)
                    sb.Append(and);

                if (t == 0)
                    sb.Append(words0[u]);
                else if (t == 1)
                    sb.Append(words1[u]);
                else
                    sb.Append(words2[t - 2] + words0[u]);

            }

            if (i != 0)
                sb.Append(words3[i - 1]);

        }

        return sb.ToString().TrimEnd();



    }
    protected void btnExport_Click(object sender, EventArgs e)
    {

        bool ErrFlag = false;
        string CmpName = "";
        string Cmpaddress = "";
        int YR = 0;
        string SalaryType = "";
        string query = "";
        string ExemptedStaff = "";
        string report_head = "";
        string Basic_Report_Date = "";
        string Basic_Report_Type = "";


        if (ddlMonths.SelectedItem.Text == "January")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "February")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else if (ddlMonths.SelectedItem.Text == "March")
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
            YR = YR + 1;
        }
        else
        {
            YR = Convert.ToInt32(ddlFinance.SelectedValue);
        }

        if (ddlcategory.SelectedValue == "-Select-")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
            ErrFlag = true;
        }
        else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
            ErrFlag = true;
        }

        else if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
            ErrFlag = true;
        }

        if (!ErrFlag)
        {
            try
            {
                //if (RdbCashBank.SelectedValue != "2")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Bank Option');", true);
                //    ErrFlag = true;
                //}
                //else if (txtBankName.SelectedValue == "-Select-")
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Bank Name');", true);
                //    ErrFlag = true;
                //}
                if (!ErrFlag)
                {
                    // query = "Select SalDet.EmpNo,EmpDet.ExistingCode as ExisistingCode,EmpDet.FirstName as EmpName,MstDpt.DeptName as DepartmentNm,EmpDet.Designation,EmpDet.AccountNo," +
                    //                      " SalDet.LOPDays,(SalDet.FFDA) as Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,EmpDet.BankName,EmpDet.BranchCode,EmpDet.IFSC_Code," +
                    //                      " SalDet.GrossEarnings,SalDet.ProvidentFund as PF,SalDet.Advance,SalDet.ESI,(SalDet.ProvidentFund + SalDet.ESI + SalDet.advance) as DedPF,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                    //                      " ((SalDet.GrossEarnings)- (SalDet.ProvidentFund + SalDet.ESI + SalDet.advance)) as NetpayIF,EmpDet.DeptName as Department,SalDet.WorkedDays " +
                    //                      " from Employee_Mst EmpDet inner Join [" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo=SalDet.EmpNo" +
                    //                      " inner Join Department_Mst as MstDpt on MstDpt.DeptCode = EmpDet.DeptCode" +
                    //                      " inner Join MstBank MB on MB.BankName = EmpDet.BankName " +
                    //                      " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                    //                      " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                    //                      " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                    //                      " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                    //                      " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                    //                      " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes') and EmpDet.Salary_Through='2' and EmpDet.Eligible_PF='1' and  EmpDet.BankName='" + txtBankName.SelectedItem.Text + "'";

                    // query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,MstDpt.DeptName,SalDet.NetPay,SalDet.FFDA,EmpDet.BankName,EmpDet.BranchCode,EmpDet.IFSC_Code," +
                    //" EmpDet.DeptName,SalDet.WorkedDays,SalDet.LOPDays,SalDet.Basic_SM,SalDet.BasicAndDANew,SalDet.BasicHRA,EmpDet.Designation,EmpDet.AccountNo,SalDet.New_Cash_Amt,SalDet.New_Bank_Amt,SalDet.New_Tot_Amt," +
                    //" SalDet.GrossEarnings,SalDet.ProvidentFund,SalDet.Advance,SalDet.ESI " +
                    //" Order by EmpDet.ExistingCode Asc";


                    query = "Select SalDet.EmpNo,EmpDet.ExistingCode as EmpCode,EmpDet.FirstName as EmpName,EmpDet.Designation,EmpDet.AccountNo,EmpDet.IFSC_Code,SalDet.NetPay,SalDet.Month" +
                                            " from Employee_Mst EmpDet inner Join[" + SessionPayroll + "]..SalaryDetails SalDet on EmpDet.EmpNo = SalDet.EmpNo" +
                                          " inner Join [" + SessionPayroll + "]..AttenanceDetails as AttnDet on EmpDet.EmpNo=AttnDet.EmpNo and SalDet.Month=AttnDet.Months And AttnDet.EmpNo=SalDet.EmpNo" +
                                          " where SalDet.Month='" + ddlMonths.SelectedItem.Text + "' AND SalDet.FinancialYear='" + ddlFinance.SelectedValue + "' and EmpDet.CatName='" + ddlcategory.SelectedItem.Text + "' and " +
                                          " AttnDet.Months='" + ddlMonths.SelectedItem.Text + "' AND AttnDet.FinancialYear='" + ddlFinance.SelectedValue + "' and " +
                                          " SalDet.Lcode='" + SessionLcode + "' And AttnDet.Lcode='" + SessionLcode + "' And " +
                                          " EmpDet.CompCode='" + SessionCcode + "' and EmpDet.LocCode='" + SessionLcode + "' and EmpDet.Wages='" + txtEmployeeType.SelectedItem.Text + "'" +
                                          " and (convert(datetime,EmpDet.DOR,105) > convert(datetime,'" + txtfrom.Text + "', 105) Or EmpDet.IsActive='Yes')";
                    query = query + "group by SalDet.EmpNo,EmpDet.ExistingCode,EmpDet.FirstName,SalDet.NetPay,EmpDet.IFSC_Code,SalDet.Month,EmpDet.Designation,EmpDet.AccountNo" +
                            " Order by EmpDet.ExistingCode Asc";
                    DataTable dt_1 = new DataTable();
                    dt_1 = objdata.RptEmployeeMultipleDetails(query);

                    Decimal NetGrTot = Convert.ToDecimal(NetPay_Grand_Total.ToString());
                    NetPay_Grand_Total_Words = NumerictoNumber(Convert.ToInt32(NetGrTot), isUK).ToString() + " " + "Only";



                    BankGV.DataSource = dt_1;
                    BankGV.DataBind();



                    string attachment = "attachment;filename=Bank Salary Report.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    //DataTable dt = new DataTable();
                    DataTable dt = new DataTable();
                    query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
                    dt = objdata.RptEmployeeMultipleDetails(query);
                    if (dt.Rows.Count > 0)
                    {
                        CmpName = dt.Rows[0]["Cname"].ToString();
                        Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                    }


                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    BankGV.RenderControl(htextw);

                    Response.Write("<table>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='19'>");
                    Response.Write("RAAJCO SPINNING MILLS - BANK SALARY REPORT");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='19'>");
                    Response.Write("" + SessionLcode + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='19'>");
                    Response.Write("" + Cmpaddress + "");
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    string Salary_Head = "";




                    Response.Write("<tr align='Center'>");
                    Response.Write("<td colspan='19'>");
                    //Response.Write("Salary Month of " + txtfrom.Text + " - " + txtTo.Text);
                    Response.Write(Salary_Head);
                    Response.Write("</td>");
                    Response.Write("</tr>");
                    //}
                    Response.Write("</table>");

                    Response.Write(stw.ToString());

                    //Response.Write("<table border='1'>");
                    //Response.Write("<tr Font-Bold='true'>");
                    //Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    //Response.Write("Grand Total");
                    //Response.Write("</td>");

                    Int32 Grand_Tot_End = 0;



                    Response.Write("<table border='1'>");
                    Response.Write("<tr Font-Bold='true'>");
                    Response.Write("<td font-Bold='true' align='right' colspan='5'>");
                    Response.Write("Grand Total");
                    Response.Write("</td>");

                    Grand_Tot_End = BankGV.Rows.Count + 5;



                    Response.Write("<td>=sum(F6:F" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(G6:G" + Grand_Tot_End.ToString() + ")</td>");
                    //Response.Write("<td>=sum(H6:H" + Grand_Tot_End.ToString() + ")</td>");



                    Response.Write("</tr></table>");

                    Response.Write("</tr>");
                    Response.Write("</table>");

                    Response.End();
                    Response.Clear();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Downloaded Successfully');", true);


                }
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
    protected void gvSalary_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void rbtnReportType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_Report_Type();
    }
    public void Load_Report_Type()
    {
        if (rbtnReportType.SelectedValue == "1")
        {
            Agent_load();
            ddlAgentName.Enabled = false;
            ddlcategory.Enabled = true;
            txtEmployeeType.Enabled = true;
        }
        else
        {
            Load_WagesType();
            ddlAgentName.Enabled = true;
            ddlcategory.Enabled = false;
            txtEmployeeType.Enabled = false;

        }
    }
    protected void btnPayslip_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "2")
            {
                if (ddlAgentName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
            }
            else
            {
                if (ddlcategory.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
                else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                    ErrFlag = true;
                }
            }




            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";

                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }


                string Report_Types = rbtnReportType.SelectedValue;
                string AgentName = ddlAgentName.SelectedItem.Text;


                string Str_ChkLeft = "";
                Str_ChkLeft = rbtIsActive.SelectedValue.ToString();
                //Str_ChkLeft = "";

                if (txtEmployeeType.SelectedItem.Text == "VOUCHER")
                {
                    ResponseHelper.Redirect("RptWeekly.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip_Pdf" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                }
                //ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                else
                {
                    ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&Headyr=" + YR + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }
    protected void btnZeroDaysPayslip_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "2")
            {
                if (ddlAgentName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
            }
            else
            {
                if (ddlcategory.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
                else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                    ErrFlag = true;
                }
            }


            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";

                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }


                string Report_Types = rbtnReportType.SelectedValue;
                string AgentName = ddlAgentName.SelectedItem.Text;


                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

                if (txtEmployeeType.SelectedItem.Text == "VOUCHER")
                {
                    ResponseHelper.Redirect("RptWeekly.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip_Pdf" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                }
                //ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&Salary=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                else
                {
                    ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&fromdate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=Payslip_Zero_Days" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnCoverSlip_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "2")
            {
                if (ddlAgentName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
            }
            else
            {
                if (ddlcategory.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
                else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                    ErrFlag = true;
                }
            }




            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";

                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Report_Types = rbtnReportType.SelectedValue;
                string AgentName = ddlAgentName.SelectedItem.Text;

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

                if (!ErrFlag)
                {
                    ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&AgentName=" + AgentName + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=PayslipCashCover" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    protected void btnPaySlipSample_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            string CmpName = "";
            string Cmpaddress = "";
            int YR = 0;
            string SalaryType = "";
            string query = "";
            string ExemptedStaff = "";
            string report_head = "";
            string Basic_Report_Date = "";
            string Basic_Report_Type = "";

            if (ddlMonths.SelectedItem.Text == "January")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "February")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else if (ddlMonths.SelectedItem.Text == "March")
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
                YR = YR + 1;
            }
            else
            {
                YR = Convert.ToInt32(ddlFinance.SelectedValue);
            }
            if (rbtnReportType.SelectedValue == "2")
            {
                if (ddlAgentName.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
            }
            else
            {
                if (ddlcategory.SelectedValue == "-Select-")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Category');", true);
                    ErrFlag = true;
                }
                else if ((txtEmployeeType.SelectedValue == "") || (txtEmployeeType.SelectedValue == "0"))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Employee Type');", true);
                    ErrFlag = true;
                }
            }




            if ((ddlMonths.SelectedValue == "0") || (ddlMonths.SelectedValue == ""))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "script", "SaveMsgAlert('Select the Months');", true);
                ErrFlag = true;
            }
            string Other_State = "";
            string Non_Other_State = "";

            if (!ErrFlag)
            {
                if (ddlcategory.SelectedValue == "STAFF")
                {
                    Stafflabour = "STAFF";
                }
                else if (ddlcategory.SelectedValue == "LABOUR")
                {
                    Stafflabour = "LABOUR";
                }

                string constr = ConfigurationManager.AppSettings["ConnectionString"];
                SqlConnection con = new SqlConnection(constr);

                string Emp_ESI_Code = "";//ddESI.SelectedValue.ToString();
                string Payslip_Format_Type = "";

                if (SessionUserType == "2")
                {
                    Payslip_Format_Type = rdbPayslipIFFormat.SelectedValue.ToString();
                }
                else
                {
                    Payslip_Format_Type = "";
                }

                string Str_PFType = "";
                if (SessionUserType == "2")
                {
                    Str_PFType = RdpIFPF.SelectedValue.ToString();
                }
                else
                {
                    Str_PFType = RdbPFNonPF.SelectedValue.ToString();
                }

                string Report_Types = rbtnReportType.SelectedValue;
                string AgentName = ddlAgentName.SelectedItem.Text;

                string Str_ChkLeft = "";
                //Str_ChkLeft = ChkLeft.SelectedValue.ToString();
                Str_ChkLeft = "0";

                //if (!ErrFlag)
                //{
                //    ResponseHelper.Redirect("NewViewReport.aspx?Cate=" + Stafflabour + "&Months=" + ddlMonths.SelectedItem.Text + "&ReportFormat=" + Report_Types + "&yr=" + ddlFinance.SelectedValue + "&FromDate=" + txtfrom.Text + "&ToDate=" + txtTo.Text + "&PF_NonPF=" + Str_PFType + "&EmpTypeCd=" + ddlcategory.SelectedItem.Text + "&AttMonths=" + ddlMonths.SelectedItem.Text + "&Finance=" + ddlFinance.SelectedValue + "&Wages=" + txtEmployeeType.SelectedItem.Text + "&Left_Emp=" + Str_ChkLeft.ToString() + "&Report_Type=PayslipSample" + "&OtherState=" + Other_State + "&ExemptedStaff=" + ExemptedStaff + "&NonOtherState=" + Non_Other_State, "_blank", "");
                //}


                string SSQL = "";
                SSQL = "Select SD.ExisistingCode as Excode, EM.FirstName as Name,EM.Lastname as FName,EM.PFNo as Epfno,EM.ESINo as ESIno ";
                SSQL = SSQL + ",EM.AccountNo as BAccno,SD.DeptName as Dept,SD.Designation as Desig,SD.Wages as Wages,Convert(varchar,EM.BirthDate,105) as DOB";
                SSQL = SSQL + ",Convert(varchar,SD.DOJ,105) as Doj,EM.Grade as Grade,SD.CatName";
                SSQL = SSQL + ",(sum(isnull(CAST(Atten.Days as decimal(18,2)),'0'))+sum(isnull(CAST(Atten.CompDays as decimal(18,2)),'0'))+sum(isnull(CAST(Atten.ELDAys as decimal(18,2)),'0'))+sum(isnull(CAST(Atten.CLDays as decimal(18,2)),'0'))) as WorkerDays, SUM(isnull(CAST(SD.Netpay as decimal(18,2)),'0')) as NetPaid,sum(isnull(CAST(Atten.TotalDays as decimal(18,2)),'0')) as MonthDays";
                SSQL = SSQL + ",SUM(isnull(CAST(SD.NFh as decimal(18,2)),'0')) as NFH,sum(isnull(CAST(SD.Weekoff as decimal(18,2)),'0')) as WeekOff,SUM(isnull(CAST(SD.LOPDays as decimal(18,2)),'0')) as Leave,SUM(isnull(CAST(SD.BasicAndDANew as decimal(18,2)),'0')) as EBasicDA";
                SSQL = SSQL + ",sum(isnull(CAST(SD.BasicHRA as decimal(18,2)),'0')) as EHRA,sum(isnull(CAST(SD.WashingAllow as decimal(18,2)),'0')) as EWA,SUM(isnull(CAST(SD.MediAllow as decimal(18,2)),'0')) as EMA";
                SSQL = SSQL + ",sum(isnull(CAST(SD.ConvAllow as decimal(18,2)),'0')) as EConveyA,SUM(isnull(CAST(SD.EduAllow as decimal(18,2)),'0')) as ECEA,SUM(isnull(SD.PfSalary,'0')) as PF";
                SSQL = SSQL + ",sum(isnull(SD.ESI,'0')) as ESI,SUM(isnull(CAST(SD.Messdeduction as decimal(18,2)),'0')) as Mess,sum(isnull(CAST(SD.ProfessionalTax as decimal(18,2)),'0')) as ProfTax";
                SSQL = SSQL + ",SUM(isnull(CAST(SD.deduction1 as decimal(18,2)),'0')) as Stiching,SUM(isnull(CAST(SD.FixBasicAndDA as decimal(18,2)),'0')) as FBasicDA,sum(isnull(CAST(SD.FixBasicHRA as decimal(18,2)),'0')) as FHRA";
                SSQL = SSQL + ",Sum(isnull(CAST(SD.FixConvAllow as decimal(18,2)),'0')) as FConveyA,sum(isnull(CAST(SD.FixEduAllow as decimal(18,2)),'0')) as FCEA,SUM(isnull(CAST(SD.FixMediAllow as decimal(18,2)),'0')) as FMA";
                SSQL = SSQL + ",SUM(isnull(CAST(SD.FixWashingAllow as decimal(18,2)),'0')) as FWA,SUM(isnull(CAST(SD.FixedLTA as decimal(18,2)),'0')) as FixedLTA";

                SSQL = SSQL + " from Employee_Mst EM inner join [" + SessionPayroll + "]..SalaryDetails SD on EM.MachineID=SD.MachineNO";
                SSQL = SSQL + " inner join [" + SessionPayroll + "]..AttenanceDetails Atten on EM.EmpNo=Atten.EmpNo";
                SSQL = SSQL + " where EM.CompCode='" + SessionCcode + "' and EM.LocCode='" + SessionLcode + "' ";
                SSQL = SSQL + " and SD.CCode='" + SessionCcode + "' and SD.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and Atten.CCode='" + SessionCcode + "' and Atten.Lcode='" + SessionLcode + "'";
                SSQL = SSQL + " and SD.Month='" + ddlMonths.SelectedItem.Text + "' and Convert(datetime,SD.FromDate,103)>=Convert(datetime,'" + txtfrom.Text + "',103)";
                SSQL = SSQL + " and Convert(datetime,SD.ToDate,103)<=Convert(datetime,'" + txtTo.Text + "',103)";
                SSQL = SSQL + " and SD.FinancialYear='" + ddlFinance.SelectedValue + "' and EM.CatName='" + ddlcategory.SelectedItem.Text + "' and SD.workedDays>0";
                SSQL = SSQL + " and Atten.Months='" + ddlMonths.SelectedItem.Text + "' and Convert(datetime,Atten.FromDate,103)>=Convert(datetime,'" + txtfrom.Text + "',103)";
                SSQL = SSQL + " and Convert(datetime,Atten.ToDate,103)<=Convert(datetime,'" + txtTo.Text + "',103)";
                SSQL = SSQL + " and Atten.FinancialYear='" + ddlFinance.SelectedValue + "'";
                if ((Str_PFType == "1"))
                {
                    SSQL = SSQL + " and SD.PFEligible != '1' and SD.ESIEligible='1' ";
                }

                if ((Str_PFType == "2"))
                {
                    SSQL = SSQL + " and SD.PFEligible='2' and SD.ESIEligible ='1' ";
                }
                if (Str_PFType == "3")
                {
                    query = query + " and SD.PFEligible='2' and SD.ESIEligible='2' ";
                }
                if (txtEmployeeType.SelectedItem.Text != "-Select-")
                {
                    SSQL = SSQL + " and SD.Wages='" + txtEmployeeType.SelectedItem.Text + "'";
                }
                SSQL = SSQL + " group by SD.ExisistingCode,EM.FirstName,EM.Lastname,EM.PFNo,EM.ESINo,EM.AccountNo,SD.DeptName,SD.Designation";
                SSQL = SSQL + ",SD.Wages,EM.BirthDate,SD.DOJ,EM.Grade,SD.CatName";
                SSQL = SSQL + " order by SD.ExisistingCode asc";
                DataTable dt_cashCover = new DataTable();
                dt_cashCover = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dt_cashCover.Rows.Count > 0)
                {

                    string attachment = "attachment;filename=Payslip Statement.xls";
                    Response.ClearContent();
                    Response.AddHeader("content-disposition", attachment);
                    Response.ContentType = "application/ms-excel";
                    string Cname = "";
                    string Add = "";
                    string regno = "";
                    SSQL = "";
                    SSQL = "Select CompName,(Add1+''+isnull(Add2,'')+'-'+Pincode) as Address,RegNo from Company_Mst where CompCode='" + SessionCcode + "'";
                    DataTable dt_comp = new DataTable();
                    dt_comp = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_comp.Rows.Count > 0)
                    {
                        Cname = dt_comp.Rows[0]["CompName"].ToString();
                        Add = dt_comp.Rows[0]["Address"].ToString();
                        regno = dt_comp.Rows[0]["RegNo"].ToString();
                    }

                    StringWriter stw = new StringWriter();
                    HtmlTextWriter htextw = new HtmlTextWriter(stw);

                    for (int i = 0; i < dt_cashCover.Rows.Count; i++)
                    {
                        Response.Write("<br/>");

                        Response.Write("<table border=1>");

                        Response.Write("</table>");

                    }
                    Response.End();
                    Response.Clear();
                }
            }
        }
        catch (Exception)
        {

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Report Error Please Try Again');", true);
        }
    }
    public void Load_TwoDates()
    {
        string FromDate = "";
        string ToDate = "";
        if (ddlMonths.SelectedValue != "0" && ddlFinance.SelectedValue != "-Select-" && txtEmployeeType.SelectedValue != "-Select-")
        {

            decimal Month_Total_days = 0;
            string Month_Last = "";
            string Year_Last = "0";
            string Temp_Years = "";
            string[] Years;


            Temp_Years = ddlFinance.SelectedItem.Text;
            Years = Temp_Years.Split('-');
            if ((ddlMonths.SelectedValue == "1") || (ddlMonths.SelectedValue == "3") || (ddlMonths.SelectedValue == "5") || (ddlMonths.SelectedValue == "7") || (ddlMonths.SelectedValue == "8") || (ddlMonths.SelectedValue == "10") || (ddlMonths.SelectedValue == "12"))
            {
                Month_Total_days = 31;
            }
            else if ((ddlMonths.SelectedValue == "4") || (ddlMonths.SelectedValue == "6") || (ddlMonths.SelectedValue == "9") || (ddlMonths.SelectedValue == "11"))
            {
                Month_Total_days = 30;
            }

            else if (ddlMonths.SelectedValue == "2")
            {
                int yrs = (Convert.ToInt32(Years[0]) + 1);
                if ((yrs % 4) == 0)
                {
                    Month_Total_days = 29;
                }
                else
                {
                    Month_Total_days = 28;
                }
            }
            switch (ddlMonths.SelectedItem.Text)
            {
                case "January":
                    Month_Last = "01";
                    break;
                case "February":
                    Month_Last = "02";
                    break;
                case "March":
                    Month_Last = "03";
                    break;
                case "April":
                    Month_Last = "04";
                    break;
                case "May":
                    Month_Last = "05";
                    break;
                case "June":
                    Month_Last = "06";
                    break;
                case "July":
                    Month_Last = "07";
                    break;
                case "August":
                    Month_Last = "08";
                    break;
                case "September":
                    Month_Last = "09";
                    break;
                case "October":
                    Month_Last = "10";
                    break;
                case "November":
                    Month_Last = "11";
                    break;
                case "December":
                    Month_Last = "12";
                    break;
                default:
                    break;
            }

            if ((ddlMonths.SelectedValue == "1") || (ddlMonths.SelectedValue == "2") || (ddlMonths.SelectedValue == "3"))
            {
                Year_Last = Years[1];
            }
            else
            {
                Year_Last = Years[0];
            }
            FromDate = "01" + "/" + Month_Last + "/" + Year_Last;
            ToDate = Month_Total_days + "/" + Month_Last + "/" + Year_Last;

            txtfrom.Text = FromDate.ToString();
            txtTo.Text = ToDate.ToString();
        }
    }

    protected void txtEmployeeType_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }

    protected void ddlFinance_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }

    protected void ddlMonths_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_TwoDates();
    }
}
