﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Payroll;
using Payroll.Configuration;
using Payroll.Data;
using Altius.BusinessAccessLayer.BALDataAccess;

public partial class AdvanceRePayment : System.Web.UI.Page
{
    protected System.Resources.ResourceManager rm;
    protected string PayrollRegisterContentMeta, pageTitle;
    BALDataAccess objdata = new BALDataAccess();
    AdvanceAmount objsal = new AdvanceAmount();
    //bool searchFlag = false;
    string Datetime_ser;
    DateTime mydate;
    //string Mont;
    //int Monthid;
    string SessionAdmin;
    static string id = "";
    static string inc_Month;
    static string dec_month;
    string Query;
    string MyMonth;
    string IncrementMonths;
    string decrementMonths;
    string SessionCcode;
    string SessionLcode;
    protected void Page_Load(object sender, EventArgs e)
    {
        SessionAdmin = Session["Isadmin"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
       
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        if (!IsPostBack)
        {
            SettingServerDate();
            Load_WagesCode();
            ddlWagesType_SelectedIndexChanged(sender, e);

        }
      
    }
    public void Load_WagesCode()
    {
        DataTable dtdsupp = new DataTable();
        ddlWagesType.Items.Clear();
        Query = "Select EmpTypeCd,EmpType from MstEmployeeType  ";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlWagesType.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["EmpType"] = "-Select-";
        dr["EmpTypeCd"] = "0";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlWagesType.DataTextField = "EmpType";
        ddlWagesType.DataValueField = "EmpTypeCd";
        ddlWagesType.DataBind();
    }
    protected void ddlWagesType_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dtdsupp = new DataTable();
        ddlExistingCode.Items.Clear();
        Query = "Select ExistingCode from Employee_Mst where Wages='" + ddlWagesType.SelectedItem.Text + "' and CompCode='" + SessionCcode + "' and LocCode='" + SessionLcode + "' And IsActive='Yes'";
        dtdsupp = objdata.RptEmployeeMultipleDetails(Query);
        ddlExistingCode.DataSource = dtdsupp;
        DataRow dr = dtdsupp.NewRow();
        dr["ExistingCode"] = "-Select-";
        dr["ExistingCode"] = "-Select-";
        dtdsupp.Rows.InsertAt(dr, 0);
        ddlExistingCode.DataTextField = "ExistingCode";
        ddlExistingCode.DataValueField = "ExistingCode";
        ddlExistingCode.DataBind();
    }
    protected void ddlExistingCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtTokenNo.Text = "";
        bool ErrFlag = false;
        clear();
        if (ddlExistingCode.SelectedValue == "0")
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Existing Code....!');", true);
            //System.Windows.Forms.MessageBox.Show("Select the Employee Code....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
            ErrFlag = true;
        }
        if (!ErrFlag)
        {
            DataTable dt = new DataTable();

          Query= "Select ED.EmpNo,ED.ExistingCode,ED.FirstName ,ED.Designation,Convert(varchar,ED.DOJ,105) as Dateofjoining,";
          Query = Query + " ED.BaseSalary,ED.DeptName  from Employee_Mst ED  ";
          Query =Query + " where ED.ExistingCode ='"+ddlExistingCode.SelectedItem.Text +"' and ED.IsActive='Yes' and ED.Compcode='"+SessionCcode +"' and ED.Loccode='"+SessionLcode +"' ";
          dt = objdata.RptEmployeeMultipleDetails(Query);
            if (dt.Rows.Count > 0)
            {
                txtEmpName.Text = dt.Rows[0]["FirstName"].ToString();
                txtTokenNo.Text = dt.Rows[0]["EmpNo"].ToString();
                txtDesignation.Text = dt.Rows[0]["Designation"].ToString();
                DataTable dtrepay = new DataTable();
                Query = " Select EmpNo,ExistNo,BalanceAmount,MonthlyDeduction,ID,IncreaseMonth,ReductionMonth";
                Query = Query + " from [Raajco_Epay].. AdvancePayment where ExistNo ='"+ddlExistingCode.SelectedItem.Text+"' and";
                Query = Query + " Completed='N' and Ccode='"+SessionCcode +"' and Lcode='"+SessionLcode +"' ";
                dtrepay = objdata.RptEmployeeMultipleDetails(Query);
                {
                    if (dtrepay.Rows.Count > 0)
                    {
                        txtBalanceAmt.Text = dtrepay.Rows[0]["BalanceAmount"].ToString();
                        id = dtrepay.Rows[0]["ID"].ToString();
                        inc_Month = dtrepay.Rows[0]["IncreaseMonth"].ToString();
                        dec_month = dtrepay.Rows[0]["ReductionMonth"].ToString();
                        if (Convert.ToDecimal(txtBalanceAmt.Text) < Convert.ToDecimal(dtrepay.Rows[0]["MonthlyDeduction"].ToString()))
                        {
                            txtAmount.Text = txtBalanceAmt.Text;
                        }
                        else
                        {
                            txtAmount.Text = dtrepay.Rows[0]["MonthlyDeduction"].ToString();
                        }


                    }
                    else
                    {
                        txtTokenNo.Text = "";
                        txtDesignation.Text = "";
                        txtAmount.Text = "";
                        id = "";
                        //lblAmt.Text = "";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Employee Not Found....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Employee Not Found....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    }
                }
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            bool ErrFlag = false;
            bool SaveFlag = false;
            if (ddlExistingCode.SelectedValue == "0")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Select the Employee No....!');", true);
                //System.Windows.Forms.MessageBox.Show("Select the Employee No....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
           
            else if (txtAmount.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                ErrFlag = true;
            }
            if (!ErrFlag)
            {
                if (Convert.ToDecimal(txtBalanceAmt.Text) < Convert.ToDecimal(txtAmount.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (Convert.ToDecimal(txtAmount.Text) == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Enter the Amount Properly....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Enter the Amount Properly....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    ErrFlag = true;
                }
                else if (dec_month == "1")
                {
                    if ((Convert.ToDecimal(txtAmount.Text) != Convert.ToDecimal(txtBalanceAmt.Text)))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Pay the full Amount....!');", true);
                        //System.Windows.Forms.MessageBox.Show("Pay the full Amount....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                        ErrFlag = true;
                    }
                }
                if (!ErrFlag)
                {
                    string Completed = "";
                    mydate = Convert.ToDateTime(txtdate.Text);

                    int Mont = Convert.ToInt32(mydate.Month);
                    #region MonthName
                    DateTime Md = new DateTime();


                    switch (Mont)
                    {
                        case 1:
                            MyMonth = "January";
                            break;

                        case 2:
                            MyMonth = "February";
                            break;
                        case 3:
                            MyMonth = "March";
                            break;
                        case 4:
                            MyMonth = "April";
                            break;
                        case 5:
                            MyMonth = "May";
                            break;
                        case 6:
                            MyMonth = "June";
                            break;
                        case 7:
                            MyMonth = "July";
                            break;
                        case 8:
                            MyMonth = "August";
                            break;
                        case 9:
                            MyMonth = "September";
                            break;
                        case 10:
                            MyMonth = "October";
                            break;
                        case 11:
                            MyMonth = "November";
                            break;
                        case 12:
                            MyMonth = "December";
                            break;
                        default:
                            break;
                    }
                    #endregion
                    IncrementMonths = (Convert.ToInt32(inc_Month) + 1).ToString();
                    decrementMonths = (Convert.ToDecimal(dec_month) - 1).ToString();
                    string bal = (Convert.ToDecimal(txtBalanceAmt.Text) - Convert.ToDecimal(txtAmount.Text)).ToString();
                    if (Convert.ToDecimal(txtAmount.Text) == Convert.ToDecimal(txtBalanceAmt.Text))
                    {
                        Completed = "Y";

                      Query="Insert Into [Raajco_Epay].. Advancerepayment (EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode) ";
                      Query = Query + " values ('" + txtTokenNo.Text + "',convert(datetime,'" + mydate + "',105),";
                      Query = Query +  "'"+ MyMonth +"','"+txtAmount.Text +"','"+id+"','"+SessionCcode +"','"+SessionLcode +"')";
                      objdata.RptEmployeeMultipleDetails(Query);

                      Query = "  Update [Raajco_Epay]..AdvancePayment Set BalanceAmount='" + bal + "',";
                         Query = Query + " IncreaseMonth='"+IncrementMonths+"',ReductionMonth='"+decrementMonths+"', ";
                         Query = Query + " Completed='"+Completed+"',Modifieddate=convert(datetime,'"+mydate+"',105)";
                         Query = Query + " where ID='"+id+"' and EmpNo='"+txtTokenNo.Text+"' and Ccode='"+SessionCcode +"' and Lcode='"+SessionLcode+"'";
                         objdata.RptEmployeeMultipleDetails(Query);

                       SaveFlag = true;
                    }
                    else
                    {
                        Completed = "N";
                        Query = "Insert Into [Raajco_Epay].. Advancerepayment (EmpNo,TransDate,Months,Amount,AdvanceId,Ccode,Lcode) ";
                        Query = Query + " values ('" + txtTokenNo.Text + "',convert(datetime,'" + mydate + "',105),";
                        Query = Query + "'" + MyMonth + "','" + txtAmount.Text + "','" + id + "','" + SessionCcode + "','" + SessionLcode + "')";
                        objdata.RptEmployeeMultipleDetails(Query);

                        Query = "  Update [Raajco_Epay]..AdvancePayment Set BalanceAmount='" + bal + "',";
                        Query = Query + " IncreaseMonth='" + IncrementMonths + "',ReductionMonth='" + decrementMonths + "', ";
                        Query = Query + " Completed='" + Completed + "',Modifieddate=convert(datetime,'" + mydate + "',105)";
                        Query = Query + " where ID='" + id + "' and EmpNo='" + txtTokenNo.Text + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                        objdata.RptEmployeeMultipleDetails(Query); 
                        
                        SaveFlag = true;
                    }
                }
                if (SaveFlag == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Saved Successfully....!');", true);
                    //System.Windows.Forms.MessageBox.Show("Saved Successfully....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
                    clear();
                    DataTable dtempty = new DataTable();
                    ddlExistingCode.DataSource = dtempty;
                    txtEmpName.Text = "";
                    txtTokenNo.Text = "";
                    
                }
            }
        }
        catch (Exception ex)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('Contact Admin....!');", true);
            //System.Windows.Forms.MessageBox.Show("Contact Admin....!", "Altius InfoSystems", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Warning);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        clear();
        txtTokenNo.Text = "";
        DataTable dtempty = new DataTable();
        ddlExistingCode.DataSource = dtempty;
        ddlExistingCode.DataBind();
        txtEmpName.Text = "";
    }
    public void SettingServerDate()
    {
        Datetime_ser = DateTime.Now.ToString("dd/MM/yyyy");
        
        txtdate.Text = Datetime_ser;
    }
    public void clear()
    {
        txtAmount.Text = "";
        txtdate.Text = "";
        txtBalanceAmt.Text = "";
        txtDesignation.Text = "";
        
        SettingServerDate();
    }

}
