﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for Classs1
/// </summary>

public class Classs1
{
    public zkemkeeper.CZKEMClass axCZKEM1 = new zkemkeeper.CZKEMClass();
    private bool bIsConnected = false;//the boolean value identifies whether the device is connected
    private int iMachineNumber = 1;//t

	public Classs1()
	{
		

	}
    public Boolean Connection(string IPAddress1, string Port_No1)
    {
        IP_Connect(IPAddress1, Port_No1);
        return bIsConnected;
    }

    public void IP_Connect(string IPAddress, string Port_No)
    {
        bIsConnected = axCZKEM1.Connect_Net(IPAddress, Convert.ToInt32(Port_No));
        iMachineNumber = 1;//In fact,when you are using the tcp/ip communication,this parameter will be ignored,that is any integer will all right.Here we use 1.
        if (axCZKEM1.RegEvent(iMachineNumber, 65535))//Here you can register the realtime events that you want to be triggered(the parameters 65535 means registering all)
        {
            this.axCZKEM1.OnAttTransactionEx += new zkemkeeper._IZKEMEvents_OnAttTransactionExEventHandler(axCZKEM1_OnAttTransactionEx);

        }
    }

    public void axCZKEM1_OnAttTransactionEx(string sEnrollNumber, int iIsInValid, int iAttState, int iVerifyMethod, int iYear, int iMonth, int iDay, int iHour, int iMinute, int iSecond, int iWorkCode)
    {
        string Machine_ID_No = sEnrollNumber.ToString();
        string Punch_Time = iYear.ToString() + "-" + iMonth.ToString() + "-" + iDay.ToString() + " " + iHour.ToString() + ":" + iMinute.ToString() + ":" + iSecond.ToString();
        string connetionString;

    }
}
