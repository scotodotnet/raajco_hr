﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

/// <summary>
/// Summary description for ContractClass
/// </summary>
public class ContractClass
{
	public ContractClass()
	{
		//
		// TODO: Add constructor logic here
		//
	}
    private string _contractName;
    private string _contracttype;
    private string _ContractYr;
    private string _ContractMonth;
    private string _ContractDays;
    private string _FixedDays;
    private string _yrDays;
    private string _calculteDays;

    public string ContractName
    {
        get { return _contractName; }
        set { _contractName = value; }
    }
    public string ContractType
    {
        get { return _contracttype; }
        set { _contracttype = value; }
    }
    public string ContractYear
    {
        get { return _ContractYr; }
        set { _ContractYr = value; }
    }
    public string ContractMonth
    {
        get { return _ContractMonth; }
        set { _ContractMonth = value; }
    }
    public string ContractDays
    {
        get { return _ContractDays; }
        set { _ContractDays = value; }
    }
    public string FixedDays
    {
        get { return _FixedDays; }
        set { _FixedDays = value; }
    }
    public string YrDays
    {
        get { return _yrDays; }
        set { _yrDays = value; }
    }
    public string PlannedDays
    {
        get { return _calculteDays; }
        set { _calculteDays = value; }
    }
    
}
