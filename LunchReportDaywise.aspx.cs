﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using System.IO;

public partial class LunchReportDaywise : System.Web.UI.Page
{

    string ModeType = "";
    string ShiftType1 = "";
    string Date = "";
    string ddlShiftType = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = ""; string status = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    DateTime ShiftdateStartIN;
    DateTime ShiftdateEndIN;
    DateTime InTime_Check;
    DateTime InToTime_Check;
    DateTime EmpdateIN;

    DataTable mEmployeeDS = new DataTable();
    DataTable mLocalDS_OUTTAB = new DataTable();
    DataTable mLocalDS_INTAB = new DataTable();
    DataTable DS_InTime = new DataTable();
    DataTable DS_Time = new DataTable();
    string SSQL = "";
    string Final_InTime;
    string mIpAddress_IN;
    string mIpAddress_OUT;
    string Time_IN_Str;
    string Time_OUT_Str;
    string Date_Value_Str;
    string Shift_Start_Time;
    string Shift_End_Time;
    string Final_Shift;
    string Total_Time_get;
    string[] Time_Minus_Value_Check;
    string[] OThour_Str;
    DataSet ds = new DataSet();
    int time_Check_dbl;
    DataTable mdatatable = new DataTable();
    DataTable LocalDT = new DataTable();
    DataTable Shift_DS = new DataTable();
    DataTable Comp_dt = new DataTable();
    DataTable Loc_dt = new DataTable();
    string Locname = "";
    string Compname = "";

    TimeSpan InTime_TimeSpan;
    string From_Time_Str = "";
    string To_Time_Str = "";
    int OT_Hour;
    int OT_Min;
    int OT_Time;
    string OT_Hour1 = "";
    string mUser = "";
    string Datestr = "";
    string Datestr1 = "";
    string Division = "";

    Boolean Shift_Check_blb = false;
    public string Left_Val(string Value, int Length)
    {
        if (Value.Length >= Length)
        {
            return Value.Substring(0, Length);
        }
        else
        {
            return Value;
        }
    }


    protected void Page_Load(object sender, EventArgs e)
    {
         if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {

                Page.Title = "Spay Module | Report-Lunch Report Day Wise";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Dashboard"));
                //li.Attributes.Add("class", "droplink active open");
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
          //SessionCompanyName = Session["CompanyName"].ToString();
          //  SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();

            //ModeType = Request.QueryString["ModeType"].ToString();
            ShiftType1 = Request.QueryString["ShiftType1"].ToString();
            Date = Request.QueryString["Date"].ToString();
            Division=Request.QueryString["Division"].ToString();
            status = Request.QueryString["status"].ToString();
            //if (SessionUserType == "1")
            //{
            //    AdminGetAttdDayWise_Change();
            //}
            if (SessionUserType == "2")
            {
                NonAdminGetAttdDayWise_Change();
            }
            else
            {
                DataSet ds = new DataSet();
                DataTable AutoDTable = new DataTable();


                AutoDTable.Columns.Add("SNo");
                AutoDTable.Columns.Add("Dept");
                AutoDTable.Columns.Add("Type");
                AutoDTable.Columns.Add("Shift");
                AutoDTable.Columns.Add("EmpCode");
                AutoDTable.Columns.Add("ExCode");
                AutoDTable.Columns.Add("Name");
                AutoDTable.Columns.Add("TimeIN");
                AutoDTable.Columns.Add("TimeOUT");
                AutoDTable.Columns.Add("MachineID");
                AutoDTable.Columns.Add("Category");
                AutoDTable.Columns.Add("SubCategory");
                AutoDTable.Columns.Add("TotalMIN");
                AutoDTable.Columns.Add("GrandTOT");
                AutoDTable.Columns.Add("ShiftDate");
                AutoDTable.Columns.Add("OTHour");
                AutoDTable.Columns.Add("CompanyName");
                AutoDTable.Columns.Add("LocationName");
                AutoDTable.Columns.Add("Desgination");
                AutoDTable.Columns.Add("MachineIDNew");
                AutoDTable.Columns.Add("Status");
                AutoDTable.Columns.Add("TimeIN2");
                AutoDTable.Columns.Add("TimeOUT2");
                AutoDTable.Columns.Add("GrandTOT2");

                //if (mReportName.Text == "DAY ATTENDANCE - DAY WISE :: BELOW 4 HOURS")
                //{
                DataTable dtIPaddress = new DataTable();
                SSQL="Select * from IPAddress_Mst where CompCode='"+SessionCcode.ToString()+"' and LocCode='"+SessionLcode.ToString()+"' ";
                dtIPaddress = objdata.RptEmployeeMultipleDetails(SSQL);

                if (dtIPaddress.Rows.Count > 0)
                {
                    for (int i = 0; i < dtIPaddress.Rows.Count; i++)
                    {
                        if (dtIPaddress.Rows[i]["IPMode"].ToString() == "LUNCH IN")
                        {
                            mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                        }
                        else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "LUNCH OUT")
                        {
                            mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                        }
                    }
                }

                SSQL = "";
                SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
                SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
                SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
                SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
                if (ShiftType1 != "ALL")
                {
                    SSQL = SSQL + " And shiftDesc='" + ShiftType1 + "'";

                }
              
                SSQL = SSQL + " Order By shiftDesc";
                LocalDT = objdata.RptEmployeeMultipleDetails(SSQL);


                string ShiftType;
                string ng = string.Format(Date, "MM-dd-yyyy");
                Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
                Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
                DateTime date1 = Convert.ToDateTime(ng);
                DateTime date2 = date1.AddDays(1);

                //if (LocalDT.Rows.Count > 0)
                //{
                //   
                //    string Start_IN = "";
                //    string End_In;

                //    string End_In_Days = "";
                //    string Start_In_Days = "";

                //    int mStartINRow = 0;
                //    int mStartOUTRow = 0;

                //    for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
                //    {

                        //if (AutoDTable.Rows.Count <= 1)
                        //{
                        //    mStartOUTRow = 0;
                        //}
                        //else
                        //{
                        //    mStartOUTRow = AutoDTable.Rows.Count - 1;
                        //}

                        //if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                        //{
                        //    ShiftType = "GENERAL";
                        //}
                        //else
                        //{
                        //    ShiftType = "SHIFT";
                        //}


                        //string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
                        //double sINdays = Convert.ToDouble(sIndays_str);
                        //string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
                        //double eINdays = Convert.ToDouble(eIndays_str);
                        string ss = "";

                        //if (ShiftType == "GENERAL")
                        //{

                            //edited here
                            ss = "select LT.MachineID,Min(LT.TimeOUT) as [TimeOUT] from LogTimeLunch_OUT LT inner join ";
                            if (status == "Approval")
                            {
                                ss = ss + "Employee_Mst";
                            }
                            else
                            {
                                ss = ss + "Employee_Mst_New_Emp";
                            }
                            ss = ss + " EM on EM.MachineID_Encrypt=LT.MachineID";
                            ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                            ss = ss + "And LT.TimeOUT >='" + date1.ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                            ss = ss + "And LT.TimeOUT <='" + date2.ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                           // ss = ss + "AND EM.ShiftType='" + ShiftType + "'";
                         
                            if (Division != "-Select-")
                            {
                               ss = ss + " and EM.Division='"+Division+"' ";
                            }
                            ss = ss + " Group By LT.MachineID Order By Min(LT.TimeOUT)";
                        //}
                        //    //And EM.IsActive='yes'

                        //else if (ShiftType == "SHIFT")
                        //{




                        //    ss = "select LT.MachineID,Min(LT.TimeOUT) as [TimeOUT] from LogTimeLunch_OUT LT inner join ";
                        //    if (status == "Approval")
                        //    {
                        //        ss = ss + "Employee_Mst";
                        //    }
                        //    else
                        //    {
                        //        ss = ss + "Employee_Mst_New_Emp";
                        //    }
                        //    ss = ss + " EM on EM.MachineID_Encrypt=LT.MachineID";
                        //    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                        //    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                        //    ss = ss + "And LT.TimeOUT >='" + date1.ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        //    ss = ss + "And LT.TimeOUT <='" + date2.ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        //    if (Division != "-Select-")
                        //    {
                        //        ss = ss + " and EM.Division='" + Division + "' ";
                        //    }
                        //    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeOUT)";
                        //    //And EM.IsActive='yes'
                        //}
                        //else
                        //{
                        //    ss = "select LT.MachineID,Min(LT.TimeOUT) as [TimeOUT] from LogTimeLunch_OUT LT inner join ";
                        //    if (status == "Approval")
                        //    {
                        //        ss = ss + "Employee_Mst";
                        //    }
                        //    else
                        //    {
                        //        ss = ss + "Employee_Mst_New_Emp";
                        //    }
                        //    ss = ss + " EM on EM.MachineID_Encrypt=LT.MachineID";
                        //    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                        //    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                        //    ss = ss + "And LT.TimeOUT >='" + date1.ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        //    ss = ss + "And LT.TimeOUT <='" + date2.ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        //    ss = ss + " AND EM.ShiftType='" + ShiftType + "' ";
                         
                        //    if (Division != "-Select-")
                        //    {
                        //        ss = ss + " and EM.Division='" + Division + "' ";
                        //    }
                        //    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeOUT)";
                        //}
                         //And EM.IsActive='yes'

                        mdatatable = objdata.RptEmployeeMultipleDetails(ss);

                        SSQL = "Select * from Company_Mst ";
                        Comp_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                         Compname = Comp_dt.Rows[0]["CompName"].ToString();

                        SSQL = "Select * from Location_Mst  where CompCode='" + SessionCcode.ToString() + "'and LocCode ='" + SessionLcode.ToString() + "' ";
                        Loc_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                        Locname = Loc_dt.Rows[0]["LocName"].ToString();
                        if (mdatatable.Rows.Count > 0)
                        {
                            string MachineID;

                            for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                            {
                                Boolean chkduplicate = false;
                                chkduplicate = false;
                                string id = ""; //mdatatable.Rows[iRow]["MachineID"].ToString();
                                id = UTF8Decryption(mdatatable.Rows[iRow]["MachineID"].ToString());
                                for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                                {
                                   
                                    //string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                                    if (id == AutoDTable.Rows[ia][9].ToString())
                                    {
                                        chkduplicate = true;
                                    }
                                }
                                if (chkduplicate == false)
                                {

                                   
 
                                    AutoDTable.NewRow();
                                    AutoDTable.Rows.Add();


                                    // AutoDTable.Rows.Add();

                                    // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                                    AutoDTable.Rows[iRow][3] = "";
                                    AutoDTable.Rows[iRow][16] = Compname.ToString();
                                    AutoDTable.Rows[iRow][17] = Locname.ToString();
                                    AutoDTable.Rows[iRow][14] = date1.ToString("dd/MM/yyyy");
                                    //if (ShiftType == "SHIFT")
                                    //{
                                    //    string str = mdatatable.Rows[iRow][1].ToString();
                                    //    // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                    //    AutoDTable.Rows[iRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeOUT"]);
                                    //}
                                    //else
                                    //{
                                        AutoDTable.Rows[iRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeOUT"]);
                                   // }


                                    MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                                    ss = UTF8Decryption(MachineID.ToString());

                                    //AutoDTable.Rows[iRow][9] = ss.ToString();


                                    AutoDTable.Rows[iRow][9] = ss.ToString();

                                    AutoDTable.Rows[iRow][19] = MachineID.ToString();
                                    //mStartINRow += 1;
                                }
                            }
                        }

                        DataTable nnDatatable = new DataTable();

                        SSQL = "";
                        SSQL = "Select MachineID,Max(TimeIN) as [TimeIN] from LogTimeLunch_IN  Where Compcode='" + SessionCcode + "'";
                        SSQL = SSQL + " and LocCode = ' " + SessionLcode + "'";
                        SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                        SSQL = SSQL + " Group By MachineID";
                        SSQL = SSQL + " Order By Max(TimeIN)";

                        mdatatable = objdata.RptEmployeeMultipleDetails(SSQL);


                        string InMachine_IP;
                        DataTable mLocalDS_out = new DataTable();

                        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                        {
                            string mach = AutoDTable.Rows[iRow2][19].ToString();

                            InMachine_IP = mach.ToString();

                            SSQL = "Select MachineID,TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00" + "'";
                            SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00" + "' Order by TimeIN desc";

                            mLocalDS_out = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (mLocalDS_out.Rows.Count <= 0)
                            {

                            }
                            else
                            {
                                if (AutoDTable.Rows[iRow2][19].ToString() == mLocalDS_out.Rows[0][0].ToString())
                                {
                                    AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                                }

                            }
                            // Above coddings are correct:

                            Time_IN_Str = "";
                            Time_OUT_Str = "";
                            Date_Value_Str = string.Format(Date, "yyyy/MM/dd");

                            if (InMachine_IP == "MTAxMjg=")
                            {
                                InMachine_IP = "MTAxMjg=";
                            }
                            SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00' Order by TimeOUT ASC";



                            mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                            //if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1" || AutoDTable.Rows[iRow2][3].ToString() == "SHIFT2")
                            //{
                                
                            //    InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                            //    InToTime_Check = InTime_Check.AddHours(2);

                            //    string s1 = InTime_Check.ToString("HH:mm:ss");
                            //    string s2 = InToTime_Check.ToString("HH:mm:ss");
                                
                            //    string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                            //    string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                            //    InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                            //    From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                            //    InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                            //    To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;

                        

                            //    SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                            //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            //    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                            //    SSQL = SSQL + " And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeIN Asc";

                            //    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                                //if (DS_Time.Rows.Count != 0)
                                //{
                                //    SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + InMachine_IP + "'";
                                //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                //    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeOUT ASC";


                                //    DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);

                                    //if (DS_InTime.Rows.Count != 0)
                                    //{
                                    //    Final_InTime = DS_InTime.Rows[0][0].ToString();
                                    //    SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                    //    Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                    //    Shift_Check_blb = false;



                                        //for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                        //{
                                        //    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                        //    int b = Convert.ToInt16(a.ToString());
                                        //    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                        //    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                        //    int b1 = Convert.ToInt16(a1.ToString());
                                        //    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                        //    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                        //    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                        //    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                        //    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                        //    {
                                        //        Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                        //        Shift_Check_blb = true;
                                                
                                        //    }
                                        //}

                                        //DayOfWeek day = date1.DayOfWeek;
                                        //string WeekofDay = day.ToString();

                                        //if (Shift_Check_blb == true)
                                        //{
                            //if (Final_Shift.ToString() == "")
                            //{
                                AutoDTable.Rows[iRow2][3] = "";
                            //}
                            //else
                            //{
                            //    AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                            //}
                            
                                            AutoDTable.Rows[iRow2][16] = "";
                                            AutoDTable.Rows[iRow2][17] = "";
                                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                                            //AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", Final_InTime);

                                           

                                        //    SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + InMachine_IP + "'";
                                        //        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        //        SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                        //        mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                                        //        SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                                        //        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        //        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeIN Asc";

                                        //        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                        //    if (DS_Time.Rows.Count != 0)
                                        //    {
                                        //        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                        //    }
                                        //}

                                        //else
                                        //{
                                           
                                        //    SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                                        //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                        //    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeIN Asc";
                                        //    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                                        //    if (DS_Time.Rows.Count != 0)
                                        //    {
                                        //        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                        //    }

                                        //}
                            //        }
                            //        else
                            //        {
                            //            SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                            //            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            //            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeIN Asc";
                            //            DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                            //            if (DS_Time.Rows.Count != 0)
                            //            {
                            //                AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

                            //            }
                            //        }
                            //    }
                            //    else
                            //    {
                                    
                            //        SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                            //        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            //        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeIN ASC";
                            //    }
                            //}


                            //else
                            //{
                            //    SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                            //    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            //    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeIN ASC";
                            //}

                                            SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "07:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "07:00' Order by TimeIN ASC";
                                           

                            mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);
                            String Emp_Total_Work_Time_1 = "00:00";
                            if (mLocalDS_INTAB.Rows.Count > 1)
                            {
                        
                            for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                                {
                          
                                //Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                                //if (mLocalDS_OUTTAB.Rows.Count > tin)
                                //{
                                //    Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                                //}
                                //else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                                //{
                                //    Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                                //}
                                //else
                                //{
                                //    Time_OUT_Str = "";
                                //}


                                if (mLocalDS_INTAB.Rows.Count == mLocalDS_OUTTAB.Rows.Count)
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();

                                if (tin == 0)
                                {
                                    string[] timeIN = Time_IN_Str.Split();
                                    string timeIN1 = timeIN[1].ToString();
                                    string[] sptTimeIN = timeIN1.Split(':');
                                    string FinTimein = (sptTimeIN[0].ToString() + ":" + sptTimeIN[1].ToString() + " " + timeIN[2].ToString()).ToString();

                                    string[] TimeOUT = Time_OUT_Str.Split();
                                    string timeOUT1 = TimeOUT[1].ToString();
                                    string[] sptTimeOUT = timeOUT1.Split(':');
                                    string FinTimeOUT = (sptTimeOUT[0].ToString() + ":" + sptTimeOUT[1].ToString() + " " + TimeOUT[2].ToString()).ToString();


                                    //AutoDTable.Rows[0][7] = mLocalDS_INTAB.Rows[tin][0].ToString();
                                    AutoDTable.Rows[iRow2][7] = FinTimein;
                                    //AutoDTable.Rows[0][8] = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                                    AutoDTable.Rows[iRow2][8] = FinTimeOUT;
                                }
                                if (tin == 1)
                                {
                                   
                                        string[] timeIN = Time_IN_Str.Split();
                                        string timeIN1 = timeIN[1].ToString();
                                        string[] sptTimeIN = timeIN1.Split(':');
                                        string FinTimein = (sptTimeIN[0].ToString() + ":" + sptTimeIN[1].ToString() + " " + timeIN[2].ToString()).ToString();

                                        string[] TimeOUT = Time_OUT_Str.Split();
                                        string timeOUT1 = TimeOUT[1].ToString();
                                        string[] sptTimeOUT = timeOUT1.Split(':');
                                        string FinTimeOUT = (sptTimeOUT[0].ToString() + ":" + sptTimeOUT[1].ToString() + " " + TimeOUT[2].ToString()).ToString();

                                        //AutoDTable.Rows[0]["TimeIN2"] = FinTimein;
                                        //AutoDTable.Rows[0]["TimeOUT2"] = FinTimeOUT;
                                        AutoDTable.Rows[iRow2]["TimeIN2"] = FinTimein;
                                        AutoDTable.Rows[iRow2]["TimeOUT2"] = FinTimeOUT;
                                        //AutoDTable.Rows]["TimeIN2"] = FinTimein;
                                        //AutoDTable.Rows[0]["TimeOUT2"] = FinTimeOUT;


                                    }
                                    //  if(tin)
                                }


                                TimeSpan ts4;
                                ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (LocalDT.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                                }

                                if (Time_IN_Str == "" || Time_OUT_Str == "")
                                {
                                    time_Check_dbl = time_Check_dbl;
                                }
                                else
                                {
                                    DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                    TimeSpan ts1;
                                    ts1 = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();
                                    // Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                    string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                        ts1 = date4.Subtract(date3);
                                        ts1 = date4.Subtract(date3);
                                        ts4 = ts4.Add(ts1);
                                        Total_Time_get = ts1.Hours.ToString();
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {

                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {

                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                    }
                                    else
                                    {

                                        //ts4 = ts4.Add(ts1);
                                        //Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Emp_Total_Work_Time_1 = ts1.Hours + ":" + ts1.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                            time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        }
                                    }
                                    TimeSpan t1 = new TimeSpan();
                                    ts1 = t1.Add(t1);
                                    ts4 = t1.Add(t1);
                                }

                                //  AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                                if (tin == 0)
                                {
                                    // AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                                    AutoDTable.Rows[iRow2]["GrandTOT"] = Emp_Total_Work_Time_1;
                                }
                                if (tin == 1)
                                {
                                    AutoDTable.Rows[iRow2]["GrandTOT2"] = Emp_Total_Work_Time_1;
                                    //AutoDTable.Rows[iRow2][22] = Emp_Total_Work_Time_1;
                                }
                                OThour_Str = Emp_Total_Work_Time_1.Split(':');
                                OT_Hour = Convert.ToInt32(OThour_Str[0]);
                                OT_Min = Convert.ToInt32(OThour_Str[1]);
                                if (OT_Hour >= 9)
                                {
                                    OT_Time = OT_Hour - 8;
                                    OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                    AutoDTable.Rows[iRow2][15] = OT_Hour1;
                                }
                                else
                                {
                                    AutoDTable.Rows[iRow2][15] = "00:00";
                                }

                               
                            }
                          


                        }



                            
                            else
                            {
                                TimeSpan ts4;
                                ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                                if (mLocalDS_INTAB.Rows.Count <= 0)
                                {
                                    Time_IN_Str = "";
                                }
                                else
                                {
                                    Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                                }
                                for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                                {
                                    if (mLocalDS_OUTTAB.Rows.Count <= 0)
                                    {
                                        Time_OUT_Str = "";
                                    }
                                    else
                                    {
                                        Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                                    }

                                }
                                if (Time_IN_Str == "" || Time_OUT_Str == "")
                                {
                                    time_Check_dbl = 0;
                                }
                                else
                                {
                                    DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                                    DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                                    TimeSpan ts1;
                                    ts1 = date4.Subtract(date3);
                                    Total_Time_get = ts1.Hours.ToString();

                                    if (Left_Val(Total_Time_get, 1) == "-")
                                    {
                                        date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                        ts1 = date4.Subtract(date3);
                                        ts1 = date4.Subtract(date3);
                                        //ts4 = ts4.Add(ts1);
                                        Total_Time_get = ts1.Hours.ToString();
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                        Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                    }
                                    else
                                    {
                                        ts4 = ts4.Add(ts1);
                                        string s1 = ts4.Minutes.ToString();
                                        if (s1.Length == 1)
                                        {
                                            string ts_str = "0" + ts4.Minutes;
                                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                        }
                                        else
                                        {
                                            Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                        }
                                        Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                        if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }

                                        if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                        {
                                            Emp_Total_Work_Time_1 = "00:00";
                                        }
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                    AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                                    AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                                    OThour_Str = Emp_Total_Work_Time_1.Split(':');
                                    OT_Hour = Convert.ToInt32(OThour_Str[0]);
                                    OT_Min = Convert.ToInt32(OThour_Str[1]);
                                    if (OT_Hour >= 9)
                                    {
                                        OT_Time = OT_Hour - 8;
                                        OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                        AutoDTable.Rows[iRow2][15] = OT_Hour1;
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][15] = "00:00";
                                    }
                           

                                }
                               
                            }
                            //string OD = "";
                            //DataTable da_OD = new DataTable(); 
                            //SSQL = "select AttnStatus from ManAttn_Details MA inner join Employee_Mst EM on " +
                            //       " Em.MachineID=MA.Machine_No where  AttnStatus='OD' and  AttnDate='" + Date_Value_Str + "' and EM.MachineID_Encrypt='" + InMachine_IP + "' ";
                            //da_OD = objdata.RptEmployeeMultipleDetails(SSQL);

                            //if (da_OD.Rows.Count != 0)
                            //{
                           

                            //    AutoDTable.Rows[iRow2][20] = "OD";
                            //}
                           
                            // Bellow codings are correct:

                        }
                        for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                        {
                            string MID = AutoDTable.Rows[iRow2][19].ToString();

                            SSQL = "";
                            SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                            SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                            SSQL = SSQL + ",FirstName";
                            SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
                            SSQL = SSQL + " from ";
                            if (status == "Approval")
                            {
                                SSQL = SSQL + "Employee_Mst";
                            }
                            else
                            {
                                SSQL = SSQL + "Employee_Mst_New_Emp";
                            }

                            SSQL = SSQL + " Where MachineID_Encrypt='" + MID + "' And Compcode='" + SessionCcode + "'";
                            SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

                            mEmployeeDS = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (mEmployeeDS.Rows.Count > 0)
                            {
                                for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                                {

                                    ss = UTF8Decryption(MID.ToString());
                                    AutoDTable.Rows[iRow2][19] = ss.ToString();
                                    string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();

                                    
                                    AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                                    AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];

                                    //Get Shift Name
                                    SSQL = "Select [Shift] as Shift_Name from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString() + "'";
                                    SSQL = SSQL + " And Convert(Datetime,Attn_Date,103)=Convert(Datetime,'" + Date + "',103) And Shift <> 'Leave'";
                                    DataTable DT_SH = new DataTable();
                                    DT_SH = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (DT_SH.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][3] = DT_SH.Rows[0]["Shift_Name"].ToString();
                                    }
                                    else
                                    {
                                        AutoDTable.Rows[iRow2][3] = "No Shift";
                                    }

                                    AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                                    AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                                    AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                                    AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                                    AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                                    AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                                    AutoDTable.Rows[iRow2][16] = Compname.ToString();
                                    AutoDTable.Rows[iRow2][17] =  Locname.ToString();
                                    AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];

                                }
                            }
                        }

                //    }
                //}

                if (AutoDTable.Rows.Count != 0)
                {

                    ds.Tables.Add(AutoDTable);
                    //ReportDocument report = new ReportDocument();
                    report.Load(Server.MapPath("crystal/LunchAttendance.rpt"));
                    report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");

                    CrystalReportViewer1.ReportSource = report;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
                }
            }
        }
    
    }

    public void NonAdminGetAttdDayWise_Change()
    {
        DataSet ds = new DataSet();
        DataTable AutoDTable = new DataTable();


        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Dept");
        AutoDTable.Columns.Add("Type");
        AutoDTable.Columns.Add("Shift");
        AutoDTable.Columns.Add("EmpCode");
        AutoDTable.Columns.Add("ExCode");
        AutoDTable.Columns.Add("Name");
        AutoDTable.Columns.Add("TimeIN");
        AutoDTable.Columns.Add("TimeOUT");
        AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("Category");
        AutoDTable.Columns.Add("SubCategory");
        AutoDTable.Columns.Add("TotalMIN");
        AutoDTable.Columns.Add("GrandTOT");
        AutoDTable.Columns.Add("ShiftDate");
        AutoDTable.Columns.Add("OTHour");
        AutoDTable.Columns.Add("CompanyName");
        AutoDTable.Columns.Add("LocationName");
        AutoDTable.Columns.Add("Desgination");
        AutoDTable.Columns.Add("MachineIDNew");
        AutoDTable.Columns.Add("Status");

        //if (mReportName.Text == "DAY ATTENDANCE - DAY WISE :: BELOW 4 HOURS")
        //{
        DataTable dtIPaddress = new DataTable();
        SSQL = "Select * from IPAddress_Mst where CompCode='" + SessionCcode.ToString() + "' and LocCode='" + SessionLcode.ToString() + "' ";
        dtIPaddress = objdata.RptEmployeeMultipleDetails(SSQL);

        if (dtIPaddress.Rows.Count > 0)
        {
            for (int i = 0; i < dtIPaddress.Rows.Count; i++)
            {
                if (dtIPaddress.Rows[i]["IPMode"].ToString() == "LUNCH IN")
                {
                    mIpAddress_IN = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
                else if (dtIPaddress.Rows[i]["IPMode"].ToString() == "LUNCH OUT")
                {
                    mIpAddress_OUT = dtIPaddress.Rows[i]["IPAddress"].ToString();
                }
            }
        }

        SSQL = "";
        SSQL = "Select shiftDesc,StartIN,EndIN,StartOUT,EndOUT,StartIN_Days,";
        SSQL = SSQL + " EndIN_Days,StartOUT_Days,EndOUT_Days ";
        SSQL = SSQL + " from Shift_Mst Where CompCode='" + SessionCcode + "'";
        SSQL = SSQL + " And LocCode='" + SessionLcode + "'";
        if (ShiftType1 != "ALL")
        {
            SSQL = SSQL + " And shiftDesc='" + ShiftType1 + "'";

        }

        SSQL = SSQL + " Order By shiftDesc";
        LocalDT = objdata.RptEmployeeMultipleDetails(SSQL);


        string ShiftType;

        if (LocalDT.Rows.Count > 0)
        {
            string ng = string.Format(Date, "MM-dd-yyyy");
            Datestr = Convert.ToDateTime(Date).AddDays(0).ToShortDateString();
            Datestr1 = Convert.ToDateTime(Date).AddDays(1).ToShortDateString();
            DateTime date1 = Convert.ToDateTime(ng);
            DateTime date2 = date1.AddDays(1);
            string Start_IN = "";
            string End_In;

            string End_In_Days = "";
            string Start_In_Days = "";

            int mStartINRow = 0;
            int mStartOUTRow = 0;

            for (int iTabRow = 0; iTabRow < LocalDT.Rows.Count; iTabRow++)
            {

                if (AutoDTable.Rows.Count <= 1)
                {
                    mStartOUTRow = 0;
                }
                else
                {
                    mStartOUTRow = AutoDTable.Rows.Count - 1;
                }

                if (LocalDT.Rows[iTabRow]["shiftDesc"].ToString() == "GENERAL")
                {
                    ShiftType = "GENERAL";
                }
                else
                {
                    ShiftType = "SHIFT";
                }


                string sIndays_str = LocalDT.Rows[iTabRow]["StartIN_Days"].ToString();
                double sINdays = Convert.ToDouble(sIndays_str);
                string eIndays_str = LocalDT.Rows[iTabRow]["EndIN_Days"].ToString();
                double eINdays = Convert.ToDouble(eIndays_str);
                string ss = "";

                if (ShiftType == "GENERAL")
                {

                    //edited here
                    ss = "select LT.MachineID,Min(LT.TimeOUT) as [TimeOUT] from LogTimeLunch_OUT LT inner join ";
                    if (status == "Approval")
                    {
                        ss = ss + "Employee_Mst";
                    }
                    else
                    {
                        ss = ss + "Employee_Mst_New_Emp";
                    }
                    ss = ss + " EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode='" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + "And LT.TimeOUT >='" + date1.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "And LT.TimeOUT <='" + date2.ToString("yyyy/MM/dd") + " " + "02:00" + "'";
                    ss = ss + "AND EM.ShiftType='" + ShiftType + "'";
                    ss = ss + " And EM.Eligible_PF='1' ";
                  
                    if (Division != "-Select-")
                    {
                        ss = ss + " and EM.Division='" + Division + "' ";
                    }
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeOUT)";
                }
                //And EM.IsActive='yes'

                else if (ShiftType == "SHIFT")
                {




                    ss = "select LT.MachineID,Min(LT.TimeOUT) as [TimeOUT] from LogTimeLunch_OUT LT inner join ";
                    if (status == "Approval")
                    {
                        ss = ss + "Employee_Mst";
                    }
                    else
                    {
                        ss = ss + "Employee_Mst_New_Emp";
                    }
                    ss = ss + " EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + "  And LT.TimeOUT >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeOUT <='" + date1.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                    ss = ss + " And EM.Eligible_PF='1' ";
                    if (Division != "-Select-")
                    {
                        ss = ss + " and EM.Division='" + Division + "' ";
                    }
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeOUT)";
                    //And EM.IsActive='yes'
                }
                else
                {
                    ss = "select LT.MachineID,Min(LT.TimeOUT) as [TimeOUT] from LogTimeLunch_OUT LT inner join ";
                    if (status == "Approval")
                    {
                        ss = ss + "Employee_Mst";
                    }
                    else
                    {
                        ss = ss + "Employee_Mst_New_Emp";
                    }
                    ss = ss + " EM on EM.MachineID_Encrypt=LT.MachineID";
                    ss = ss + " Where LT.Compcode= '" + SessionCcode.ToString() + "' And LT.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And EM.Compcode='" + SessionCcode.ToString() + "' And EM.LocCode='" + SessionLcode.ToString() + "'";
                    ss = ss + " And LT.TimeOUT >='" + date1.AddDays(sINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["StartIN"].ToString() + "' ";
                    ss = ss + " And LT.TimeOUT <='" + date2.AddDays(eINdays).ToString("yyyy/MM/dd") + " " + LocalDT.Rows[iTabRow]["EndIN"].ToString() + "' ";
                    ss = ss + " AND EM.ShiftType='" + ShiftType + "' ";
                    ss = ss + " And EM.Eligible_PF='1' ";
                    if (Division != "-Select-")
                    {
                        ss = ss + " and EM.Division='" + Division + "' ";
                    }
                    ss = ss + " Group By LT.MachineID Order By Min(LT.TimeOUT)";
                }
                //And EM.IsActive='yes'

                mdatatable = objdata.RptEmployeeMultipleDetails(ss);

                SSQL = "Select * from Company_Mst ";
                Comp_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                Compname = Comp_dt.Rows[0]["CompName"].ToString();

                SSQL = "Select * from Location_Mst  where CompCode='" + SessionCcode.ToString() + "'and LocCode ='" + SessionLcode.ToString() + "' ";
                Loc_dt = objdata.RptEmployeeMultipleDetails(SSQL);
                Locname = Loc_dt.Rows[0]["LocName"].ToString();
                if (mdatatable.Rows.Count > 0)
                {
                    string MachineID;

                    for (int iRow = 0; iRow < mdatatable.Rows.Count; iRow++)
                    {
                        Boolean chkduplicate = false;
                        chkduplicate = false;
                        string id = ""; //mdatatable.Rows[iRow]["MachineID"].ToString();
                        id = UTF8Decryption(mdatatable.Rows[iRow]["MachineID"].ToString());
                        for (int ia = 0; ia < AutoDTable.Rows.Count; ia++)
                        {

                            //string id = mdatatable.Rows[iRow]["MachineID"].ToString();

                            if (id == AutoDTable.Rows[ia][9].ToString())
                            {
                                chkduplicate = true;
                            }
                        }
                        if (chkduplicate == false)
                        {



                            AutoDTable.NewRow();
                            AutoDTable.Rows.Add();


                            // AutoDTable.Rows.Add();

                            // AutoDTable.Rows[mStartINRow][0] = iRow + 1;
                            AutoDTable.Rows[mStartINRow][3] = LocalDT.Rows[iTabRow]["ShiftDesc"].ToString();
                            AutoDTable.Rows[mStartINRow][16] = Compname.ToString();
                            AutoDTable.Rows[mStartINRow][17] = Locname.ToString();
                            AutoDTable.Rows[mStartINRow][14] = date1.ToString("dd/MM/yyyy");
                            if (ShiftType == "SHIFT")
                            {
                                string str = mdatatable.Rows[iRow][1].ToString();
                                // string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1])
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeOUT"]);
                            }
                            else
                            {
                                AutoDTable.Rows[mStartINRow][7] = String.Format("{0:hh:mm tt}", mdatatable.Rows[iRow]["TimeOUT"]);
                            }


                            MachineID = mdatatable.Rows[iRow]["MachineID"].ToString();

                            ss = UTF8Decryption(MachineID.ToString());

                            //AutoDTable.Rows[iRow][9] = ss.ToString();


                            AutoDTable.Rows[mStartINRow][9] = ss.ToString();

                            AutoDTable.Rows[mStartINRow][19] = MachineID.ToString();
                            mStartINRow += 1;
                        }
                    }
                }

                DataTable nnDatatable = new DataTable();

                SSQL = "";
                SSQL = "Select MachineID,Max(TimeIN) as [TimeIN] from LogTimeLunch_IN  Where Compcode='" + SessionCcode + "'";
                SSQL = SSQL + " and LocCode = ' " + SessionLcode + "'";
                SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                SSQL = SSQL + " Group By MachineID";
                SSQL = SSQL + " Order By Max(TimeIN)";

                mdatatable = objdata.RptEmployeeMultipleDetails(SSQL);


                string InMachine_IP;
                DataTable mLocalDS_out = new DataTable();

                for (int iRow2 = mStartOUTRow; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string mach = AutoDTable.Rows[iRow2][19].ToString();

                    InMachine_IP = mach.ToString();

                    SSQL = "Select MachineID,TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " and TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00" + "'";
                    SSQL = SSQL + " and TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00" + "' Order by TimeIN desc";

                    mLocalDS_out = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mLocalDS_out.Rows.Count <= 0)
                    {

                    }
                    else
                    {
                        if (AutoDTable.Rows[iRow2][19].ToString() == mLocalDS_out.Rows[0][0].ToString())
                        {
                            AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", mLocalDS_out.Rows[0][1]);
                        }

                    }
                    // Above coddings are correct:

                    Time_IN_Str = "";
                    Time_OUT_Str = "";
                    Date_Value_Str = string.Format(Date, "yyyy/MM/dd");

                    if (InMachine_IP == "MTAxMjg=")
                    {
                        InMachine_IP = "MTAxMjg=";
                    }
                    SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + InMachine_IP + "'";
                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeOUT ASC";



                    mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (AutoDTable.Rows[iRow2][3].ToString() == "SHIFT1" || AutoDTable.Rows[iRow2][3].ToString() == "SHIFT2")
                    {

                        InTime_Check = Convert.ToDateTime(AutoDTable.Rows[iRow2][7].ToString());
                        InToTime_Check = InTime_Check.AddHours(2);

                        string s1 = InTime_Check.ToString("HH:mm:ss");
                        string s2 = InToTime_Check.ToString("HH:mm:ss");

                        string InTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InTime_Check);
                        string InToTime_Check_str = string.Format("{0:hh\\:mm\\:ss}", InToTime_Check);
                        InTime_TimeSpan = TimeSpan.Parse(InTime_Check_str);
                        From_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;
                        InTime_TimeSpan = TimeSpan.Parse(InToTime_Check_str);
                        To_Time_Str = InTime_TimeSpan.Hours + ":" + InTime_TimeSpan.Minutes;



                        SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + From_Time_Str + "'";
                        SSQL = SSQL + " And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "' Order by TimeIN Asc";

                        DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                        if (DS_Time.Rows.Count != 0)
                        {
                            SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "02:00' And TimeOUT <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "02:00' Order by TimeOUT ASC";


                            DS_InTime = objdata.RptEmployeeMultipleDetails(SSQL);

                            if (DS_InTime.Rows.Count != 0)
                            {
                                Final_InTime = DS_InTime.Rows[0][0].ToString();
                                SSQL = "Select * from Shift_Mst Where CompCode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "' And ShiftDesc like '%SHIFT%'";
                                Shift_DS = objdata.ReturnMultipleValue(SSQL);
                                Shift_Check_blb = false;



                                for (int k = 0; k < Shift_DS.Rows.Count; k++)
                                {
                                    string a = Shift_DS.Rows[k]["StartIN_Days"].ToString();
                                    int b = Convert.ToInt16(a.ToString());
                                    Shift_Start_Time = date1.AddDays(b).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["StartIN"].ToString();
                                    string a1 = Shift_DS.Rows[k]["EndIN_Days"].ToString();
                                    int b1 = Convert.ToInt16(a1.ToString());
                                    Shift_End_Time = date1.AddDays(b1).ToString("yyyy/MM/dd") + " " + Shift_DS.Rows[k]["EndIN"].ToString();

                                    ShiftdateStartIN = Convert.ToDateTime(Shift_Start_Time.ToString());
                                    ShiftdateEndIN = Convert.ToDateTime(Shift_End_Time.ToString());

                                    EmpdateIN = Convert.ToDateTime(Final_InTime.ToString());
                                    if (EmpdateIN >= ShiftdateStartIN && EmpdateIN <= ShiftdateEndIN)
                                    {
                                        Final_Shift = Shift_DS.Rows[k]["ShiftDesc"].ToString();
                                        Shift_Check_blb = true;

                                    }
                                }

                                DayOfWeek day = date1.DayOfWeek;
                                string WeekofDay = day.ToString();

                                if (Shift_Check_blb == true)
                                {
                                    AutoDTable.Rows[iRow2][3] = Final_Shift.ToString();
                                    AutoDTable.Rows[iRow2][16] = SessionCompanyName.ToString();
                                    AutoDTable.Rows[iRow2][17] = SessionLocationName.ToString();
                                    AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                                    //AutoDTable.Rows[iRow2][7] = String.Format("{0:hh:mm tt}", Final_InTime);



                                    SSQL = "Select TimeOUT from LogTimeLunch_OUT where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeOUT >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + To_Time_Str + "'";
                                    mLocalDS_INTAB = objdata.RptEmployeeMultipleDetails(SSQL);

                                    SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeIN Asc";

                                    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);
                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                    }
                                }

                                else
                                {

                                    SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                                    SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                    SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "13:00' Order by TimeIN Asc";
                                    DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                                    if (DS_Time.Rows.Count != 0)
                                    {
                                        AutoDTable.Rows[iRow2][8] = String.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);


                                    }

                                }
                            }
                            else
                            {
                                SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                                SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                                SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "06:00' And TimeIN <='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "23:00' Order by TimeIN Asc";
                                DS_Time = objdata.RptEmployeeMultipleDetails(SSQL);

                                if (DS_Time.Rows.Count != 0)
                                {
                                    AutoDTable.Rows[iRow2][8] = string.Format("{0:hh:mm tt}", DS_Time.Rows[0][0]);

                                }
                            }
                        }
                        else
                        {

                            SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                            SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                            SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeIN ASC";
                        }
                    }


                    else
                    {
                        SSQL = "Select TimeIN from LogTimeLunch_IN where MachineID='" + InMachine_IP + "'";
                        SSQL = SSQL + " And Compcode='" + SessionCcode.ToString() + "' And LocCode='" + SessionLcode.ToString() + "'";
                        SSQL = SSQL + " And TimeIN >='" + date1.AddDays(0).ToString("yyyy/MM/dd") + " " + "10:00' And TimeIN <='" + date1.AddDays(1).ToString("yyyy/MM/dd") + " " + "10:00' Order by TimeIN ASC";
                    }



                    mLocalDS_OUTTAB = objdata.RptEmployeeMultipleDetails(SSQL);
                    String Emp_Total_Work_Time_1 = "00:00";
                    if (mLocalDS_INTAB.Rows.Count > 1)
                    {
                        for (int tin = 0; tin < mLocalDS_INTAB.Rows.Count; tin++)
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();

                            if (mLocalDS_OUTTAB.Rows.Count > tin)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[tin][0].ToString();
                            }
                            else if (mLocalDS_OUTTAB.Rows.Count > mLocalDS_INTAB.Rows.Count)
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[mLocalDS_OUTTAB.Rows.Count - 1][0].ToString();
                            }
                            else
                            {
                                Time_OUT_Str = "";
                            }

                            TimeSpan ts4;
                            ts4 = Convert.ToDateTime(String.Format("{0:hh:mm tt}", Emp_Total_Work_Time_1)).TimeOfDay;
                            if (LocalDT.Rows.Count <= 0)
                            {
                                Time_IN_Str = "";
                            }
                            else
                            {
                                Time_IN_Str = mLocalDS_INTAB.Rows[tin][0].ToString();
                            }

                            if (Time_IN_Str == "" || Time_OUT_Str == "")
                            {
                                time_Check_dbl = time_Check_dbl;
                            }
                            else
                            {
                                DateTime date3 = Convert.ToDateTime(Time_IN_Str);
                                DateTime date4 = Convert.ToDateTime(Time_OUT_Str);
                                TimeSpan ts1;
                                ts1 = date4.Subtract(date3);
                                Total_Time_get = ts1.Hours.ToString();
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;

                                string[] Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Total_Time_get, 1) == "-")
                                {
                                    date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                    ts1 = date4.Subtract(date3);
                                    ts1 = date4.Subtract(date3);
                                    ts4 = ts4.Add(ts1);
                                    Total_Time_get = ts1.Hours.ToString();
                                    time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');


                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {

                                        Emp_Total_Work_Time_1 = "00:00";
                                    }

                                }
                                else
                                {
                                    ts4 = ts4.Add(ts1);
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                    Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                    if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                    }
                                    if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                    {
                                        Emp_Total_Work_Time_1 = "00:00";
                                        time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                    }
                                }
                            }

                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;
                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }

                        }
                    }
                    else
                    {
                        TimeSpan ts4;
                        ts4 = Convert.ToDateTime(String.Format("{0:hh:mm}", Emp_Total_Work_Time_1)).TimeOfDay;
                        if (mLocalDS_INTAB.Rows.Count <= 0)
                        {
                            Time_IN_Str = "";
                        }
                        else
                        {
                            Time_IN_Str = mLocalDS_INTAB.Rows[0][0].ToString();
                        }
                        for (int tout = 0; tout <= mLocalDS_OUTTAB.Rows.Count; tout++)
                        {
                            if (mLocalDS_OUTTAB.Rows.Count <= 0)
                            {
                                Time_OUT_Str = "";
                            }
                            else
                            {
                                Time_OUT_Str = mLocalDS_OUTTAB.Rows[0][0].ToString();
                            }

                        }
                        if (Time_IN_Str == "" || Time_OUT_Str == "")
                        {
                            time_Check_dbl = 0;
                        }
                        else
                        {
                            DateTime date3 = System.Convert.ToDateTime(Time_IN_Str);
                            DateTime date4 = System.Convert.ToDateTime(Time_OUT_Str);
                            TimeSpan ts1;
                            ts1 = date4.Subtract(date3);
                            Total_Time_get = ts1.Hours.ToString();

                            if (Left_Val(Total_Time_get, 1) == "-")
                            {
                                date4 = System.Convert.ToDateTime(Time_OUT_Str).AddDays(1);
                                ts1 = date4.Subtract(date3);
                                ts1 = date4.Subtract(date3);
                                ts4 = ts4.Add(ts1);
                                Total_Time_get = ts1.Hours.ToString();
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                                Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');

                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                            }
                            else
                            {
                                ts4 = ts4.Add(ts1);
                                string s1 = ts4.Minutes.ToString();
                                if (s1.Length == 1)
                                {
                                    string ts_str = "0" + ts4.Minutes;
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts_str;
                                }
                                else
                                {
                                    Emp_Total_Work_Time_1 = ts4.Hours + ":" + ts4.Minutes;
                                }
                                Time_Minus_Value_Check = Emp_Total_Work_Time_1.Split(':');
                                if (Left_Val(Time_Minus_Value_Check[0], 1) == "-" | Left_Val(Time_Minus_Value_Check[1], 1) == "-")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }

                                if (Left_Val(Emp_Total_Work_Time_1, 1) == "0:" | Emp_Total_Work_Time_1 == "0:-1" | Emp_Total_Work_Time_1 == "0:-3")
                                {
                                    Emp_Total_Work_Time_1 = "00:00";
                                }
                                time_Check_dbl = Convert.ToInt16(Total_Time_get);
                            }
                            AutoDTable.Rows[iRow2][12] = Emp_Total_Work_Time_1;

                            AutoDTable.Rows[iRow2][13] = Emp_Total_Work_Time_1;
                            OThour_Str = Emp_Total_Work_Time_1.Split(':');
                            OT_Hour = Convert.ToInt32(OThour_Str[0]);
                            OT_Min = Convert.ToInt32(OThour_Str[1]);
                            if (OT_Hour >= 9)
                            {
                                OT_Time = OT_Hour - 8;
                                OT_Hour1 = Convert.ToString(OT_Time) + " : " + Convert.ToString(OT_Min);
                                AutoDTable.Rows[iRow2][15] = OT_Hour1;
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][15] = "00:00";
                            }


                        }

                    }
                    //string OD = "";
                    //DataTable da_OD = new DataTable(); 
                    //SSQL = "select AttnStatus from ManAttn_Details MA inner join Employee_Mst EM on " +
                    //       " Em.MachineID=MA.Machine_No where  AttnStatus='OD' and  AttnDate='" + Date_Value_Str + "' and EM.MachineID_Encrypt='" + InMachine_IP + "' ";
                    //da_OD = objdata.RptEmployeeMultipleDetails(SSQL);

                    //if (da_OD.Rows.Count != 0)
                    //{


                    //    AutoDTable.Rows[iRow2][20] = "OD";
                    //}

                    // Bellow codings are correct:

                }
                for (int iRow2 = 0; iRow2 < AutoDTable.Rows.Count; iRow2++)
                {
                    string MID = AutoDTable.Rows[iRow2][19].ToString();

                    SSQL = "";
                    SSQL = "select Distinct isnull(MachineID_Encrypt,'') as [MachineID],isnull(DeptName,'') as [DeptName]";
                    SSQL = SSQL + ",isnull(TypeName,'') as [TypeName],EmpNo,isnull(ExistingCode,'') as [ExistingCode]";
                    SSQL = SSQL + ",isnull(FirstName,'') + '.'+ isnull(MiddleInitial,'') as [FirstName]";
                    SSQL = SSQL + ",isnull(CatName,'') as [CatName], isnull(SubCatName,'') as [SubCatName],isnull(Designation,'') as Desgination";
                    SSQL = SSQL + " from ";
                    if (status == "Approval")
                    {
                        SSQL = SSQL + "Employee_Mst";
                    }
                    else
                    {
                        SSQL = SSQL + "Employee_Mst_New_Emp";
                    }
                    SSQL = SSQL +" Where MachineID_Encrypt='" + MID + "' And Compcode='" + SessionCcode + "'";
                    SSQL = SSQL + " And LocCode='" + SessionLcode + "' And IsActive='Yes'" + " order by ExistingCode Asc";

                    mEmployeeDS = objdata.RptEmployeeMultipleDetails(SSQL);

                    if (mEmployeeDS.Rows.Count > 0)
                    {
                        for (int iRow1 = 0; iRow1 < mEmployeeDS.Rows.Count; iRow1++)
                        {

                            ss = UTF8Decryption(MID.ToString());
                            AutoDTable.Rows[iRow2][19] = ss.ToString();
                            string MID1 = mEmployeeDS.Rows[iRow1]["MachineID"].ToString();


                            AutoDTable.Rows[iRow2][1] = mEmployeeDS.Rows[iRow1]["DeptName"];
                            AutoDTable.Rows[iRow2][2] = mEmployeeDS.Rows[iRow1]["TypeName"];

                            //Get Shift Name
                            SSQL = "Select [Shift] as Shift_Name from LogTime_Days where CompCode='" + SessionCcode + "' And LocCode='" + SessionLcode + "' And ExistingCode='" + mEmployeeDS.Rows[iRow1]["ExistingCode"].ToString() + "'";
                            SSQL = SSQL + " And Convert(Datetime,Attn_Date,103)=Convert(Datetime,'" + Date + "',103)";
                            DataTable DT_SH = new DataTable();
                            DT_SH = objdata.RptEmployeeMultipleDetails(SSQL);
                            if (DT_SH.Rows.Count != 0)
                            {
                                AutoDTable.Rows[iRow2][3] = DT_SH.Rows[0]["Shift_Name"].ToString();
                            }
                            else
                            {
                                AutoDTable.Rows[iRow2][3] = "No Shift";
                            }
                            AutoDTable.Rows[iRow2][4] = mEmployeeDS.Rows[iRow1]["EmpNo"];
                            AutoDTable.Rows[iRow2][5] = mEmployeeDS.Rows[iRow1]["ExistingCode"];
                            AutoDTable.Rows[iRow2][6] = mEmployeeDS.Rows[iRow1]["FirstName"];
                            AutoDTable.Rows[iRow2][10] = mEmployeeDS.Rows[iRow1]["CatName"];
                            AutoDTable.Rows[iRow2][11] = mEmployeeDS.Rows[iRow1]["SubCatName"];
                            AutoDTable.Rows[iRow2][14] = date1.ToString("dd/MM/yyyy");
                            AutoDTable.Rows[iRow2][16] = Compname.ToString();
                            AutoDTable.Rows[iRow2][17] = Locname.ToString();
                            AutoDTable.Rows[iRow2][18] = mEmployeeDS.Rows[iRow1]["Desgination"];

                        }
                    }
                }

            }
        }



        ds.Tables.Add(AutoDTable);
        //ReportDocument report = new ReportDocument();
        report.Load(Server.MapPath("crystal/LunchAttendance.rpt"));
        report.SetDatabaseLogon("username", "password", "sa", "Altius2005");
        report.Database.Tables[0].SetDataSource(ds.Tables[0]);
        CrystalReportViewer1.ReportSource = report;
    }


    private static string UTF8Decryption(string encryptpwd)
    {
        string decryptpwd = "0";
        BALDataAccess objdata_new = new BALDataAccess();

        string query = "Select * from Employee_Mst where MachineID_Encrypt='" + encryptpwd + "'";
        DataTable DT = new DataTable();
        DT = objdata_new.RptEmployeeMultipleDetails(query);
        if (DT.Rows.Count != 0)
        {
            decryptpwd = DT.Rows[0]["MachineID"].ToString();
        }
        else
        {
            query = "Select * from Employee_Mst_New_Emp where MachineID_Encrypt='" + encryptpwd + "'";
            DT = objdata_new.RptEmployeeMultipleDetails(query);
            if (DT.Rows.Count != 0)
            {
                decryptpwd = DT.Rows[0]["MachineID"].ToString();
            }
            else
            {
                decryptpwd = "0";
            }
        }

        //UTF8Encoding encodepwd = new UTF8Encoding();
        //Decoder Decode = encodepwd.GetDecoder();
        //byte[] todecode_byte = Convert.FromBase64String(encryptpwd);
        //int charCount = Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
        //char[] decoded_char = new char[charCount];
        //Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
        //decryptpwd = new String(decoded_char);
        return decryptpwd;
    }

    public static string UTF8Encryption_OLD(string password)
    {
        string strmsg = string.Empty;
        byte[] encode = new byte[password.Length];
        encode = Encoding.UTF8.GetBytes(password);
        strmsg = Convert.ToBase64String(encode);
        return strmsg;
    }

    public string UTF8Encryption(string mvarPlanText)
    {
        string cipherText = "";
        try
        {
            string passPhrase = "Altius";
            string saltValue = "info@altius.co.in";
            string hashAlgorithm = "SHA1";
            string initVector = "@1B2c3D4e5F6g7H8";
            int passwordIterations = 2;
            int keySize = 256;
            byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
            byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(mvarPlanText);
            PasswordDeriveBytes password = new PasswordDeriveBytes(passPhrase, saltValueBytes, hashAlgorithm, passwordIterations);
            byte[] keyBytes = password.GetBytes(keySize / 8);
            RijndaelManaged symmetricKey = new RijndaelManaged();
            symmetricKey.Mode = CipherMode.CBC;
            ICryptoTransform encryptor = symmetricKey.CreateEncryptor(keyBytes, initVectorBytes);
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
            cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] cipherTextBytes = memoryStream.ToArray();
            memoryStream.Close();
            cryptoStream.Close();
            cipherText = Convert.ToBase64String(cipherTextBytes);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        return cipherText;
    }

    private void CloseReports(ReportDocument reportDocument)
    {
        Sections sections = reportDocument.ReportDefinition.Sections;
        foreach (Section section in sections)
        {
            ReportObjects reportObjects = section.ReportObjects;
            foreach (ReportObject reportObject in reportObjects)
            {
                if (reportObject.Kind == ReportObjectKind.SubreportObject)
                {
                    SubreportObject subreportObject = (SubreportObject)reportObject;
                    ReportDocument subReportDocument = subreportObject.OpenSubreport(subreportObject.SubreportName);
                    subReportDocument.Close();
                }
            }
        }
        reportDocument.Close();
    }
    private void Page_Unload(object sender, EventArgs e)
    {
        if (report.FileName != "")
        {
            CloseReports(report);
            report.Dispose();
            CrystalReportViewer1.Dispose();
            CrystalReportViewer1 = null;
        }

    }
}
