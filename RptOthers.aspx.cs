﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;
public partial class RptOthers : System.Web.UI.Page
{
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    ReportDocument rd = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    SqlConnection con;
    string SessionCcode;
    string SessionLcode;
    string SessionAdmin; string SessionUserType;
    string DaScore;
    string ReportName;
    string SessionPayroll;
    string query;
    DataTable dt_Query = new DataTable();
    string CmpName;
    string Cmpaddress;
    string FromDate;
    string ToDate;
    string Months;
    string[] SliptFromDate;
    protected void Page_Load(object sender, EventArgs e)
    {
        SqlConnection con = new SqlConnection(constr);
        SessionAdmin = Session["Isadmin"].ToString();
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        string ss = Session["UserId"].ToString();
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserType = Session["Isadmin"].ToString();
        SessionPayroll = Session["SessionEpay"].ToString();
        DaScore = Request.QueryString["DaSore"].ToString();
        ReportName = Request.QueryString["ReportName"].ToString();

      //  Load_DB();

        if (ReportName == "DA Point")
        {
            Load_DaScore_Report();
        }

        
    }
    public void Load_DaScore_Report()
    {
        SqlConnection con = new SqlConnection(constr);
        query = "";
        query = query + "select *  from [" + SessionPayroll + "]..DaCalculation where DAscore='" + DaScore + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' ";
        dt_Query = objdata.RptEmployeeMultipleDetails(query);

        if (dt_Query.Rows.Count > 0)
        {
            
            FromDate=DateTime.Now.ToString("dd/MM/yyyy").ToString();
            //FromDate = dt_Query.Rows[0]["FromDate"].ToString();
            ToDate = dt_Query.Rows[0]["ToDate"].ToString();
            SliptFromDate = FromDate.Split('/');

            switch (Convert.ToString(SliptFromDate[1]))
            {
                case "01":
                    Months = "JANUARY";
                    break;
                case "02":
                    Months = "FEBRUARY";
                    break;
                case "03":
                    Months = "MARCH";
                    break;
                case "04":
                    Months = "APRIL";
                    break;
                case "05":
                    Months = "MAY";
                    break;
                case "06":
                    Months = "JUNE";
                    break;
                case "07":
                    Months = "JULY";
                    break;
                case "08":
                    Months = "AUGUST";
                    break;
                case "09":
                    Months = "SEPTEMBER";
                    break;
                case "10":
                    Months = "OCTOBER";
                    break;
                case "11":
                    Months = "NOVEMBER";
                    break;
                case "12":
                    Months = "DECEMBER";
                    break;
                

                default:
                    break;
            }
            DataTable dt = new DataTable();
            query = "Select Cname,Location,Address1,Address2,Location,Pincode from [" + SessionPayroll + "]..AdminRights where Ccode='" + SessionCcode + "' and LCode='" + SessionLcode + "'";
            dt = objdata.RptEmployeeMultipleDetails(query);
            if (dt.Rows.Count > 0)
            {
                CmpName = dt.Rows[0]["Cname"].ToString();
                //Cmpaddress = (dt.Rows[0]["Address1"].ToString() + ", " + dt.Rows[0]["Address2"].ToString() + ", " + dt.Rows[0]["Location"].ToString() + "-" + dt.Rows[0]["Pincode"].ToString());
                Cmpaddress = dt.Rows[0]["Location"].ToString();
            }
            SqlCommand cmdComp = new SqlCommand(query, con);
            SqlDataAdapter sdaComp = new SqlDataAdapter(cmdComp);

            con.Open();
            DataTable dt_1 = new DataTable();
            sdaComp.Fill(dt_1);
            con.Close();

            rd.Load(Server.MapPath("crystal/DaPoint.rpt"));
            rd.SetDataSource(dt_Query);

            rd.DataDefinition.FormulaFields["CompanyName"].Text = "'" + CmpName + "'";
            //rd.DataDefinition.FormulaFields["Unit"].Text = "'" + SessionLcode + "'";
            rd.DataDefinition.FormulaFields["CompanyAddress"].Text = "'" + Cmpaddress + "'";
            rd.DataDefinition.FormulaFields["Months"].Text = "'" + Months + "'";
            rd.DataDefinition.FormulaFields["Years"].Text = "'" + SliptFromDate[2] + "'";

            rd.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
            //CrystalReportViewer1.AllowedExportFormats = formats;

            CrystalReportViewer1.ReportSource = rd;
            CrystalReportViewer1.RefreshReport();
            CrystalReportViewer1.DataBind();
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window", "alert('No Data Found');", true);
        }

    }
    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
    //public void Load_DB()
    //{
    //    //Get Database Name
    //    string query = "";
    //    DataTable dt_DB = new DataTable();
    //    query = "select CMS,Rights,Sales,Stores,Weaving,Maintaince,Production,Payroll from [Ramalinga_Rights]..MstDBname";
    //    dt_DB = objdata.RptEmployeeMultipleDetails(query);
    //    if (dt_DB.Rows.Count > 0)
    //    {
    //        SessionPayroll = dt_DB.Rows[0]["Payroll"].ToString();
    //        //SessionRights = dt_DB.Rows[0]["Rights"].ToString();
    //    }
    //}
}
