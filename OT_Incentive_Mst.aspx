﻿<%@ Page Language="C#" MasterPageFile="~/PayrollMaster.master" AutoEventWireup="true" CodeFile="OT_Incentive_Mst.aspx.cs" Inherits="OT_Incentive_Mst" Title="" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<asp:UpdatePanel ID="Basic" runat="server">
   <ContentTemplate>
     <!-- begin #content -->
<div id="content" class="content">
    <ol class="breadcrumb pull-right">
	    <li class="active">Salary Category</li>
	</ol>
	<h1 class="page-header">OT Incentive Master</h1>
	<div class="row">
        <div class="col-md-12">
		    <div class="panel panel-inverse">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                    </div>
                    <h4 class="panel-title">OT Incentive Master</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <h5></h5>
                        <div class="row">
                            <div class="col-md-4">
							    <div class="form-group">
								    <label>Category</label>
								    <asp:DropDownList ID="ddlCategory" runat="server" class="form-control select2"  AutoPostBack="true"
                                        onselectedindexchanged="ddlCategory_SelectedIndexChanged">
								       <asp:ListItem Text="-Select-" Value="0"></asp:ListItem>
								       <asp:ListItem Text="STAFF" Value="1"></asp:ListItem>
								       <asp:ListItem Text="LABOUR" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                    
                                
								</div>
                            </div>
                            <div class="col-md-4">
								<div class="form-group">
								    <label>Employee Type</label>
								    <asp:DropDownList runat="server" ID="ddlEmployeeType" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" 
                                        onselectedindexchanged="ddlEmployeeType_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                             <div class="col-md-3">
								<div class="form-group">
								    <label>Shift</label>
								    <asp:DropDownList runat="server" ID="ddlshift" AutoPostBack="true"
                                        class="form-control select2" style="width:100%;" OnSelectedIndexChanged="ddlshift_SelectedIndexChanged">
							 	    </asp:DropDownList>
								</div>
                            </div>
                        </div>
                       
                        <div class="row">
                
                             <div class="col-md-2">
								<div class="form-group">
								  <label>OT Hours From</label>
								  <asp:TextBox runat="server" ID="txtOTHrs_Frm" class="form-control" Text="0"></asp:TextBox>
								  <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>--%>
								</div>
                            </div>
                            <div class="col-md-2">
								<div class="form-group">
								  <label> OT Hours To</label>
								  <asp:TextBox runat="server" ID="txtOTHrs_To" class="form-control" Text="0"></asp:TextBox>
								  <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>--%>
								</div>
                            </div>

                            <div class="col-md-2">
								<div class="form-group">
								  <label>Amount</label>
								  <asp:TextBox runat="server" ID="txtAmount" class="form-control" Text="0"></asp:TextBox>
								  <%--<cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                    TargetControlID="txtIncAmt" ValidChars="0123456789">
                                   </cc1:FilteredTextBoxExtender>--%>
								</div>
                            </div>
                            </div>





                            		  <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4">
								<div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        onclick="btnSave_Click" />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                        onclick="btnClear_Click" />
						    	</div>
                            </div>
                            <div class="col-md-3"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
   </ContentTemplate>
</asp:UpdatePanel>


</asp:Content>

