﻿<%@ Page Language="C#" MasterPageFile="~/MainPage.master" AutoEventWireup="true" CodeFile="Uniform_Stock_Adjustment.aspx.cs" Inherits="Uniform_Stock_Adjustment" Title="Uniform Stock Adjustment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<script src="assets/js/master_list_jquery.min.js"></script>
<script src="assets/js/master_list_jquery-ui.min.js"></script>
<link href="assets/css/master_list_jquery-ui.css" rel="stylesheet" type="text/css"/>
<%--<script src="assets/js/form-wizards.demo.min.js"></script>--%>
 <script type="text/javascript">
     $(document).ready(function() {
         $('#example').dataTable();
      
     });
	</script>
<script type="text/javascript">
    //On UpdatePanel Refresh
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (prm != null) {
        prm.add_endRequest(function(sender, e) {
            if (sender._postBackSettings.panelsToUpdate != null) {
                $('#example').dataTable();
                $('.select2').select2();
                $('.datepicker').datepicker({
                    format: "dd/mm/yyyy",
                    autoclose: true
                });
            }
        });
    };
</script>

<asp:UpdatePanel ID="UpdatePanel5" runat="server">
<ContentTemplate>
<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb pull-right">
				<li><a href="javascript:;">Uniform</a></li>
				<li class="active">Stock Adjustment</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Stock Adjustment</h1>
			<!-- end page-header -->
			
			<!-- begin row -->
			<div class="row">
                <!-- begin col-12 -->
			    <div class="col-md-12">
			        <!-- begin panel -->
                    <div class="panel panel-inverse">
                        <div class="panel-heading">
                            <div class="panel-heading-btn">
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-repeat"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                                <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                            </div>
                            <h4 class="panel-title">Stock Adjustment</h4>
                        </div>
                        <div class="panel-body">
                        <!-- begin row -->
                          <div class="row">
                           <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Trans ID</label>
								  <asp:Label runat="server" ID="txtTransID" class="form-control" BackColor="#F3F3F3"></asp:Label>
								</div>
                               </div>
                              <!-- end col-4 -->
                              <!-- begin col-4 -->
                              <div class="col-md-2">
								<div class="form-group">
								  <label>Date</label>
								  <asp:TextBox runat="server" ID="txtDate" class="form-control datepicker" ></asp:TextBox>
								   <asp:RequiredFieldValidator ControlToValidate="txtDate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator1" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtDate" ValidChars="0123456789/">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                              <!-- end col-4 -->
                              
                             <!-- begin col-2 -->
                              <div class="col-md-3">
								<div class="form-group">
								  <label>Remarks</label>
								  <asp:TextBox runat="server" ID="txtRemarks" TextMode="MultiLine" class="form-control"></asp:TextBox>
								</div>
                               </div>
                              <!-- end col-2 -->
                              
                               </div>
                        <!-- end row -->
                         <div class="row">
                            <h6>Item Details</h6>
                         </div>
                         <div class="row">
                            <div class="col-md-3">
								<div class="form-group">
								  <label>Item Name</label>
								  <asp:DropDownList runat="server" ID="txtItemName" class="form-control select2" 
								    AutoPostBack="true" onselectedindexchanged="txtItemName_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="txtItemName" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator5" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <asp:HiddenField runat="server" ID="txtItemID" />
								</div>
                               </div>
                               
                               <div class="col-md-3">
								<div class="form-group">
								  <label>Size</label>
								  <asp:DropDownList runat="server" ID="txtSize" class="form-control select2" 
								    AutoPostBack="true" onselectedindexchanged="txtSize_SelectedIndexChanged">
								  </asp:DropDownList>
								  <asp:RequiredFieldValidator ControlToValidate="txtSize" Display="Dynamic" InitialValue="-Select-" ValidationGroup="Validate_Field1" class="form_error" ID="RequiredFieldValidator6" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
								</div>
                               </div>
                               
                               <div class="col-md-2">
								<div class="form-group">
								  <label>Type</label>
								  <asp:DropDownList runat="server" ID="txtStock_Type" class="form-control select2">
								  <asp:ListItem Value="Add">Add</asp:ListItem>
								  <asp:ListItem Value="Minus">Minus</asp:ListItem>
								  </asp:DropDownList>
								</div>
                               </div>
                               
                               <div class="col-md-2">
								<div class="form-group">
								  <label>Qty</label>
								  <asp:TextBox runat="server" ID="txtQty" class="form-control" Text="0"></asp:TextBox>
								   <asp:RequiredFieldValidator ControlToValidate="txtQty" Display="Dynamic" ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator7" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtQty" ValidChars="0123456789">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               
                               
                               
                               <div class="col-md-2" runat="server" visible="false">
								<div class="form-group">
								  <label>Rate</label>
								  <asp:TextBox runat="server" ID="txtRate" class="form-control" Text="0"></asp:TextBox>
								   <asp:RequiredFieldValidator ControlToValidate="txtRate" Display="Dynamic"  ValidationGroup="Validate_Field" class="form_error" ID="RequiredFieldValidator8" runat="server" EnableClientScript="true" ErrorMessage="This field is required.">
                                  </asp:RequiredFieldValidator>
                                  <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterMode="ValidChars" FilterType="Custom,Numbers"
                                              TargetControlID="txtRate" ValidChars="0123456789.">
                                  </cc1:FilteredTextBoxExtender>
								</div>
                               </div>
                               <div class="col-md-2">
                                     <div class="form-group">
                                           <asp:Button ID="btnAdd" runat="server" class="btn btn-success" style="margin-top: 16%;" Text="ADD" ValidationGroup="Validate_Field1"
                                               onclick="btnAdd_Click"/>
                                     </div>
                                 </div>
                         </div>
                         
                       <!-- table start -->
					<div class="col-md-12">
					    <div class="row">
					        <asp:Repeater ID="Repeater1" runat="server" EnableViewState="false">
			                    <HeaderTemplate>
                                    <table id="example" class="display table">
                                        <thead>
                                            <tr>
                                                <th>S.No</th>
                                                <th>Item Name</th>
                                                <th>Size</th>
                                                <th>Type</th>
                                                <th>Qty</th>
                                                <th>Rate</th>
                                                <th>Value</th>
                                                <th>Mode</th>
                                            </tr>
                                        </thead>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td><%# Container.ItemIndex + 1 %></td>
                                        <td><%# Eval("ItemName")%></td>
                                        <td><%# Eval("SizeName")%></td>
                                        <td><%# Eval("Add_Type")%></td>
                                        <td><%# Eval("Qty")%></td>
                                        <td><%# Eval("ItemRate")%></td>
                                        <td><%# Eval("LineTotal")%></td>
                                        <td>
                                            <asp:LinkButton ID="btnDeleteGrid" class="btn btn-danger btn-sm fa fa-trash-o"  runat="server" 
                                                Text="" OnCommand="GridDeleteClick" CommandArgument='<%# Eval("Type_SizeName")%>' CommandName='<%# Eval("ItemID")%>' 
                                                CausesValidation="true" OnClientClick="return confirm('Are you sure you want to delete this Item details?');">
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate></table></FooterTemplate>                                
			                </asp:Repeater>
					    </div>
					</div>
					<!-- table End -->
					<div class="row">
					    <div class="col-md-2"></div>
					    <div class="col-md-2">
							<div class="form-group">
							  <label>(+) Total Qty</label>
							  <asp:Label runat="server" ID="txtQtyTotal" class="form-control" BackColor="#F3F3F3"></asp:Label>
							</div>
                        </div>
                        <div class="col-md-2">
							<div class="form-group">
							  <label>(-)Total Qty</label>
							  <asp:Label runat="server" ID="txtMinus_Qty_Total" class="form-control" BackColor="#F3F3F3"></asp:Label>
							</div>
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
							<div class="form-group">
							  <label>(+) Total Amount</label>
							  <asp:Label runat="server" ID="txtTotalAmount" class="form-control" BackColor="#F3F3F3"></asp:Label>
							</div>
                        </div>
                        <div class="col-md-2">
							<div class="form-group">
							  <label>(-) Total Amount</label>
							  <asp:Label runat="server" ID="txtMinus_TotalAmount" class="form-control" BackColor="#F3F3F3"></asp:Label>
							</div>
                        </div>
					</div>
                         <div class="row">
                         <div class="col-md-4"></div>
                           <!-- begin col-4 -->
                                <div class="col-md-4">
								 <div class="form-group">
									<br />
									<asp:Button runat="server" id="btnSave" Text="Save" class="btn btn-success" 
                                        ValidationGroup="Validate_Field" onclick="btnSave_Click"  />
									<asp:Button runat="server" id="btnClear" Text="Clear" class="btn btn-danger" 
                                         onclick="btnClear_Click" />
                                    <asp:Button runat="server" id="btnBack" Text="Back" class="btn btn-success" 
                                        onclick="btnBack_Click"  />
								 </div>
                               </div>
                              <!-- end col-4 -->
                              <div class="col-md-4"></div>
                         </div>
                        <!-- end row -->
                        
                        
                        
                        </div>
                    </div>
                    <!-- end panel -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row -->
        </div>
<!-- end #content -->
</ContentTemplate>
</asp:UpdatePanel>



</asp:Content>





