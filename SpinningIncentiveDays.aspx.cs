﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using System.Globalization;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class SpinningIncentiveDays : System.Web.UI.Page
{
    string WagesType = "";
    string FromDate = "";
    string ToDate = "";
    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string ss = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();

    
     DateTime  date1=new DateTime();
DateTime  Date2=new DateTime();
string Date_Value_Str;
string Date_value_str1;
    string SSQL = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {

            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Spinning Incentive Days";
                //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("ManualEntry"));
                //li.Attributes.Add("class", "droplink active open");
            }

            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            //SessionAdmin = Session["Isadmin"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            Division = Request.QueryString["Division"].ToString();
            string TempWages = Request.QueryString["Wages"].ToString();
            WagesType = TempWages.Replace("_", "&");
            //WagesType = Request.QueryString["Wages"].ToString();

            FromDate = Request.QueryString["FromDate"].ToString();
            ToDate = Request.QueryString["ToDate"].ToString();

            
                AdminSpinningDays();
           
        }

    }


    public void AdminSpinningDays()
    {
        DataCell.Columns.Add("CompanyName");
        DataCell.Columns.Add("Location");
        DataCell.Columns.Add("MonthName");
        DataCell.Columns.Add("Get_Year");
        DataCell.Columns.Add("Wages_Type");
        DataCell.Columns.Add("ExistingCode");
        DataCell.Columns.Add("EmpName");
        DataCell.Columns.Add("Department");
        DataCell.Columns.Add("Designation");
        DataCell.Columns.Add("DOJ");
        DataCell.Columns.Add("SPG_Days");
        DataCell.Columns.Add("SPG_PerDay_Amt");
        DataCell.Columns.Add("SPG_Amt");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);

        DateTime Fdayy = Convert.ToDateTime(date1.AddDays(0).ToShortDateString());
        DateTime TDay = Convert.ToDateTime(Date2.AddDays(0).ToShortDateString());
        Date_Value_Str = Fdayy.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
        Date_value_str1 = TDay.ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
  //   Convert(varchar,EM.DOJ,105) as DOJ,AD.FullNight,EM.Alllowance1,SD.allowances1 from Employee_Mst EM 
  //   inner join[Raajco_Epay]..AttenanceDetails AD on Em.EmpNo=AD.EmpNo 
  // inner join [Raajco_Epay]..SalaryDetails SD on Em.EmpNo=SD.EmpNo 
  //  Where EM.CompCode ='ESM ' And EM.LocCode ='UNIT I' And EM.IsActive ='Yes'
  //And EM.Wages='REGULAR' And AD.Ccode='ESM' And AD.Lcode='UNIT I' 
  //And AD.FullNight <> '0.0' and AD.FromDate = '2017/12/01' and 
  //AD.ToDate = '2017/12/31' And SD.Ccode='ESM' And SD.Lcode='UNIT I'
  // and SD.FromDate = '2017/12/01' and SD.ToDate = '2017/12/31'

     

       SSQL="Select EM.ExistingCode,Em.FirstName as EmpName,Em.DeptName as DepartmentNm,Em.Designation as Designation,";
       SSQL = SSQL + " Convert(varchar,EM.DOJ,105) as DOJ,AD.FullNight,isnull(EM.Alllowance1,'0') as Alllowance1amt,SD.allowances1 from Employee_Mst EM  ";
       SSQL=SSQL +" inner join[Raajco_Epay]..AttenanceDetails AD on Em.EmpNo=AD.EmpNo   inner join [Raajco_Epay]..SalaryDetails SD on Em.EmpNo=SD.EmpNo ";
       SSQL=SSQL+" Where EM.CompCode ='"+ SessionCcode +"' And EM.LocCode ='"+ SessionLcode +"' And EM.IsActive ='Yes' ";
       SSQL=SSQL+"  And EM.Wages='"+ WagesType +"' And AD.Ccode='"+ SessionCcode +"' And AD.Lcode='"+ SessionLcode +"'  ";
       SSQL = SSQL + " And AD.FullNight <> '0.0'and AD.FromDate =convert(varchar,'" + Date_Value_Str + "',103) and ";
       SSQL = SSQL + " AD.ToDate = convert(varchar,'" + Date_value_str1 + "',103)  And SD.Ccode='" + SessionCcode + "' And SD.Lcode='" + SessionLcode + "' ";
       SSQL = SSQL + " and  SD.FromDate =convert(varchar,'" + Date_Value_Str + "',103) and SD.ToDate = convert(varchar,'" + Date_value_str1 + "',103) ";
       if (Division != "-Select-")
       {
           SSQL = SSQL + " And EM.Division = '" + Division + "'";
       }
       AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


       SSQL = "Select * from Company_Mst where CompCode='"+SessionCcode +"' ";
       dt = objdata.RptEmployeeMultipleDetails(SSQL);
       string name = dt.Rows[0]["CompName"].ToString();
       if (AutoDTable.Rows.Count != 0)
       {
           for (int i = 0; i < AutoDTable.Rows.Count; i++)
           {
               DataCell.NewRow();
               DataCell.Rows.Add();



               DataCell.Rows[i]["CompanyName"] = name;
               DataCell.Rows[i]["Location"] = SessionLcode;
               DataCell.Rows[i]["MonthName"] = date1.Month;
               DataCell.Rows[i]["Get_Year"] = date1.Year;
               DataCell.Rows[i]["Wages_Type"] = WagesType;
               DataCell.Rows[i]["ExistingCode"] = AutoDTable.Rows[i]["ExistingCode"];
               DataCell.Rows[i]["EmpName"] = AutoDTable.Rows[i]["EmpName"];
               DataCell.Rows[i]["Department"] = AutoDTable.Rows[i]["DepartmentNm"];
               DataCell.Rows[i]["Designation"] = AutoDTable.Rows[i]["Designation"];
               DataCell.Rows[i]["DOJ"] = AutoDTable.Rows[i]["DOJ"];
               DataCell.Rows[i]["SPG_Days"] = AutoDTable.Rows[i]["FullNight"];
               DataCell.Rows[i]["SPG_PerDay_Amt"] = AutoDTable.Rows[i]["Alllowance1amt"];
               DataCell.Rows[i]["SPG_Amt"] = AutoDTable.Rows[i]["allowances1"];


           }
           ds.Tables.Add(DataCell);
           //ReportDocument report = new ReportDocument();
           report.Load(Server.MapPath("crystal/Spinning_Incentive_Days.rpt"));
           
           report.Database.Tables[0].SetDataSource(ds.Tables[0]);
           report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
           CrystalReportViewer1.ReportSource = report;



       }
       else
       {
           lblReport.Text = "No Records Matched..";
       }
    }

    protected void Page_Unload(object sender, EventArgs e)
    {
        CrystalReportViewer1.Dispose();
    }
}
