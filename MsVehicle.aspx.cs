﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using Altius.BusinessAccessLayer.BALDataAccess;
public partial class MsVehicle : System.Web.UI.Page
{
    BALDataAccess objdata = new BALDataAccess();

    string SessionCcode;
    string SessionLcode;
    string SessionUserID;
    string SessionUserName;
    string SessionRights;
    
    string SSQL = "";
    bool ErrFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UserId"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        SessionCcode = Session["Ccode"].ToString();
        SessionLcode = Session["Lcode"].ToString();
        SessionUserName = Session["Usernmdisplay"].ToString();
        SessionUserID = Session["UserId"].ToString();
        SessionRights = Session["Rights"].ToString();

        if (!IsPostBack)
        {
            Page.Title = "HR Module :: Agent Master";
            //HtmlGenericControl li = (HtmlGenericControl)(Page.Master.FindControl("Link_Admin_Master"));
            //li.Attributes.Add("class", "droplink active open");
        }

        Load_Data();
    }
    
    private void Load_Data()
    {
        SSQL = "";
        SSQL = "Select * from MstVehicle where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
        DataTable dt_vehicle = new DataTable();
        dt_vehicle = objdata.RptEmployeeMultipleDetails(SSQL);
        Repeater1.DataSource = dt_vehicle;
        Repeater1.DataBind();
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        string SaveMode = "";
        string query = "";
        DataTable DT = new DataTable();
        if (txtVihicleType.Text == "")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Vehicle type')", true);
            ErrFlag = true;
            return;
        }
        if (txtCommission.Text == "" || txtCommission.Text=="0")
        {
            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('Please Enter the Vehicle Commission Amount')", true);
            ErrFlag = true;
            return;
        }
        if (!ErrFlag)
        {
            if (btnSave.Text == "Update")
            {
                SaveMode = "Update";

                query = "Update MstVehicle set Mobile='" + txtMobileNo.Text + "',";
                query = query + "Location='" + txtLocation.Text + "',Commission='" + txtCommission.Text + "' where VehicleType='" + txtVihicleType.Text.ToUpper() + "'";
                query = query + " and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                objdata.RptEmployeeMultipleDetails(query);

            }
            else
            {
                query = "select * from MstVehicle where Mobile='" + txtVihicleType.Text.ToUpper() + "' and Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "'";
                DT = objdata.RptEmployeeMultipleDetails(query);

                if (DT.Rows.Count > 0)
                {
                    SaveMode = "Already";
                }
                else
                {
                    SaveMode = "Insert";
                    query = "Insert into MstVehicle (VehicleType,Mobile,Location,Commission,Ccode,Lcode)";
                    query = query + "values('" + txtVihicleType.Text.ToUpper() + "','" + txtMobileNo.Text + "','" + txtLocation.Text + "','" + txtCommission.Text + "','" + SessionCcode + "','" + SessionLcode + "')";
                    objdata.RptEmployeeMultipleDetails(query);
                }

            }

            if (SaveMode == "Update")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Vehicle Type Updated Successfully...!');", true);
                btnClear_Click(sender, e);
            }
            else if (SaveMode == "Insert")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Vehicle Type Saved Successfully...!');", true);
                btnClear_Click(sender,e);
            }
            else if (SaveMode == "Already")
            {
                ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SaveMsgAlert('Vehicle Type Already Exists!');", true);
               
            }
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtVihicleType.Text = "";
        txtCommission.Text = "0";
        txtLocation.Text = "";
        txtMobileNo.Text = "";
        txtVihicleType.Enabled = true;
        Load_Data();
        btnSave.Text = "Save";
    }

    protected void btnEditEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        string vehicletype = e.CommandName.ToString();
        SSQL = "";
        SSQL = "Select * from MstVehicle where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and Vehicletype='" + vehicletype + "'";
        DataTable dt_get = new DataTable();
        dt_get = objdata.RptEmployeeMultipleDetails(SSQL);
        if (dt_get.Rows.Count > 0)
        {
            txtVihicleType.Text = dt_get.Rows[0]["VehicleType"].ToString();
            txtVihicleType.Enabled = false;
            txtCommission.Text = dt_get.Rows[0]["Commission"].ToString();
            txtLocation.Text = dt_get.Rows[0]["Location"].ToString();
            txtMobileNo.Text = dt_get.Rows[0]["Mobile"].ToString();
            btnSave.Text = "Update";
        }
    }

    protected void btnDeleteEnquiry_Grid_Command(object sender, CommandEventArgs e)
    {
        SSQL = "";
        SSQL = "delete from MstVehicle where CCode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and vehicletype='" + e.CommandName.ToString() + "'";
        objdata.RptEmployeeMultipleDetails(SSQL);
        Load_Data();
    }
}