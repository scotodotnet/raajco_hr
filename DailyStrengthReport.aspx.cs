﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.Text;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;


public partial class DailyStrengthReport : System.Web.UI.Page
{
    string ShiftType1 = "";
    string Date = "";

    String constr = ConfigurationManager.AppSettings["ConnectionString"];
    SqlConnection con;

    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay = "";
    string ss = "";
    string ReportType = "";
    ReportDocument report = new ReportDocument();
    BALDataAccess objdata = new BALDataAccess();
    //  EmployeeDetailsClass objEmpDetails = new EmployeeDetailsClass();



    string SSQL = "";
    string FromDate = "";
    string ToDate = "";
    string EmpType = "";

    DataSet ds = new DataSet();

    string Division = "";
    DataTable AutoDTable = new DataTable();
    DataTable DataCell = new DataTable();
    DataTable dt = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }
        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report-Daily Strength Report";
             
            }
            SessionCcode = Session["Ccode"].ToString();
            SessionLcode = Session["Lcode"].ToString();
            SessionAdmin = Session["Isadmin"].ToString();
            SessionEpay = Session["SessionEpay"].ToString();
            //SessionCompanyName = Session["CompanyName"].ToString();
            //SessionLocationName = Session["LocationName"].ToString();
            SessionUserType = Session["Isadmin"].ToString();
            ReportType = Request.QueryString["ReportType"].ToString();

            if (ReportType == "DAILY STRENGTH REPORT")
            {
                Date = Request.QueryString["Date"].ToString();
                EmpType = Request.QueryString["Wages"].ToString();
                //ToDate = Request.QueryString["ToDate"].ToString();
            }
            else
            {
                //ModeType = Request.QueryString["ModeType"].ToString();
                ShiftType1 = Request.QueryString["ShiftType1"].ToString();
                Date = Request.QueryString["Date"].ToString();
                Division = Request.QueryString["Division"].ToString();
            }

            Get_Report();
        }
    }

    private void Get_Report()
    {      
        if (ReportType == "DAILY STRENGTH REPORT")
        {
            DataCell.Columns.Add("SNo");
            DataCell.Columns.Add("Dept");
            DataCell.Columns.Add("Type");
            DataCell.Columns.Add("Shift");

            DataCell.Columns.Add("EmpCode");
            DataCell.Columns.Add("ExCode");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("TimeIN");
            DataCell.Columns.Add("TimeOUT");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Category");
            DataCell.Columns.Add("SubCategory");
            DataCell.Columns.Add("TotalMIN");
            DataCell.Columns.Add("GrandTOT");
            DataCell.Columns.Add("ShiftDate");
            DataCell.Columns.Add("CompanyName");
            DataCell.Columns.Add("LocationName");

            SSQL = "";
            SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
            SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName,LD.Wh_Present_Count from LogTime_Days LD";
            SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
            //SSQL = SSQL + " inner join MstCategory MC on EM.WageCategoty = MC.CateID";
            //SSQL = SSQL + " inner join AgentMst AM on EM.BrokerName = AM.AgentID";
            //SSQL = SSQL + " inner join MstGrade MG on EM.Grade = MG.GradeID";
            SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " and (EM.IsActive='Yes' or CONVERT(DATETIME,EM.DOR,103) >= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103))";
            SSQL = SSQL + " and CONVERT(DATETIME,EM.DOJ,103) <= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
            //SSQL = SSQL + " And AM.CompCode='" + Session["SessionCcode"].ToString() + "' ANd AM.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";
            //SSQL = SSQL + " And MG.CompCode='" + Session["SessionCcode"].ToString() + "' ANd MG.LocCode='" + Request.Cookies["SessionLcode"].Value.ToString() + "'";

            //if (Division != "-Select-")
            //{
            //    SSQL = SSQL + " And EM.Division = '" + Division + "'";
            //}
            //if (ShiftType1 != "ALL")
            //{
            //    SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            //}
            if (Date != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
            }
            if (EmpType != "-Select-")
            {
                SSQL = SSQL + " and EM.Wages='" + EmpType + "'";
            }

           // SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
            // 
            AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


            if (AutoDTable.Rows.Count != 0)
            {
                SSQL = "Delete from Count";
                objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    string WH_OT = "0";
                    string OT = "0";
                    SSQL = "";
                    SSQL = "Select * from [" + SessionEpay + "]..OT_Hrs_Mst where CCode='" + SessionCcode + "' and LCode='" + SessionLcode + "' and MachineID='" + AutoDTable.Rows[i]["MachineID"].ToString() + "'";
                    SSQL = SSQL + " and Convert(date,Attn_Date_Str,103)=Convert(date,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
                    DataTable dt_OT = new DataTable();
                    dt_OT = objdata.RptEmployeeMultipleDetails(SSQL);
                    if (dt_OT.Rows.Count > 0)
                    {
                        if(Convert.ToDecimal(AutoDTable.Rows[i]["Wh_Present_Count"].ToString()) > 0)
                        {
                            WH_OT = "1";
                        }
                        else
                        {
                            OT="1";
                        }
                    }
                    else
                    {
                        WH_OT = "0";
                        OT = "0";
                    }

                    if (AutoDTable.Rows[i]["SubCatName"].ToString().ToUpper() == "INSIDER")
                    {
                        SSQL = "Insert into Count (Department,Shift,SubCategory,Catname,WH_OT,OT) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "','" + AutoDTable.Rows[i]["CatName"].ToString() + "','" + WH_OT + "','" + OT + "')";
                    }
                    else
                    {
                        SSQL = "Insert into Count (Department,Shift,OutCat,Catname,WH_OT,OT) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "','" + AutoDTable.Rows[i]["CatName"].ToString() + "','" + WH_OT + "','" + OT + "')";
                    }
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                //New Query
                //SSQL = "SELECT Dept,isnull(SHIFT4,0) as Sht4,isnull(SHIFT1,0) as Sht1,isnull(SHIFT2,0) as Sht2,isnull(SHIFT3,0) as Sht3,";
                //SSQL = SSQL + " (isnull(SHIFT4,0) + isnull(SHIFT1,0) + isnull(SHIFT2,0) + isnull(SHIFT3,0)) as All_Sht_Total";
                //SSQL = SSQL + " FROM (";
                //SSQL = SSQL + " Select Department as Dept, Shift, (Count(SubCategory) + Count(OutCat)) as Total";
                //SSQL = SSQL + " from Count where SubCategory='INSIDER' OR OutCat='OUTSIDER'";
                //SSQL = SSQL + " group by Department, Shift";
                //SSQL = SSQL + " ) as s";
                //SSQL = SSQL + " PIVOT ( sum(Total) FOR [Shift] IN ([SHIFT4], [SHIFT1], [SHIFT2], [SHIFT3]) ) AS pivot_tbl order by Dept";




                //For Raajco by Selva
                SSQL = "";
                SSQL = "Select Dept as DeptName	,Sum(Sht4) as [4SEng],Sum(Sht3) as [3SEng],SUM(Sht2) as [2SEng],Sum(Sht1) as [1SEng],Sum(All_Sht_Total) as All_Sht_Total,Sum(TotalEmp) as TotalEmp,Sum(Sh1WHOT) as [1SWOT],Sum(Sh2WHOT) as [2SWOT],Sum(Sh3WHOT) as [3SWOT],Sum(Sh4WHOT) as [4SWOT],Sum(Sh1OT) as [1SOT],Sum(Sh2OT) as [2SOT],Sum(Sh3OT) as [3SOT],Sum(Sh4OT) as [4SOT]  from (";
                SSQL = SSQL + "SELECT Dept,isnull(Sum([SHIFT 4]),0) as Sht4,isnull(SUM([SHIFT 1]),0) as Sht1,isnull(SUM([SHIFT 2]),0) as Sht2,isnull(SUM(SHIFT3),0) as Sht3, (isnull(SUM([SHIFT 4]),0) + isnull(SUM([SHIFT 1]),0) + isnull(SUM([SHIFT 2]),0) + isnull(SUM(SHIFT3),0)) as All_Sht_Total,";
                SSQL = SSQL + " (isnull(SUM([SHIFT 4]),0) + isnull(SUM([SHIFT 1]),0) + isnull(SUM([SHIFT 2]),0) + isnull(SUM(SHIFT3),0)+isnull(sum(Leave),0)+isnull(sum([No Shift]),0)) as TotalEmp ,";
                SSQL = SSQL + "'0' as Sh1WHOT,'0' as Sh2WHOT,'0' as Sh3WHOT,'0' as Sh4WHOT,'0' as Sh1OT,'0' as Sh2OT,'0' as Sh3OT,'0' as Sh4OT";
                SSQL = SSQL + " FROM ( Select Department as Dept, Shift, (Count(SubCategory) + Count(OutCat)) as Total from Count where (SubCategory='INSIDER' OR OutCat='OUTSIDER') and CatName='LABOUR'   group by Department, Shift )";
                SSQL = SSQL + "as s PIVOT ( sum(Total) FOR [Shift] IN ([SHIFT 4], [SHIFT 1], [SHIFT 2], [SHIFT3], [Leave], [No Shift])) AS pivot_tbl group by Dept";
                SSQL = SSQL + " UNION ALL ";
                SSQL = SSQL + "SELECT Dept,'0' as Sht4,'0' as Sht3,'0' as Sht2,'0' as Sht1, '0' as All_Sht_Total,";
                SSQL = SSQL + "'0' as TotalEmp ,";
                SSQL = SSQL + "[SHIFT 1] as Sh1WHOT,[SHIFT 2] as Sh2WHOT,[SHIFT3] as Sh3WHOT,[SHIFT 4] as Sh4WHOT,'0' as Sh1OT,'0' as Sh2OT,'0' as Sh3OT,'0' as Sh4OT";
                SSQL = SSQL + " FROM ( Select Department as Dept, Shift, sum(WH_OT) as WH_OT  from Count where (SubCategory='INSIDER' OR OutCat='OUTSIDER') and CatName='LABOUR'   group by Department, Shift )";
                SSQL = SSQL + "as s PIVOT (sum(WH_OT) FOR [Shift] IN ([SHIFT 4], [SHIFT 1], [SHIFT 2], [SHIFT3], [Leave], [No Shift])) AS pivot_tbl ";
                SSQL = SSQL + " UNION ALL ";
                SSQL = SSQL + "SELECT Dept,'0' as Sht4,'0' as Sht3,'0' as Sht2,'0' as Sht1, '0' as All_Sht_Total,";
                SSQL = SSQL + "'0' as TotalEmp ,";
                SSQL = SSQL + "'0' as Sh1WHOT,'0' as Sh2WHOT,'0' as Sh3WHOT,'0' as Sh4WHOT,[SHIFT 1] as Sh1OT,[SHIFT 2] as Sh2OT,[SHIFT3] as Sh3OT,[SHIFT 4] as Sh4OT";
                SSQL = SSQL + " FROM ( Select Department as Dept, Shift, SUM(OT) as OT from Count where (SubCategory='INSIDER' OR OutCat='OUTSIDER') and CatName='LABOUR'   group by Department, Shift )";
                SSQL = SSQL + "as s PIVOT ( sum(OT) FOR [Shift] IN ([SHIFT 4], [SHIFT 1], [SHIFT 2], [SHIFT3], [Leave], [No Shift])) AS pivot_tbl ";
                SSQL = SSQL + ") A group by Dept  order by Dept";

                DataCell = objdata.RptEmployeeMultipleDetails(SSQL);

                ds.Tables.Add(DataCell);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Daily_Strength_Report_New.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //Get Company Name
                SSQL = "Select CompName,(Add1+','+Add2+','+City+','+State+','+Country+'-'+Pincode) as Address from Company_Mst Where CompCode='" + SessionCcode + "'";
                DataTable DT_For = new DataTable();
                DT_For = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_For.Rows.Count != 0)
                {
                    report.DataDefinition.FormulaFields["Company"].Text = "'" + DT_For.Rows[0]["CompName"].ToString() + "'";
                }
                report.DataDefinition.FormulaFields["Address"].Text = "'" + DT_For.Rows[0]["Address"].ToString() + "'";
                //if (Division.ToUpper().ToString() != "-Select-".ToUpper().ToString())
                //{
                //    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + Division.ToString() + "'";
                //}
                report.DataDefinition.FormulaFields["Date"].Text = "'" + Date.ToString() + "'";
                //Division
                //Date
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
        else if (ReportType == "DEPARTMENT ABSTRACT REPORT")
        {
            DataCell.Columns.Add("SNo");
            DataCell.Columns.Add("Dept");
            DataCell.Columns.Add("Type");
            DataCell.Columns.Add("Shift");

            DataCell.Columns.Add("EmpCode");
            DataCell.Columns.Add("ExCode");
            DataCell.Columns.Add("Name");
            DataCell.Columns.Add("TimeIN");
            DataCell.Columns.Add("TimeOUT");
            DataCell.Columns.Add("MachineID");
            DataCell.Columns.Add("Category");
            DataCell.Columns.Add("SubCategory");
            DataCell.Columns.Add("TotalMIN");
            DataCell.Columns.Add("GrandTOT");
            DataCell.Columns.Add("ShiftDate");
            DataCell.Columns.Add("CompanyName");
            DataCell.Columns.Add("LocationName");

            SSQL = "";
            SSQL = "select LD.MachineID,LD.ExistingCode,isnull(LD.DeptName,'') As DeptName,LD.Shift,LD.TypeName,isnull(LD.FirstName,'') as FirstName,";
            SSQL = SSQL + " LD.TimeIN,LD.TimeOUT,LD.Total_Hrs1 as Total_Hrs,EM.CatName as CatName,EM.SubCatName as SubCatName from LogTime_Days LD";
            SSQL = SSQL + " inner join Employee_Mst EM on EM.MachineID = LD.MachineID";
            SSQL = SSQL + " where LD.CompCode='" + Session["Ccode"].ToString() + "' ANd LD.LocCode='" + Session["Lcode"].ToString() + "'";
            SSQL = SSQL + " And EM.CompCode='" + Session["Ccode"].ToString() + "' ANd EM.LocCode='" + Session["Lcode"].ToString() + "'";
           
            if (Division != "-Select-")
            {
                SSQL = SSQL + " And EM.Division = '" + Division + "'";
            }
            if (ShiftType1 != "ALL")
            {
                SSQL = SSQL + " And Shift='" + ShiftType1 + "'";
            }
            if (Date != "")
            {
                SSQL = SSQL + " And CONVERT(DATETIME,Attn_Date_Str,103)= CONVERT(DATETIME,'" + Convert.ToDateTime(Date).ToString("dd/MM/yyyy") + "',103)";
            }

            SSQL = SSQL + "  And Shift !='No Shift' And TimeIN!=''  ";
            // 
            AutoDTable = objdata.RptEmployeeMultipleDetails(SSQL);


            if (AutoDTable.Rows.Count != 0)
            {
                SSQL = "Delete from Count";
                objdata.RptEmployeeMultipleDetails(SSQL);

                for (int i = 0; i < AutoDTable.Rows.Count; i++)
                {
                    if (AutoDTable.Rows[i]["SubCatName"].ToString().ToUpper() == "INSIDER")
                    {
                        SSQL = "Insert into Count (Department,Shift,SubCategory) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "')";
                    }
                    else
                    {
                        SSQL = "Insert into Count (Department,Shift,OutCat) values ('" + AutoDTable.Rows[i]["DeptName"].ToString() + "','" + AutoDTable.Rows[i]["Shift"].ToString() + "','" + AutoDTable.Rows[i]["SubCatName"].ToString() + "')";
                    }
                    objdata.RptEmployeeMultipleDetails(SSQL);
                }

                //New Query
                SSQL = "SELECT Dept,isnull(GENERAL,0) as GEN,isnull(SHIFT1,0) as Sht1,isnull(SHIFT2,0) as Sht2,isnull(SHIFT3,0) as Sht3,";
                SSQL = SSQL + " (isnull(GENERAL,0) + isnull(SHIFT1,0) + isnull(SHIFT2,0) + isnull(SHIFT3,0)) as All_Sht_Total";
                SSQL = SSQL + " FROM (";
                SSQL = SSQL + " Select Department as Dept,Shift, (Count(SubCategory) + Count(OutCat)) as Total";
                SSQL = SSQL + " from Count where SubCategory='INSIDER' OR OutCat='OUTSIDER'";
                SSQL = SSQL + " group by Department,Shift";
                SSQL = SSQL + " ) as s";
                SSQL = SSQL + " PIVOT ( sum(Total) FOR [Shift] IN ([GENERAL], [SHIFT1], [SHIFT2], [SHIFT3]) ) AS pivot_tbl order by Dept";

                DataCell = objdata.RptEmployeeMultipleDetails(SSQL);

                ds.Tables.Add(DataCell);
                //ReportDocument report = new ReportDocument();
                report.Load(Server.MapPath("crystal/Day_Department_Abstract.rpt"));

                report.Database.Tables[0].SetDataSource(ds.Tables[0]);

                //Get Company Name
                SSQL = "Select * from Company_Mst Where CompCode='" + SessionCcode + "'";
                DataTable DT_For = new DataTable();
                DT_For = objdata.RptEmployeeMultipleDetails(SSQL);
                if (DT_For.Rows.Count != 0)
                {
                    report.DataDefinition.FormulaFields["CompanyName"].Text = "'" + DT_For.Rows[0]["CompName"].ToString() + "'";
                }
                report.DataDefinition.FormulaFields["Location_Name"].Text = "'" + SessionLcode.ToString() + "'";
                if (Division.ToUpper().ToString() != "-Select-".ToUpper().ToString())
                {
                    report.DataDefinition.FormulaFields["Division_Name_Str"].Text = "'" + Division.ToString() + "'";
                }
                report.DataDefinition.FormulaFields["Date"].Text = "'" + Date.ToString() + "'";
                //Division
                //Date
                report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                CrystalReportViewer1.ReportSource = report;

            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "window", "alert('No Records Found');", true);
            }
        }
    }
}