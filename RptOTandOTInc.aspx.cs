﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using Altius.BusinessAccessLayer.BALDataAccess;
using System.Globalization;


public partial class RptOTandOTInc : System.Web.UI.Page
{
    string SessionCompanyName;
    string SessionLocationName;
    string SessionAdmin;
    string SessionCcode;
    string SessionLcode;
    string SessionUserType;
    string SessionEpay;
    string Division;
    string Status;
    double Final_Count;
    string TokenNo;

    double Present_Count;
    double Absent_Count;
    decimal Fixed_Work_Days;
    decimal Month_WH_Count;
    double Present_WH_Count;
    double NFH_Days_Count;
    double NFH_Days_Present_Count;
    BALDataAccess objdata = new BALDataAccess();

    string SSQL = "";
    DataTable AutoDTable = new DataTable();
    DataTable mEmployeeDT = new DataTable();
    DataTable MEmployeeDS = new DataTable();
    DateTime date1;
    DateTime Date2 = new DateTime();
    int intK;
    string FromDate;
    string ToDate;
    string Date_Value_Str;
    string Date_value_str1;
    string[] Time_Minus_Value_Check;
    string WagesType;
    double leaveCount;
    string Month = "";
    string EmpType = "";
    string Category = "";
    string Date1 = "";
    string date2 = "";
    string Pf = "1";
    string MachineID = "";
    string RptType = "";
    DataSet ds = new DataSet();
    string Payslip_Folder = "Payslip";
    protected void Page_Load(object sender, EventArgs e)

    {
        if (Session["Isadmin"] == null)
        {
            Response.Redirect("Default.aspx");
            Response.Write("Your session expired");
        }

        else
        {
            if (!IsPostBack)
            {
                Page.Title = "Spay Module | Report- =Weekly Payment details for the Period From " + FromDate + "To" + ToDate + ")";
                SessionCcode = Session["Ccode"].ToString();
                SessionLcode = Session["Lcode"].ToString();
                //SessionUserName = Session["Usernmdisplay"].ToString();
                //SessionUserID = Session["UserId"].ToString();
                //SessionRights = Session["Rights"].ToString();
                SessionUserType = Session["Isadmin"].ToString();
                SessionEpay = Session["SessionEpay"].ToString();
                Category = Request.QueryString["Category"].ToString();
                EmpType = Request.QueryString["EmpType"].ToString();
                FromDate = Request.QueryString["fromdate"].ToString();
                ToDate = Request.QueryString["ToDate"].ToString();
                Pf = Request.QueryString["pf"].ToString();
                Date1 = Request.QueryString["fromdate"].ToString();
                date2 = Request.QueryString["ToDate"].ToString();
                RptType = Request.QueryString["RptType"].ToString();
                Month = Request.QueryString["Months"].ToString();
                GetData();
                if (AutoDTable.Rows.Count != 0)
                {
                    ds.Tables.Add(AutoDTable);

                    ReportDocument report = new ReportDocument();

                    if ((RptType == "OTMonthly"))
                    {
                        report.Load(Server.MapPath(Payslip_Folder + "/OT_Amt.rpt"));
                    }
                    if ((RptType == "OT_Inc"))
                    {
                        report.Load(Server.MapPath(Payslip_Folder + "/OT_Inc_Amt.rpt"));
                    }



                    report.Database.Tables[0].SetDataSource(ds.Tables[0]);
                    report.DataDefinition.FormulaFields["Wages"].Text = "'" + EmpType + "'";
                    report.DataDefinition.FormulaFields["Months"].Text = "'" + Month + "'";
                    // report.DataDefinition.FormulaFields["Year"].Text = "'" + str_yr + "'";
                    report.DataDefinition.FormulaFields["FromDate"].Text = "'" + FromDate + "'";
                    report.DataDefinition.FormulaFields["ToDate"].Text = "'" + ToDate + "'";

                    report.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, false, "");
                    CrystalReportViewer1.ReportSource = report;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "alert", "alert('No data Found !!!');", true);
                }
            }
        }
    }
    private void GetData()
    {
        WagesType = EmpType;
        string TableName = "";
        TableName = "Employee_Mst";

        double Total_Time_get;
        DataTable dt = new DataTable();
        DataTable dt1 = new DataTable();
        DataTable mLocalDS = new DataTable();
        DataTable dtAmt = new DataTable();
        DataTable dtSal = new DataTable();
        DataTable DT_Emp = new DataTable();
        AutoDTable.Columns.Add("SNo");
        AutoDTable.Columns.Add("Emp.Code");
        //AutoDTable.Columns.Add("MachineID");
        AutoDTable.Columns.Add("DeptName");

        //AutoDTable.Columns.Add("ExistingCode");

        AutoDTable.Columns.Add("FirstName");
        AutoDTable.Columns.Add("WH");

        date1 = Convert.ToDateTime(FromDate);
        string dat = ToDate;
        Date2 = Convert.ToDateTime(dat);
        DateTime Query_Date_Check = new DateTime();
        DateTime DOJ_Date_Check_Emp = new DateTime();
        int daycount = (int)((Date2 - date1).TotalDays);
        int daysAdded = 0;
        string Query_header = "";
        string Query_Pivot = "";
        string whDays = "0";
        string TotDays = "0";
        string salary = "0";
        string OTAmount = "0";
        string TotAmount = "0";
        string NetPay = "0";
        string TotDed = "0";
        // string oneday = "0";
        string WH_Amount = "0";
        string basesalary = "0";
        string DeptName = "";
        string OneDay = "0";
        string OTAdd = "0";
        string Round = "0";
        string BasicDA = "0";
        string OTDay = "0";
        string fourHrs = "0";
        string EightsHrs = "0";
        string TotalInc = "0";
        string FourAmt = "0";
        string EightAmt = "0";
        string ESI = "0";
        string ESI_Wages = "0";
        string Total = "0";
        string NetAmt = "0";
        string WHAmt = "0";

        AutoDTable.Columns.Add("WH_Days");
        AutoDTable.Columns.Add("TotHrs");
        AutoDTable.Columns.Add("R.of.Wage/Day");
        AutoDTable.Columns.Add("R.of(Basic+DA)/Day");
        AutoDTable.Columns.Add("OT_Amount");
        AutoDTable.Columns.Add("Four_Hrs");
        AutoDTable.Columns.Add("Eight_Hrs");
        AutoDTable.Columns.Add("Four_Hrs_inc_Amount");
        AutoDTable.Columns.Add("Eight_Hrs_inc_Amount");

        AutoDTable.Columns.Add("OT_Inc_Amt");
        AutoDTable.Columns.Add("ESI_Wages");
        AutoDTable.Columns.Add("ESI_Amount");
        AutoDTable.Columns.Add("Total_Amount");
        AutoDTable.Columns.Add("Total_Ded");
        AutoDTable.Columns.Add("Total");
        AutoDTable.Columns.Add("R.Off");
        AutoDTable.Columns.Add("NetPay");

        string query = "";
        query = "Select EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,LEFT(EM.WeekOff,3) as Weekoff, ";
        query = query + " sum(LD.Present )Present  from Employee_Mst EM inner join";
        query = query + " LogTime_Days LD on EM.MachineID=LD.MachineID ";
        query = query + " Where EM.Wages='" + WagesType + "' and EM.Eligible_PF='" + Pf + "' ";
        query = query + " And LD.Wages='" + WagesType + "' and LD.Present!='0'";

        query = query + " And CONVERT(datetime, LD.Attn_Date,103) >= CONVERT(datetime, '" + FromDate + " ', 103)";
        query = query + " And CONVERT(datetime, LD.Attn_Date,103) <= CONVERT(datetime, '" + ToDate + "', 103)";

        query = query + " Group by EM.MachineID,EM.DeptName,EM.MachineID,EM.ExistingCode,EM.FirstName,EM.DOJ,EM.WeekOff order by ExistingCode Asc";
        dt = objdata.RptEmployeeMultipleDetails(query);

        for (int i = 0; i < dt.Rows.Count; i++)
        {

            string Sumdays = "";
            Sumdays = dt.Rows[i]["Present"].ToString();
            if (Sumdays != "0.0")
            {

                AutoDTable.NewRow();
                AutoDTable.Rows.Add();

                string MachineID = dt.Rows[i]["MachineID"].ToString();
                int DT_Row = AutoDTable.Rows.Count - 1;
                AutoDTable.Rows[DT_Row]["SNo"] = (Convert.ToDecimal(i) + Convert.ToDecimal(1)).ToString();
                AutoDTable.Rows[DT_Row]["DeptName"] = dt.Rows[i]["DeptName"].ToString();
                //  AutoDTable.Rows[DT_Row]["MachineID"] = dt.Rows[i]["MachineID"].ToString();
                //AutoDTable.Rows[DT_Row]["ExistingCode"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[DT_Row]["Emp.Code"] = dt.Rows[i]["ExistingCode"].ToString();
                AutoDTable.Rows[DT_Row]["FirstName"] = dt.Rows[i]["FirstName"].ToString();

                //string Emp_WH = dt.Rows[i]["WeekOff"].ToString();
                AutoDTable.Rows[DT_Row]["WH"] = dt.Rows[i]["WeekOff"].ToString();

                string DOJ_Date_Str = "";
                if (dt.Rows[i]["DOJ"].ToString() != "")
                {
                    DOJ_Date_Str = dt.Rows[i]["DOJ"].ToString();
                }
                else
                {
                    DOJ_Date_Str = "";
                }
                SSQL = "";
                SSQL = "Select * from Employee_Mst where MachineID ='" + MachineID + "'";
                DT_Emp = objdata.RptEmployeeMultipleDetails(SSQL);

                basesalary = DT_Emp.Rows[0]["BaseSalary"].ToString();
                DeptName = DT_Emp.Rows[0]["DeptName"].ToString();
                OneDay = (Convert.ToDecimal(basesalary) / 26).ToString();
                OneDay = (Math.Round(Convert.ToDecimal(OneDay), 0, MidpointRounding.AwayFromZero)).ToString();

                BasicDA = (Convert.ToDecimal(OneDay) / 2).ToString();
                AutoDTable.Rows[DT_Row]["R.of.Wage/Day"] = OneDay;
                AutoDTable.Rows[DT_Row]["R.of(Basic+DA)/Day"] = BasicDA;


                AutoDTable.Rows[DT_Row]["OT_Amount"] = "0";
                AutoDTable.Rows[DT_Row]["TotHrs"] = "0";
                AutoDTable.Rows[DT_Row]["Four_Hrs"] = "0";
                AutoDTable.Rows[DT_Row]["Eight_Hrs"] = "0";
                AutoDTable.Rows[DT_Row]["ESI_Wages"] = "0";
                AutoDTable.Rows[DT_Row]["ESI_Amount"] = "0";
                AutoDTable.Rows[DT_Row]["Total_Ded"] = "0";
                AutoDTable.Rows[DT_Row]["Total_Amount"] = "0";
                AutoDTable.Rows[DT_Row]["R.Off"] = "0";
                AutoDTable.Rows[DT_Row]["NetPay"] = "0";
                AutoDTable.Rows[DT_Row]["WH_Days"] = "0";


                //SSQL = "";
                //SSQL = "select MachineID,Oneday_Wages,Sum(Cast(Tot_OTHrs as int)) as Tot_OTHrs,Sum(cast(Amount as decimal(18,2))) Amount,Sum(Cast(WH as int))WH   from [Raajco_Epay]..OT_Amt where MachineID='" + dt.Rows[i]["ExistingCode"].ToString() + "'";
                //SSQL = SSQL + " And LCode='" + SessionLcode + "'";
                //SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) >= CONVERT(datetime,'" + FromDate + "',103) ";
                //SSQL = SSQL + " And CONVERT(datetime,OT_AttenDate,103) <= CONVERT(datetime,'" + ToDate + "',103) group by MachineID,Oneday_Wages";
                //dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);

                //SSQL = "";
                //SSQL = "   select A.MachineID,A.EmpName,sum(Tot_OTHrs)Tot_OTHrs,Sum(Amount)Amount,sum(Four_Hrs_Inc)Four_Hrs,sum(Four_Hrs_inc_Amount)Four_Hrs_inc_Amount,Sum(Eight_Hrs_Inc)Eight_Hrs," +
                //         "sum(Eight_Hrs_inc_Amount)Eight_Hrs_inc_Amount,Sum(WH)WH,Sum(WH_Amt)WHAmt from (" +
                //        "select MachineID, EmpName, OT_AttenDate, cast(Tot_OTHrs as int)Tot_OTHrs, cast(Amount as decimal(18, 2)) Amount ,Oneday_Wages,OT_Wages,0 Four_Hrs_Inc, 0 Eight_Hrs_Inc,0 Four_Hrs_inc_Amount, " +
                //         "0 Eight_Hrs_inc_Amount,0 WH,0 WH_Amt " +
                //         " from  [Raajco_Epay]..OT_Amt where CCode = 'Raajco' and LCode = 'UNIT I' and  convert(DateTime,OT_AttenDate,103) >= Convert(Datetime,'" + FromDate + "',103) and " +
                //         "convert(DateTime, OT_AttenDate, 103) <= Convert(Datetime, '" + ToDate + "', 103) and Wages='" + EmpType + "' and MachineID='" + MachineID + "'" +
                //         "Union All " +
                //        "select MachineID,EmpName,OT_AttenDate,0 Tot_OTHrs,  0.0 Amount,0 Oneday_Wages,0 OT_Wages,Four_Hrs," +
                //        "Eight_Hrs,Four_Hrs_inc_Amount,Eight_Hrs_inc_Amount,WH,WH_Amt " +
                //         " from  [Raajco_Epay]..OT_Inc_Amt where  CCode = 'Raajco' and LCode = 'UNIT I' and MachineID='" + MachineID + "'  and  convert(DateTime,OT_AttenDate,103) >= Convert(Datetime,'" + FromDate + "',103) and convert(DateTime,OT_AttenDate, 103) <= Convert(Datetime,'" + ToDate + "' , 103)  )A Group by A.MachineID,A.EmpName";


                SSQL = "Select MachineID,EmpName,sum(CAST(isnull(OT_Hrs,'0') as decimal(18,2))) as Tot_OTHrs,Sum(CAST(isnull(Amount,'0') as decimal(18,2))) as Amount,Sum(CAST(isnull(Four_Hrs_Inc,'0') as decimal(18,2))) as Four_Hrs,";
                SSQL = SSQL + "Sum(CAST(isnull(Eight_Hrs_Inc,'0') as decimal(18,2))) as Eight_Hrs,Sum(CAST(isnull(Four_Hrs_Inc_Amount,'0') as decimal(18,2))) as Four_Hrs_inc_Amount,Sum(CAST(isnull(Eight_Hrs_Inc_Amount,'0') as decimal(18,2))) as Eight_Hrs_inc_Amount,";
                SSQL = SSQL + "Sum(CAST(isnull(Weekoff,'0') as decimal(18,2))) as WH,Sum(CAST(Isnull(WH_Amt,'0') as decimal(18,2))) as WHAmt from [" + SessionEpay + "]..OT_Hrs_Mst";
                SSQL = SSQL + " where Ccode='" + SessionCcode + "' and Lcode='" + SessionLcode + "' and MachineID='" + MachineID + "' and ";
                SSQL = SSQL + " Convert(date,Attn_Date_Str,103)>=Convert(date,'" + FromDate + "',103) and Convert(date,Attn_Date_Str,103)<=Convert(date,'" + ToDate + "',103)";
                SSQL = SSQL + " and Wages='" + EmpType + "'";
                SSQL = SSQL + " group by MachineID,EmpName order by MachineID asc";

                dtAmt = objdata.RptEmployeeMultipleDetails(SSQL);



                if (dtAmt.Rows.Count != 0)
                {
                    for (int j = 0; j < dtAmt.Rows.Count; j++)


                    {
                        if (dtAmt.Rows[0]["Tot_OTHrs"].ToString() != "")
                        {
                            AutoDTable.Rows[DT_Row]["TotHrs"] = dtAmt.Rows[j]["Tot_OTHrs"].ToString();
                        }
                        OTAdd = dtAmt.Rows[j]["Amount"].ToString();
                        fourHrs = dtAmt.Rows[j]["Four_Hrs"].ToString();
                        EightsHrs = dtAmt.Rows[j]["Eight_Hrs"].ToString();
                        FourAmt = dtAmt.Rows[j]["Four_Hrs_inc_Amount"].ToString();
                        EightAmt = dtAmt.Rows[j]["Eight_Hrs_inc_Amount"].ToString();
                        WHAmt = dtAmt.Rows[j]["WHAmt"].ToString();

                        TotalInc = (Convert.ToDecimal(FourAmt) + Convert.ToDecimal(EightAmt) + Convert.ToDecimal(WHAmt)).ToString();



                        //OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();
                        whDays = dtAmt.Rows[j]["WH"].ToString();


                        if (whDays != "0")
                        {
                            WH_Amount = (Convert.ToDecimal(OneDay) * Convert.ToDecimal(whDays)).ToString();

                        }
                        else
                        {
                            WH_Amount = "0";
                        }

                        OTAmount = (Convert.ToDecimal(OTAdd) + Convert.ToDecimal(WH_Amount)).ToString();
                        AutoDTable.Rows[DT_Row]["WH_Days"] = whDays;
                        AutoDTable.Rows[DT_Row]["OT_Amount"] = OTAmount;
                        AutoDTable.Rows[DT_Row]["Four_Hrs"] = fourHrs;
                        AutoDTable.Rows[DT_Row]["Eight_Hrs"] = EightsHrs;
                        AutoDTable.Rows[DT_Row]["ESI_Wages"] = OTAmount;


                        if (OTAmount != "0")
                        {
                            ESI_Wages = OTAmount;
                            ESI = (Convert.ToDecimal(OTAmount) * Convert.ToDecimal(0.75)).ToString();
                            ESI = (Convert.ToDecimal(ESI) / Convert.ToDecimal(100)).ToString();
                            ESI = (Math.Round(Convert.ToDecimal(ESI), 1, MidpointRounding.AwayFromZero)).ToString();
                        }

                        if (RptType == "OTMonthly")
                        {
                            NetAmt = (Convert.ToDecimal(OTAmount)).ToString();
                        }
                        if ((RptType == "OT_Inc"))
                        {
                            NetAmt = (Convert.ToDecimal(TotalInc)).ToString();
                        }
                        if (RptType == "OTMonthly")
                        {
                            Total = (Convert.ToDecimal(NetAmt) - Convert.ToDecimal(ESI)).ToString();
                        }
                        if ((RptType == "OT_Inc"))
                        {
                            Total = (Convert.ToDecimal(NetAmt)).ToString();
                        }
                        NetPay = (Math.Round(Convert.ToDecimal(Total), 0, MidpointRounding.AwayFromZero)).ToString();
                        Round = (Convert.ToDecimal(NetPay) - Convert.ToDecimal(Total)).ToString();

                        AutoDTable.Rows[DT_Row]["OT_Inc_Amt"] = TotalInc;
                        AutoDTable.Rows[DT_Row]["Four_Hrs_inc_Amount"] = FourAmt;
                        AutoDTable.Rows[DT_Row]["Eight_Hrs_inc_Amount"] = EightAmt;
                        AutoDTable.Rows[DT_Row]["ESI_Amount"] = ESI;
                        AutoDTable.Rows[DT_Row]["Total_Ded"] = ESI;
                        AutoDTable.Rows[DT_Row]["Total_Amount"] = NetAmt;
                        AutoDTable.Rows[DT_Row]["Total"] = Total;
                        AutoDTable.Rows[DT_Row]["NetPay"] = NetPay;
                        AutoDTable.Rows[DT_Row]["R.Off"] = Round;

                        TotalInc = "0";
                        EightsHrs = "0";
                        fourHrs = "0";
                        OTAmount = "0";
                        Total = "0";
                        NetPay = "0";
                        Round = "0";
                        ESI = "0";
                        NetAmt = "0";
                        whDays = "0";
                    }
                }
            }
        }
    }
}